# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

r"""
.. _example_protein_water_gas:

Solution Scattering Simulation Example
======================================

Contributed by Rick Kirian

Simulation of gas, water and protein scattering patterns/profiles.
This is not fast, because we first simulate every pixel on the 2D detector,
and subsequently we reduce the 2D data into a 1D scattering profile.
Notably, gas scatter simulations require sampling the scatter at many points
along an extended path (so turning off the gas will speed things up).
"""

# %%
# Note that the DENSS package is needed for the protein scattering profile.  It can be installed like this:
#
#.. code-block:: console
#
#    pip install git+https://github.com/tdgrant1/denss.git

# %%
# Assuming you have DENSS, the imports below should work:

import numpy as np
import pyqtgraph as pg
from reborn import source, detector
from reborn.analysis.saxs import RadialProfiler
from reborn.data import lysozyme_pdb_file
from reborn.simulate.gas import get_gas_background
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.viewers.qtviews import PADView
from reborn.external.denss import get_pad_pdb_scattering_intensity
from reborn.const import eV, mbar

# %%
# Basic configurations.  Units are SI, as always in reborn.
pdb_file = lysozyme_pdb_file
detector_distance = 0.15
photon_energy = 9000*eV
beam_diameter = 1e-6
pulse_energy = 1e-3
chamber_pressure = 1e-5*mbar
liquid_temperature = 280
sample_thickness = 4e-6
protein_concentration = 10
n_q_bins = 500
pad_binning = 10

# %%
# Set up the beam info:
beam = source.Beam(photon_energy=photon_energy, pulse_energy=pulse_energy, diameter_fwhm=beam_diameter)
# %%
# Set up the PAD geometry info.  We use the "stock" Eiger 4M detector geometry included with reborn.
# We are binning the detector pixels to save time; set the pad_binning to 1 if you want better accuracy.
geom = detector.eiger4M_pad_geometry_list(detector_distance=detector_distance, binning=pad_binning)
# %%
# With the beam and detector, we can simulate the protein scattering intensity, and the water intensity:
protein_intensity = get_pad_pdb_scattering_intensity(pdb_file, create_bio_assembly=True, pad_geometry=geom, beam=beam, mass_density=protein_concentration, sample_thickness=sample_thickness, as_list=False)
water_intensity = get_pad_solution_intensity(beam=beam, pad_geometry=geom, thickness=sample_thickness, liquid='water', as_list=False)
# %%
# In the line below, we set the gas scatter to zero, because it is slow to compute.  But the function is
# shown behind a comment in case you want to include gas.
gas_intensity = 0 #get_gas_background(pad_geometry=geom, beam=beam, path_length=[0, detector_distance], gas_type="he", pressure=chamber_pressure, as_list=False)
total_intensity = water_intensity + gas_intensity + protein_intensity
# %%
# RadialProfiler is a class we use to assist with making scattering profile statistics:
profiler = RadialProfiler(pad_geometry=geom, beam=beam, n_bins=n_q_bins)
# %%
# First we sum all the photons according to scattering profile q bins
psum = profiler.get_sum_profile(total_intensity)
# %%
# The above are expectation values for the mean number of photons.  This is an appropriate place to add Poisson nose:
psum = np.random.poisson(psum).astype(float)
# %%
# Sensible units for scattering profiles is photons per solid angle.  So we should sum the solid angles per q bin also.
# Note that solid angles and polarization factors appear as a product in |Equation1|, so we actually need to sum
# that product.
sap = profiler.get_sum_profile(geom.solid_angles()*geom.polarization_factors(beam=beam))
# %%
# Divide summed photons counts by summed solid angles, being mindful of divide-by-zero issues:
saxs = np.divide(psum, sap, out=np.zeros_like(psum), where=sap!=0)
# %%
# Look at the profile we created:
plot = pg.plot(profiler.q_bin_centers/1e10, saxs, pen=None, symbolBrush="w", symbolSize=2, symbolPen=None)
plot.setLabel('bottom', 'Q (1/A)')
plot.setLabel('left', 'I(Q)')
# %%
# Use PADView to see the 2D pattern:
pv = PADView(pad_geometry=geom, beam=beam, data=total_intensity)
pv.start()