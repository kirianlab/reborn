""" This module is a modified version of qtgallery found here:
https://github.com/ixjlyons/qtgallery
The modifications avoid the need for the qtpy package,
and include a correction needed for PyQt6."""

from pyvirtualdisplay import Display
from sphinx_gallery import scrapers
# from qtpy.QtWidgets import QApplication
from pyqtgraph.Qt.QtWidgets import QApplication

def qtscraper(block, block_vars, gallery_conf):
    """Basic implementation of a Qt window scraper.

    Looks for any non-hidden windows in the current application instance and
    uses ``grab`` to render an image of the window. The window is closed
    afterward, so you have to call ``show()`` again to render it in a
    subsequent cell.

    ``processEvents`` is called once in case events still need to propagate.
    """
    imgpath_iter = block_vars['image_path_iterator']

    app = QApplication.instance()
    if app is None:
        app = QApplication([])
    app.processEvents()

    # get top-level widgets that aren't hidden
    widgets = [w for w in app.topLevelWidgets() if not w.isHidden()]

    rendered_imgs = []
    for widg, imgpath in zip(widgets, imgpath_iter):
        pixmap = widg.grab()
        pixmap.save(imgpath)
        rendered_imgs.append(imgpath)
        widg.close()

    return scrapers.figure_rst(rendered_imgs, gallery_conf['src_dir'])


def reset_qapp(gallery_conf, fname):
    """Shutdown an existing QApplication and disable exec_.

    Disabling ``QApplication.exec_`` means your example scripts can call the
    exec_ (so the scripts work when run normally) without blocking example
    execution by sphinx-gallery.

    With PySide2, it seems to be necessary to destroy the QApplication instance
    between example runs.
    """
    try:
        # pyside-specific
        if qApp:
            qApp.shutdown()
    except NameError:
        pass
    QApplication.exec_ = lambda _: None
    QApplication.exec = lambda _: None


disp = None


def start_display(app, config):
    global disp
    disp = Display(backend="xvfb", size=(800, 600))
    disp.start()


def stop_display(app, exception):
    # seems to be necessary to avoid "fatal IO error on X server..."
    reset_qapp(None, None)

    if disp is not None:
        disp.stop()


def setup(app):
    app.connect("config-inited", start_display)
    app.connect("build-finished", stop_display)

    return {"version": "0.1"}
