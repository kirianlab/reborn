.. _overview_anchor:

Brief Overview
==============

.. note::

    Use of the reborn package requires knowledge of the python programming language, including
    object-oriented programming, along with good knowledge of |numpy|.  It is
    not designed as a "program" that you can figure out via mouse clicking.  With that
    understanding, here we describe the structure of reborn in terms of the most important
    class definitions.

The reborn package originated as an effort to have a standard specification of "pixel array detector"
(PAD) geometry, with an emphasis on simplifying diffraction simulations and analysis.  As such, a core
component of reborn is the |PADGeometry| class.  This class provides the needed information and methods
to map diffraction intensity values stored in memory to physical locations in space.  A general
explanation of how PAD geometry is defined in terms of vector components is found in the |detector_documentation|
page.  Examples of how to use |PADGeometry| are found in the reborn |examples| page (most relevant
is the |pad_geometry_example|).

Since XFEL facilities usually combine many PADs to form a single "detector", reborn has a
|PADGeometryList| class that combines several |PADGeometry| instances.  Most users of reborn will rarely
(if ever) encounter an isolated |PADGeometry| instance; they will work almost exclusively with
|PADGeometryList| instances.  The |PADGeometryList| class exists in part so that you do not need
to repeatedly loop over individual PADs in your analysis code.  It also serves the purpose of
providing a standard way to split a contiguous detector data block into individual 2D PAD arrays,
and vice versa.  Examples of how to use |PADGeometryList| are found in the reborn
|examples| page (most relevant is the |pad_geometry_example|).

The reborn package does not currently have a Detector class, which would contain detector
characteristics such as dark currents, gain settings, statistical characteristics, methods to
convert digitized signals to photon counts, and so on.  It is inevitable that a Detector class
will emerge some day, and you can expect it to have PADGeometryList as an attribute.

The reborn |Beam| class contains information about the x-ray beam, most importantly the photon
energy, pulse energy, beam diameter, beam direction, and polarization state.  This is a relatively
passive class that contains information but does not have many methods for operating on data.  When
a |Beam| is combined with a |PADGeometryList|, we can begin calculating essential quantities for
diffraction analysis such as the :math:`\vec{q}` vectors, polarization factors, and so on.
For more on the |Beam| class, look to the |beam_documentation|.  The |examples| page has various
scripts that make use of the |Beam| class.

The reborn |DataFrame| class can be thought of as a standard container for a single data readout
event, which usually corresponds to single x-ray pulse or exposure.
At minimum, a |DataFrame| has a |PADGeometryList| instance along with raw data.  Normally there is
also a |Beam| instance, but sometimes we have data without an incident XFEL pulse (i.e. a "dark frame").
In reborn, raw data comes in the form of numpy arrays; we will discuss that later.  There are some
usage examples of |DataFrame| in the |framegetter_example|.

A very important element of reborn is the |FrameGetter| abstract base class (ABC).  This ABC provides
the foundation for structuring a sequence of x-ray pulses or data recording events, which we would often
call a "run".  A new |FrameGetter| subclass should be defined for each new source of data.  The creation
of a |FrameGetter| subclass might be understood as a way to condense and hide all of the ugly code that
is needed in order to load data.  Once that is done, the rest of your analysis pipeline will then have
a standard interface to the data.  In order to
create a new |FrameGetter| subclass, you only need to define the __init__ method and the get_data
method.  In some cases, a |FrameGetter| subclass might already exist for your data source (for example,
LCLS data).  The |FrameGetter| class will always return |DataFrame| instances, so you can confidently build
code basd on that data structure.  To learn more about the |FrameGetter| class look to
the |framegetter_example|.

The |PADView| class allows you to look at PAD data and flip through a sequence of frames.  This is a
graphical interface that uses PyQt.  It can be used without knowledge of PyQt, but good knowledge of
PyQt will be needed if you wish to develop it or add extensions.  The |PADView| class aims to be a useful
tool for visualization of diffraction, has some advanced features for masking bad pixels, finding the beam
center, filtering data, adjusting PAD geometry, and so on.  There is a |padview_example| page to get
you started.

With a |FrameGetter| subclass, you can build your custom-tailored analysis code.  For common analysis
tasks, we aim to provide standard analysis pipelines.  For example, there is a pipeline for taking a
first-pass through a run of data, which will produce the average intensities, min/max intensity values,
standard deviations, and pixel-by-pixel histograms of intensity values.  Those histograms can then
be used to create a map of gains and offsets for each pixel.  We hope to add examples of this in the
near future.

The reborn package also has a number of tools for simulating diffraction.  Bulk water (as a function
of temperature), gas (as a function of pressure and path length), and simple objects such as
spheroids are included.  All-atom simulations can be done on a GPU using the |ClCore| class.  A
few wrappers are included for simulating via the |DENSS| package, which is good for proteins in a
solvent.  Our documentation on simulations is presently very limited but we hope to update it soon.