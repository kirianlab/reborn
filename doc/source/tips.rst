.. _tips:

Tips
====

This is the page for miscellaneous tips that might be helpful when using reborn.

.. toctree::
   :maxdepth: 1

   tips/git
   tips/numpy
   tips/pyqtgraph
   tips/profiling_code
   tips/windows
   tips/research_computing

