import numpy as np
from .. import detector
from ..misc import correlate


class FriedelPearsonCC:
    def __init__(self, dataframe=None, max_shift=10, min_unmasked=100):
        self.max_shift = max_shift
        self.min_unmasked = min_unmasked
        self.plan = None
        self.assembler = None
        self.geom = None
        if dataframe is not None:
            self.get_shifts(dataframe)
    def get_shifts(self, dataframe):
        data = dataframe.get_processed_data_flat()
        if self.assembler is None:
            geom = dataframe.get_pad_geometry()
            self.assembler = detector.PADAssembler(pad_geometry=geom, centered=True)
        if self.plan is None:
            mask = dataframe.get_mask_flat()
            x_mask = self.assembler.assemble_data(mask)
            x_mask = np.array(x_mask >= 1, dtype=float)
            y_mask = x_mask[::-1, ::-1]
            self.plan = correlate.plan_masked_pearson_cc_2d(x_mask, y_mask, max_shift=self.max_shift, min_unmasked=self.min_unmasked)
        data2d = self.assembler.assemble_data(data)
        mpcc = correlate.masked_pearson_cc_2d(data2d, None, data2d[::-1, ::-1], None, plan=self.plan)
        nx, ny = mpcc.shape
        nn = self.max_shift
        mpcc = mpcc[int(nx / 2 - nn):int(nx / 2 + nn), int(ny / 2 - nn):int(ny / 2 + nn)]
        nx, ny = mpcc.shape
        fit = correlate.fit_gaussian_2d(mpcc)
        if fit is None:
            return None, None, None, dict(pearson_cc=mpcc)
        c, x0, y0 = fit[:3]
        x = np.linspace(-nx / 2, nx / 2, nx)
        y = np.linspace(-ny / 2, ny / 2, ny)
        xy = np.meshgrid(x, y, indexing="ij")
        gfit = correlate.gaussian_2d(xy, *fit).reshape((nx, ny))
        extra = dict(fit_parameters=fit, pearson_cc=mpcc, gaussian_fit=gfit)
        return x0 / 2, y0 / 2, c, extra
    def get_shifted_geom(self, dataframe):
        x0, y0, c0, extra = self.get_shifts(dataframe)
        if x0 is None:
            return None, None, None, None, extra
        geom = dataframe.get_pad_geometry()
        pixel_size = geom[0].pixel_size()
        geom.translate([-x0 * pixel_size, 0, 0])
        geom.translate([0, -y0 * pixel_size, 0])
        return geom, x0, y0, c0, extra