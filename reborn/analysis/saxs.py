# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
from .. import detector
from .. import fileio
from .. import utils
from .. import fortran
from .parallel import ParallelAnalyzer
from ..dataframe import DataFrame
from ..external import pyqtgraph as pg

debug = False


def debug_message(*args, caller=True, **kwargs):
    r"""Standard debug message, which includes the function called."""
    if debug:
        s = ""
        if caller:
            s = utils.get_caller(1)
        print("DEBUG:" + s + ":", *args, **kwargs)


class QHistogram:
    count = 0  # Number of frames contributing to the histogram.
    bin_min = None  # The minimum value corresponding to histogram bin *centers*.
    bin_max = None  # The maximum value corresponding to histogram bin *centers*.
    n_bins = None  # The number of histogram bins.
    n_q_bins = None

    def __init__(
        self,
        bin_min=None,
        bin_max=None,
        n_bins=None,
        n_q_bins=None,
        pad_geometry=None,
        beam=None,
    ):
        r"""Creates an intensity histogram for each pixel in a PAD.  For a PAD with N pixels in total, this class
        will produce an array of shape (M, N) assuming that you requested M bins in the histogram.

        Arguments:
            bin_min (float): The minimum value corresponding to histogram bin *centers*.
            bin_max (float): The maximum value corresponding to histogram bin *centers*.
            n_bins (int): The number of histogram bins.
            n_q_bins (int): The number of q bins.
        """
        self.bin_min = float(bin_min)
        self.bin_max = float(bin_max)
        self.n_bins = int(n_bins)
        self.n_q_bins = int(n_q_bins)
        self.bin_size = (self.bin_max - self.bin_min) / (self.n_bins - 1)
        q_mags = pad_geometry.q_mags(beam=beam).astype(np.float64)
        self.q_min = q_mags.min()
        self.q_max = q_mags.max()
        self.q_bin_size = (self.q_max - self.q_min) / (self.n_q_bins - 1)
        self.q_map = q_bin_indices(
            n_q_bins=self.n_q_bins,
            q_bin_size=self.q_bin_size,
            q_min=self.q_min,
            qs=q_mags,
        )
        self.b_idx = np.arange(self.n_bins, dtype=int)
        self.q_idx = np.arange(self.n_q_bins, dtype=int)
        self.histogram = np.zeros((self.n_bins, self.n_q_bins), dtype=np.uint16)

    def to_dict(self):
        r"""Convert class to dictionary for storage."""
        return dict(
            bin_min=self.bin_min,
            bin_max=self.bin_max,
            n_bins=self.n_bins,
            histogram=self.histogram,
        )

    def from_dict(self, d):
        r"""Set data according to data stored in dictionary created by to_dict."""
        self.bin_min = d["bin_min"]
        self.bin_max = d["bin_max"]
        self.n_bins = d["n_bins"]
        self.histogram = d["histogram"]

    def get_bin_centers(self):
        r"""Returns an 1D array of histogram bin centers."""
        return np.linspace(self.bin_min, self.bin_max, self.n_bins)

    def get_q_bin_centers(self):
        r"""Returns an 1D array of histogram q bin centers."""
        return np.linspace(self.q_min, self.q_max, self.n_q_bins)

    def get_histogram(self):
        r"""Returns a copy of the histogram - an |ndarray| of shape (M, N) where M is the number of pixels and
        N is the number of requested bins per pixel."""
        return self.histogram.copy()

    def add_frame(self, data, mask=None):
        r"""Add PAD measurement to the histogram."""
        bin_index = np.floor((data - self.bin_min) / self.bin_size).astype(int)
        bin_index[bin_index < 0] = 0
        bin_index[bin_index >= self.n_bins] = self.n_bins - 1
        idx = self.q_map
        if mask is not None:
            idx = idx[mask > 0]
            bin_index = bin_index[mask > 0]
        np.add.at(self.histogram, (idx, bin_index), 1)

    def process_framegetter(self, framegetter):
        """ TODO: docstring """
        for f in framegetter:
            data = f.get_processed_data_flat()
            mask = f.get_mask_flat()
            self.add_frame(data=data, mask=mask)


class RadialProfiler:
    r"""
    A class for creating radial profiles from image data. Standard profiles are computed using fortran code.
    Bin indices are cached for speed, provided that the |PADGeometry| and |Beam| do not change.
    Arbitrary profiles can also be computed (slowly!) provided a function handle that operates on an |ndarray|.
    """
    # pylint: disable=too-many-instance-attributes
    n_bins = None  # Number of bins in radial profile
    q_range = None  # The range of q magnitudes in the 1D profile.  These correspond to bin centers
    q_edge_range = None  # Same as above, but corresponds to bin edges not centers
    bin_centers = None  # q magnitudes corresponding to 1D profile bin centers
    bin_edges = (
        None  # q magnitudes corresponding to 1D profile bin edges (length is n_bins+1)
    )
    bin_size = None  # The size of the 1D profile bin in q space
    _q_mags = None  # q magnitudes corresponding to diffraction pattern intensities
    _mask = (
        None  # The default mask, in case no mask is provided upon requesting profiles
    )
    _fortran_indices = None  # For speed, pre-index arrays
    _pad_geometry = None  # List of PADGeometry instances
    _beam = None  # Beam instance for creating q magnitudes

    def __init__(
        self, mask=None, n_bins=None, q_range=None, pad_geometry=None, beam=None
    ):
        r"""
        For a parallelized version see: ..:py:class::`~reborn.analysis.saxs.RadialProfiler`

        Arguments:
            mask (|ndarray|): Optional. The arrays will be multiplied by this mask, and the counts per radial bin
                                will come from this (e.g. use values of 0 and 1 if you want a normal average, otherwise
                                you get a weighted average).
            n_bins (int): Number of radial bins you desire.
            q_range (list-like): The minimum and maximum of the *centers* of the q bins.
            pad_geometry (|PADGeometryList|):  Optional.  Will be used to generate q magnitudes.  You must
                                                             provide beam if you provide this.
            beam (|Beam|): Optional, unless pad_geometry is provided. Wavelength and beam direction are
                           needed in order to calculate q magnitudes.
        """
        self.make_plan(
            mask=mask,
            n_bins=n_bins,
            q_range=q_range,
            pad_geometry=pad_geometry,
            beam=beam,
        )

    def set_mask(self, mask):
        r"""Update the mask.  Concatenate (if needed), copy, and make it un-writeable.

        Arguments:
            mask (|ndarray|): Mask.
        """
        if mask is not None:
            self._mask = self.concat_data(mask).copy()
        else:
            self._mask = np.ones(len(self._q_mags))
        self._mask = self._mask.astype(np.float64)  # Because fortran code is involved
        self._mask.flags["WRITEABLE"] = False

    def set_beam(self, beam):
        r"""Update the |Beam| with a copy and clear out derived caches.

        Arguments:
            beam (|Beam|): Beam properties.
        """
        self._beam = beam.copy()
        self._q_mags = None
        self._fortran_indices = None

    def set_pad_geometry(self, geom):
        r"""Update the |PADGeometryList| with a copy and clear out derived caches.

        Arguments:
            geom (|PADGeometryList|): PAD geometry.
        """
        self._pad_geometry = detector.PADGeometryList(geom).copy()
        self._q_mags = None
        self._fortran_indices = None

    def _calc_q_mags(self):
        self._q_mags = self._pad_geometry.q_mags(beam=self._beam).astype(np.float64)

    @property
    def q_bin_centers(self):
        r"""The centers of the q bins."""
        return self.bin_centers

    def make_plan(
        self,
        mask=None,
        n_bins=None,
        q_range=None,
        pad_geometry=None,
        beam=None,
    ):
        r"""
        Set up the binning indices for the creation of radial profiles.  Cache some useful data to speed things up
        later.

        Arguments:
            mask (|ndarray|): Optional.  The arrays will be multiplied by this mask, and the counts per radial bin
                                will come from this (e.g. use values of 0 and 1 if you want a normal average, otherwise
                                you get a weighted average).
            n_bins (int): Number of radial bins you desire.
            q_range (tuple): The minimum and maximum of the *centers* of the q bins.
            pad_geometry (list of |PADGeometry| instances):  Optional.  Will be used to generate q magnitudes.  You must
                                                             provide beam if you provide this.
            beam (|Beam| instance): Optional, unless pad_geometry is provided.  Wavelength and beam direction are
                                     needed in order to calculate q magnitudes.
        """
        if pad_geometry is None:
            raise ValueError(
                "You must provide a |PADGeometry| to initialize RadialProfiler"
            )
        if beam is None:
            raise ValueError("You must provide a |Beam| to initialize RadialProfiler")
        self.set_pad_geometry(pad_geometry)
        self.set_beam(beam)
        self._calc_q_mags()
        q_mags = self._q_mags
        if q_range is None:
            q_range = (0, np.max(q_mags))
        if n_bins is None:
            n_bins = int(np.sqrt(q_mags.size) / 4.0)
        q_range = np.array(q_range)
        bin_size = (q_range[1] - q_range[0]) / float(n_bins - 1)
        bin_centers = np.linspace(q_range[0], q_range[1], n_bins)
        bin_edges = np.linspace(
            q_range[0] - bin_size / 2, q_range[1] + bin_size / 2, n_bins + 1
        )
        q_edge_range = np.array([q_range[0] - bin_size / 2, q_range[1] + bin_size / 2])
        self.n_bins = n_bins
        self.bin_centers = bin_centers
        self.bin_edges = bin_edges
        self.bin_size = bin_size
        self.q_range = q_range
        self.q_edge_range = q_edge_range
        self.set_mask(mask)
        self.bin_centers.flags["WRITEABLE"] = False
        self.bin_edges.flags["WRITEABLE"] = False
        self.q_range.flags["WRITEABLE"] = False
        self.q_edge_range.flags["WRITEABLE"] = False

    def concat_data(self, data):
        r"""Concatenate 2D diffraction data."""
        if self._pad_geometry is not None:
            return self._pad_geometry.concat_data(data)
        utils.depreciate(
            "Do not use RadialProfiler without a |PADGeometryList|.  Data structure may depend on it."
        )
        return detector.concat_pad_data(data)

    def get_profile_statistic(self, data, mask=None, statistic=None):
        r"""
        Calculate the radial profile using an arbitrary function.

        Arguments:
            data (|ndarray|): The intensity data from which the radial profile is formed.
            mask (|ndarray|): Optional.  A mask to indicate bad pixels.  Zero is bad, one is good.  If no mask is
                                 provided here, the mask configured with :meth:`set_mask` will be used.
            statistic (function or list of functions): Provide a function of your choice that runs on each radial bin.

        Returns: |ndarray|
        """
        data = self.concat_data(data)
        q = self._q_mags
        if mask is None:
            mask = self._mask
        w = np.where(self._mask)
        data = data[w]
        q = q[w]
        list_type = False
        if isinstance(statistic, list):
            list_type = True
        statistic = utils.ensure_list(statistic)
        stat = []
        for s in statistic:
            stat.append(
                utils.binned_statistic(
                    q, data, s, self.n_bins, (self.bin_edges[0], self.bin_edges[-1])
                )
            )
        if not list_type:
            stat = stat[0]
        return stat

    def get_counts_profile(self, mask=None, quick=True):
        r"""Calculate the radial profile of counts that fall in each bin.  If the mask is not binary, then the "counts"
        are actually the sum of weights in each bin.

        Arguments:
            mask (|ndarray|): Optional mask.  Uses cache if not provided.

        Returns:
            |ndarray|
        """
        data = np.empty(len(self._q_mags), dtype=np.float64)
        stats = self.quickstats(data, weights=mask)
        return stats["weight_sum"]

    def get_sum_profile(self, data, mask=None, quick=True):
        r"""
        Calculate the radial profile of summed intensities.  This is divided by counts to get an average.

        Arguments:
            data (|ndarray|):  The intensity data from which the radial profile is formed.
            mask (|ndarray|):  Optional.  A mask to indicate bad pixels.  Zero is bad, one is good.

        Returns:  |ndarray|
        """
        stats = self.quickstats(data, weights=mask)
        return stats["sum"]

    def get_mean_profile(self, data, mask=None, quick=True):
        r"""
        Calculate the radial profile of averaged intensities.

        Arguments:
            data (|ndarray|):  The intensity data from which the radial profile is formed.
            mask (|ndarray|):  Optional.  A mask to indicate bad pixels.  Zero is bad, one is good.  If no mask is
                                 provided here, the mask configured with :meth:`set_mask` will be used.

        Returns: |ndarray|
        """
        stats = self.quickstats(data, weights=mask)
        return stats["mean"]

    def get_median_profile(self, data, mask=None):
        r"""
        Calculate the radial profile of averaged intensities.

        Arguments:
            data (|ndarray|):  The intensity data from which the radial profile is formed.
            mask (|ndarray|):  Optional.  A mask to indicate bad pixels.  Zero is bad, one is good.  If no mask is
                                 provided here, the mask configured with :meth:`set_mask` will be used.

        Returns:  |ndarray|
        """
        return self.get_profile_statistic(data, mask=mask, statistic=np.median)

    def get_sdev_profile(self, data, mask=None, quick=True):
        r"""
        Calculate the standard deviations of radial bin.

        Arguments:
            data (|ndarray|):  The intensity data from which the radial profile is formed.
            mask (|ndarray|):  Optional.  A mask to indicate bad pixels.  Zero is bad, one is good.  If no mask is
                                 provided here, the mask configured with :meth:`set_mask` will be used.

        Returns:  |ndarray|
        """
        stats = self.quickstats(data, weights=mask)
        return stats["sdev"]

    def subtract_profile(self, data, mask=None, statistic=np.median):
        r"""
        Given some PAD data, subtract a radial profile (mean or median).

        Arguments:
            data (|ndarray|):  The intensity data from which the radial profile is formed.
            mask (|ndarray|):  Optional.  A mask to indicate bad pixels.  Zero is bad, one is good.  If no mask is
                                 provided here, the mask configured with :meth:`set_mask` will be used.
            statistic (function): Provide a function of your choice that runs on each radial bin.

        Returns:

        """
        as_list = False
        if isinstance(data, list):
            as_list = True
        if statistic == "mean":
            mprof = self.get_mean_profile(data, mask=mask)
        elif statistic == "median":
            mprof = self.get_median_profile(data, mask=mask)
        else:
            mprof = self.get_profile_statistic(data, mask=mask, statistic=statistic)
        mprofq = self.bin_centers
        mpat = np.interp(self._q_mags, mprofq, mprof)
        mpat = self.concat_data(mpat)
        data = self.concat_data(data)
        data = data.copy()
        if np.issubdtype(data.dtype, np.integer):
            data = data.astype(float)
        data -= mpat
        if as_list:
            data = self._pad_geometry.split_data(data)
        return data

    def divide_profile(self, data, mask=None, statistic=np.median):
        r"""
        Given some PAD data, subtract a radial profile (mean or median).

        Arguments:
            data (|ndarray|):  The intensity data from which the radial profile is formed.
            mask (|ndarray|):  Optional.  A mask to indicate bad pixels.  Zero is bad, one is good.  If no mask is
                                 provided here, the mask configured with :meth:`set_mask` will be used.
            statistic (function): Provide a function of your choice that runs on each radial bin.

        Returns:
            |ndarray|
        """
        as_list = False
        if isinstance(data, list):
            as_list = True
        if statistic == "mean":
            mprof = self.get_mean_profile(data, mask=mask)
        elif statistic == "median":
            mprof = self.get_median_profile(data, mask=mask)
        else:
            mprof = self.get_profile_statistic(data, mask=mask, statistic=statistic)
            # raise ValueError('Statistic %s not recognized' % (statistic,))
        mprofq = self.bin_centers
        mpat = np.interp(self._q_mags, mprofq, mprof)
        mpat = self.concat_data(mpat)
        data = self.concat_data(data)
        data = data.copy()
        data /= mpat
        if as_list:
            data = self._pad_geometry.split_data(data)
        return data

    def subtract_median_profile(self, data, mask=None):
        r"""
        Given some PAD data, calculate the radial median and subtract it from the data.

        Arguments:
            data (|ndarray|): Intensities.
            mask (|ndarray|): Mask.

        Returns:
            |ndarray|
        """
        return self.subtract_profile(data, mask=mask, statistic=np.median)

    def quickstats(self, data, weights=None):
        r"""Use the faster fortran functions to get the mean and standard deviations.  These are weighted by the
        mask, and you are allowed to use non-binary values in the mask.  The proper weighting is most likely equal
        to the product of the pixel solid angle, the polarization factor, and the binary mask.

        Arguments:
            data (|ndarray|): Diffraction intensities.
            weights (|ndarray|): Weights (or binary mask if you prefer unweighted average)

        Returns:
            dict
        """
        q = self._q_mags
        data = self.concat_data(data).astype(np.float64).copy()
        if weights is None:
            weights = self._mask.astype(np.float64)
        else:
            weights = self.concat_data(weights).astype(np.float64)
        q_min = self.q_range[0]
        q_max = self.q_range[1]
        n_bins = self.n_bins
        sum_ = np.zeros(n_bins, dtype=np.float64)
        sum2 = np.zeros(n_bins, dtype=np.float64)
        w_sum = np.zeros(n_bins, dtype=np.float64)
        indices = self._fortran_indices
        data[weights==0] = 0
        data[weights!=0] /= weights[weights!=0] #since the data already has weights referred above
        if indices is None:  # Cache for faster computing next time.
            indices = np.zeros(len(q), dtype=np.int32)
            fortran.scatter_f.profile_indices(q, n_bins, q_min, q_max, indices)
            self._fortran_indices = indices

        fortran.scatter_f.profile_stats_indexed(
            data, indices, weights, sum_, sum2, w_sum
        )
        # fortran.scatter_f.profile_stats(data, q, weights, n_bins, q_min, q_max, sum_, sum2, w_sum)
        meen = np.empty(n_bins, dtype=np.float64)
        std = np.empty(n_bins, dtype=np.float64)
        fortran.scatter_f.profile_stats_avg(sum_, sum2, w_sum, meen, std)
        out = dict(mean=meen, sdev=std, sum=sum_, sum2=sum2, weight_sum=w_sum)
        return out


class ParallelRadialProfiler(ParallelAnalyzer):
    r"""
    Parallelized class for creating radial profiles from x-ray diffraction data.



    """

    include_median = False
    profiler = None
    radials = None

    def __init__(
        self,
        framegetter=None,
        n_q_bins=1000,
        q_range=None,
        pad_geometry=None,
        beam=None,
        mask=None,
        include_median=False,
        **kwargs
    ):
        r"""
        Parallel analyzer that produces scattering ("radial") profiles from diffraction patterns.
        See the :meth:`~to_dict` method for details of what the output is.

        Standard profiles are computed using fortran code. Bin indices are cached for
        speed, provided that the |PADGeometry| and |Beam| do not change.

        Arguments:
            framegetter (|FrameGetter|): The FrameGetter that serves the data for analysis.
            n_bins (int): Number of radial bins (default is 1000).
            q_range (list-like): The minimum and maximum of the *centers* of the q bins.
            pad_geometry (|PADGeometryList|): Detector geometry, used to generate q magnitudes.
                                              If None, then automatically retrieved from raw data.
            beam (|Beam|): X-ray beam. Wavelength and beam direction required to calculate q magnitudes.
                           If None, then automatically retrieved from raw data.
            mask (List or |ndarray|): Optional (default is no masked pixels). Data will be multiplied by this mask,
                                      and the counts per radial bin will come from this (e.g. use values of
                                      0 and 1 if you want a normal average, otherwise you get a weighted average).
            include_median (bool): Set to True to include median profile (default is False).
            start (int): Which frame to start with.
            stop (int): Which frame to stop at.
            step (int): Step size between frames (default 1).
            n_processes (int): How many processes to run in parallel (if parallel=True).
            **kwargs: Any key-word arguments you would like to pass to the base class.
                      See: ..:py:class::`~reborn.analysis.parallel.ParallelAnalyzer`
        """
        super().__init__(framegetter=framegetter, **kwargs)
        self.logger.debug("set up ParallelAnalyser superclass")
        self.kwargs["n_q_bins"] = n_q_bins
        self.kwargs["q_range"] = q_range
        self.kwargs["pad_geometry"] = pad_geometry
        self.kwargs["beam"] = beam
        self.kwargs["mask"] = mask
        self.kwargs["include_median"] = include_median
        self.analyzer_name = "ParallelRadialProfiler"
        if pad_geometry is None:
            raise ValueError("pad_geometry cannot be None")
        if beam is None:
            raise ValueError("beam cannot be None")
        if mask is None:
            mask = pad_geometry.ones()
        mask = pad_geometry.concat_data(mask)
        self.sap = None #Solid angle multiplied by polarization factor

        self.logger.debug("setting up profiler")
        self.profiler = RadialProfiler(
            mask=mask,
            n_bins=n_q_bins,
            q_range=q_range,
            pad_geometry=pad_geometry,
            beam=beam,
        )
        self.radials = {
            "sum": [],
            "sum2": [],
            "wsum": [],
            "mean": [],
            "sdev": [],
            "median": [],
            "frame_id": [],
            "n_frames": 0,
            "n_q_bins": n_q_bins,
            "q_bins": self.profiler.q_bin_centers,
            "experiment_id": self.framegetter.experiment_id,
            "run_id": self.framegetter.run_id,
            "pad_geometry": pad_geometry,
            "beam": beam,
            "mask": mask,
        }

    def add_frame(self, dat: DataFrame):
        if dat.validate():
            #data = dat.get_raw_data_flat()
            data = dat.get_processed_data_flat()
            mask = dat.get_mask_flat()
            if self.sap is None:
                g = dat.get_pad_geometry()
                b = dat.get_beam()
                self.sap = g.polarization_factors(beam=b)*g.solid_angles()
            weights= self.sap*mask
            out = self.profiler.quickstats(data=data, weights=weights)
            self.radials["sum"].append(out["sum"])
            self.radials["sum2"].append(out["sum2"])
            self.radials["wsum"].append(out["weight_sum"])
            self.radials["mean"].append(out["mean"])
            self.radials["sdev"].append(out["sdev"])
            self.radials["frame_id"].append(dat.get_frame_id())
            self.radials["n_frames"] += 1
            if self.kwargs["include_median"]:
                m = self.profiler.get_median_profile(data=data, mask=mask)
                self.radials["median"].append(m)

    def concatenate(self, stats: dict):
        self.radials["sum"].extend(stats["sum"])
        self.radials["sum2"].extend(stats["sum2"])
        self.radials["wsum"].extend(stats["wsum"])
        self.radials["mean"].extend(stats["mean"])
        self.radials["sdev"].extend(stats["sdev"])
        self.radials["median"].extend(stats["median"])
        self.radials["frame_id"].extend(stats["frame_id"])
        self.radials["n_frames"] += stats["n_frames"]

    def to_dict(self):
        """
        Create a dictionary of the results.

        Returns:
            (dict):
                - **mean** (|ndarray|) -- Mean of unmasked intensities
                - **sdev** (|ndarray|) -- Standard deviation of unmasked intensities
                - **median** (|ndarray|) -- Median of unmasked intensities (only if requested; this is slow)
                - **sum** (|ndarray|) -- Sum of unmasked intensities
                - **sum2** (|ndarray|) -- Sum of squared unmasked intensities
                - **wsum** (|ndarray|) -- Weighted sum (if no weights are provided,
                          then this is the number of unmasked pixels in the q bin)
                - **n_frames** (int) -- Number of frames analyzed.
                - **initial_frame** (int) -- First frame from which geometry and beam data are extracted.
                - **n_q_bins** (int) -- Number of q bins in radial profiles.
                - **q_bins** (|ndarray|) -- The centers of the q bins.
                - **experiment_id** (str) -- Identifier for experiment being analyzed.
                - **run_id** (str) -- Identifier for run being analyzed
                - **pad_geometry** (|PADGeometryList|) -- Detector geometry used to set up RadialProfiler.
                - **beam** (|Beam|) -- X-ray beam used to set up RadialProfiler.
                - **mask** (|ndarray|) -- Mask used to set up RadialProfiler.
        """
        return self.radials

    def from_dict(self, stats: dict):
        self.radials["sum"] = stats["sum"]
        self.radials["sum2"] = stats["sum2"]
        self.radials["wsum"] = stats["wsum"]
        self.radials["mean"] = stats["mean"]
        self.radials["sdev"] = stats["sdev"]
        self.radials["median"] = stats["median"]
        self.radials["frame_id"] = stats["frame_id"]
        self.radials["n_frames"] = stats["n_frames"]
        self.radials["n_q_bins"] = stats["n_q_bins"]
        self.radials["q_bins"] = stats["q_bins"]
        self.radials["experiment_id"] = stats["experiment_id"]
        self.radials["run_id"] = stats["run_id"]
        self.radials["pad_geometry"] = stats["pad_geometry"]
        self.radials["beam"] = stats["beam"]
        self.radials["mask"] = stats["mask"]


def save_profiles(stats, filepath):
    fileio.misc.save_pickle(stats, filepath)


def load_profiles(filepath):
    stats = fileio.misc.load_pickle(filepath)
    with np.errstate(divide="ignore", invalid="ignore"):
        meen = stats["sum"] / stats["counts"]
        meen2 = stats["sum2"] / stats["counts"]
        sdev = np.nan_to_num(
            meen2 - meen**2, copy=True, nan=0.0, posinf=0.0, neginf=0.0
        )
        sdev[sdev < 0] = 0
        sdev = np.sqrt(sdev)
    stats["mean"] = meen
    stats["sdev"] = sdev
    return stats


def view_profile_runstats(stats):
    q = stats["q_bins"]
    labels = ["Mean", "Standard Deviation"]
    images = np.array([stats["mean"], stats["sdev"]])
    pg.imview(
        images,
        fs_label="q [A]",
        ss_label="frame #",
        fs_lims=[q[0] / 1e10, q[-1] / 1e10],
        hold=True,
        frame_names=labels,
    )


def normalize_profile_stats(stats, q_range=None):
    r"""
    Given a stats dictionary, normalize by setting a particular q range to an average of 1.

    Arguments:
        stats (dict): A stats dictionary created by get_profile_stats_from_pandas()
        q_range (tuple): Optionally specify the normalization range with a tuple containing (q_min, q_max)

    Returns:
        dict: An updates stats dictionary
    """

    out_keys = ["median", "mean", "sum", "sum2", "counts", "q_bins"]
    q = stats["q_bins"][0, :]
    if q_range is None:
        q_range = (np.min(q), np.max(q))
    run_pmedian = stats["median"].copy()
    run_pmean = stats["mean"].copy()
    run_psdev = stats["sdev"].copy()
    run_psum = stats["sum"].copy()
    run_psum2 = stats["sum2"].copy()
    qmin = q_range[0]
    qmax = q_range[1]
    w = np.where((q > qmin) * (q < qmax))
    s = np.mean(run_pmean[:, w[0]], axis=1)
    out_vals = [
        (run_pmedian.T / s).T,
        (run_pmean.T / s).T,
        (run_psdev.T / s).T,
        (run_psum.T / s).T,
        (run_psum2.T / s**2).T,
        stats["counts"].copy(),
        stats["q_bins"].copy(),
    ]
    return dict(zip(out_keys, out_vals))
