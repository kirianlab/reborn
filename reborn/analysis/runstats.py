# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

r""" Utilities for gathering statistics from data runs. """
import numpy as np
import pyqtgraph as pg
from scipy.signal import find_peaks, peak_prominences

try:
    from joblib import Parallel, delayed
except ImportError:
    Parallel = None
    delayed = None
from .parallel import ParallelAnalyzer
from .. import detector
from .. import fileio
from ..dataframe import DataFrame
from ..external.pyqtgraph import imview
from ..fileio.getters import ListFrameGetter
import time
import matplotlib.pyplot as plt


class PixelHistogram:
    count = 0  #: Number of frames contributing to the histogram.
    bin_min = None  #: The minimum value corresponding to histogram bin *centers*.
    bin_max = None  #: The maximum value corresponding to histogram bin *centers*.
    n_bins = None  #: The number of histogram bins.

    def __init__(
        self,
        bin_min=None,
        bin_max=None,
        n_bins=None,
        n_pixels=None,
        zero_photon_peak=None,
        one_photon_peak=None,
        peak_width=None,
    ):
        r"""Creates an intensity histogram for each pixel in a PAD.  For a PAD with N pixels in total, this class
        will produce an array of shape (M, N) assuming that you requested M bins in the histogram.

        Arguments:
            bin_min (float): The minimum value corresponding to histogram bin *centers*.
            bin_max (float): The maximum value corresponding to histogram bin *centers*.
            n_bins (int): The number of histogram bins.
            n_pixels (int): How many pixels there are in the detector.
            zero_photon_peak (float): Where you think the peak of the zero-photon signal should be.
            one_photon_peak (float): Where you think the peak of the one-photon signal should be.
            peak_width (float): Width of peaks for fitting to 2nd-order polynomial
        """
        self.bin_min = float(bin_min)
        self.bin_max = float(bin_max)
        self.n_bins = int(n_bins)
        self.n_pixels = int(n_pixels)
        self.zero_photon_peak = zero_photon_peak
        self.one_photon_peak = one_photon_peak
        self.peak_width = peak_width
        if self.peak_width is None:
            if not None in [self.one_photon_peak, self.zero_photon_peak]:
                self.peak_width = (one_photon_peak - zero_photon_peak) / 4
        self.bin_delta = (self.bin_max - self.bin_min) / (self.n_bins - 1)
        self._idx = np.arange(self.n_pixels, dtype=int)
        self.histogram = np.zeros((self.n_pixels, self.n_bins), dtype=np.uint16)

    def to_dict(self):
        r"""Convert class to dictionary for storage."""
        return dict(
            bin_min=self.bin_min,
            bin_max=self.bin_max,
            n_bins=self.n_bins,
            n_pixels=self.n_pixels,
            one_photon_peak=self.one_photon_peak,
            zero_photon_peak=self.zero_photon_peak,
            peak_width=self.peak_width,
            histogram=self.histogram,
        )

    def from_dict(self, d):
        r"""Set data according to data stored in dictionary created by to_dict."""
        self.bin_min = d["bin_min"]
        self.bin_max = d["bin_max"]
        self.n_bins = d["n_bins"]
        self.n_pixels = d["n_pixels"]
        self.one_photon_peak = d["one_photon_peak"]
        self.zero_photon_peak = d["zero_photon_peak"]
        self.histogram = d["histogram"]

    def concatentate(self, hist):
        r"""Combines two histograms (e.g. if parallel processing and chunks need to be combined)."""
        self.histogram += hist.histogram

    def get_bin_centers(self):
        r"""Returns an 1D array of histogram bin centers."""
        return np.linspace(self.bin_min, self.bin_max, self.n_bins)

    def get_histogram_normalized(self):
        r"""Returns a normalized histogram - an |ndarray| of shape (M, N) where M is the number of pixels and
        N is the number of requested bins per pixel."""
        count = np.sum(self.histogram, axis=1)
        with np.errstate(divide="ignore", invalid="ignore"):
            return np.nan_to_num(self.histogram.T / count).T

    def get_histogram(self):
        r"""Returns a copy of the histogram - an |ndarray| of shape (M, N) where M is the number of pixels and
        N is the number of requested bins per pixel."""
        return self.histogram.copy()

    def add_frame(self, data, mask=None):
        r"""Add PAD measurement to the histogram."""
        t = time.time()
        bin_index = np.floor((data - self.bin_min) / self.bin_delta).astype(int)
        bin_index[bin_index < 0] = 0
        bin_index[bin_index >= self.n_bins] = self.n_bins - 1
        idx = self._idx
        if mask is not None:
            idx = idx[mask > 0]
            bin_index = bin_index[mask > 0]
        # np.add.at(self.histogram, (idx, bin_index), 1)
        self.histogram[idx, bin_index] += 1
        # print(f"histogram.add_frame timer: {time.time()-t}")

    def get_peaks_fast(self):
        r"""Finds the local maxima in the neighborhood of estimated
        zero-photon and one-photon peak locations, and peak width estimate."""
        hist = self.get_histogram_normalized()
        x = self.get_bin_centers()

        if None in [self.zero_photon_peak, self.one_photon_peak]:
            p0, p1, _ = self.get_average_peak_locations()
            # print(p0,p1)
            if self.peak_width is None and np.nan not in [p0, p1]:
                self.peak_width = (p1 - p0) / 4
        else:
            p0 = self.zero_photon_peak
            p1 = self.one_photon_peak
        g = (p1 - p0) / 2  # Peak-width estimate

        # Defining vicinitty around the peak
        m0 = (x > p0 - g) * (x < p0 + g)
        m1 = (x > p1 - g) * (x < p1 + g)

        # Local maxima around zeroth and one-photon peaks
        i0 = np.argmax(hist * m0, axis=1)
        i1 = np.argmax(hist * m1, axis=1)
        # print(i0,i1)

        x0 = x[i0]
        x1 = x[i1]
        if np.isnan(p0):
            x0[:] = np.nan
            i0[:] = np.nan
        if np.isnan(p1):
            x1[:] = np.nan
            i1[:] = np.nan
        # print (x0, x1, i0, i1)

        return x0, x1, i0, i1

    def get_average_peak_locations(self):
        r"""Average all pixels in the entire detector, estimate the locations of the
        zero-photon and one-photon peaks.  Assumes a very smooth histogram.

        Returns np.nan values when there is one or zero peaks found (e.g. on a dark dataset
        there will not be a second peak!)
        """
        h = self.histogram.copy().astype(float)
        h = np.sum(h, axis=0)
        assert h.size > 10
        h[0] = h[1]  # The edge bins can overflow
        h[-1] = h[-2]
        peaks, properties = find_peaks(h)
        prominences = peak_prominences(h, peaks)
        x = self.get_bin_centers()
        peak0 = np.nan
        peak1 = np.nan
        if peaks.size > 0:
            peak0 = x[peaks[0]]
        if peaks.size > 1:
            peak1 = x[peaks[1]]
        # TODO: Refine these peak locations with polynomial fits.
        extra = dict(peaks=peaks, properties=properties, prominences=prominences)
        return peak0, peak1, extra

    def gain_and_offset(self):
        x0, x1, i0, i1 = self.get_peaks_fast()
        return x1 - x0, x0

    def get_refined_gain_and_offset(self, n_processes=1):
        r"""Does a 2nd-order polynomial fit to the peak.  First guess is the local maximum."""
        params = dict(
            n_bins=self.n_bins,
            bin_min=self.bin_min,
            bin_max=self.bin_max,
            n_pixels=self.n_pixels,
        )
        stats = dict(histogram=self.histogram, histogram_params=params)
        return get_gain_and_offset(stats, n_processes=n_processes)
        # print("get_refined_gain_and_offset")
        # hist = self.get_histogram_normalized()
        # x = self.get_bin_centers()
        # n_pixels = hist.shape[0]
        # n_bins = hist.shape[1]
        # p0, p1, i0, i1 = self.get_peaks_fast()
        # poly = np.polynomial.Polynomial
        # peak0 = np.zeros(n_pixels)
        # peak1 = np.zeros(n_pixels)
        # order = 2
        # fails = 0
        # for i in range(n_pixels):
        #     print(f"{i} of {n_pixels} ({i*100/n_pixels:0.2f}% done, {fails} fails)")
        #     h = hist[i, :]
        #     w = int(np.ceil(i1[i] - i0[i]))
        #     if w < order:
        #         fails += 1
        #         print("gap too small")
        #         continue
        #     p = i0[i]
        #     a = max(0, p - w)
        #     b = min(n_bins - 1, p + w)
        #     yd = h[a : b + 1]
        #     xd = x[a : b + 1]
        #     fit, extra = poly.fit(xd, yd, 2, full=True)
        #     peak0[i] = fit.deriv(1).roots()[0]
        #     p = i1[i]
        #     a = max(0, p - w)
        #     b = min(n_bins - 1, p + w)
        #     yd = h[a : b + 1]
        #     xd = x[a : b + 1]
        #     fit, extra = poly.fit(xd, yd, 2, full=True)
        #     peak1[i] = fit.deriv(1).roots()[0]
        # self.offset = peak0
        # self.gain = peak1 - peak0
        # return self.gain, self.offset

    def correct_gain_and_offset(self, histogram=None, gain=None, offset=None):
        r"""Apply gain and offset values to a histogram using interpolations."""
        if histogram is None:
            histogram = self.histogram
        histogram = histogram.copy().astype(float)
        param = [gain, offset]
        noneval = [item for item in param if item is None]
        if noneval:  # None in [gain, offset]:
            gain, offset = self.get_refined_gain_and_offset()
        h = np.zeros_like(histogram)
        b = self.get_bin_centers()
        bp = np.linspace(-1, 5, num=b.size)
        for i, (p, g, o) in enumerate(zip(histogram, gain, offset)):
            if (
                np.sum(p) == 0
            ):  # Checks whether any pixel histogram is zero due to masking
                continue
            bmod = bp * g + o
            h[i, :] = np.interp(bmod, b, p)
        return h, bp

    def convert_to_q_histogram(
        self,
        n_q_bins=None,
        q_range=None,
        pad_geometry=None,
        beam=None,
        mask=None,
        normalized=False,
    ):
        r"""
        Convert pixel histogram to q-bin histogram.

        Arguments:
            n_q_bins (int): = None,
            q_range (tuple): = None,
            pad_geometry (|PADGeometry|): = None,
            beam (|Beam|): = None,

        Returns:
            q_histogram (|ndarray|):
        """
        q_mags = pad_geometry.q_mags(beam=beam)
        if mask is None:
            mask = np.ones(q_mags.size, dtype=self.histogram.dtype)
        if q_range is None:
            q_range = (0, np.max(q_mags))
        if n_q_bins is None:
            n_q_bins = int(np.sqrt(q_mags.size) / 4.0)
        bin_size = (q_range[1] - q_range[0]) / float(n_q_bins - 1)
        q_index = np.floor((q_mags - q_range[0]) / bin_size)
        q_index[q_index < 0] = 0
        q_index[q_index >= n_q_bins] = n_q_bins - 1
        q_index = q_index.astype(int)
        q_histogram = np.zeros((n_q_bins, self.n_bins), dtype=int)
        for i, q in enumerate(q_index):
            q_histogram[q, :] += (mask[i] * self.histogram[i]).astype(int)
        if normalized:
            with np.errstate(divide="ignore", invalid="ignore"):  # Allow NaNs
                q_histogram = (q_histogram.T / np.sum(q_histogram, 1)).T.copy()
        return q_histogram

    def get_median_profile(
        self,
        n_q_bins=None,
        q_range=None,
        pad_geometry=None,
        beam=None,
        mask=None,
        normalized=False,
    ):
        r"""
        Convert pixel histogram to q-bin histogram and then produce a scattering
        profile based on median values of each q bin.

        Arguments:
            n_q_bins (int): = None,
            q_range (tuple): = None,
            pad_geometry (|PADGeometry|): = None,
            beam (|Beam|): = None,

        Returns:
            tuple:
                - **q_bins** (|ndarray|): Q bins
                - **median** (|ndarray|): Median profile
        """
        q_bins = np.linspace(q_range[0], q_range[1], n_q_bins)
        adu_bins = np.linspace(self.bin_min, self.bin_max, self.n_bins)
        qb = self.convert_to_q_histogram(
            pad_geometry=pad_geometry, n_q_bins=n_q_bins, q_range=q_range, beam=beam
        )
        cp = np.cumsum(qb, axis=1)
        med = np.zeros(n_q_bins)
        with np.errstate(divide="ignore", invalid="ignore"):
            for i in range(n_q_bins):
                c = cp[i, :].ravel()
                c /= c[-1]
                med[i] = np.interp(0.5, c, adu_bins)
        return q_bins, med

    def process_framegetter(self, framegetter):
        # TODO: Add docstring
        for f in framegetter:
            data = f.get_processed_data_flat()
            mask = f.get_mask_flat()
            self.add_frame(data=data, mask=mask)


class ParallelPADStats(ParallelAnalyzer):
    def __init__(self, framegetter=None, histogram_params=None, **kwargs):
        r"""Gather PAD statistics for a dataset.

        Given a |FrameGetter| subclass instance, fetch the mean intensity, mean squared intensity, minimum,
        and maximum intensities, and optionally a pixel-by-pixel intensity histogram.  The function can run on multiple
        processors via the joblib library.  Logfiles and checkpoint files are created.

        The return of this function is a dictionary with the following keys:

        * 'sum': Sum of PAD intensities
        * 'dataset_id': A unique identifier for the data set (e.g. 'run0154')
        * 'pad_geometry': PADGeometryList
        * 'mask': Bad pixel mask
        * 'n_frames': Number of valid frames contributing to the statistics
        * 'sum': Sum of PAD intensities
        * 'sum2': Sum of squared PAD intensities
        * 'min': Pixel-by-pixel minimum of PAD intensities
        * 'max': Pixel-by-pixel maximum of PAD intensities
        * 'counts': Sum of the PAD mask
        * 'beam': Beam info
        * 'start': Frame at which processing started (global framegetter index)
        * 'stop': Frame at which processing stopped (global framegetter index)
        * 'step': Step size between frames (helpful to speed up processing by sub-sampling)
        * 'wavelengths': An array of wavelengths
        * 'histogram_params': Dictionary with histogram parameters
        * 'histogram': Pixel-by-pixel intensity histogram (MxN array, with M the number of pixels)

        There is a corresponding view_padstats function to view the results in this dictionary.

        padstats needs a configuration dictionary with the following contents:

        * 'log_file': Base path/name for status logging.  Set to None to skip logging.
        * 'checkpoint_file': Base path/name for saving check points.  Set to None to skip checkpoints.
        * 'checkpoint_interval': How many frames between checkpoints.
        * 'message_prefix': Prefix added to all logging messages.  For example: "Run 35" might be helpful
        * 'debug': Set to True for more logging messages.
        * 'reduce_from_checkpoints': True by default, this indicates that data produced by multiple processors should be
          compiled by loading the checkpoints from disk.  Without this, you might have memory problems.  (The need for this
          is due to the joblib package; normally the reduce functions from MPI would be used to avoid hitting the disk.)
        * 'histogram_params': If not None, triggers production of a pixel-by-pixel histogram.  This is a dictionary with
          the following entries: dict(bin_min=-30, bin_max=100, n_bins=100, zero_photon_peak=0, one_photon_peak=30)

        Arguments:
            framegetter (|FrameGetter|): A FrameGetter subclass.  If running in parallel, you should instead pass a
                                         dictionary with keys 'framegetter' (with reference to FrameGetter subclass,
                                         not an actual class instance) and 'kwargs' containing a dictionary of keyword
                                         arguments needed to create a class instance.
            histogram_params (dict):
            start (int): Which frame to start with.
            stop (int): Which frame to stop at.
            step (int): Step size between frames (default 1).
            n_processes (int): How many processes to run in parallel (if parallel=True).
            **kwargs: Any additional key-word arguments you would like to pass to the base class.
                      See: ..:py:class::`~reborn.analysis.parallel.ParallelAnalyzer`

        Returns: dict"""
        super().__init__(framegetter=framegetter, **kwargs)
        self.kwargs["histogram_params"] = histogram_params
        self.logger.debug(f"histogram_params: {histogram_params}")
        self.analyzer_name = "ParallelPADStats"
        self.initialized = False
        # Data for pixel-by-pixel histograms
        self.histogrammer = None  # Instance of |PixelHistogram|
        self.histogram_params = histogram_params  # Configs for |PixelHistogram|
        # Simple pass-through data for convenience
        self.dataset_id = None  # Often something like "r0045" or similar
        self.pad_geometry = None  # First PadGeometryList found in the FrameGetter
        self.beam = None  # Will have the median wavelength
        self.mask = None  # First mask found in the FrameGetter
        # Cumulative pixel-by-pixel data
        self.min_pad = None  # Array of minimum intensities
        self.max_pad = None  # Array of maximum intensities
        self.sum_pad = None  # Sum of all intensities
        self.sum_pad2 = None  # Sum of all squared intensities
        self.counts = None  # Counts
        # Shot-by-shot data
        self.wavelengths = None  # Array of all wavelengths
        self.percentiles = None  # Shot-by-shot percentiles
        self.sums = None  # Shot-by-shot sums over the whole PAD
        self.frame_ids = None  # Shot-by-shot frame IDs (not including skipped frames)
        # # Results derived at the end of data gathering
        # self.gain = None  # A crude estimate of detector gain (probably wrong)
        # self.offset = None  # A crude estimate of detector offset (probably wrong)

    def add_frame(self, dat):
        if dat is None:
            self.logger.warning(
                f"DataFrame {self.processing_index} is None.  Skipping frame"
            )
            return
        rdat = dat.get_raw_data_flat()
        mask = dat.get_mask_flat().copy()
        if np.sum(mask) == 0:
            self.logger.warning(
                f"DataFrame {self.processing_index} is fully masked.  Skipping frame"
            )
            return
        idx = np.where(mask > 0)
        rdat = rdat * mask
        if rdat is None:
            self.logger.warning(f"Raw data is None.  Skipping frame.")
            return
        if not self.initialized:
            self.initialize_data(rdat)
        if dat.validate():
            beam = dat.get_beam()
            self.wavelengths[self.processing_index] = beam.wavelength
            if self.beam is None:
                self.beam = beam
        else:
            self.logger.warning(
                "DataFrame is invalid.  If it is a dark run this could be due to missing Beam info."
            )
        self.sum_pad += rdat
        self.sum_pad2 += rdat**2
        self.min_pad[idx] = np.minimum(self.min_pad[idx], rdat[idx])
        self.max_pad[idx] = np.maximum(self.max_pad[idx], rdat[idx])
        self.counts += mask
        self.sums[self.processing_index] = np.sum(rdat[idx])
        self.percentiles[self.processing_index, :] = np.percentile(
            rdat[idx], [50, 90, 95, 99]
        )
        self.frame_ids.append(dat.get_frame_id())
        if self.dataset_id is None:
            self.dataset_id = dat.get_dataset_id()
        if self.pad_geometry is None:
            self.pad_geometry = dat.get_pad_geometry()
        if self.mask is None:
            self.mask = dat.get_mask_flat()
        if self.histogrammer is not None:
            self.histogrammer.add_frame(rdat, mask=mask)

    def finalize(self):
        r"""Compute gain and offset from histogram, trim arrays."""
        self.logger.info("Finalizing analysis")
        # if self.histogrammer is not None:
        #     self.logger.info("Attempting to get gain and offset from histogram")
        #     self.gain, self.offset = self.histogrammer.gain_and_offset()
        self.wavelengths = self.wavelengths[0 : self.n_processed]
        self.sums = self.sums[0 : self.n_processed]
        self.percentiles = self.percentiles[0 : self.n_processed, :]

    # def clear_data(self):
    #     self.wavelengths = None
    #     self.sum_pad = None
    #     self.sum_pad2 = None
    #     self.max_pad = None
    #     self.min_pad = None
    #     self.n_processed = 0
    #     self.histogrammer = None
    #     self.initialized = False

    def initialize_data(self, rdat):
        r"""Allocate memory, initialize counters."""
        self.logger.debug("Initializing arrays")
        s = rdat.size
        self.wavelengths = np.zeros(self.n_chunk)
        self.sum_pad = np.zeros(s)
        self.sum_pad2 = np.zeros(s)
        self.counts = np.zeros(s)
        self.max_pad = rdat.copy()
        self.min_pad = rdat.copy()
        self.sums = np.zeros(self.n_chunk)
        self.percentiles = np.zeros([self.n_chunk, 4])
        self.frame_ids = []
        self.n_processed = 0
        if self.histogram_params is not None:
            self.logger.info("Setting up histogram")
            if self.histogram_params.get("n_pixels", None) is None:
                self.histogram_params["n_pixels"] = self.sum_pad.shape[0]
            self.histogrammer = PixelHistogram(**self.histogram_params)
        self.initialized = True

    def to_dict(self):
        stats = dict()
        stats["dataset_id"] = self.dataset_id
        stats["pad_geometry"] = self.pad_geometry
        stats["mask"] = self.mask
        stats["n_frames"] = self.n_processed
        stats["sum"] = self.sum_pad
        stats["sum2"] = self.sum_pad2
        stats["min"] = self.min_pad
        stats["max"] = self.max_pad
        stats["counts"] = self.counts
        stats["beam"] = self.beam
        stats["start"] = self.start
        stats["stop"] = self.stop
        stats["step"] = self.step
        stats["wavelengths"] = self.wavelengths
        stats["sums"] = self.sums
        stats["percentiles"] = self.percentiles
        stats["frame_ids"] = self.frame_ids
        if self.histogrammer is not None:
            stats["histogram"] = self.histogrammer.histogram
            stats["histogram_params"] = self.histogram_params
        # if self.gain is not None:
        #     stats["gain"] = self.gain
        #     stats["offset"] = self.offset
        return stats

    def from_dict(self, stats):
        # self.clear_data()
        if stats.get("sum") is None:
            self.logger.warning("Stats dictionary is empty!")
            return
        self.dataset_id = stats["dataset_id"]
        self.pad_geometry = stats["pad_geometry"]
        self.mask = stats["mask"]
        self.n_processed = stats["n_frames"]
        self.sum_pad = stats["sum"]
        self.sum_pad2 = stats["sum2"]
        self.min_pad = stats["min"]
        self.max_pad = stats["max"]
        self.counts = stats.get(
            "counts", np.zeros(stats["sum"].size) + stats["n_frames"]
        )
        self.beam = stats["beam"]
        self.start = stats["start"]
        self.stop = stats["stop"]
        self.step = stats["step"]
        self.wavelengths = stats["wavelengths"]
        self.frame_ids = stats["frame_ids"]
        self.percentiles = stats["percentiles"]
        self.sums = stats["sums"]
        if stats.get("histogram_params"):
            self.histogram_params = stats["histogram_params"]
            self.histogrammer = PixelHistogram(**stats["histogram_params"])
            self.histogrammer.histogram = stats.get("histogram")
        self.initialized = True

    def concatenate(self, stats):
        r"""Take the existing data in this class instance and 'add' the date in the incoming
        stats dictionary.  E.g. extend arrays as needed, or sum them.  Also known as a 'reduce' step.
        One can think of 'merging' the data."""
        if not self.initialized:
            self.from_dict(stats)
            self.initialized = True
            return
        if stats["n_frames"] == 0:
            self.logger.debug("No frames to concatentate!")
            return
        self.start = min(self.start, stats["start"])
        self.stop = max(self.stop, stats["stop"])
        self.n_processed += stats["n_frames"]
        self.wavelengths = np.concatenate([self.wavelengths, stats["wavelengths"]])
        if stats["sum"] is None:
            return
        self.sum_pad += stats["sum"]
        self.sum_pad2 += stats["sum2"]
        self.min_pad = np.minimum(self.min_pad, stats["min"])
        self.max_pad = np.minimum(self.max_pad, stats["max"])
        c = stats.get("counts", np.zeros(stats["sum"].size) + stats["n_frames"])
        self.counts += c
        self.frame_ids = self.frame_ids + stats["frame_ids"]
        self.percentiles = np.concatenate([self.percentiles, stats["percentiles"]])
        self.sums = np.concatenate([self.sums, stats["sums"]])
        if stats.get("histogram") is not None:
            self.histogrammer.histogram += stats["histogram"]


def save_padstats(stats: dict, filepath: str):
    r"""Saves the results of the padstats function.

    Arguments:
        stats (dict): Dictionary output of padstats.
        filepath (str): Path to the file you want to save.
    """
    fileio.misc.save_pickle(stats, filepath)


def load_padstats(filepath: str):
    r"""Load the results of padstats from disk.

    Arguments:
        filepath (str): Path to the file you want to load.

    Returns: dict
    """
    stats = fileio.misc.load_pickle(filepath)
    c = stats.get("counts", stats["n_frames"])
    with np.errstate(divide="ignore", invalid="ignore"):
        meen = stats["sum"] / c
        meen2 = stats["sum2"] / c
        meen[~np.isfinite(meen)] = 0
        meen2[~np.isfinite(meen2)] = 0
        sdev = np.nan_to_num(meen2 - meen**2)
        # sdev[ ~np.isfinite(sdev)] = 0
    sdev[sdev < 0] = 0
    sdev = np.sqrt(sdev)
    stats["mean"] = meen
    stats["sdev"] = sdev
    if isinstance(stats.get("histogram"), np.ndarray):
        stats["histogrammer"] = PixelHistogram(**stats["histogram_params"])
        stats["histogrammer"].histogram = stats["histogram"]
    return stats


def padstats_framegetter(stats):
    r"""Make a FrameGetter that can flip through the output of padstats.

    Arguments:
        stats (dict): Output of padstats.

    Returns: ListFrameGetter
    """
    beam = stats["beam"]
    geom = stats["pad_geometry"]
    geom = detector.PADGeometryList(geom)
    mask = stats["mask"]
    c = stats.get("counts", np.zeros(stats["sum"].size) + stats["n_frames"])
    with np.errstate(divide="ignore", invalid="ignore"):
        meen = stats["sum"] / c
        meen2 = stats["sum2"] / c
        meen[~np.isfinite(meen)] = 0
        meen2[~np.isfinite(meen2)] = 0
        sdev = np.nan_to_num(meen2 - meen**2)
        # sdev[ ~np.isfinite(sdev)] = 0
    sdev[sdev < 0] = 0
    sdev = np.sqrt(sdev)
    dats = [
        ("mean", meen),
        ("sdev", sdev),
        ("min", stats["min"]),
        ("max", stats["max"]),
        ("counts", c),
    ]
    if "gain" in stats:
        dats.append(("gain", stats["gain"]))
    if "offset" in stats:
        dats.append(("offset", stats["offset"]))
    dfs = []
    for a, b in dats:
        d = DataFrame()
        d.set_dataset_id(stats["dataset_id"])
        d.set_frame_id(a)
        d.set_pad_geometry(geom)
        if beam is not None:
            d.set_beam(beam)
        d.set_mask(mask)
        d.set_raw_data(b)
        d.set_dataset_id("PADSTATS")
        dfs.append(d)
    return ListFrameGetter(dfs)


def view_padstats(
    stats,
    histogram=False,
    start=True,
    main=True,
    show_corrected_histogram=False,
    pad_geometry=None,
    beam=None,
    debug=0,
):
    r"""View the output of padstats.

    Arguments:
        stats (dict): Output dictionary from padstats.
        histogram (bool): Show the histogram
        start (bool): Choose if the PADView app should be started (executed)
        main (bool): Choose if the window should be a Qt main window.
        show_corrected_histogram (bool): Undocumented.
        pad_geometry (|PadGeometryList|): PAD geometry
        beam (|Beam|): Beam parameters
    """
    from ..viewers.qtviews.padviews import PADView
    fg = padstats_framegetter(stats)
    pv = PADView(
        frame_getter=fg, pad_geometry=pad_geometry, beam=beam, main=main, debug=debug
    )
    if histogram:
        histdat = stats["histogram"]
        bin_range = (
            stats["histogram_params"]["bin_min"],
            stats["histogram_params"]["bin_max"],
        )
        if show_corrected_histogram:
            pixhist = PixelHistogram(**stats["histogram_params"])
            pixhist.histogram = histdat
            histdat, x = pixhist.correct_gain_and_offset(histogram=pixhist.histogram)
            bin_range = (x[0], x[-1])
        pv.histogram_widget(histdat=histdat, bin_range=bin_range)
    if start:
        pv.start()
    return pv


#
#
# def view_histogram(stats, start=True, main=True, show_corrected_histogram=False):
#     r"""View the output of padstats with histogram enabled."""
#     pv = PADView(frame_getter=padstats_framegetter(stats), main=main)
#     histdat = stats["histogram"]
#     bin_range = (
#         stats["histogram_params"]["bin_min"],
#         stats["histogram_params"]["bin_max"],
#     )
#     if show_corrected_histogram:
#         pixhist = PixelHistogram(**stats["histogram_params"])
#         pixhist.histogram = histdat
#         histdat, x = pixhist.correct_gain_and_offset(histogram=pixhist.histogram)
#         bin_range = (x[0], x[-1])
#     pv.histogram_widget(histdat=histdat, bin_range=bin_range)
#     if start:
#         pv.start()
#
# geom = stats["pad_geometry"]
# mn = stats["histogram_params"]["bin_min"]
# mx = stats["histogram_params"]["bin_max"]
# nb = stats["histogram_params"]["n_bins"]
# x = np.linspace(mn, mx, nb)
# histdat = stats["histogram"]
# h = np.mean(stats["histogram"], axis=0)
# mean_histplot = pg.plot(x, np.log10(h + 1))
# mean_histplot.setLabel("bottom", "Intensity Value")
# mean_histplot.setLabel("left", "Counts")
# mean_histplot.setTitle("Total Histogram (All Pixels)")
# histplot = pg.plot(x, np.log10(h + 1))
# histplot.setLabel("bottom", "Intensity Value")
# histplot.setLabel("left", "Counts")
# histplot.setTitle("Histogram (One Pixel)")
# if show_corrected_histogram:
#     pixhist = PixelHistogram(**stats["histogram_params"])
#     pixhist.histogram = stats["histogram"]
#     histdat, x = pixhist.correct_gain_and_offset(histogram=pixhist.histogram)
#     # x = np.linspace(mn, mx, nb)
# imv = imview(
#     np.log10(histdat + 1),
#     fs_lims=[mn, mx],
#     fs_label="Intensity",
#     ss_label="Flat Pixel Index",
#     title="Intensity Histogram",
# )
# line = imv.add_line(vertical=True, movable=True)
#
# def update_histplot(line):
#     i = int(np.round(line.value()))
#     i = max(i, 0)
#     i = min(i, histdat.shape[0] - 1)
#     histplot.plot(x, np.log10(histdat[i, :] + 1), clear=True)
#
# flat_indices = np.arange(0, geom.n_pixels)
# flat_indices = stats["pad_geometry"].split_data(flat_indices)
# fg = padstats_framegetter(stats)
# pv = PADView(frame_getter=fg, main=main)
#
# def set_line_index(evt):
#     if evt is None:
#         print("Event is None")
#         return
#     ss, fs, pid = pv.get_pad_coords_from_mouse_pos()
#     if pid is None:
#         pass
#     else:
#         fs = int(np.round(fs))
#         ss = int(np.round(ss))
#         line.setValue(flat_indices[pid][ss, fs])
#     pass
#
# line.sigPositionChanged.connect(update_histplot)
# pv.proxy2 = pg.SignalProxy(
#     pv.viewbox.scene().sigMouseMoved, rateLimit=30, slot=set_line_index
# )
# pv.show()
# if start:
#     pv.start()
# return pv


def get_gain_and_offset(stats, n_processes=1, _peaks=None, debug=1):
    r"""Analyze histogram and attempt to extract offsets and gains from the zero- and one-photon peak.  Experimental.
    Use at your own risk!"""

    def dbgmsg(*args, **kwargs):
        if debug:
            print(*args, **kwargs)

    if _peaks is None:
        hist = stats["histogram"]
        pixel_histogram = PixelHistogram(**stats["histogram_params"])
        pixel_histogram.histogram = hist
        ap0, ap1, _ = pixel_histogram.get_average_peak_locations()
        # print(ap0,ap1, "average peak locations before get peaks fast")
        _peaks = (ap0, ap1)
    if n_processes > 1:
        if Parallel is None:
            raise ImportError("You need the joblib package to run in parallel mode.")
        stats_split = [
            dict(histogram=h, histogram_params=stats["histogram_params"])
            for h in np.array_split(stats["histogram"], n_processes, axis=0)
        ]
        # for s in stats_split:
        #     s["histogram_params"] = stats["histogram_params"]
        out = Parallel(n_jobs=n_processes)(
            [
                delayed(get_gain_and_offset)(s, _peaks=_peaks, debug=debug)
                for s in stats_split
            ]
        )
        return dict(
            gain=np.concatenate([out[i]["gain"] for i in range(n_processes)]),
            offset=np.concatenate([out[i]["offset"] for i in range(n_processes)]),
            coef0=np.concatenate([out[i]["coef0"] for i in range(n_processes)]),
            coef1=np.concatenate([out[i]["coef1"] for i in range(n_processes)]),
            mask=np.concatenate([out[i]["mask"] for i in range(n_processes)]),
        )
    hist = stats["histogram"]
    pixel_histogram = PixelHistogram(**stats["histogram_params"])
    pixel_histogram.histogram = hist
    x = pixel_histogram.get_bin_centers()
    n_pixels = hist.shape[0]
    n_bins = hist.shape[1]
    ap0, ap1 = _peaks
    (
        p0,
        p1,
        i0,
        i1,
    ) = pixel_histogram.get_peaks_fast()  # Gets estimate of zero and one-photon peaks
    poly = np.polynomial.Polynomial
    peak0 = np.empty(n_pixels) * np.nan
    peak1 = np.empty(n_pixels) * np.nan
    order = 2
    mask = np.ones(
        [
            n_pixels,
        ]
    )
    coef0 = (
        np.zeros([n_pixels, order + 1]) * np.nan
    )  # Initialize zeroth photon peak fit-coefficients
    coef1 = (
        np.zeros([n_pixels, order + 1]) * np.nan
    )  # Initialize first photon peak fit-coefficients
    has_peak_1 = not np.isnan(ap1)
    if not np.isnan(ap0):
        for i in range(n_pixels):
            dbgmsg(f"{i} of {n_pixels} ({i*100/n_pixels:0.2f}% done)")
            h = hist[i, :]  # Pulls pixel histogram
            if np.sum(h) == 0:
                continue
            if has_peak_1:
                # Gets the width of the peak in terms of histbins for fitting polynomia
                # width was found to be large hence multiplied by 0.75
                w = int(np.ceil(i1[i] - i0[i]) * 0.75)

                if (
                    w <= order + 1
                ):  # if no of bins to be fit (width) is less than coefficients to be found out
                    w += 2
            else:
                w = order * 2 + 1
            p = i0[i]
            # print(p, "rough estimate zero-photon peakindices")

            # Finding the lower and upper limits to the surrounding bins to the zeroth-photon peak
            a = max(0, p - int(w / 2))
            b = min(n_bins - 1, p + int(w / 2))

            # Slicing the histogram peak
            yd = h[a : b + 1]
            xd = x[a : b + 1]

            if yd.size <= order:
                print(yd.size)
                continue
            fit, extra = poly.fit(xd, yd, order, full=True)
            coef = (
                fit.convert().coef
            )  # Coefficients of unscaled and unshifted basis polynomials
            if coef is None:
                continue

            coef0[i, :] = coef
            try:
                peak0[i] = fit.deriv(1).roots()[
                    0
                ]  # exact zeroth-photon  peak location by first order derivative of the fit
            except:
                # print(fit.deriv(1),  "derivative")
                # print(fit.deriv().roots(),  "roots of derivative")
                # print(coef, "coefficient")
                # print(p, "the zeroth photon peak given by get_peaks fast")
                # print(a, b, "the surroundings of the peak used for fitting")
                # plt.plot(h)
                # plt.title(r"{i}th pixel histogram")
                # plt.plot(xd, yd)
                # plt.plot(xd, coef[0] +coef[1]*xd +coef[2]*xd**2)
                # plt.show()
                mask[i] = 0

            if not has_peak_1:
                continue

            # Finding the lower and upper limits to the surrounding bins to the one-photon peak
            p = i1[i]
            # print(p, "rough estimate one-photon peak")

            a = max(0, p - int(w / 2))
            b = min(n_bins - 1, p + int(w / 2))

            # Slicing the histogram peak
            yd = h[a : b + 1]
            xd = x[a : b + 1]

            fit, extra = poly.fit(xd, yd, order, full=True)
            coef = fit.convert().coef
            if coef is None:
                continue
            coef1[i, :] = coef
            try:
                peak1[i] = fit.deriv(1).roots()[
                    0
                ]  # exact one-photon peak location by first order derivative of the fit
            except:
                # print(fit.deriv(1),  "derivative")
                # print(fit.deriv().roots(),  "roots of derivative")
                # plt.plot(h)
                # plt.show()
                pass

    offset = peak0
    gain = peak1 - peak0
    return dict(gain=gain, offset=offset, order=2, coef0=coef0, coef1=coef1, mask=mask)


# def find_two_peaks(h, ap0, ap1):
