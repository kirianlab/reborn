# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from .. import fortran


def q_bin_indices(n_q_bins, q_bin_size, q_min, qs):
    r""" Polar q bin indices.  Given an arbitrary array of q magnitudes along with the binning specification
    (the *center* of the first bin, the bin size and the number of bins), output an array of bin indices.  The
    indices correspond to Python indices, with the first index equal to zero.

    Arguments:
        n_q_bins (int): number of q bins
        q_bin_size (float): q bin size
        q_min (float): minimum q
        qs (|ndarray|): q coordinates

    Returns:
        q_index (|ndarray|): q polar mapping index (value of -1 means out of bounds)
    """
    q_index = np.floor((qs - q_min) / q_bin_size)
    q_index[q_index < 0] = -1
    q_index[q_index > n_q_bins] = -1
    return q_index.astype(int)


def p_bin_indices(n_p_bins, p_bin_size, p_min, ps):
    r""" Polar azimuthal ("phi") bin indices.  Given an arbitrary array of phis along with the binning specification
    (the *center* of the first bin, the bin size and the number of bins), output an array of bin indices.  The
    indices correspond to Python indices, with the first index equal to zero.

    Arguments:
        n_p_bins (int): number of phi bins
        p_bin_size (float): phi bin size
        p_min (float): minimum phi
        ps (|ndarray|): phi coordinates

    Returns:
        p_index (|ndarray|): phi polar mapping index (value of -1 means out of bounds)
    """
    tp = 2 * np.pi
    # https://gcc.gnu.org/onlinedocs/gfortran/MODULO.html#MODULO
    # MODULO(A, P) = A - FLOOR(A / P) * P
    # ps = phis % 2 * np.pi
    p = ps - np.floor(ps / tp) * tp  # modulo as defined in fortran
    p_index = np.floor((p - p_min) / p_bin_size)
    p_index[p_index < 0] = -1
    p_index[p_index > n_p_bins] = -1
    return p_index.astype(int)


def stats(data, q, p, weights=None,
          n_q_bins=100, q_min=0, q_max=3e10,
          n_p_bins=360, p_min=0, p_max=3.141592653589793,
          sum_=None, sum2=None, w_sum=None):
    r"""
    Gather polar-binned stats.  Namely, the weighted sum, weighted sum of squares, and
    the sum of weights.

    Args:
        data (|ndarray|):
        q:
        p:
        weights:
        n_q_bins:
        q_min:
        q_max:
        n_p_bins:
        p_min:
        p_max:
        sum_:
        sum2:
        w_sum:

    Returns:

    """
    # Check all datatypes
    n_q_bins = int(n_q_bins)
    n_p_bins = int(n_p_bins)
    q_min = float(q_min)
    q_max = float(q_max)
    p_min = float(p_min)
    p_max = float(p_max)
    if data.dtype != np.float64:
        raise ValueError('Data type must be np.float64')
    if weights is None:
        weights = np.ones_like(data)
    if weights.dtype != np.float64:
        raise ValueError('Data type must be np.float64')
    if q.dtype != np.float64:
        raise ValueError('Data type must be np.float64')
    if p.dtype != np.float64:
        raise ValueError('Data type must be np.float64')
    shape = (n_q_bins, n_p_bins)
    if sum_ is None:
        sum_ = np.zeros(shape, dtype=np.float64)
    if sum2 is None:
        sum2 = np.zeros(shape, dtype=np.float64)
    if w_sum is None:
        w_sum = np.zeros(shape, dtype=np.float64)
    if sum_.dtype != np.float64:
        raise ValueError('Data type must be np.float64')
    if sum2.dtype != np.float64:
        raise ValueError('Data type must be np.float64')
    if w_sum.dtype != np.float64:
        raise ValueError('Data type must be np.float64')
    data = data.ravel()
    q = q.ravel()
    p = p.ravel()
    weights = weights.ravel()
    sum_ = sum_.reshape(n_q_bins, n_p_bins)
    sum2 = sum2.reshape(n_q_bins, n_p_bins)
    w_sum = w_sum.reshape(n_q_bins, n_p_bins)
    fortran.polar_f.polar_binning.stats(data.T, q.T, p.T, weights.T,
                                        n_q_bins, q_min, q_max,
                                        n_p_bins, p_min, p_max,
                                        sum_.T, sum2.T, w_sum.T, 1)
    meen = np.empty(shape, dtype=np.float64)
    std = np.empty(shape, dtype=np.float64)
    fortran.polar_f.polar_binning.stats_mean(sum_.T, sum2.T, w_sum.T, meen.T, std.T)
    out = dict(mean=meen, sdev=std, sum=sum_, sum2=sum2, weight_sum=w_sum)
    return out


class PolarPADAssembler:
    r"""A class for converting PAD data to polar coordinates."""

    def __init__(
        self,
        pad_geometry=None,
        beam=None,
        n_q_bins=50,
        q_range=None,
        n_phi_bins=None,
        phi_range=None,
    ):
        r"""
        Arguments:
            pad_geometry (|PADGeometryList|): PAD Geometry.
            beam (|Beam|): Beam information.
            n_q_bins (int): Number of q bins.
            q_range (tuple): Minimum and maximum q bin centers.  If None, the range is [0, maximum q in PAD].
            n_phi_bins (int): Number of phi bins.
            phi_range (tuple): Minimum and maximum phi bin centers.  If None, the full 2*pi ring is assumed.
        """
        qms = pad_geometry.q_mags(beam=beam)
        if q_range is None:
            q_range = [0, np.max(qms)]
        q_bin_size = (q_range[1] - q_range[0]) / float(n_q_bins - 1)
        q_centers = np.linspace(q_range[0], q_range[1], n_q_bins)
        q_edges = np.linspace(
            q_range[0] - q_bin_size / 2, q_range[1] + q_bin_size / 2, n_q_bins + 1
        )
        q_min = q_edges[0]
        if phi_range is None:  # Then we go from 0 to 2pi...
            phi_bin_size = 2 * np.pi / n_phi_bins
            phi_range = [phi_bin_size / 2, 2 * np.pi - phi_bin_size / 2]
        else:
            phi_bin_size = (phi_range[1] - phi_range[0]) / float(n_phi_bins - 1)
        phi_centers = np.linspace(phi_range[0], phi_range[1], n_phi_bins)
        phi_edges = np.linspace(
            phi_range[0] - phi_bin_size / 2,
            phi_range[1] + phi_bin_size / 2,
            n_phi_bins + 1,
        )
        phi_min = phi_edges[0]
        self.q_range = q_range
        self.phi_range = phi_range
        self.q_bin_size = q_bin_size
        self.q_bin_centers = q_centers
        self.q_bin_edges = q_edges
        self.n_q_bins = n_q_bins
        self.q_min = q_min
        self.phi_bin_size = phi_bin_size
        self.phi_bin_centers = phi_centers
        self.phi_bin_edges = phi_edges
        self.n_phi_bins = n_phi_bins
        self.phi_min = phi_min
        self.qms = qms
        self.phis = pad_geometry.azimuthal_angles(beam=beam)
        self.beam = beam
        self.pad_geometry = pad_geometry
        self.polar_shape = (n_q_bins, n_phi_bins)
        self.sa = pad_geometry.solid_angles()

    @classmethod
    def from_resolution(cls, pad_geometry, beam, sample_diameter, oversample=2):
        r"""
        Instantiate PolarPADAssembler the correct way, with resolution and oversample ratio.

        Arguments:
            pad_geometry (|PADGeometryList|): PAD Geometry.
            beam (|Beam|): Beam information.
            sample_diameter (float): Size of sample (L).
            oversample (int): Oversmaple ratio (s=2 is default).
        """
        qms = pad_geometry.q_mags(beam=beam)
        qm = qms.max()
        qn = qms.min()
        dq = 2 * np.pi / (sample_diameter * oversample)
        qs = np.arange(qn, qm, dq)
        theta = 2 * np.arcsin(qm * beam.wavelength / (4 * np.pi))
        qperp = qm * np.cos(theta / 2)  # component of q-vector perpendicular to beam
        dphi = dq / qperp  # phi step size
        n_q = int(qs.size)  # number of q bins for Shannon sampling
        n_p = int(2 * np.pi / dphi)  # number of phi bins for Shannon sampling
        n_p += n_p % 2  # make it even
        return cls(
            pad_geometry=pad_geometry,
            beam=beam,
            n_q_bins=n_q,
            q_range=(qn, qm),
            n_phi_bins=n_p,
            phi_range=(0, 2 * np.pi),
        )

    def stats(self, data, weights=None):
        return stats(
            data,
            self.q_bin_centers,
            self.phi_bin_centers,
            weights=weights,
            n_q_bins=self.n_q_bins,
            q_min=self.q_range[0],
            q_max=self.q_range[1],
            n_p_bins=self.n_phi_bins,
            p_min=self.phi_range[0],
            p_max=self.phi_range[1],
        )
