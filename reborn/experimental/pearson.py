import numpy as np
from numpy.fft import fftn, ifftn
from scipy.optimize import curve_fit
from .. import detector


def _compact_slice(array):
    r""" Return the smallest rectangular slice that contains all non-zero elements of array. """
    array1 = np.sum(array, axis=0)
    array0 = np.sum(array, axis=1)
    a0 = np.arange(array0.size)[array0 > 0]
    a1 = np.arange(array1.size)[array1 > 0]
    s = np.s_[np.min(a0):np.max(a0) + 1, np.min(a1):np.max(a1) + 1]
    return s


def _pad02(x):
    r""" Zero-pad such that the array is extended to 2x the original shape. """
    n, m = x.shape
    return np.pad(x, ((0, n),(0, m)))


def _masked_pearson_cc_slow(x, X, y, Y, max_shift=None, min_unmasked=9):
    r"""
    A clone of masked_pearson_cc_2d() that is **very** slow, but without
    FFTs.  It is for testing purposes.
    """
    nv, nw = x.shape
    P = np.zeros((nv * 2, nw * 2))
    x = _pad02(x)
    y = _pad02(y)
    X = _pad02(X)
    Y = _pad02(Y)
    x = np.roll(x, np.array(x.shape) // 4, (0, 1))
    y = np.roll(y, np.array(x.shape) // 4, (0, 1))
    X = np.roll(X, np.array(x.shape) // 4, (0, 1))
    Y = np.roll(Y, np.array(x.shape) // 4, (0, 1))
    for i in range(0, 2 * nv):
        for j in range(0, 2 * nw):
            yy = np.roll(y, (i - nv, j - nw), (0, 1))
            YY = np.roll(Y, (i - nv, j - nw), (0, 1))
            XYY = X * YY
            if np.sum(XYY) > min_unmasked:
                w = np.where(XYY > 0)
                xxw = x[w]
                xxw -= np.mean(xxw)
                yyw = yy[w]
                yyw -= np.mean(yyw)
                b = np.sqrt(np.sum(xxw ** 2) * np.sum(yyw ** 2))
                if b > 0:
                    P[i, j] = np.sum(xxw * yyw) / b
    if max_shift is not None:
        v = np.arange(nv * 2) - nv
        w = np.arange(nw * 2) - nw
        vv, ww = np.meshgrid(v, w, indexing="ij")
        r2 = vv ** 2 + ww ** 2
        m = np.ones((nv * 2, nw * 2))
        m[r2 > max_shift ** 2] = 0
        P *= m
    return P


def plan_masked_pearson_cc_2d(x_mask, y_mask, max_shift=None, min_unmasked=100):
    r""" Create a plan to speed up the masked_pearson_cc_2d function.  Determines the
    minimal slice of the image that is needed for the correlation.  Stores the sliced
    masks and FFTs of the sliced/padded masks to avoid re-calculation.

    Arguments:
        x (|ndarray|): First image
        x_mask (|ndarray|): First image mask
        y (|ndarray|): Second image
        y_mask (|ndarray|): Second image mask
        max_shift (float): Set the correlations to zero where the shift
               (distance, in pixel units) is larger than this number.  If
               the value is less than one, it is interpreted as a fraction
               of the radius of the smallest edge of the image.
        min_unmasked (int): Set the correlations to zero where the number
               of jointly unmasked pixels is less than this

    Returns:
        dict
    """
    shape = x_mask.shape
    a, b = x_mask.shape
    s = _compact_slice(x_mask + y_mask)
    X = _pad02(x_mask[s])
    Y = _pad02(y_mask[s])
    ftY = fftn(Y)
    ftX = fftn(X)
    XcY = np.real(ifftn(ftX * np.conj(ftY)))
    where_min_unmasked = (XcY <= min_unmasked)
    p_mask = None
    if max_shift is not None:
        if max_shift < 1:
            max_shift = min(a, b)*max_shift/2.0
        v = np.arange(a*2) - a
        w = np.arange(b*2) - b
        vv, ww = np.meshgrid(v, w, indexing="ij")
        r2 = vv**2 + ww**2
        m = np.ones((a*2, b*2))
        m[r2 > max_shift**2] = 0
        p_mask = m
    plan = dict()
    plan["original_shape"] = shape
    plan["slice"] = s
    plan["x_mask"] = x_mask
    plan["y_mask"] = y_mask
    plan["x_mask_padded"] = X
    plan["y_mask_padded"] = Y
    plan["x_mask_fft"] = ftX
    plan["y_mask_fft"] = ftY
    plan["x_mask_cc_y_mask"] = XcY
    plan["p_mask"] = p_mask
    plan["where_min_unmasked"] = where_min_unmasked
    return plan


def masked_pearson_cc_2d(x, x_mask, y, y_mask, max_shift=None, min_unmasked=9, plan=None):
    r""" Cross correlate two masked arrays.  Unlike an ordinary cross correlation,
    this cross correlation normalizes the result so that it spans from -1 to 1 as
    defined by the Pearson correlation formula.  Masked pixels are handled by excluding
    them from the cross correlations; i.e. for every shift in the cross correlation,
    only the pixels for which *both* pixels are *unmasked* will contribute to the
    cross correlation.  FFT operations are used for speed.  Options are provided to
    set correlations to zero wherever the number of jointly unmasked pixels is too low
    (poor statistics) or where the shift is too large (avoids FFT artifacts).

    The returned Pearson cross correlation is shifted such that the shifts of an
    NxM array are as follows:

    .. code-block:: python

        nv, nw = image.shape
        vshifts = np.arange(nv*2) - nv
        wshifts = np.arange(nw*2) - nw

    Arguments:
        x (|ndarray|): First image
        x_mask (|ndarray|): First image mask
        y (|ndarray|): Second image
        y_mask (|ndarray|): Second image mask
        max_shift (float): Set the correlations to zero where the shift
               (distance, in pixel units) is larger than this number.  If
               the value is less than one, it is interpreted as a fraction
               of the radius of the smallest edge of the image.
        min_unmasked (int): Set the correlations to zero where the number
               of jointly unmasked pixels is less than this

    Returns:
        (|ndarray|) -- The Pearson correlation
    """
    a, b = x.shape
    if plan is not None:
        X = plan["x_mask"]
        Y = plan["y_mask"]
        s = plan["slice"]
        ftX = plan["x_mask_fft"]
        ftY = plan["y_mask_fft"]
        XcY = plan["x_mask_cc_y_mask"]
        where_min_unmasked = plan["where_min_unmasked"]
        x *= X
        y *= Y
        x = x[s]
        y = y[s]
        x = _pad02(x)
        y = _pad02(y)
    else:
        X = x_mask
        Y = y_mask
        x *= X
        y *= Y
        x = _pad02(x)
        y = _pad02(y)
        X = _pad02(X)
        Y = _pad02(Y)
        ftY = fftn(Y)
        ftX = fftn(X)
        XcY = np.real(ifftn(ftX * np.conj(ftY)))
        where_min_unmasked = (XcY <= min_unmasked)
    nv, nw = x.shape
    ftx = fftn(x)
    fty = fftn(y)
    ftx2 = fftn(x ** 2)
    fty2 = fftn(y ** 2)
    xcy = np.real(ifftn(ftx*np.conj(fty)))
    xcY = np.real(ifftn(ftx*np.conj(ftY)))
    Xcy = np.real(ifftn(ftX*np.conj(fty)))
    x2cY = np.real(ifftn(ftx2*np.conj(ftY)))
    Xcy2 = np.real(ifftn(ftX*np.conj(fty2)))
    with np.errstate(divide='ignore', invalid='ignore'):
        A = xcy -  Xcy* xcY / XcY
        B = np.sqrt((x2cY-xcY**2/XcY)*(Xcy2-Xcy**2/XcY))
        P = np.real(A/B)
        P = np.nan_to_num(P)
    P[where_min_unmasked] = 0
    P = np.fft.fftshift(P)
    if plan is not None:
        c, d = P.shape
        n = int((2 * a - c) / 2)
        m = int((2 * b - d) / 2)
        P = np.pad(P, ((n, n), (m, m)))
        P *= plan["p_mask"]
        max_shift = None
    if max_shift is not None:
        if max_shift < 1:
            max_shift = min(nv, nw)*max_shift/2.0
        v = np.arange(a*2) - a
        w = np.arange(b*2) - b
        vv, ww = np.meshgrid(v, w, indexing="ij")
        r2 = vv**2 + ww**2
        m = np.ones((a*2, b*2))
        m[r2 > max_shift**2] = 0
        P *= m
    return P


def gaussian_2d(xy, amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
    r""" 2D Gaussian function. """
    x, y = xy
    xo = float(xo)
    yo = float(yo)
    a = (np.cos(theta) ** 2) / (2 * sigma_x ** 2) + (np.sin(theta) ** 2) / (2 * sigma_y ** 2)
    b = -(np.sin(2 * theta)) / (4 * sigma_x ** 2) + (np.sin(2 * theta)) / (4 * sigma_y ** 2)
    c = (np.sin(theta) ** 2) / (2 * sigma_x ** 2) + (np.cos(theta) ** 2) / (2 * sigma_y ** 2)
    g = offset + amplitude * np.exp(- (a * ((x - xo) ** 2)
                                       + 2 * b * (x - xo) * (y - yo)
                                       + c * ((y - yo) ** 2)))
    return g.ravel()


def fit_gaussian_2d(data):
    r""" Fit a 2D Gaussian to a data array. """
    nx, ny = data.shape
    x = np.linspace(-nx/2, nx/2, nx)
    y = np.linspace(-ny/2, ny/2, ny)
    x, y = np.meshgrid(x, y, indexing="ij")
    initial_guess = (0.5, 0, 0, 10, 10, 0, 0)
    try:
        popt, _ = curve_fit(gaussian_2d, (x, y), data.ravel(), p0=initial_guess)
    except:
        popt = None
    return popt


def pearson_friedel_shift(data, mask=None, max_shift=10, min_unmasked=100, return_extra=True):
    r""" Given a 2D array and a corresponding mask, take the masked Pearson cross correlatin between the
    array and an inverted copy of the array. """
    x = data.copy()
    if mask is None:
        mask = np.ones_like(data)
    X = mask.copy()
    y = x.copy()
    y = y[::-1, ::-1]
    Y = X.copy()
    Y = Y[::-1, ::-1]
    P = masked_pearson_cc_2d(x, X, y, Y, max_shift=0.5, min_unmasked=min_unmasked)
    nx, ny = P.shape
    nn = max_shift
    P = P[int(nx / 2 - nn):int(nx / 2 + nn), int(ny / 2 - nn):int(ny / 2 + nn)]
    nx, ny = P.shape
    fit = fit_gaussian_2d(P)
    if fit is None:
        return None, None, None, dict(pearson_cc=P)
    c, x0, y0 = fit[:3]
    extra = None
    if return_extra:
        x = np.linspace(-nx / 2, nx / 2, nx)
        y = np.linspace(-ny / 2, ny / 2, ny)
        xy = np.meshgrid(x, y, indexing="ij")
        gfit = gaussian_2d(xy, *fit).reshape((nx, ny))
        extra = dict(fit_parameters=fit, pearson_cc=P, gaussian_fit=gfit)
    return x0/2, y0/2, c, extra