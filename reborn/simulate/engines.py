# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import os
import numpy as np
from scipy.spatial.transform import Rotation
try:
    from .clcore import ClCore
except:
    ClCore = None
from ..source import Beam
from ..dataframe import DataFrame
from ..detector import PADGeometryList
from ..simulate.solutions import water_scattering_factor_squared, water_number_density
from ..fileio.getters import FrameGetter
from ..target.crystal import CrystalStructure, CrystalDensityMap, get_pdb_file, temp_dir
from ..utils import (
    rotation_about_axis,
    random_unit_vector,
    random_beam_vector,
    max_pair_distance,
    ensure_list,
)

try:
    from ..external import denss
except:
    denss = None


class MoleculePatternSimulator(object):
    def __init__(
        self,
        beam=None,
        molecule=None,
        pad=None,
        oversample=10,
        max_mesh_size=200,
        clcore=None,
    ):
        if clcore is None:
            clcore = ClCore(group_size=32)
        self.clcore = clcore
        self.pad = pad
        self.molecule = molecule
        self.beam = beam
        self.oversample = oversample
        self.q_vecs = pad.q_vecs(beam=beam)
        self.f = molecule.get_scattering_factors(beam=beam)
        self.intensity_prefactor = pad.f2phot(beam=beam)
        self.resolution = pad.max_resolution(beam=beam)
        self.mol_size = max_pair_distance(molecule.coordinates)
        self.qmax = 2 * np.pi / self.resolution
        self.mesh_size = int(np.ceil(10 * self.mol_size / self.resolution))
        self.mesh_size = int(min(self.mesh_size, max_mesh_size))
        self.a_map_dev = clcore.to_device(
            shape=(self.mesh_size**3,), dtype=clcore.complex_t
        )
        self.q_dev = clcore.to_device(self.q_vecs, dtype=clcore.real_t)
        self.a_out_dev = clcore.to_device(
            dtype=clcore.complex_t, shape=self.q_vecs.shape[0]
        )
        clcore.phase_factor_mesh(
            self.molecule.coordinates,
            self.f,
            N=self.mesh_size,
            q_min=-self.qmax,
            q_max=self.qmax,
            a=self.a_map_dev,
        )

    def generate_pattern(self, rotation=None, poisson=False):
        if rotation is None:
            rotation = Rotation.random().as_matrix()
        self.clcore.mesh_interpolation(
            self.a_map_dev,
            self.q_dev,
            N=self.mesh_size,
            q_min=-self.qmax,
            q_max=self.qmax,
            R=rotation,
            a=self.a_out_dev,
        )
        if poisson:
            intensity = np.random.poisson(
                self.intensity_prefactor * np.abs(self.a_out_dev.get()) ** 2
            )
        else:
            intensity = self.intensity_prefactor * np.abs(self.a_out_dev.get()) ** 2
        return intensity


class SAXSPatternSimulator:

    _dataframe = None

    def __init__(
        self,
        pdb_file_path: str,
        beam: Beam,
        pad_geometry: PADGeometryList,
        concentration: float,
        thickness: float,
    ):
        r"""
        Arguments:
            pdb_file_path: Path to a PDB file.
            beam: Beam information.
            pad_geometry: Geometry information.
            concentration: Protein concentration in kg/m^3.
            thickness: Sample thickness in m.
        """
        self.initialize(pdb_file_path, beam, pad_geometry, concentration, thickness)

    def initialize(
        self,
        pdb_file_path: str,
        beam: Beam,
        pad_geometry: PADGeometryList,
        concentration: float,
        thickness: float,
    ):
        if denss is None:
            raise ModuleNotFoundError(
                "You need to install DENSS: https://github.com/tdgrant1/denss.git"
            )
        self._dataframe = None
        create_bio_assembly = False
        molecule = CrystalStructure(
            pdb_file_path=pdb_file_path, create_bio_assembly=create_bio_assembly
        ).molecule
        molecular_weight = molecule.get_molecular_weight()
        q_mags = pad_geometry.q_mags(beam)
        illuminated_volume = np.pi * (beam.diameter_fwhm / 2) ** 2 * thickness
        protein_number_density = concentration / molecular_weight
        n_proteins = protein_number_density * illuminated_volume
        if not os.path.exists(pdb_file_path):
            pdb_file_path = get_pdb_file(pdb_file_path)
        _, F2_protein = denss.get_scattering_profile(
            pdb_file_path, create_bio_assembly=create_bio_assembly, q_mags=q_mags
        )
        F2_protein *= n_proteins
        F2_water = water_scattering_factor_squared(
            q_mags, volume=illuminated_volume
        )
        self.pad_geometry = pad_geometry
        self.beam = beam
        self.F2_protein = F2_protein
        self.F2_water = F2_water
        self.f2phot = pad_geometry.f2phot(beam)

    def generate_pattern(self, poisson_noise: bool=True):
        pat = (self.F2_protein + self.F2_water) * self.f2phot
        return np.random.poisson(pat)

    def generate_dataframe(self):
        if self._dataframe is None:
            self._dataframe = DataFrame()
            self._dataframe.set_beam(self.beam)
            self._dataframe.set_pad_geometry(self.pad_geometry)
        self._dataframe.set_raw_data(self.generate_pattern())


class SAXSPatternFrameGetter(FrameGetter):

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.simulator = SAXSPatternSimulator(*args, **kwargs)
        self.n_frames = 1e6

    def get_data(self, frame_number=0):
        np.random.seed(frame_number)
        return self.simulator.generate_dataframe()
