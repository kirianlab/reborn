# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from .. import utils, const, detector, source
from ..target import crystal, atoms
from ..target.molecule import Molecule, get_molecule
from ..fortran import scatter_f


def isotropic_gas_intensity_profile(
    r_vecs=None,
    q_mags=None,
    atomic_numbers=None,
    photon_energy=None,
    molecule=None,
    beam=None,
):
    r"""Calculate the isotropic scatter from *one* gas molecule using the Debye formula.

    Arguments:
        r_vecs (|ndarray|): Position vectors.
        q_mags (|ndarray|): Q vectors.
        atomic_numbers (|ndarray|): Atomic numbers.
        photon_energy (float): Photon energy.
        molecule (reborn.target.molecule.Molecule or str): Molecule (overrides r_vecs and atomic_numbers)
        beam (|Beam|): Beam instance.  Overrides photon_energy.

    Returns:
        |ndarray|: Intensity profile I(q)
    """

    if molecule is not None:
        molecule = get_molecule(molecule)
        r_vecs = molecule.coordinates
        atomic_numbers = molecule.atomic_numbers
    if r_vecs is None or atomic_numbers is None:
        raise ValueError("Specify either the molecule or the list of atomic numbers with coordinates")
    if beam is not None:
        photon_energy = beam.photon_energy
    if photon_energy is None:
        raise ValueError("Specify either the Beam or the photon energy")
    atomic_numbers = utils.atleast_1d(np.array(atomic_numbers))
    if atomic_numbers.size == 1:
        f = atoms.cmann_henke_scattering_factors(
            q_mags=q_mags, atomic_number=atomic_numbers[0], photon_energy=photon_energy
        )
        return np.abs(f) ** 2
    r_vecs = utils.atleast_2d(
        r_vecs
    )  # Make sure it works with a single atom, just to keep things general
    q_mags = np.float64(q_mags)
    uz = np.sort(np.unique(atomic_numbers))
    f_idx = np.sum(np.greater.outer(atomic_numbers, uz), 1).astype(
        int
    )  # Map atom type to scattering factor
    ff = np.zeros(
        (uz.size, q_mags.size), dtype=np.complex128
    )  # Scatter factor array.  One row for each unique atom type.
    for i in range(uz.size):
        ff[i, :] = atoms.cmann_henke_scattering_factors(
            q_mags=q_mags, atomic_number=uz[i], photon_energy=photon_energy
        )
    intensity = np.zeros(q_mags.size, dtype=np.float64)
    r_vecs = np.ascontiguousarray(r_vecs).astype(np.float64)
    scatter_f.debye(r_vecs.T, q_mags, f_idx, ff.T, intensity)
    return np.real(intensity)


# def air_intensity_profile(q_mags=None, beam=None):
#     r"""Calls `:func:isotropic_gas_intensity_profile and sums the contributions from O2 and N2."""
#     n2_profile = isotropic_gas_intensity_profile(
#         molecule="N2", beam=beam, q_mags=q_mags
#     )
#     o2_profile = isotropic_gas_intensity_profile(
#         molecule="O2", beam=beam, q_mags=q_mags
#     )
#     return n2_profile * 0.79 + o2_profile * 0.21
#
#
# def humid_he_intensity_profile(
#     q_mags=None, beam=None, he_percent=0.8, water_percent=0.2
# ):
#     r"""Calls isotropic_gas_intensity_profile and sums the contributions from O2 and N2. Read the docs for"""
#     if np.abs((he_percent + water_percent) - 1) > 1e-6:
#         raise ValueError("The fraction of helium and water must sum to one.")
#     he_profile = 0
#     water_profile = 0
#     if he_percent > 0:
#         he_profile = isotropic_gas_intensity_profile(
#             molecule="He", beam=beam, q_mags=q_mags
#         )
#     if water_percent > 0:
#         water_profile = isotropic_gas_intensity_profile(
#             molecule="H2O", beam=beam, q_mags=q_mags
#         )
#     return he_profile * he_percent + water_profile * water_percent
#
#
# def humid_air_intensity_profile(
#     q_mags=None, beam=None, air_percent=0.8, water_percent=0.2
# ):
#     r"""Calls isotropic_gas_intensity_profile and sums the contributions from O2 and N2. Read the docs for"""
#     if np.abs((air_percent + water_percent) - 1) > 1e-6:
#         raise ValueError("The fraction of air and water must sum to one.")
#     air_profile = 0
#     water_profile = 0
#     if air_percent > 0:
#         air_profile = air_intensity_profile(q_mags=q_mags, beam=beam)
#     if water_percent > 0:
#         water_profile = isotropic_gas_intensity_profile(
#             molecule="H2O", beam=beam, q_mags=q_mags
#         )
#     return air_profile * air_percent + water_profile * water_percent
#
#
# def helium_and_air_intensity_profile(
#     q_mags=None, beam=None, air_percent=0.2, he_percent=0.8
# ):
#     r"""Calls isotropic_gas_intensity_profile and sums the contributions from O2 and N2. Read the docs for"""
#     if np.abs((he_percent + he_percent) - 1) > 1e-6:
#         raise ValueError("The fraction of helium and air must sum to one.")
#     air_profile = 0
#     he_profile = 0
#     if air_percent > 0:
#         air_profile = air_intensity_profile(q_mags=q_mags, beam=beam)
#     if he_percent > 0:
#         he_profile = isotropic_gas_intensity_profile(
#             molecule="he", beam=beam, q_mags=q_mags
#         )
#     return air_profile * air_percent + he_profile * he_percent


def get_gas_background(
    pad_geometry: detector.PADGeometryList,
    beam: source.Beam,
    path_length=[0.0, 1.0],
    gas_type=None,
    temperature=293.15,
    pressure=101325.0,
    n_simulation_steps=20,
    poisson=False,
    verbose=False,
    as_list=False,
    __gamma=10,
    __polarization_off=False,
    __fixed_f2=None,
):
    r"""
    Given a |PADGeometryList| along with a |Beam|, calculate the scattering intensity
    :math:`I(q)` of chamber gas.

    Args:
        pad_geometry (|PADGeometryList|): PAD geometry info.
        beam (|Beam|): X-ray beam parameters.
        path_length (list of float): Start and end points corresponding to the gas that the beam passes through
        gas_type (str, list): String indicating the gas type, or list of strings indicating types
        temperature (float): Temperature of the gas.
        pressure (float, list): Pressure of the gas, or list of partial pressures
        poisson (bool): If True, add Poisson noise (default=True)

    Returns:
        List of |ndarray| instances containing intensities.
    """
    # gas_type = gas_type.lower()
    # gas_options = ["he", "n2", "air", "o2", "humid_he", "humid_air", "he_air"]
    if gas_type is None and __fixed_f2 is None:
        raise ValueError(
            f"Specify the type(s) of gas."
        )
    if not isinstance(gas_type, (list, tuple, np.ndarray)):
        gas_type = [gas_type]
    if not isinstance(pressure, (list, tuple, np.ndarray)):
        pressure = [pressure]
    if len(gas_type) != len(pressure):
        raise ValueError(f"Number of gas types must equal number of partial pressures")
    if gas_type[0] is not None:
        molecules = [get_molecule(m) for m in gas_type]
    geom0 = detector.PADGeometryList(pad_geometry).copy()
    ddist = np.max(geom0.position_vecs().dot(beam.beam_vec))
    a, b = path_length
    n = n_simulation_steps
    if b is None or b > ddist:
        b = ddist
    if __gamma is not None:
        # Progressively increase the sampling frequency as we get closer to the detector:
        g = __gamma  # Tuning factor - larger numbers mean finer sampling near the detector
        x = np.linspace(0, 1, n + 1)
        x = (1 - np.exp(-g * x)) / (1 - np.exp(-g))
        dx = x[1:] - x[:-1]
        x = x[:-1] + dx / 2
        offsets = a + x * (b - a)
        dx = dx * (b - a)
    else:  # Testing purposes only:
        # Uniform sapling
        offsets = np.linspace(a, b, n + 1)
        offsets = (offsets[1:] + offsets[:-1]) / 2
        dx = np.ones(n) * (b - a) / n
    # =================================================================
    # Some tests for checking the non-uniform sampling scheme
    # sdx = np.sum(dx)
    # ba = b-a
    # err = np.abs(sdx-ba) / ba
    # print(sdx, ba, err)
    # # assert err < 1e-6
    # import matplotlib.pyplot as plt
    # plt.plot(np.arange(n), offsets, '.', label="midpoint")
    # plt.plot(np.arange(n)+0.5, np.cumsum(dx), '.', label="cumulative widths")
    # plt.xlabel("Sample index")
    # plt.ylabel("Sample position")
    # plt.legend()
    # plt.show()
    # ==================================================================
    total = geom0.zeros()
    polarization = 1
    for i in range(n):
        if verbose:
            print(offsets[i])
        volume = np.pi * dx[i] * (beam.diameter_fwhm / 2) ** 2  # volume of a cylinder
        geom = geom0.copy().translate(-beam.beam_vec * offsets[i])
        geom.clear_cache()
        # print(f"step {i}, distance", geom.average_detector_distance(beam), ",", n_molecules, "molecules")
        mask = (geom.scattering_angles(beam) < np.pi / 2).astype(float)
        if not __polarization_off:  # For testing purposes only
            polarization = geom.polarization_factors(beam)
        solid_angles = geom.solid_angles2()  # Exact solid angles
        q_mags = geom.q_mags(beam)
        if __fixed_f2 is not None:  # For testing purpose only
            scatt = __fixed_f2
            scatt *= pressure[0] * volume / (const.k * temperature)
        else:
            scatt = 0
            for m, p in zip(molecules, pressure):
                scatt += isotropic_gas_intensity_profile(molecule=m, q_mags=q_mags, beam=beam)*p * volume / (const.k * temperature)
        total += solid_angles * polarization * scatt * mask
    total *= const.r_e**2 * beam.photon_number_fluence
    if poisson:
        total = np.random.poisson(total).astype(
            np.double
        )  # add in some Poisson noise for funsies
    if as_list:
        total = geom.concat_data(total)
    return total
