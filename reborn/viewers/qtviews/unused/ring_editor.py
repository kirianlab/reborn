# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import sys
import numpy as np
import pyqtgraph as pg
from pyqtgraph import Qt
from pyqtgraph.Qt import QtCore, QtGui, QtWidgets
from pyqtgraph.Qt.QtWidgets import (
    QApplication,
    QWidget,
    QVBoxLayout,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QComboBox,
    QColorDialog,
    QLineEdit,
    QHeaderView,
    QCheckBox,
    QHBoxLayout,
    QTableView,
    QSizePolicy,
    QStyledItemDelegate
)
from reborn.gui.dialogs.pen_editor import PenStylePreview, PenStyleDialog
from reborn import utils

class QPenDelegate(QtWidgets.QAbstractItemDelegate):

    def createEditor(self, parent, option, index):
        print('createEditor')
        editor = PenStyleDialog()
        return editor

    def setEditorData(self, editor, index):
        print('setEditorData')
        value = index.data(QtCore.Qt.EditRole)
        if isinstance(value, QtGui.QPen):
            editor.set_pen(value)

    def setModelData(self, editor, model, index):
        print('setModelData')
        pen = editor.get_pen()
        model.setData(index, pen, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        print('updateEditorGeometry')
        editor.setGeometry(option.rect)

    def paint(self, painter, option, index):
        print('paint')
        value = index.data(QtCore.Qt.EditRole)
        # display = index.data(QtCore.Qt.DisplayRole)
        # display.set_pen(value)


class MyTableModel(QtCore.QAbstractTableModel):
    def __init__(self, data=[], header=None):

        super().__init__()
        self._data = data
        self._headers = header
        self._n_columns = 3

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._data)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self._data[0]) if self._data else 0

    def data(self, index, role):
        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            return self._data[index.row()][index.column()]
        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        if role == QtCore.Qt.EditRole:
            if index.column() == 1:
                # Ensure floating-point numbers in the second column
                try:
                    float_value = float(value)
                    self._data[index.row()][index.column()] = float_value
                    print(f"Value updated: {float_value}")
                except ValueError:
                    print("Invalid float value")
            else:
                self._data[index.row()][index.column()] = value
            self.dataChanged.emit(index, index)
            return True
        return False

    def flags(self, index):
        return QtCore.Qt.ItemIsEditable | super().flags(index)

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal:
            return self._headers[section]

    def insertRow(self, position, rows=1, index=QtCore.QModelIndex()):
        self.beginInsertRows(QtCore.QModelIndex(), position, position + rows - 1)
        for i in range(rows):
            default_data = ['' for _ in range(self.columnCount(QtCore.QModelIndex()))]
            self._data.insert(position, default_data)
        self.endInsertRows()

    def removeRow(self, position, rows=1, index=QtCore.QModelIndex()):
        self.beginRemoveRows(QtCore.QModelIndex(), position, position + rows - 1)
        del self._data[position:position + rows]
        self.endRemoveRows()


class Widget(QWidget):

    def __init__(self, padview):
        r""" Ring editor widget for PADView. """
        super().__init__()
        self.ring_type_labels = ['q=4πsin(θ/2)/λ [A]', 'd=2π/q [A]', 'r [mm @ 1m]', 'θ [degrees]']
        self.table_header = ['Type', 'Value', 'Style']
        self.padview = padview
        self.default_pen = pg.mkPen('r')
        self.table = QTableView()
        # self.setCentralWidget(self.table)

        data = [
            [1, 1.5, QtGui.QPen(QtCore.Qt.red)],
            [2, 2.7, QtGui.QPen(QtCore.Qt.blue)],
            [3, 3.0, QtGui.QPen(QtCore.Qt.green)]
        ]

        headers = ["ID", "Value", "Pen"]

        self.model = MyTableModel(data, headers)
        self.table.setModel(self.model)

        add_button = QPushButton("Add Ring")
        add_button.clicked.connect(self.add_row)

        remove_button = QPushButton("Remove Selected Ring(s)")
        remove_button.clicked.connect(self.remove_selected_rows)

        self.table.setItemDelegateForColumn(2, QPenDelegate())  # Set delegate for the third column

        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)  # Stretch the horizontal header
        self.table.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        layout = QVBoxLayout()
        layout.addWidget(self.table)
        layout.addWidget(add_button)
        layout.addWidget(remove_button)
        self.setLayout(layout)
        # container = QWidget()
        # container.setLayout(layout)
        # self.setCentralWidget(container)

    def add_row(self):
        row_position = self.model.rowCount(QtCore.QModelIndex())
        self.model.insertRow(row_position)

    def remove_selected_rows(self):
        selected_cells = self.table.selectionModel().selectedIndexes()
        unique_rows = set(index.row() for index in selected_cells)
        for row in sorted(unique_rows, reverse=True):
            self.model.removeRow(row)

    # FIXME: This function is far too complicated for the simple task it does...
    def add_rings(
            self,
            radii=None,
            angles=None,
            q_mags=None,
            d_spacings=None,
            pens=None,
            repeat=False,
    ):
        r"""Plot rings.  Note that these are in a plane located 1 meter from the sample position; calculate the radius
        needed for an equivalent detector at that distance.  If you know the scattering angle, the radius is
        tan(theta).  The repeat keyword will include all rings for a given d-spacing."""
        self.debug("add_rings")
        pens = utils.ensure_list(pens)
        if len(pens) < 1:
            pens = [None]
        # We allow various input types... so we must now ensure they are either list or None.
        input = []
        for d in [radii, angles, q_mags, d_spacings]:
            if isinstance(d, np.ndarray):
                d = [i for i in d]
            if not d:
                input.append(None)
                continue
            d = utils.ensure_list(d)
            input.append(d)
        radii, angles, q_mags, d_spacings = input
        if radii is not None:
            pens *= int(len(radii) / len(pens))
            # self.debug('add_rings:radii', radii, ', pens', pens)
            for r, p in zip(radii, pens):
                self.add_ring(radius=r, pen=p)
            return True
        if angles is not None:
            pens *= int(len(angles) / len(pens))
            # self.debug('add_rings:angles', angles, ', pens', pens)
            for r, p in zip(angles, pens):
                self.add_ring(angle=r, pen=p)
            return True
        if q_mags is not None:
            pens *= int(len(q_mags) / len(pens))
            # self.debug('add_rings:q_mags', q_mags, ', pens', pens)
            for r, p in zip(q_mags, pens):
                self.add_ring(q_mag=r, pen=p)
            return True
        if d_spacings is not None:
            pens *= int(len(d_spacings) / len(pens))
            # self.debug('add_rings:d_spacings', d_spacings, ', pens', pens)
            if repeat is True:
                d_spacings = [d_spacings[0] / i for i in range(1, 21)]
            for r, p in zip(d_spacings, pens):
                self.add_ring(d_spacing=r, pen=p)
            return True
        return False

    def add_ring(self, radius=None, angle=None, q_mag=None, d_spacing=None, pen=None):
        self.debug("add_ring")  # , radius, angle, q_mag, d_spacing, pen)
        if angle is not None:
            if angle >= np.pi:
                return False
            radius = np.tan(angle)
        if q_mag is not None:
            a = q_mag * self.dataframe.get_beam().wavelength / (4 * np.pi)
            angle = 2 * np.arcsin(a)
            if angle >= np.pi:
                return False
            radius = np.tan(angle)
        if d_spacing is not None:
            a = self.get_beam().wavelength / (2 * d_spacing)
            angle = 2 * np.arcsin(a)
            if angle >= np.pi:
                return False
            q_mag = 4 * np.pi / d_spacing
            radius = np.tan(angle)
        if pen is None:
            pen = self.ring_pen
        ring = pg.CircleROI(
            pos=[-radius, -radius], size=2 * radius, pen=pen, movable=False
        )
        ring.q_mag = q_mag
        self.rings.append(ring)
        self.viewbox.addItem(ring)
        for handle in ring.handles:
            ring.removeHandle(handle["item"])
        return True

    def update_rings(self):
        r"""Update rings (needed if the |Beam| changes)."""
        self.debug("update_rings")
        if self.rings:
            for ring in self.rings:
                if ring.q_mag:
                    r = np.tan(
                        2
                        * np.arcsin(
                            ring.q_mag * self.get_beam().wavelength / (4 * np.pi)
                        )
                    )
                    ring.setState({"pos": [-r, -r], "size": 2 * r, "angle": 0})

    def hide_ring_radius_handles(self):
        self.debug("hide_ring_radius_handles")
        for circ in self.rings:
            for handle in circ.handles:
                circ.removeHandle(handle["item"])

    def remove_rings(self):
        self.debug("remove_rings")
        if self.rings is None:
            return
        for i in range(0, len(self.rings)):
            self.viewbox.removeItem(self.rings[i])


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Widget(None)
    window.setGeometry(100, 100, 500, 300)
    window.setWindowTitle('Ring Editor Example')
    window.show()
    sys.exit(app.exec_())
