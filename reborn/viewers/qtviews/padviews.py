# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
import sys
import os
import time
import glob
import importlib
import pickle
import json
import datetime
import numpy as np
import functools
import logging
import pyqtgraph as pg
from ...external.pyqt import (
    QKeySequence,
    QTransform,
    QAction,
    QShortcut,
    QObject,
    pyqtSignal,
    Qt,
    QMainWindow,
    QWidget,
    QHBoxLayout,
    QVBoxLayout,
    QSplitter,
    QGridLayout,
    QLabel,
    QSpinBox,
    QCheckBox,
    QDialog,
    QColorDialog,
    QFileDialog,
    QInputDialog,
    QLineEdit,
    Accepted,
    Horizontal,
    QFontDatabase,
)
from ... import source, detector, utils
from ...dataframe import DataFrame, save_pickled_dataframe, load_pickled_dataframe
from ...fileio.getters import FrameGetter
from ...external.pyqtgraph import imview, ImageItemPlus
from . import padview_widgets as widgets


logger = logging.getLogger(__name__)

# Add some pre-defined colormaps (called "gradients" in pyqtgraph)
g = pg.graphicsItems.GradientEditorItem.Gradients
g["bipolar2"] = {
    "ticks": [
        (0.0, (255, 0, 0, 255)),
        (0.5, (0, 0, 0, 255)),
        (1.0, (0, 255, 255, 255)),
    ],
    "mode": "rgb",
}
pg.graphicsItems.GradientEditorItem.Gradients = g

plugin_path = utils.resource_path("viewers/qtviews/plugins")


class PADViewMainWindow(QMainWindow):
    r"""A QMainWindow that closes all windows when it is closed.  Be careful..."""

    def __init__(self, main=True):
        super().__init__()
        self.app = pg.mkQApp()
        # self.app.setKeyboardInputInterval()
        self.main = main

    def closeEvent(self, *args, **kwargs):
        if self.main:
            pg.mkQApp().closeAllWindows()


class PADView(QObject):
    r"""
    This is supposed to be an easy way to view PAD data.  It is a work in progress... but here is the basic concept:

    |PADView| relies on a |FrameGetter| subclass to serve up |DataFrame| instances.
    """

    # Options that the user can modify to affect behavior
    show_histogram = True  # Changing frames is faster if you don't show the histogram
    downsample_function = None  # Change the way that downsampling is done when zooming out.  Default is the nearest pixel.
    hold_pad_geometry = True  # If true, carry over geometry from previous dataframe (don't fetch from framegetter)
    hold_beam = True  # If True, carry over beam from previous dataframe (don't fetch from framegetter)
    hold_mask = True  # If True, carry over mask from previous dataframe (don't fetch from framegetter)
    hold_levels = False  # If True, don't auto-update colormap levels
    skip_empty_frames = True  # Automatically skip empty frames

    # Protected attributes
    _debug_level = 0  # Debug level: 0 means don't create terminal loger.  1-3 creates logger and increases verbosity.
    _frame_getter = None  # The current |FrameGetter| from which data is fetched
    _dataframe = None  # The currently displayed dataframe
    _fetching_frame = False
    _pad_labels = None  # List of the Qt pad label objects
    _mask_image_items = None  # List of the Qt mask image objects
    _mask_color = [128, 0, 0, 255]  # Color of the main mask
    _mask_rgba_arrays = None  # The RGB arrays formed from the binary mask
    _mask_rois = None  # List of region-of-interest Qt objects for masking
    _pad_image_items = None  # List of Qt data image items
    _scatter_plots = None  # List of pyqtgraph ScatterPlotItems overlaid on the display
    _plot_items = None  # List of pyqtgraph PlotDataItems overlaid on the display
    _rings = []  # List of Qt objects for currently displayed rings
    _ring_pen = pg.mkPen([255, 255, 255], width=2)  # TODO: Why only one? Is this used?
    _grid = None  # Grid overlay
    _coord_axes = None  # Coordinate axes pyqtgraph objects (arrows and text)
    _scan_arrows = None
    _px_mode = False
    _shortcuts = []
    _status_string_mouse = ""
    _status_string_getter = " Frame 1 of 1 | "
    _evt = None
    _plugins = None
    _fixed_title = False
    _dataframe_preprocessor = None
    # Management of colormap levels
    _fixed_levels = [None, None]  # Explicit level set points
    _percentile_levels = [1, 99]  # Percentile specification for auto-scaling
    _mirror_levels = False  # Presently unused (eventually, set min to negative of max)
    _levels_ignore_masked = True  # Include masked pixels when auto-scaling?
    _tic_times = []  # Timepoints for timing actions in debug mode
    # Built-in widgets
    _display_editor_widget = None
    _display_options_widget = None
    _mask_editor_widget = None
    _ring_editor_widget = None
    _frame_navigator_widget = None
    _runstats_widget = None
    _edit_geometry_widget = None
    _swaxs_simulator_widget = None
    _beam_editor_widget = None
    _status_bar_style = "background-color:rgb(30, 30, 30);color:rgb(255,0,255);font-weight:bold;font-family:Monospace,Menlo,Courier;"
    # TODO: Figure out why MacOS prints: qt.qpa.fonts: Populating font family aliases took 322 ms. Replace uses of missing font family "Monospace" with one that exists to avoid this cost. 
    #app = pg.mkQApp()
    #fonts = QFontDatabase().families()
    #print(fonts)
    _scatterplot_style = {
        "pen": pg.mkPen("g"),
        "brush": None,
        "width": 5,
        "size": 10,
        "pxMode": True,
    }
    # The signals below produce sphinx warnings.  Moving them to __init__
    # gets rid of those warnings.  However, signals must be part of the
    # class definition and cannot be dynamically added as class attributes
    # after the class has been defined.  We are stuck with the warnings...
    sig_geometry_changed = pyqtSignal()
    sig_beam_changed = pyqtSignal()
    sig_dataframe_changed = pyqtSignal()

    def __init__(
        self,
        frame_getter=None,
        data=None,
        pad_geometry=None,
        mask=None,
        beam=None,
        levels=None,
        percentiles=None,
        debug_level=0,
        main=True,
        dataframe_preprocessor=None,
        title=None,
        **kwargs,
    ):
        """
        Arguments:
            frame_getter (|FrameGetter| subclass): The preferred way to flip through many frames.
            data (|DataFrame| or |ndarray|): The data to be displayed.
            pad_geometry (|PADGeometryList|): PAD geometry information.
            mask (|ndarray|): Data masks.
            beam (|Beam|): X-ray beam parameters.
            levels (tuple): The minimum and maximum levels for the colormap.
            percentiles (tuple): The minimum and maximum percentages for the colormap.
            debug_level (int): The debug level. Larger values are more verbose.
            main (bool): Set to False if you don't want this window to close all other windows when it is closed.
            hold_levels (bool): If True, do not attempt to re-scale the colormap for each frame.
            dataframe_preprocessor (function): Upon updating the |DataFrame| instance, apply this function.
                                               Must return a |DataFrame| instance.
        """
        self._debug_level = debug_level
        self._setup_logger()
        self.debug("Initializing PADView instance.")
        super().__init__()
        if levels is not None:
            self._fixed_levels = list(levels)
        elif percentiles is not None:
            self._percentile_levels = list(percentiles)
        else:
            self._percentile_levels = [1, 99]
        # If True, close all other windows when this main window closes
        self.main = main
        # If not None, this function modifies the DataFrame instance upon setting self.dataframe
        self._dataframe_preprocessor = dataframe_preprocessor
        if frame_getter is None:
            frame_getter = DummyFrameGetter(
                data=data, pad_geometry=pad_geometry, mask=mask, beam=beam, **kwargs
            )
        self._frame_getter = frame_getter
        self.app = pg.mkQApp()
        self._setup_ui()
        self._setup_shortcuts()
        self._setup_menubar()
        self.statusbar.setStyleSheet(self._status_bar_style)
        self.show_first_frame()
        # self.set_colormap("flame")
        show_and_raise(self.main_window)
        self._display_editor_widget = widgets.display_editor.Widget(padview=self)
        self._display_options_widget = widgets.display_options.Widget(padview=self)
        self._mask_editor_widget = widgets.mask_editor.Widget(padview=self)
        self._ring_editor_widget = widgets.ring_editor.Widget(padview=self)
        self._frame_navigator_widget = widgets.frame_navigator.Widget(padview=self)
        self._runstats_widget = widgets.run_stats.Widget(padview=self)
        self._edit_geometry_widget = widgets.edit_geometry.Widget(padview=self)
        # self._swaxs_simulator_widget = widgets.swaxs_simulator.Widget(padview=self)
        self._beam_editor_widget = widgets.edit_beam.Widget(padview=self)
        self.viewbox.disableAutoRange()
        if title is not None:
            self.set_title(title, fixed=True)
        self.app.processEvents()
        self.debug("Initialization complete.")

    def _setup_logger(self):
        if self._debug_level > 0:
            level = logging.DEBUG - (self._debug_level-1)*2  # Map historical level spec to logging standards
            utils.log_to_terminal(level=level)

    def debug(self, msg="", level=1):
        r"""
        Add debug message to the reborn logger.  For historical reasons, the PADView debug levels are:

        - 1: Ordinary debug message (e.g. change mask).
        - 2: Verbose debug message (e.g. PAD ImageItem transformations).
        - 3: Very verbose debug (e.g. mouse movement).

        These levels differ from standard Python logging debug level.  The above values map to
        Python logging as follows:

        - 1: logging.DEBUG = 10
        - 2: logging.DEBUG - 2 = 8
        - 3: logging.DEBUG - 4 = 6

        Arguments:
            msg (str): The desired debug message
            level (int): Specify level of verbosity required to log the message
        """
        lev = logging.DEBUG
        if level > 0:
            lev -= (level-1)*2
        logger.log(lev, msg)

    # @property
    # def dataframe(self):
    #     r"""The current |DataFrame| instance."""
    #     return self._dataframe
    #
    # @dataframe.setter
    # def dataframe(self, val):
    #     self.debug("dataframe setter")
    #     # if self._dataframe is None:
    #     #     self._dataframe = val
    #     # if self._dataframe_preprocessor is not None:
    #     #     self.debug("dataframe preprocessor")
    #     #     val = self._dataframe_preprocessor(val)
    #     if isinstance(val, DataFrame):
    #         self._dataframe = val
    #         self.sig_dataframe_changed.emit()
    #         return
    #     self.debug(f"Attempted to set dataframe to wrong type!!!! {val}")

    def get_pad_geometry(self) -> detector.PADGeometryList:
        r"""Return the currently displayed |PADGeometryList|"""
        return self.get_dataframe().get_pad_geometry()

    def set_pad_geometry(self, geom):
        self.get_dataframe().set_pad_geometry(geom)

    def get_dataframe(self) -> DataFrame:
        r"""Return the currently shown |DataFrame| instance."""
        return self._dataframe

    def set_dataframe(self, data):
        data.validate()
        self.sig_dataframe_changed.emit()
        self._dataframe = data

    def get_beam(self) -> source.Beam:
        r"""Return the currently displayed |Beam|"""
        return self.get_dataframe().get_beam()

    def set_beam(self, beam: (None, source.Beam)):
        if beam is not None:
            beam.validate()
        self.get_dataframe().set_beam(beam)

    def get_mask(self, as_list=False) -> np.ndarray:
        r"""Return the currently displayed mask"""
        if as_list:
            mask = self.get_dataframe().get_mask_list()
        else:
            mask = self.get_dataframe().get_mask_flat()
        return mask

    def set_mask(self, mask: (None, np.ndarray)):
        self.get_dataframe().set_mask(mask)

    def get_pad_display_data(self, as_list=False) -> (list, np.ndarray):
        self.debug("get_pad_display_data", level=2)
        if as_list:
            return self.get_dataframe().get_processed_data_list()
        else:
            return self.get_dataframe().get_processed_data_flat()

    def set_pad_display_data(self, data):
        self.get_dataframe().set_processed_data(data)

    def _setup_ui(self):
        r"""Creates the main interface: QMainWindow, menubar, statusbar, viewbox, etc."""
        self.debug("setup_ui")
        self.main_window = PADViewMainWindow(main=self.main)
        self.menubar = self.main_window.menuBar()
        self.statusbar = self.main_window.statusBar()
        self.hbox = QHBoxLayout()
        self.splitter = QSplitter(orientation=Horizontal)
        # self.side_panel = QWidget()
        # self.side_panel_layout = QVBoxLayout()
        # self.side_panel_layout.setAlignment(Qt.AlignTop)
        # box = misc.CollapsibleBox('Display') ###########################
        # lay = QGridLayout()
        # lay.addWidget(QLabel('CMap min:'), 1, 1)
        # maxspin = QSpinBox()
        # lay.addWidget(maxspin, 1, 2)
        # box.setContentLayout(lay)
        # self.side_panel_layout.addWidget(box)
        # box = misc.CollapsibleBox('Analysis') ###########################
        # lay = QGridLayout()
        # row = 0
        # row += 1
        # lay.addWidget(QLabel('Polarization Correction'), row, 1)
        # polarization_button = QCheckBox()
        # # polarization_button.toggled.connect()
        # lay.addWidget(polarization_button, row, 2, alignment=Qt.AlignCenter)
        # row += 1
        # lay.addWidget(self.widget_plugin, row, 1)
        # box.setContentLayout(lay)
        # self.side_panel_layout.addWidget(box)
        # self.side_panel.setLayout(self.side_panel_layout)
        # self.splitter.addWidget(self.side_panel)
        self.graphics_view = pg.GraphicsView()
        self.viewbox = pg.ViewBox()
        self.viewbox.invertX()
        self.viewbox.setAspectLocked()
        self.graphics_view.setCentralItem(self.viewbox)
        self.splitter.addWidget(self.graphics_view)
        self.histogram = pg.HistogramLUTWidget()
        self.histogram.plot.setPen(None)
        c = (50, 50, 150)
        self.histogram.item.fillHistogram(color=c)
        # self.histogram.item.disableAutoHistogramRange()
        b = (50, 50, 80, 100)
        self.histogram.item.regions[0].setBrush(b)
        self.histogram.item.regions[0].setHoverBrush(b)
        p = pg.mkPen(color=(0, 200, 0), width=2)
        self.histogram.item.regions[0].lines[0].setPen(p)
        self.histogram.item.regions[0].lines[1].setPen(p)
        self.histogram.item.gradient.tickLevels = []
        self.histogram.item.sigLevelsChanged.connect(self._set_levels_to_hist)
        self.histogram.item.sigLevelChangeFinished.connect(self._set_levels_to_hist)
        self.histogram.item.sigLookupTableChanged.connect(self._set_colormap_to_hist)
        self.splitter.addWidget(self.histogram)
        self.hbox.addWidget(self.splitter)
        self.main_widget = QWidget()
        self.main_widget.setLayout(self.hbox)
        self.main_window.setCentralWidget(self.main_widget)
        self.proxy = pg.SignalProxy(
            self.viewbox.scene().sigMouseMoved, rateLimit=30, slot=self.mouse_moved
        )
        # Other signal connections
        self.sig_beam_changed.connect(self.update_rings)

    def set_title(self, title=None, fixed=False):
        r"""Set the title of the main window."""
        df = self.get_dataframe()
        self.debug("set_title")
        if fixed:
            self.main_window.setWindowTitle(title)
            self._fixed_title = True
        if title is not None:
            self.main_window.setWindowTitle(title)
        if not self._fixed_title:
            title = ""
            try:
                t = df.get_dataset_id().__str__()
                if t:
                    title += t + " "
                f = df.get_frame_id().__str__()
                if f:
                    title += f
                self.main_window.setWindowTitle(title)
            except:
                pass

    def _setup_menubar(self):
        r"""Connect menu items (e.g. "File") so that they actually do something when clicked."""
        self.debug("setup_menubar")

        def add_menu(append_to, name, short=None, tip=None, connect=None):
            action = QAction(name, self.main_window)
            if short:
                action.setShortcut(QKeySequence(short))
            if tip:
                action.setStatusTip(tip)
            if connect:
                action.triggered.connect(connect)
            append_to.addAction(action)
            return action

        ############### File #############################################
        m = self.menubar.addMenu("File")
        add_menu(m, "Open file...", connect=self.open_data_file_dialog)
        add_menu(m, "Save file...", connect=self.save_data_file_dialog)
        add_menu(m, "Save screenshot...", connect=self.save_screenshot_dialog)
        add_menu(m, "Exit", short="Ctrl+Q", connect=self.app.quit)
        ############### Display ################################################
        m = self.menubar.addMenu("Display")
        add_menu(
            m,
            "Level controls...",
            connect=lambda: show_and_raise(self.display_editor_widget),
            short="Ctrl+Shift+L"
        )
        add_menu(
            m,
            "Ring editor...",
            connect=lambda: show_and_raise(self.ring_editor_widget),
            short="Ctrl+Shift+R"
        )
        add_menu(
            m,
            "Other display options...",
            connect=lambda: show_and_raise(self.display_options_widget),
        )
        ################ Data ############################################
        m = self.menubar.addMenu("Data")
        add_menu(
            m,
            "Frame navigator...",
            connect=lambda: show_and_raise(self.frame_navigator_widget),
            short="Ctrl+Shift+N"
        )
        add_menu(m, "Clear processed data", connect=self.clear_processed_data)
        add_menu(m, "Reload dataframe", connect=self.reload_current_frame)
        ############### Geometry ##############################################
        m = self.menubar.addMenu("Geometry")
        add_menu(
            m,
            "Edit geometry...",
            connect=lambda: show_and_raise(self.edit_geometry_widget),
            short="Ctrl+Shift+G"
        )
        add_menu(m, "Show coordinates", connect=self.toggle_coordinate_axes)
        add_menu(m, "Show grid", connect=self.toggle_grid)
        add_menu(m, "Show PAD labels", connect=self.toggle_pad_labels)
        add_menu(m, "Show PAD indices", connect=self.toggle_pad_indices)
        add_menu(m, "Show scan directions", connect=self.toggle_fast_scan_directions)
        add_menu(m, "Save PAD geometry...", connect=self.save_pad_geometry)
        add_menu(m, "Load PAD geometry...", connect=self.load_pad_geometry)
        ############### Mask ###############################################
        m = self.menubar.addMenu("Mask")
        add_menu(m, "Mask editor...", connect=lambda: show_and_raise(self.mask_editor_widget), short="Ctrl+Shift+M")
        add_menu(m, "Clear masks", connect=self.clear_masks)
        add_menu(m, "Toggle masks visible", connect=self.toggle_masks, short="Ctrl+M")
        add_menu(m, "Save mask...", connect=self.save_masks)
        add_menu(m, "Load mask...", connect=self.load_masks)
        ################ Beam ###############################################
        m = self.menubar.addMenu("Beam")
        add_menu(m, "Edit beam...", connect=lambda: show_and_raise(self.beam_editor_widget), short="Ctrl+Shift+B")
        add_menu(m, "Save beam...", connect=self.save_beam)
        add_menu(m, "Load beam...", connect=self.load_beam)
        ################ Analysis ######################################
        m = self.menubar.addMenu("Analysis")
        add_menu(
            m,
            "Gather statistics...",
            connect=lambda: show_and_raise(self.runstats_widget),
        )
        add_menu(
            m,
            "Scattering profile...",
            connect=lambda: self.run_plugin("scattering_profile"),
        )
        ################ Simulate ######################################
        # m = self.menubar.addMenu("Simulate")
        # add_menu(
        #     m,
        #     "SWAXS Simulation...",
        #     connect=lambda: show_and_raise(self.swaxs_simulator_widget),
        # )
        ################ Plugins ###############################################
        m = self.menubar.addMenu("Plugins")
        self.plugin_names = []
        for plg in sorted(glob.glob(str(plugin_path / "**.py"), recursive=True)):
            plugin_name = plg.replace(str(plugin_path), "")[
                1:
            ]  # os.path.basename(plg).replace('.py', '')
            if "__init__" in plugin_name:
                continue
            self.plugin_names.append(plugin_name)
            # self.debug("\tSetup plugin " + plugin_name)
            add_menu(
                m,
                plugin_name,  # .replace('_', ' '),
                connect=functools.partial(self.run_plugin, plugin_name),
            )

    def set_shortcut(self, key_sequence, function, allow_auto_repeat=False):
        r"""Setup one keyboard shortcut so it connects to some function, assuming no arguments are needed."""
        shortcut = QShortcut(QKeySequence(key_sequence), self.main_window)
        shortcut.activated.connect(function)
        shortcut.setAutoRepeat(allow_auto_repeat)
        self._shortcuts.append(shortcut)

    def _setup_shortcuts(self):
        r"""Connect all standard keyboard shortcuts to functions."""
        # FIXME: Many of these shortcuts were randomly assigned in a hurry.  Need to re-think the keystrokes.
        self.debug("setup_shortcuts")
        shortcut = self.set_shortcut
        # shortcut = lambda s, f: self._shortcuts.append(
        #     QShortcut(QKeySequence(s), self.main_window).activated.connect(f)
        # )
        shortcut(Qt.Key.Key_Right, self.show_next_frame)
        shortcut(Qt.Key.Key_Left, self.show_previous_frame)
        shortcut("f", self.show_next_frame)
        shortcut("b", self.show_previous_frame)
        shortcut("r", self.show_random_frame)
        shortcut("n", self.show_history_next)
        shortcut("p", self.show_history_previous)
        shortcut("Ctrl+a", self.toggle_coordinate_axes)
        shortcut("Ctrl+l", self.toggle_pad_labels)
        shortcut("m", self.toggle_masks)

    def _update_status_string(self, frame_number=None, n_frames=None):
        r"""Update status string at the bottom of the main window."""
        self.debug("update_status_string", level=2)
        strn = ""
        if frame_number is None:
            frame_number = self._frame_getter.current_frame
        if n_frames is None:
            n_frames = self._frame_getter.n_frames
        if frame_number is not None:
            strn += " Frame %d" % (frame_number + 1)
            if n_frames is not None:
                if n_frames == np.inf:
                    strn += " of inf"
                else:
                    strn += " of %d" % n_frames
            strn += " | "
            self._status_string_getter = strn
        self.statusbar.showMessage(
            self._status_string_getter + self._status_string_mouse
        )

    def _set_colormap_to_hist(self):
        if self._pad_image_items is not None:
            lut = self.histogram.getLookupTable
            for item in self._pad_image_items:
                item.setLookupTable(lut)
        self.app.processEvents()

    def set_colormap(self, preset=None):
        r"""Change the colormap to one of the presets configured in pyqtgraph.  Right-click on the colorbar to find
        out what values are allowed.
        """
        self.debug("set_colormap")
        self.histogram.gradient.loadPreset("flame")

    def get_levels(self):
        r"""Get the minimum and maximum levels of the current image display."""
        return list(self.histogram.item.getLevels())

    def set_levels(self, levels=None, percentiles=None, colormap=None):
        r"""
        This method handles setting levels (i.e. mapping the upper and lower bounds of detector pixel values to the
        span of the color table).

        If no arguments are provided, the default settings are used.

        Level setting is a somewhat complicated matter.  For both the upper and lower bounds, there are 5 ways that
        we presently set the levels of the display data.  In order of priority, highest first:

          1) Set the level to the particular value specified in the levels keyword argument of this method.
          2) Set the level to the value corresponding to the Nth percentile of the current display data,
             using the percentiles keyword argument.
          3) Leave the levels alone; keep the previous levels.

        An additional option is to mirror the levels: i.e. set the levels to range from [-x, +x].  This is controlled
        via the "mirror_levels" attribute.

        When determining levels from percentiles, it is possible to ignore masked pixels.  This is controlled by the
        attribute "levels_ignore_masked".

        Arguments:
            levels (tuple of floats): Fixed-value levels.  Default is (None, None).
            percentiles (tuple of floats): Percentiles for determining levels.  Values should be 0-100.  Default is
                                           (None, None)
            colormap:  For convenience, you may also specify a new colormap.  This is passed to "set_colormap".
        """
        self.debug("set_levels")
        if self.hold_levels:
            self.debug("\tholding levels")
            return
        data = None
        if percentiles is not None:
            self._percentile_levels = list(percentiles)
            for i in range(2):
                if self._percentile_levels[i] is not None:
                    self._fixed_levels[i] = None
        if levels is not None:
            self._fixed_levels = list(levels)
            for i in range(2):
                if self._fixed_levels[i] is not None:
                    self._percentile_levels[i] = None
        new_levels = [None, None]
        df = self.get_dataframe()
        for i in range(2):
            # First priority: fixed level
            if self._fixed_levels[i] is not None:
                level = self._fixed_levels[i]
                self.debug(f"\tnew level {i} (fixed): {level}")
            # Second priority: requested percentile
            elif self._percentile_levels[i] is not None:
                if data is None:
                    data = self.get_pad_display_data(as_list=False)
                    if data is None:
                        return
                    if self._levels_ignore_masked:
                        mask = df.get_mask_flat()
                        data = data[np.where(mask)]
                        if data.size <= 0:
                            return
                level = np.percentile(data, self._percentile_levels[i])
                self.debug(f"\tnew level {i} (percent): {level}")
            # Third priority: do not change the level.  Keep the current levels.
            else:
                level = self.get_levels()[i]
                self.debug(f"\tnew level {i} (hold): {level}")
            new_levels[i] = level
        # # Special override: Mirror levels if requested.  I.e. set the range to span [-abs(max), +abs(max)]
        # if self._mirror_levels:
        #     self.debug(f"mirror levels")
        #     a = np.max(np.abs(np.array(new_levels)))
        #     new_levels[0] = -a
        #     new_levels[1] = a
        self.histogram.item.setLevels(new_levels[0], new_levels[1])
        self.histogram.item.setHistogramRange(
            new_levels[0], new_levels[1]
        )  # , padding=0.02)
        if colormap is not None:
            self.set_colormap(colormap)

    def _set_levels_to_hist(self):
        for image in self._pad_image_items:
            image.setLevels(self.histogram.item.getLevels())

    def add_rectangle_roi(self, pos=(0, 0), size=None, movable=True):
        r"""Adds a |pyqtgraph| RectROI"""
        self.debug("add_rectangle_roi")
        if type(pos) != tuple:
            pos = (0, 0)
        if size is None:
            br = self.get_view_bounding_rect()
            s = min(br[2], br[3])
            size = (s / 4, s / 4)
        self.debug((size, pos).__str__())
        pos = (pos[0] - size[0] / 2, pos[1] - size[0] / 2)
        roi = pg.RectROI(
            pos=pos,
            size=size,
            centered=True,
            sideScalers=True,
            movable=movable,
            removable=True,
        )
        # roi.setBrush((0,0, 0))
        roi.name = "rectangle"
        roi.addRotateHandle(pos=(0, 1), center=(0.5, 0.5))
        if self._mask_rois is None:
            self._mask_rois = []
        # FIXME: We need better handling of these ROIs: better manipulations of sizes/shapes, more features (e.g. the
        # FIXME: ability to show a histogram of pixels within the ROI)
        self._mask_rois.append(roi)
        self.viewbox.addItem(roi)

    # FIXME: Circle ROI should have an option to fix the center on the beam center.
    def add_circle_roi(self, pos=(0, 0), radius=None, moveable=True):
        self.debug("add_circle_roi")
        if radius is None:
            br = self.get_view_bounding_rect()
            radius = min(br[2], br[3]) / 2
        pos = np.array(pos) - radius / 2
        roi = pg.CircleROI(pos=pos, size=radius, movable=moveable, removable=True)
        roi.name = "circle"
        if self._mask_rois is None:
            self._mask_rois = []
        self._mask_rois.append(roi)
        self.viewbox.addItem(roi)

    def show_rois(self):
        self.debug("show_rois")
        if self._mask_rois is not None:
            for roi in self._mask_rois:
                roi.setVisible(True)

    def hide_rois(self):
        self.debug("hide_rois")
        if self._mask_rois is not None:
            for roi in self._mask_rois:
                roi.setVisible(False)

    def toggle_rois(self):
        self.debug("toggle_rois")
        if self._mask_rois is None:
            return
        for roi in self._mask_rois:
            if roi.isVisible():
                self.hide_rois()
                return
        self.show_rois()

    def show_coordinate_axes(self):
        self.debug("show_coordinate_axes")
        if self._coord_axes is None:
            geom = self.get_pad_geometry()
            corners = self.vector_to_view_coords(
                np.vstack([p.corner_position_vectors() for p in geom])
            )
            length = np.max(np.abs(corners[:, 0:2])) / 10
            xl = pg.PlotDataItem([0, length], [0, 0], pen="r")
            self.viewbox.addItem(xl)
            yl = pg.PlotDataItem([0, 0], [0, length], pen="g")
            self.viewbox.addItem(yl)
            x = pg.ArrowItem(
                pos=(length, 0), brush=pg.mkBrush("r"), angle=0, pen=None
            )  # , pxMode=self._px_mode)
            self.viewbox.addItem(x)
            y = pg.ArrowItem(
                pos=(0, length), brush=pg.mkBrush("g"), angle=90, pen=None
            )  # , pxMode=self._px_mode)
            self.viewbox.addItem(y)
            z1 = pg.ScatterPlotItem(
                [0], [0], pen=None, brush=pg.mkBrush("b"), size=15, symbol="x"
            )  # , pxMode=self._px_mode)
            self.viewbox.addItem(z1)
            z2 = pg.ScatterPlotItem(
                [0], [0], pen="b", brush=None, size=15, symbol="o"
            )  # , pxMode=self._px_mode)
            self.viewbox.addItem(z2)
            xt = pg.TextItem(text="x", color="r", anchor=(0.5, 0.5))
            xt.setPos(length * 0.6, -length / 3)
            self.viewbox.addItem(xt)
            yt = pg.TextItem(text="y", color="g", anchor=(0.5, 0.5))
            yt.setPos(-length / 3, length * 0.6)
            self.viewbox.addItem(yt)
            zt = pg.TextItem(text="z", color="b", anchor=(0.5, 0.5))
            zt.setPos(-length / 3, -length / 3)
            self.viewbox.addItem(zt)
            self._coord_axes = [xl, yl, x, y, z1, z2, xt, yt, zt]

    def hide_coordinate_axes(self):
        self.debug("hide_coordinate_axes")
        if self._coord_axes is not None:
            for c in self._coord_axes:
                self.viewbox.removeItem(c)
            self._coord_axes = None

    def toggle_coordinate_axes(self):
        self.debug("toggle_coordinate_axes")
        if self._coord_axes is None:
            self.show_coordinate_axes()
        else:
            self.hide_coordinate_axes()

    def show_fast_scan_directions(self):
        self.debug("show_fast_scan_directions")
        if self._scan_arrows is None:
            self._scan_arrows = []
            for p in self.get_pad_geometry():
                t = p.t_vec
                f = p.fs_vec
                n = p.n_fs
                x = self.vector_to_view_coords(np.array([t, t + f * n / 3]))
                plot = pg.PlotDataItem(x[:, 0], x[:, 1], pen="r")
                self._scan_arrows.append(plot)
                self.viewbox.addItem(plot)
                ang = np.arctan2(f[1], f[0]) * 180 / np.pi
                a = pg.ArrowItem(
                    pos=(x[1, 0], x[1, 1]), angle=ang, brush=pg.mkBrush("r"), pen=None
                )  # , pxMode=False)
                self._scan_arrows.append(a)
                self.viewbox.addItem(a)

    def hide_fast_scan_directions(self):
        self.debug("hide_fast_scan_directions")
        if self._scan_arrows is not None:
            for a in self._scan_arrows:
                self.viewbox.removeItem(a)
            self._scan_arrows = None

    def toggle_fast_scan_directions(self):
        self.debug("toggle_fast_scan_directions")
        if self._scan_arrows is None:
            self.show_fast_scan_directions()
        else:
            self.hide_fast_scan_directions()

    def show_pad_labels(self, as_indices=False):
        self.debug("show_pad_labels")
        if self._pad_labels is None:
            self._pad_labels = []
            pad_geometry = self.get_pad_geometry()
            for i in range(0, len(self.get_pad_geometry())):
                g = pad_geometry[i]
                name = ""
                if (not hasattr(g, "name")) or (g.name is None) or (g.name == ""):
                    name = f"{i}"
                if as_indices or name == "":
                    name = f"{i}"
                lab = pg.TextItem(
                    text=name,
                    fill=pg.mkBrush(20, 20, 20, 128),
                    color="w",
                    anchor=(0.5, 0.5),
                    border=pg.mkPen("w"),
                )
                vec = pad_geometry[i].center_pos_vec()
                vec = self.vector_to_view_coords(vec)
                lab.setPos(vec[0], vec[1])
                self._pad_labels.append(lab)
                self.viewbox.addItem(lab)

    def hide_pad_labels(self):
        self.debug("hide_pad_labels")
        if self._pad_labels is not None:
            for lab in self._pad_labels:
                self.viewbox.removeItem(lab)
            self._pad_labels = None

    def toggle_pad_labels(self, as_indices=False):
        self.debug("toggle_pad_labels")
        if self._pad_labels is None:
            self.show_pad_labels(as_indices=as_indices)
        else:
            self.hide_pad_labels()

    def show_pad_indices(self):
        self.debug("show_pad_indices")
        self.show_pad_labels(as_indices=True)

    def hide_pad_indices(self):
        self.debug("hide_pad_indices")
        self.hide_pad_labels(as_indices=True)

    def toggle_pad_indices(self):
        self.debug("toggle_pad_indices")
        self.toggle_pad_labels(as_indices=True)

    def _apply_pad_transform(self, im, p):
        self.debug("_apply_pad_transform", level=3)
        f = p.fs_vec.copy()
        s = p.ss_vec.copy()
        t = p.t_vec.copy() - (f + s) / 2.0
        trans = QTransform()
        trans.setMatrix(s[0], s[1], s[2], f[0], f[1], f[2], t[0], t[1], t[2])
        im.setTransform(trans)

    def choose_mask_color(self):
        self.debug("choose_mask_color")
        dialog = QColorDialog()
        dialog.setOption(QColorDialog.ColorDialogOption.ShowAlphaChannel, on=True)
        if dialog.exec() == Accepted:
            color = dialog.selectedColor()
        #     if color.isValid():
        # color = QColorDialog.getColor()
        if color is None:
            self.debug("Color is None")
            return
        if not color.isValid():
            self.debug("Color is invalid")
            return
        self._mask_color[0] = color.red()
        self._mask_color[1] = color.green()
        self._mask_color[2] = color.blue()
        self._mask_color[3] = color.alpha()
        self._mask_rgba_arrays = None
        self.update_masks()

    def get_mask_color(self):
        return self._mask_color
        
    def set_mask_color(self, color):
        self._mask_color = color
        self._mask_rgba_arrays = None
        self.update_masks()

    def update_masks(self, masks=None):
        r"""Update the mask that is displayed.
        
        note: 
            As a side effect, the current dataframe will be updated with the provided mask"""
        self.debug("update_masks")
        if self._mask_image_items is None:
            self._setup_image_items()
        self.tic("update_masks")
        self.tic("Getting mask...")
        geom = self.get_pad_geometry()
        if geom is None:
            geom = self.get_pad_geometry()
        if masks is None:
            masks = self.get_mask()
        if masks is None:
            masks = self.get_dataframe().get_mask_flat()
        if not isinstance(masks, np.ndarray):
            masks = geom.concat_data(masks)
        self.get_dataframe().set_mask(masks)
        masks = self.get_mask(as_list=True)
        self.toc()
        if len(self._mask_color) < 4:
            self._mask_color.append(0)
        if self._mask_rgba_arrays is None:
            self.tic("Initializing RGBA mask arrays...")
            self._mask_rgba_arrays = []
            for i, mask in enumerate(masks):
                m = np.zeros((mask.shape[0], mask.shape[1], 4), dtype=np.uint8)
                m[:, :, 0] = int(self._mask_color[0])
                m[:, :, 1] = int(self._mask_color[1])
                m[:, :, 2] = int(self._mask_color[2])
                m[:, :, 3] = int(self._mask_color[3])
                self._mask_rgba_arrays.append(m)
            self.toc()
        for item, mask, rgba in zip(
            self._mask_image_items, masks, self._mask_rgba_arrays
        ):
            # self.tic("Updating RGBA mask array...")
            m = rgba[:, :, 3]
            m[:, :] = 0
            m[np.where(mask == 0)] = self._mask_color[3]
            # self.toc()
            # self.tic("Setting mask image...")
            item.setImage(rgba)
            # self.toc()
        self.toc()

    def hide_masks(self):
        self.debug("hide_masks")
        if self._mask_image_items is not None:
            for im in self._mask_image_items:
                im.setVisible(False)

    def show_masks(self):
        self.debug("show_masks")
        if self._mask_image_items is not None:
            for im in self._mask_image_items:
                im.setVisible(True)

    def toggle_masks(self):
        self.debug("toggle_masks")
        if self._mask_image_items is not None:
            for im in self._mask_image_items:
                im.setVisible(not im.isVisible())

    def save_masks(self):
        r"""Save list of masks in pickle or reborn mask format."""
        self.debug("save_masks")
        file_name, file_type = QFileDialog.getSaveFileName(
            self.main_window,
            "Save Masks",
            "mask",
            "reborn Mask File (*.mask);;Python Pickle (*.pkl);;OM hdf5 (*.h5);;Flat Numpy (*.npy);;ND Numpy (*.npy)",
        )
        if file_name == "":
            return
        geom = self.get_pad_geometry()
        split = geom.split_data
        mask = self.get_mask()
        if file_type == "reborn Mask File (*.mask)":
            if file_name.split(".")[-1] != "mask":
                file_name += ".mask"
        print("Saving masks: " + file_name)
        if file_type == "Python Pickle (*.pkl)":
            with open(file_name, "wb") as f:
                pickle.dump(split(mask), f)
        if file_type == "reborn Mask File (*.mask)":
            detector.save_pad_masks(file_name, split(mask))
        if file_type == "OM hdf5 (*.h5)":
            detector.save_mask_as_om_h5(
                split(mask),
                geom,
                file_name,
            )
        if file_type == "Flat Numpy (*.npy)":
            np.save(file_name, mask)
        if file_type == "ND Numpy (*.npy)":
            np.save(file_name, geom.reshape(mask))

    def load_masks(self):
        r"""Load list of masks that have been saved in pickle or reborn mask format."""
        self.debug("load_masks")
        dialog = widgets.misc.GetMaskFileDialog(self.main_window, "Load Masks")
        out = dialog.exec_()
        if not out:
            return
        file_names = dialog.selectedFiles()
        # file_name, file_type = dialog.getOpenFileName(
        #     self.main_window,
        #     "Load Masks",
        #     "mask",
        #     "reborn Mask File (*.mask);;Python Pickle (*.pkl)",
        #     options=options,
        # )
        overwrite = dialog.overwrite
        combine = dialog.combine
        geom = self.get_pad_geometry()
        mask = self.get_mask()
        if overwrite:
            mask = mask * 0 + 1
        for file_name in file_names:
            s = file_name.split(".")
            if len(s) > 1:
                extension = s[-1]
            else:
                extension = None
            if extension == "pkl":
                with open(file_name, "rb") as f:
                    _mask = geom.concat_data(pickle.load(f))
            elif extension == "mask":
                _mask = geom.concat_data(detector.load_pad_masks(file_name))
            elif extension == "npy":
                _mask = geom.concat_data(np.load(file_name))
            else:
                raise ValueError("Mask format not recognized")
            if combine:
                mask *= _mask
            else:
                mask = _mask
            self.debug("Loaded mask: " + file_name)
        self.set_mask(mask)
        self.update_masks()

    def _get_hovering_roi_indices(self, flat=True):
        r"""Get the indices within the ROI that the mouse is presently hovering over.  flat=True indicates that you wish
        to have the flattened indices, where are pads are concatenated.  If no ROI is selected, return None, None.
        Otherwise, return indices, roi.name (str).
        """
        if flat is False:
            raise ValueError(
                "flat=False is not implemented in get_hovering_roi_indices (yet)"
            )
        if self._mask_rois is None:
            return None, None
        roi = [r for r in self._mask_rois if r.mouseHovering]
        if len(roi) == 0:
            return None, None
        roi = roi[0]
        geom = self.get_pad_geometry()
        p_vecs = geom.position_vecs()
        v_vecs = self.vector_to_view_coords(p_vecs)[:, 0:2]
        if roi.name == "rectangle":  # Find all pixels within the rectangle
            self.debug("\tGetting rectangle ROI indices")
            sides = [roi.size()[1], roi.size()[0]]
            corner = np.array([roi.pos()[0], roi.pos()[1]])
            angle = roi.angle() * np.pi / 180
            v1 = np.array([-np.sin(angle), np.cos(angle)])
            v2 = np.array([np.cos(angle), np.sin(angle)])
            d = v_vecs - corner
            ind1 = np.dot(d, v1)
            ind2 = np.dot(d, v2)
            inds = (ind1 >= 0) * (ind1 <= sides[0]) * (ind2 >= 0) * (ind2 <= sides[1])
        elif roi.name == "circle":
            self.debug("\tGetting circle ROI indices")
            radius = roi.size()[0] / 2.0
            center = np.array([roi.pos()[0], roi.pos()[1]]) + radius
            inds = np.sqrt(np.sum((v_vecs - center) ** 2, axis=1)) < radius
        return inds, roi.name

    def clear_masks(self):
        self.debug("clear_masks")
        self.set_mask(None)
        self.update_masks()

    def clear_processed_data(self):
        r"""Clear processed data and (show raw data)."""
        self.reload_current_frame()
        # self.get_dataframe().set_processed_data(None)
        # self.update_display(beam_changed=False, geom_changed=False, mask_changed=False)


    def _setup_image_items(self):
        r"""Creates PAD and mask ImageItems.  Removes existing ImageItems if they exist."""
        self.debug("setup_image_items")
        gradient_state = self.histogram.gradient.saveState()
        if self._pad_image_items:
            for item in self._pad_image_items:
                self.viewbox.removeItem(item)
        if self._mask_image_items:
            for item in self._mask_image_items:
                self.viewbox.removeItem(item)
        self._pad_image_items = []
        self._mask_image_items = []
        # self._mask_rgba_arrays = []
        geom = self.get_pad_geometry()
        for i in range(len(geom)):
            ds = self.downsample_function
            im = ImageItemPlus(autoDownsample=ds)
            im.downsample_function = ds
            self._pad_image_items.append(im)
            self.viewbox.addItem(im)
            im = ImageItemPlus(autoDownsample=ds)
            self._mask_image_items.append(im)
            self.viewbox.addItem(im)
            if geom is not None:
                self._apply_pad_transform(self._pad_image_items[i], geom[i])
                self._apply_pad_transform(self._mask_image_items[i], geom[i])
        self.histogram.gradient.restoreState(gradient_state)
        # self.update_pad_geometry()
        # self.update_pads()
        # self.update_masks()
        # self.setup_histogram_tool()
        # self.set_levels(percentiles=(2, 98))

    def update_pads(self):
        r"""Update only the PAD data that is displayed, including colormap.
        Usually you should run update_display, which will trigger this
        method at the appropriate time."""
        self.debug("update_pads")
        self.tic("Updating PAD displays.")
        self.tic("Fetching display data...")
        data = self.get_pad_display_data(as_list=True)
        geom = self.get_pad_geometry()
        self.toc()
        self.tic("Set image items")
        if data is None:
            self.debug("get_pad_display_data returned None!")
            return
        if self._pad_image_items is None:
            self._setup_image_items()
        for i, d in enumerate(data):
            self._pad_image_items[i].setImage(
                np.nan_to_num(d)
            )  # TODO: make nan_to_num optional for speed
        self.toc()
        self.tic("Updating histogram")
        self.histogram.regionChanged()
        if self.show_histogram:
            data = geom.concat_data(data)
            hist, bins = np.histogram(data, bins=100)
            x = np.zeros(bins.size * 2)
            x[::2] = bins
            x[1::2] = bins
            y = np.zeros_like(x)
            y[1:-2:2] = hist
            y[2:-1:2] = hist
            self.histogram.plot.setData(x, y)
        self.toc()
        self.set_levels()
        self.toc()

    def update_pad_geometry(self, pad_geometry=None):
        r"""Use this when you want the display to reflect a change in
        the geometry.  Applies Qt transforms to the PAD ImageItems.
        Also emits the sig_geometry_changed signal.

        If you are changing an entire dataframe, use the update_display
        method instead as it will catch geometry changes according to the
        geometry policy.

        Arguments:
            pad_geometry (|PADGeometryList|): PAD Geometry.  If None, get
                the geometry from the current dataframe.
        """
        self.debug("update_pad_geometry")
        if pad_geometry is None:
            pad_geometry = self.get_pad_geometry()
        else:
            self.set_pad_geometry(pad_geometry)
        df = self.get_dataframe()
        for i in range(df.n_pads):
            if self._pad_image_items is not None:
                self._apply_pad_transform(self._pad_image_items[i], pad_geometry[i])
            if self._mask_image_items is not None:
                self._apply_pad_transform(self._mask_image_items[i], pad_geometry[i])
        if self._pad_labels is not None:
            self.toggle_pad_labels()
            self.toggle_pad_labels()
        self._update_status_string()
        self.sig_geometry_changed.emit()

    def update_beam(self, beam=None):  # FIXME: Refactor to set_beam?
        r"""Use this when you want the display to reflect a change in
        the beam properties.  Affects e.g. the q values shown in the statusbar.
        Also emits the sig_beam_changed signal.

        If you are changing an entire dataframe, use the update_display
        method instead as it will catch beam changes according to the
        beam policy.

        Arguments:
            beam (|Beam|): Beam info.  If None, get the beam from the
                current dataframe.
        """
        self.debug("update_beam")
        if beam is None:
            beam = self.get_beam()
        else:
            self.set_beam(beam)
        self._update_status_string()
        self.sig_beam_changed.emit()

    def update_dataframe(self, dataframe, _run_preprocessor=False):
        r"""
        Change the dataframe.  If need be, this will trigger calls to the
        update_beam method, the update_pad_geometry method, and the
        update_mask method.

        Args:
            dataframe (|DataFrame|): The new dataframe
            _run_preprocessor (bool): Run the pre-processors on the dataframe.  This should only be done by
                                     methods such as get_next_frame.
        """
        self.debug("update_dataframe")
        geom_changed = False
        beam_changed = False
        mask_changed = False
        pdf = self.get_dataframe()  # Hang on to the previous dataframe
        if pdf is None:
            geom_changed = True
            beam_changed = True
            mask_changed = True
        else:
            if self.hold_pad_geometry:
                self.debug("Holding geometry from previous frame")
                dataframe.set_pad_geometry(pdf.get_pad_geometry())
            else:
                self.tic("Check for geometry change")
                geom_changed = pdf.get_pad_geometry().is_different(
                    dataframe.get_pad_geometry()
                )
                self.toc()
            if self.hold_beam:
                self.debug("Holding beam from previous frame")
                dataframe.set_beam(pdf.get_beam())
            else:
                self.tic("Check for beam change")
                beam = dataframe.get_beam()
                if beam is not None:
                    beam_changed = beam.is_different(pdf.get_beam())
                self.toc()
            if _run_preprocessor and self._dataframe_preprocessor is not None:
                self.debug("Running dataframe preprocessor")
                dataframe = self._dataframe_preprocessor(dataframe)
            if self.hold_mask and self.get_mask() is not None:
                self.debug("Holding mask from previous frame")
                dataframe.set_mask(
                    self.get_mask()
                )  # FIXME: Don't modify the original dataframe!
            else:
                if self.get_mask() is None:
                    mask_changed = True
                else:
                    self.tic("Check for mask change")
                    if not np.array_equiv(self.get_mask(), dataframe.get_mask_flat()):
                        mask_changed = True
                    self.toc()
        self.update_display(
            dataframe=dataframe,
            beam_changed=beam_changed,
            geom_changed=geom_changed,
            mask_changed=mask_changed,
        )

    def update_display(
        self, dataframe=None, data_changed=True, beam_changed=True, geom_changed=True, mask_changed=True
    ):
        r"""
        Update display, e.g. when moving to next frame.  The PAD data will always be updated, and will reflect
        the data in the current |DataFrame| unless it is explicitly provided in the method call.  The |Beam|,
        |PADGeometryList|, and mask will be updated but optionally these updates may be skipped using the relevant
        options.

        Side effects: self.dataframe might be modified, if a |DataFrame| is provided.  When updating

        Arguments:
            dataframe (|DataFrame|): Optional input dataframe.  Will use self.dataframe otherwise.
        """
        self.debug("update_display")
        if dataframe is not None:
            self.set_dataframe(dataframe)
        if beam_changed:
            self.update_beam()
        if geom_changed:
            self.update_pad_geometry()
        if mask_changed:
            self.update_masks()
        if data_changed:
            self.update_pads()
        self.set_title()
        self._update_status_string()
        self.mouse_moved(self._evt)

    def save_pad_geometry(self):
        r"""Save list of pad geometry specifications in json format."""
        self.debug("save_pad_geometry")
        file_name, file_type = QFileDialog.getSaveFileName(
            self.main_window,
            "Save PAD Geometry",
            "geometry",
            "reborn PAD Geometry File (*.json);;",
        )
        if file_name == "":
            return
        self.debug(f"Saving PAD geometry to file: {file_name}")
        detector.save_pad_geometry_list(file_name, self.get_pad_geometry())

    def load_pad_geometry(self):
        r"""Load list of pad geometry specifications in json format."""
        self.debug("load_pad_geometry")
        file_name, file_type = QFileDialog.getOpenFileName(
            self.main_window,
            "Load PAD Geometry",
            "geometry",
            "reborn PAD Geometry File (*.json);;",
        )
        if file_name == "":
            return
        self.debug(f"Loading PAD geometry from file: {file_name}")
        try:
            geom = detector.load_pad_geometry_list(file_name)
        except:
            from ...external.crystfel import geometry_file_to_pad_geometry_list
            geom = geometry_file_to_pad_geometry_list(file_name)
        self.update_pad_geometry(geom)

    def save_beam(self):
        r"""Save beam specifications in json format."""
        self.debug("save_beam")
        file_name, file_type = QFileDialog.getSaveFileName(
            self.main_window,
            "Save Beam",
            "beam",
            "reborn Beam File (*.json);;",
        )
        if file_name == "":
            return
        self.debug(f"Saving Beam to file: {file_name}")
        source.save_beam(self.get_beam(), file_name)

    def load_beam(self):
        r"""Load beam specifications in json format."""
        self.debug("load_beam")
        file_name, file_type = QFileDialog.getOpenFileName(
            self.main_window,
            "Load Beam",
            "beam",
            "reborn Beam File (*.json);;",
        )
        if file_name == "":
            return
        self.debug("Loading Beam from file: {file_name}")
        beam = source.load_beam(file_name)
        self.update_beam(beam)

    @staticmethod
    def vector_to_view_coords(vec):
        r"""If you have a vector (or vectors) pointing in some direction in space, this function provides the
        point at which it intercepts with the view plane (the plane that is 1 meter away from the origin).
        """
        vec = np.atleast_2d(vec)
        vec = (vec.T / np.squeeze(vec[:, 2])).T.copy()
        vec[:, 2] = 1
        return np.squeeze(vec)

    def get_pad_coords_from_view_coords(self, view_coords):
        r"""Get PAD coordinates (slow-scan index, fast-scan index, PAD index) from view coordinates.  The view
        coordinates correspond to the plane that is 1 meter away from the origin."""
        self.debug("get_pad_coords_from_view_coords", level=3)
        x = view_coords[0]
        y = view_coords[1]
        pad_idx = None
        geom = self.get_pad_geometry()
        for n in range(len(geom)):
            vec = np.array(
                [x, y, 1]
            )  # This vector points from origin to the plane of the scene
            ss_idx, fs_idx = geom[n].vectors_to_indices(vec, insist_in_pad=True)
            if np.isfinite(ss_idx[0]) and np.isfinite(fs_idx[0]):
                pad_idx = n
                break
        return ss_idx, fs_idx, pad_idx

    def get_pad_coords_from_mouse_pos(self):
        r"""Maps mouse cursor position to view coordinates, then maps the view coords to PAD coordinates."""
        self.debug("get_pad_coords_from_mouse_pos", level=2)
        view_coords = self.get_view_coords_from_mouse_pos()
        ss_idx, fs_idx, pad_idx = self.get_pad_coords_from_view_coords(view_coords)
        self.debug("PAD coords: " + (ss_idx, fs_idx, pad_idx).__str__(), level=3)
        return ss_idx[0], fs_idx[0], pad_idx

    def get_view_coords_from_mouse_pos(self):
        r"""Map the current mouse cursor position to the display plane situated 1 meter from the interaction point."""
        self.debug("get_view_coords_from_mouse_pos", level=2)
        if self._evt is None:  # Note: self.evt is updated by _mouse_moved
            return 0, 0
        sc = self.viewbox.mapSceneToView(self._evt[0])
        self.debug("\tview coords: " + sc.__str__(), level=2)
        return sc.x(), sc.y()

    def get_view_bounding_rect(self):
        r"""Bounding rectangle of everything presently visible, in view (i.e real-space, 1-meter plane) coordinates."""
        vb = self.viewbox
        return (
            vb.mapSceneToView(vb.mapToScene(vb.rect()).boundingRect())
            .boundingRect()
            .getRect()
        )

    def mouse_moved(self, evt):
        r"""Updates the status string with info about mouse position (e.g. data value under cursor)."""
        self.debug("mouse_moved", level=2)
        self.debug("mouse position: " + evt.__str__(), level=2)
        if evt is None:
            return
        self._evt = evt
        ss, fs, pid = self.get_pad_coords_from_mouse_pos()
        if pid is None:
            self._status_string_mouse = ""
        else:
            fs = int(np.round(fs))
            ss = int(np.round(ss))
            intensity = self.get_pad_display_data(as_list=True)[pid][ss, fs]
            self._status_string_mouse = "| PAD %2d  |  Pix %4d,%4d  |  Val=%8g  |" % (
                pid,
                ss,
                fs,
                intensity,
            )
            df = self.get_dataframe()
            if df.has_beam():
                q = df.get_q_mags_list()[pid][ss, fs] / 1e10
                self._status_string_mouse += " q=%8g/A |" % (q,)
                self._status_string_mouse += " d=%8g A |" % (2 * np.pi / q,)
        self._update_status_string()

    def add_ring(self, radius=None, angle=None, q_mag=None, d_spacing=None, pen=None):
        self.debug("add_ring")  # , radius, angle, q_mag, d_spacing, pen)
        beam = self.get_beam()
        if angle is not None:
            if angle >= np.pi:
                return False
            radius = np.tan(angle)
        if q_mag is not None:
            if beam is None:
                return False
            a = q_mag * beam.wavelength / (4 * np.pi)
            angle = 2 * np.arcsin(a)
            if angle >= np.pi:
                return False
            radius = np.tan(angle)
        if d_spacing is not None:
            if beam is None:
                return False
            a = beam.wavelength / (2 * d_spacing)
            angle = 2 * np.arcsin(a)
            if angle >= np.pi:
                return False
            q_mag = 4 * np.pi / d_spacing
            radius = np.tan(angle)
        if pen is None:
            pen = self._ring_pen
        ring = pg.CircleROI(
            pos=[-radius, -radius], size=2 * radius, pen=pen, movable=False
        )
        ring.q_mag = q_mag
        self._rings.append(ring)
        self.viewbox.addItem(ring)
        for handle in ring.handles:
            ring.removeHandle(handle["item"])
        return True

    def update_rings(self):
        r"""Update rings (needed if the |Beam| changes)."""
        self.debug("update_rings")
        beam = self.get_beam()
        if self._rings:
            for ring in self._rings:
                if ring.q_mag:
                    r = np.tan(
                        2 * np.arcsin(ring.q_mag * beam.wavelength / (4 * np.pi))
                    )
                    ring.setState({"pos": [-r, -r], "size": 2 * r, "angle": 0})

    def remove_rings(self):
        self.debug("remove_rings")
        if self._rings is None:
            return
        for i in range(0, len(self._rings)):
            if self._rings[i].scene():
                self.viewbox.removeItem(self._rings[i])

    def show_grid(self):
        self.debug("show_grid")
        if self._grid is None:
            self._grid = pg.GridItem()
        self.viewbox.addItem(self._grid)

    def hide_grid(self):
        self.debug("hide_grid")
        if self._grid is not None:
            self.viewbox.removeItem(self._grid)
            self._grid = None

    def toggle_grid(self):
        self.debug("toggle_grid")
        if self._grid is None:
            self.show_grid()
        else:
            self.hide_grid()

    def show_pad_border(self, n, pen=None):
        self.debug("show_pad_border")
        if self._pad_image_items is None:
            return
        if pen is None:
            pen = pg.mkPen([0, 255, 0], width=2)
        self._pad_image_items[n].setBorder(pen)

    def hide_pad_border(self, n):
        self.debug("hide_pad_border")
        if self._pad_image_items is None:
            return
        self._pad_image_items[n].setBorder(None)

    def show_pad_borders(self, pen=None):
        self.debug("show_pad_borders")
        if self._pad_image_items is None:
            return
        if pen is None:
            pen = pg.mkPen([0, 255, 0], width=1)
        for image in self._pad_image_items:
            image.setBorder(pen)

    def hide_pad_borders(self):
        self.debug("hide_pad_borders")
        if self._pad_image_items is None:
            return
        for image in self._pad_image_items:
            image.setBorder(None)

    def show_history_next(self):
        self.debug("show_history_next")
        if self._fetching_frame:
            self.debug("busy")
            return
        self._fetching_frame = True
        dataframe = self._frame_getter.get_history_next()
        self.update_dataframe(dataframe=dataframe, _run_preprocessor=True)
        self._fetching_frame = False

    def show_history_previous(self):
        self.debug("show_history_previous")
        if self._fetching_frame:
            self.debug("busy")
            return
        self._fetching_frame = True
        dataframe = self._frame_getter.get_history_previous()
        self.update_dataframe(dataframe=dataframe, _run_preprocessor=True)
        self._fetching_frame = False

    def show_next_frame(self, skip=1):
        self.debug("show_next_frame")
        if self._fetching_frame:
            self.debug("busy")
            return
        self._fetching_frame = True
        dataframe = self._frame_getter.get_next_frame(skip=skip)
        self.update_dataframe(dataframe=dataframe, _run_preprocessor=True)
        self._fetching_frame = False
        # self.app.processEvents()

    def show_previous_frame(self, skip=1):
        self.debug("show_previous_frame")
        if self._fetching_frame:
            self.debug("busy")
            return
        self._fetching_frame = True
        dataframe = self._frame_getter.get_previous_frame(skip=skip)
        self.update_dataframe(dataframe=dataframe, _run_preprocessor=True)
        self._fetching_frame = False

    def show_random_frame(self):
        self.debug("show_random_frame")
        if self._fetching_frame:
            self.debug("busy")
            return
        self._fetching_frame = True
        dataframe = self._frame_getter.get_random_frame()
        self.update_dataframe(dataframe=dataframe, _run_preprocessor=True)
        self._fetching_frame = False

    def show_frame(self, frame_number=0):
        self.debug("show_frame")
        if self._fetching_frame:
            self.debug("busy")
            return
        self._fetching_frame = True
        dataframe = self._frame_getter.get_frame(frame_number=frame_number)
        self.update_dataframe(dataframe=dataframe, _run_preprocessor=True)
        self._fetching_frame = False

    def show_first_frame(self):
        self.debug("show_first_frame")
        if self._fetching_frame:
            self.debug("busy")
            return
        self._fetching_frame = True
        dataframe = self._frame_getter.get_first_frame()
        self.update_dataframe(dataframe=dataframe, _run_preprocessor=True)
        self._fetching_frame = False

    def reload_current_frame(self):
        self.debug("show_first_frame")
        current_frame = self._frame_getter.current_frame
        self.show_frame(current_frame)

    def add_plot_item(self, *args, **kargs):
        r"""
        Example: self.add_plot_item(x, y, pen=pg.mkPen(width=3, color='g'))
        """
        self.debug("add_plot_item")
        if self._plot_items is None:
            self._plot_items = []
        plot_item = pg.PlotDataItem(*args, **kargs)
        self._plot_items.append(plot_item)
        self.viewbox.addItem(plot_item)
        return plot_item

    def add_scatter_plot(self, *args, **kargs):
        self.debug("add_scatter_plot")
        if self._scatter_plots is None:
            self._scatter_plots = []
        scat = pg.ScatterPlotItem(*args, **kargs)
        self._scatter_plots.append(scat)
        self.viewbox.addItem(scat)

    def remove_scatter_plots(self):
        self.debug("remove_scatter_plots")
        if self._scatter_plots is not None:
            for scat in self._scatter_plots:
                self.viewbox.removeItem(scat)
        self._scatter_plots = None

    # def load_geometry_file(self):
    #     self.debug("load_geometry_file")
    #     file_name, file_type = QFileDialog.getOpenFileName(
    #         self.main_window,
    #         "Load geometry file",
    #         "",
    #         "CrystFEL geom (*.geom)",
    #     )
    #     if file_name == "":
    #         return
    #     if file_type == "CrystFEL geom (*.geom)":
    #         print("CrystFEL geom not implemented.")
    #         pass

    def load_pickled_dataframe(self, file_name):
        r"""Load data in pickle format.  Should be a dictionary with keys:

        mask_data, pad_display_data, beam, pad_geometry
        """
        self.debug("load_pickled_dataframe")
        df = load_pickled_dataframe(filename=file_name)
        self.update_dataframe(df)
        self.debug(f"Loaded pickle from {file_name}")

    def save_pickled_dataframe(self, file_name):
        r"""Save dataframe in pickle format.  It is a dictionary with the keys:

        mask_data, pad_display_data, beam, pad_geometry
        """
        self.debug("save_pickled_dataframe")
        df = self.get_dataframe()
        save_pickled_dataframe(df, file_name)
        self.debug(f"Saved pickle to {file_name}")

    def load_npy_file(self, file_name):
        r"""Load data in numpy (npy) format.  Note that this format only contains
        the diffraction intensity data; there is no geometry or beam information.
        """
        self.debug("load_npy_file")
        data = np.load(file_name)
        df = DataFrame(raw_data=data, pad_geometry=self.get_pad_geometry(), beam=self.get_beam(), mask=self.get_mask())
        self.update_dataframe(df)
        self.debug(f"Loaded numpy array from {file_name}")

    def save_npy_file(self, file_name):
        r"""Save data in numpy (npy) format.  Note that this format only contains
        the diffraction intensity data; there is no geometry or beam information.
        """
        self.debug("save_npy_file")
        data = self.get_pad_display_data(as_list=False)
        data = self.get_pad_geometry().reshape(data)
        np.save(file_name, data)
        self.debug(f"Save numpy array to {file_name}")

    def open_data_file_dialog(self):
        self.debug("open_data_file_dialog")
        file_name, file_type = QFileDialog.getOpenFileName(
            self.main_window,
            "Load data file",
            "",
            "Python Pickle (*.pkl);;Numpy File (*.npy)",
        )
        if file_name == "":
            return
        if file_type == "Python Pickle (*.pkl)":
            self.load_pickled_dataframe(file_name)
        if file_type == "Numpy File (*.npy)":
            self.load_npy_file(file_name)

    def save_data_file_dialog(self):
        r"""Save list of masks in pickle or reborn mask format."""
        self.debug("save_data_file_dialog")
        file_name, file_type = QFileDialog.getSaveFileName(
            self.main_window,
            "Save Data Frame",
            "data",
            "Python Pickle (*.pkl);;Numpy File (*.npy)",
        )
        if file_name == "":
            return
        if file_type == "Python Pickle (*.pkl)":
            self.save_pickled_dataframe(file_name)
        if file_type == "Numpy File (*.npy)":
            self.save_npy_file(file_name)

    def vector_coords_to_2d_display_coords(self, vecs):
        r"""Convert 3D vector coords to the equivalent coords in the 2D display plane.  This corresponds to ignoring
        the "z" coordinate, and scaling the "x,y" coordinates to that of an equivalent detector located at a distance
        of 1 meter from the origin.  Simply put: remove the z component, divide the x,y components by the z component
        """
        return (vecs[:, 0:2].T / vecs[:, 2]).T.copy()

    def panel_scatter_plot(self, panel_number, ss_coords, fs_coords, style=None):
        r"""Scatter plot points given coordinates (i.e. indices) corresponding to a particular panel.  This will
        take care of the re-mapping to the display coordinates."""
        if style is None:
            style = self._scatterplot_style
        vecs = self.pad_geometry[panel_number].indices_to_vectors(ss_coords, fs_coords)
        vecs = self.vector_coords_to_2d_display_coords(vecs)
        self.add_scatter_plot(vecs[:, 0], vecs[:, 1], **style)

    @property
    def display_editor_widget(self):
        if self._display_editor_widget is None:
            self._display_editor_widget = widgets.display_editor.Widget(padview=self)
        return self._display_editor_widget

    @property
    def mask_editor_widget(self):
        if self._mask_editor_widget is None:
            self._mask_editor_widget = widgets.mask_editor.Widget(padview=self)
        return self._mask_editor_widget

    @property
    def frame_navigator_widget(self):
        if self._frame_navigator_widget is None:
            self._frame_navigator_widget = widgets.frame_navigator.Widget(padview=self)
        return self._frame_navigator_widget

    @property
    def ring_editor_widget(self):
        if self._ring_editor_widget is None:
            self._ring_editor_widget = widgets.ring_editor.Widget(padview=self)
        return self._ring_editor_widget

    @property
    def runstats_widget(self):
        if self._runstats_widget is None:
            self._runstats_widget = widgets.run_stats.Widget(padview=self)
        return self._runstats_widget

    @property
    def display_options_widget(self):
        if self._display_options_widget is None:
            self._display_options_widget = widgets.display_options.Widget(padview=self)
        return self._display_options_widget

    @property
    def edit_geometry_widget(self):
        if self._edit_geometry_widget is None:
            self._edit_geometry_widget = widgets.edit_geometry.Widget(padview=self)
        return self._edit_geometry_widget

    @property
    def swaxs_simulator_widget(self):
        if self._swaxs_simulator_widget is None:
            self._swaxs_simulator_widget = widgets.swaxs_simulator.Widget(padview=self)
        return self._swaxs_simulator_widget

    @property
    def beam_editor_widget(self):
        if self._beam_editor_widget is None:
            self._beam_editor_widget = widgets.beam_editor.Widget(padview=self)
        return self._beam_editor_widget

    def import_plugin_module(self, module_name):
        self.debug("import_plugin_module")
        if module_name in self._plugins:
            return self._plugins[
                module_name
            ]  # Check if module already imported and cached
        module_path = __package__ + ".plugins." + module_name
        if module_path[-3:] == ".py":
            module_path = module_path[:-3]
        module_path = module_path.replace("/", ".")
        self.debug("\tImporting plugin: %s" % module_path)
        module = importlib.import_module(module_path)  # Attempt to import
        if self._plugins is None:
            self._plugins = {}
        self._plugins[module_name] = module  # Cache the module
        return module

    def run_plugin(self, module_name):
        self.debug("run_plugin " + module_name)
        if self._plugins is None:
            self._plugins = {}
        if not module_name in self._plugins.keys():
            module = self.import_plugin_module(
                module_name
            )  # Get the module (import or retrieve from cache)
        else:
            module = self._plugins[module_name]
        if hasattr(
            module, "plugin"
        ):  # If the module has a simple plugin function, run the function and return
            module.plugin(self)
            return
        # If the plugin has a widget already, show it:
        if module_name + ".widget" in self._plugins.keys():
            show_and_raise(self._plugins[
                module_name + ".widget"
            ])  # Check if a widget is already cached.  If so, show it.
            return
        # If the plugin is a class with an action method, run it:
        if module_name + ".action" in self._plugins.keys():
            self._plugins[module_name + ".action"]()
            return
        if hasattr(module, "Plugin"):
            plugin_instance = module.Plugin(
                self
            )  # Check if the plugin defines a class.  If so, create an instance.
            self._plugins[module_name + ".class_instance"] = (
                plugin_instance  # Cache the instance
            )
            self.debug("\tCreated plugin class instance.")
            if hasattr(plugin_instance, "widget"):
                self._plugins[module_name + ".widget"] = (
                    plugin_instance.widget
                )  # Get the widget and cache it.
                # show_and_raise(plugin_instance.widget)  # Show the widget.
                self.debug("\tShowing widget.")
            if hasattr(plugin_instance, "action"):
                self._plugins[module_name + ".action"] = (
                    plugin_instance.action
                )  # Get the widget and cache it.
                self.debug("\tConfiguring action method.")
            return
        self.debug("\tPlugin module has no functions or classes defined.")
        return

    # FIXME: Figure out how to make start work without blocking the ipython terminal.
    def start(self):
        self.debug("start")
        self.app.aboutToQuit.connect(self.stop)
        if self.main:
            self.app.exec()

    def stop(self):
        self.debug("stop")

    def show(self):
        self.debug("show")
        show_and_raise(self.main_window)
        # self.main_window.callback_pb_load()

    def save_screenshot_dialog(self):
        self.debug("save_screenshot_dialog")
        dt = datetime.datetime.now().strftime("%Y-%m-%d_%H.%M.%S")
        filepath, _ = self.save_file_dialog(
            title="Screenshot", default=f"padview_screenshot_{dt}.jpg"
        )
        if filepath:
            self.debug(f"    Saving screenshot to {filepath}")
            self.save_screenshot(filepath)
        else:
            self.debug("    No file selected")

    def save_screenshot(self, filename):
        self.debug("save_screenshot")
        p = self.main_window.grab()
        p.save(filename)

    def save_file_dialog(self, title="Filepath", default="file", types=None):
        self.debug("save_file_dialog")
        name, type = QFileDialog.getSaveFileName(
            self.main_window, title, default, types
        )
        return name, type

    def tic(self, msg):
        self.debug("tic " + "  " * len(self._tic_times) + msg)
        self._tic_times.append(time.time())

    def toc(self):
        if len(self._tic_times) == 0:
            print("Something is wrong with tic/toc")
        tic = self._tic_times.pop()
        self.debug(
            "toc " + "  " * len(self._tic_times) + f"{(time.time()-tic)*1000:7.2f} ms."
        )

    def histogram_widget(self, histdat, bin_range, extra={}):
        r"""Experimental.  Opens some plots that show pixel histograms.

        Arguments:
            histdat (|ndarray|): A 2D array with pixel histograms.  First index is pixel index.
            bin_range (tuple): The first and last values corresponding to bin centers.  Assumes regular binning.
        """
        # TODO: Combine the plots into a single window.
        from scipy.signal import find_peaks, peak_prominences

        geom = self.get_pad_geometry()
        assert histdat.shape[0] == geom.n_pixels
        nb = histdat.shape[1]
        mn, mx = bin_range
        x = np.linspace(mn, mx, nb)
        h = np.mean(histdat, axis=0)
        histplot = pg.plot()
        histplot.setLabel("bottom", "Intensity Value")
        histplot.setLabel("left", "log10(Counts+1)")
        histplot.setTitle("Pixel Histogram")
        mean_histplot = histplot.plot(x, np.log10(h + 1), pen="g")
        pixel_histplot = histplot.plot(x, np.log10(h + 1))
        imv = imview(
            np.log10(histdat + 1),
            fs_lims=[mn, mx],
            fs_label="Intensity",
            ss_label="Flat Pixel Index",
            title="Intensity Histogram",
        )
        line = imv.add_line(vertical=True, movable=True)

        self.hp = None

        def update_histplot(line):
            i = int(np.round(line.value()))
            i = max(i, 0)
            i = min(i, histdat.shape[0] - 1)
            h = histdat[i, :]
            pixel_histplot.setData(x, np.log10(h + 1))
            peaks, properties = find_peaks(h)
            prominences = peak_prominences(h, peaks)[0]
            if peaks.size > 0:
                if self.hp is None:
                    self.hp = histplot.plot(
                        x[peaks],
                        np.log10(h[peaks] + 1),
                        pen=None,
                        symbolBrush="r",
                        symbolPen=None,
                        symbol="o",
                    )
                else:
                    self.hp.setData(
                        x[peaks],
                        np.log10(h[peaks] + 1),
                        symbolSize=20 * prominences / np.max(prominences),
                    )

        flat_indices = np.arange(0, geom.n_pixels)
        flat_indices = geom.split_data(flat_indices)

        def set_line_index(evt):
            if evt is None:
                print("Event is None")
                return
            ss, fs, pid = self.get_pad_coords_from_mouse_pos()
            if pid is None:
                pass
            else:
                fs = int(np.round(fs))
                ss = int(np.round(ss))
                line.setValue(flat_indices[pid][ss, fs])
            pass

        line.sigPositionChanged.connect(update_histplot)
        self.proxy2 = pg.SignalProxy(
            self.viewbox.scene().sigMouseMoved, rateLimit=30, slot=set_line_index
        )

        # peaks, properties = find_peaks(h)
        # print("peak prominences", peak_prominences(h, peaks)[0])
        # if peaks.size > 0:
        #     histplot.plot(
        #         x[peaks],
        #         np.log10(h[peaks] + 1),
        #         pen=None,
        #         symbolBrush="r",
        #         symbolPen=None,
        #         symbol="o",
        #     )
        self.histogram_line = line
        self.histogram_imv = imv
        self.histogram_mean_histplot = mean_histplot
        self.histogram_pixel_histplot = pixel_histplot
        self.histogram_histplot = histplot


def view_pad_data(data=None, pad_geometry=None, show=True, title=None, **kwargs):
    r"""Convenience function that creates a PADView instance and starts it."""
    pv = PADView(data=data, pad_geometry=pad_geometry, **kwargs)
    if title is not None:
        pv.set_title(title)
    if show:
        pv.start()


class DummyFrameGetter(FrameGetter):
    r"""Makes a FrameGetter for a single DataFrame."""

    def __init__(self, data=None, pad_geometry=None, mask=None, beam=None, **kwargs):
        r"""
        Ideally, a |DataFrame| instance is provided, which has |PADGeometry|, |Beam|, etc. information.

        Alternatively, data can be provided as a single |ndarray|, as a list of |ndarray|, or, for legacy purposes,
        as a dictionary with the 'pad_data' key in it.  If so, then you should at least provide the |PADGeometry| and
        |Beam| information, else some garbage numbers will be created for you.

        Args:
            dataframe:
            data:
            pad_geometry:
        """
        super().__init__()
        if isinstance(data, DataFrame):
            self._dataframe = data
            self._dataframe.validate()
        else:
            # This is a mess.  It is exactly the reason why the |DataFrame| class was created...
            if mask is None:
                mask = kwargs.pop("mask_data", None)
                if mask is not None:
                    utils.depreciate(
                        'The "mask_data" keyword argument is no longer used.  Use "mask" instead.'
                    )
            if data is None:
                data = kwargs.pop("pad_data", None)
                if data is not None:
                    utils.depreciate(
                        'The "pad_data" keyword argument is no longer used.  Use "data" instead.'
                    )
            if data is None:
                data = kwargs.pop("raw_data", None)
                if data is not None:
                    utils.depreciate(
                        'The "raw_data" keyword argument is no longer used.  Use "data" instead.'
                    )
            if data is None:
                # This should never happen...
                logger.warning("No data was provided.  Making up some *GARBAGE* data.")
                if pad_geometry is None:
                    data = np.random.rand(100, 100)
                else:
                    pad_geometry = detector.PADGeometryList(pad_geometry)
                    data = np.random.rand(pad_geometry.n_pixels)
            if isinstance(data, dict):
                data = data["pad_data"]
            if pad_geometry is None:
                if isinstance(data, np.ndarray):
                    data = [data]
                logger.warning(
                    "Making up some *GARBAGE* PAD geometry because you did not provide a geometry."
                )
                pad_geometry = []
                shft = 0
                for dat in data:
                    if len(dat.shape) == 1:
                        logger.warning(
                            "Your PAD data is a 1D array and you did not provide geometry information."
                        )
                    pad = detector.PADGeometry(
                        distance=1.0, pixel_size=1.0, shape=dat.shape
                    )
                    pad.t_vec[0] += shft
                    shft += pad.shape()[0]
                    pad_geometry.append(pad)
            pad_geometry = detector.PADGeometryList(pad_geometry)
            data = pad_geometry.concat_data(data)
            # Handling of beam info:
            if beam is None:
                logger.warning(
                    "Making up some *GARBAGE* beam information because you provided no specification."
                )
                beam = source.Beam(photon_energy=9000 * 1.602e-19)
            self._dataframe = DataFrame(
                raw_data=data, pad_geometry=pad_geometry, beam=beam, mask=mask
            )
        self.n_frames = 1

    def get_data(self, frame_number=0):
        return self._dataframe


def show_and_raise(widget):
    widget.show()
    widget.raise_()
