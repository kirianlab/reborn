# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from ....external.pyqt import QFont, QWidget, QCheckBox, QHBoxLayout, QVBoxLayout, QLabel, QPushButton, QComboBox
from ....gui.misc.spinbox import BetterDoubleSpinBox


class Widget(QWidget):
    def __init__(self, *args, padview=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.padview = padview
        bold = QFont()
        bold.setBold(True)
        italic = QFont()
        italic.setItalic(True)
        # ====== Title ============================
        self.setWindowTitle('Display Options')
        self.layout = QVBoxLayout()
        self.reload_button = QPushButton("Reload Current Dataframe")
        self.reload_button.pressed.connect(self.reload_dataframe)
        self.layout.addWidget(self.reload_button)
        # ======  Histogram  ================
        label = QLabel('Histogram')
        label.setFont(bold)
        self.layout.addWidget(label)
        layout = QHBoxLayout()
        self.auto_hist_checkbox = QCheckBox()
        self.auto_hist_checkbox.setText("Auto-update histogram")
        if self.padview.show_histogram is True:
            self.auto_hist_checkbox.setChecked(True)
        self.auto_hist_checkbox.toggled.connect(self.auto_hist_checkbox_action)
        layout.addWidget(self.auto_hist_checkbox)
        self.layout.addLayout(layout)
        # ======  Dataframe  ================
        label = QLabel('Dataframe')
        label.setFont(bold)
        self.layout.addWidget(label)
        label = QLabel('On loading new dataframe...')
        label.setFont(italic)
        self.layout.addWidget(label)
        self.auto_geom_checkbox = QCheckBox()
        self.auto_geom_checkbox.setText("Auto-update geometry")
        if self.padview.hold_pad_geometry is False:
            self.auto_geom_checkbox.setChecked(True)
        self.auto_geom_checkbox.toggled.connect(self.auto_geom_checkbox_action)
        self.layout.addWidget(self.auto_geom_checkbox)
        self.auto_beam_checkbox = QCheckBox()
        self.auto_beam_checkbox.setText("Auto-update beam")
        if self.padview.hold_beam is False:
            self.auto_beam_checkbox.setChecked(True)
        self.auto_beam_checkbox.toggled.connect(self.auto_beam_checkbox_action)
        self.layout.addWidget(self.auto_beam_checkbox)
        self.auto_mask_checkbox = QCheckBox()
        self.auto_mask_checkbox.setText("Auto-update mask")
        if self.padview.hold_mask is False:
            self.auto_mask_checkbox.setChecked(True)
        self.auto_mask_checkbox.toggled.connect(self.auto_mask_checkbox_action)
        self.layout.addWidget(self.auto_mask_checkbox)
        # ======  Framegetter  ================
        label = QLabel('Framegetter')
        label.setFont(bold)
        self.layout.addWidget(label)
        label = QLabel('Experimental features:')
        label.setFont(italic)
        self.layout.addWidget(label)
        self.fgcache_checkbox = QCheckBox()
        self.fgcache_checkbox.setText("Pre-cache dataframes")
        if self.padview._frame_getter._cache_forward is True:
            self.fgcache_checkbox.setChecked(True)
        self.fgcache_checkbox.toggled.connect(self.fgcache_checkbox_action)
        self.layout.addWidget(self.fgcache_checkbox)
        # ======  Images  ================
        label = QLabel('Downsampling')
        label.setFont(bold)
        self.layout.addWidget(label)
        hbox = QHBoxLayout()
        label = QLabel('Downsample with:')
        hbox.addWidget(label)
        self.downsampling_combobox = QComboBox()
        self.downsampling_combobox.addItem("Nearest (fastest)")
        self.downsampling_combobox.addItem("Min")
        self.downsampling_combobox.addItem("Max")
        self.downsampling_combobox.addItem("Mean")
        self.downsampling_combobox.addItem("Median")
        self.downsampling_combobox.currentIndexChanged.connect(self.downsampling_action)
        hbox.addWidget(self.downsampling_combobox)
        self.layout.addLayout(hbox)
        self.setLayout(self.layout)

    def reload_dataframe(self):
        self.padview.reload_current_frame()

    def auto_hist_checkbox_action(self):
        # print('auto_hist_checkbox_action')
        if self.auto_hist_checkbox.isChecked():
            self.padview.show_histogram = True
        else:
            self.padview.show_histogram = False

    def auto_geom_checkbox_action(self):
        # print('auto_geom_checkbox_action')
        if self.auto_geom_checkbox.isChecked():
            self.padview.hold_pad_geometry = False
        else:
            self.padview.hold_pad_geometry = True

    def auto_beam_checkbox_action(self):
        # print('auto_beam_checkbox_action')
        if self.auto_beam_checkbox.isChecked():
            self.padview.hold_beam = False
        else:
            self.padview.hold_beam = True

    def auto_mask_checkbox_action(self):
        # print('auto_mask_checkbox_action')
        if self.auto_mask_checkbox.isChecked():
            self.padview.hold_mask = False
        else:
            self.padview.hold_mask = True

    def downsampling_action(self, index):
        selected_option = self.downsampling_combobox.itemText(index)
        func = None
        if "nearest" in selected_option.lower():
            func = None
        if "min" in selected_option.lower():
            func = np.min
        if "max" in selected_option.lower():
            func = np.max
        if "mean" in selected_option.lower():
            func = np.mean
        if "median" in selected_option.lower():
            func = np.median
        self.padview.downsample_function = func
        if self.padview._pad_image_items is None:
            return
        b = func is not None
        for im in self.padview._pad_image_items:
            im.downsample_function = func
            im.setOpts(autoDownsample=b)
        for im in self.padview._mask_image_items:
            im.downsample_function = np.max
            im.setOpts(autoDownsample=b)

    def fgcache_checkbox_action(self):
        if self.fgcache_checkbox.isChecked():
            self.padview._frame_getter._cache_forward = True
        else:
            self.padview._frame_getter._cache_forward = False
