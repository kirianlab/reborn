# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import os
import tempfile
from pyqtgraph.Qt.QtGui import QFont
from pyqtgraph.Qt.QtWidgets import (
    QWidget,
    QCheckBox,
    QHBoxLayout,
    QVBoxLayout,
    QLabel,
    QPushButton,
    QFileDialog,
    QLineEdit,
)
from ....gui.misc.spinbox import BetterDoubleSpinBox
from ....simulate.engines import SAXSPatternFrameGetter
from ....target.crystal import get_pdb_file

class Widget(QWidget):
    _concentration = 10
    _thickness = 5e-6
    def __init__(self, *args, padview=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.padview = padview
        levels = self.padview.get_levels()
        self.setWindowTitle("SWAXS Simulator")
        self.layout = QVBoxLayout()
        # bold = QFont()
        # bold.setBold(True)
        layout = QHBoxLayout()
        self.file_path_label = QLabel("PDB File:")
        layout.addWidget(self.file_path_label)
        self.file_path_edit = QLineEdit("2LYZ")
        layout.addWidget(self.file_path_edit)
        self.pick_button = QPushButton("Choose...")
        self.pick_button.clicked.connect(self.pick_file)
        layout.addWidget(self.pick_button)
        self.layout.addLayout(layout)
        layout = QHBoxLayout()
        self.temp_path_label = QLabel("Temp Directory:")
        layout.addWidget(self.temp_path_label)
        self.temp_path_edit = QLineEdit(os.path.join(tempfile.gettempdir(), "reborn"))
        layout.addWidget(self.temp_path_edit)
        self.temp_button = QPushButton("Choose...")
        self.temp_button.clicked.connect(self.pick_directory)
        layout.addWidget(self.temp_button)
        self.layout.addLayout(layout)
        self.run_button = QPushButton("Run simulation")
        self.run_button.clicked.connect(self.run)
        self.layout.addWidget(self.run_button)
        # self.layout.addLayout(layout)
        self.setLayout(self.layout)

    def pick_file(self):
        file_dialog = QFileDialog()
        file_dialog.setFileMode(QFileDialog.ExistingFile)
        file_path, _ = file_dialog.getOpenFileName(self, "Select File")
        if file_path:
            self.file_path_edit.setText(file_path)

    def pick_directory(self):
        directory_dialog = QFileDialog()
        directory_dialog.setFileMode(QFileDialog.DirectoryOnly)
        directory_path = directory_dialog.getExistingDirectory(self, "Select Directory")
        if directory_path:
            self.temp_path_edit.setText(directory_path)

    def run(self):
        geom = self.padview.get_pad_geometry()
        beam = self.padview.get_beam()
        pdb = self.file_path_edit.text()
        fg = SAXSPatternFrameGetter(
            pdb_file_path=pdb,
            beam=beam,
            pad_geometry=geom,
            concentration=self._concentration,
            thickness=self._thickness,
        )
        self.padview._frame_getter = fg
        self.padview.show_first_frame()
