# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

from reborn import source, detector
import numpy as np
import json
from ....external.pyqt import (
    QWidget,
    QFont,
    QKeySequence,
    QShortcut,
    QCheckBox,
    QHBoxLayout,
    QVBoxLayout,
    QGridLayout,
    QLabel,
    QPushButton,
    QComboBox,
    QTextEdit,
)
from ....gui.misc.spinbox import BetterDoubleSpinBox
from .... import utils
from ....const import eV


class Widget(QWidget):
    editor_widget = None

    def __init__(self, padview):
        super().__init__()
        self.padview = padview
        beam = padview.get_beam()
        if beam is None:
            beam = source.Beam()
        self.setWindowTitle("Beam Editor")
        bold = QFont()
        bold.setBold(True)
        italic = QFont()
        italic.setItalic(True)
        self.layout = QVBoxLayout()
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Beam direction vector:"))
        self.beam_vec_label = QLabel(f"{beam.beam_vec}")
        hlayout.addWidget(self.beam_vec_label)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Beam polarization vector:"))
        self.beam_pol_label = QLabel(f"{beam.e1_vec}")
        hlayout.addWidget(self.beam_pol_label)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Photon Energy (eV)"))
        self.photon_energy_spinbox = BetterDoubleSpinBox()
        self.photon_energy_spinbox.valueChanged.connect(self.update_photon_energy)
        self.photon_energy_spinbox.setMinimum(0)
        hlayout.addWidget(self.photon_energy_spinbox)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Wavelength (A)"))
        self.wavelength_spinbox = BetterDoubleSpinBox()
        self.wavelength_spinbox.setMinimum(0)
        self.wavelength_spinbox.valueChanged.connect(self.update_wavelength)
        hlayout.addWidget(self.wavelength_spinbox)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Pulse energy (mJ)"))
        self.pulse_energy_spinbox = BetterDoubleSpinBox()
        self.pulse_energy_spinbox.setMinimum(0)
        self.pulse_energy_spinbox.valueChanged.connect(self.update_pulse_energy)
        hlayout.addWidget(self.pulse_energy_spinbox)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Photons per pulse"))
        self.n_photons_spinbox = BetterDoubleSpinBox()
        self.n_photons_spinbox.setMinimum(0)
        self.n_photons_spinbox.valueChanged.connect(self.update_n_photons)
        hlayout.addWidget(self.n_photons_spinbox)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Beam diameter (nm)"))
        self.diameter_spinbox = BetterDoubleSpinBox()
        self.diameter_spinbox.setMinimum(0)
        self.diameter_spinbox.valueChanged.connect(self.update_diameter)
        hlayout.addWidget(self.diameter_spinbox)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Polarization angle (deg)"))
        self.polarization_spinbox = BetterDoubleSpinBox()
        # self.polarization_spinbox.setMinimum(0)
        # self.polarization_spinbox.setMaximum(90)
        self.polarization_spinbox.valueChanged.connect(self.update_polarization)
        hlayout.addWidget(self.polarization_spinbox)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel("Polarization weight"))
        self.polarization_weight_spinbox = BetterDoubleSpinBox()
        self.polarization_weight_spinbox.setMinimum(0)
        self.polarization_weight_spinbox.setMaximum(1)
        self.polarization_weight_spinbox.valueChanged.connect(self.update_polarization_weight)
        hlayout.addWidget(self.polarization_weight_spinbox)
        self.layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        self.clear_button = QPushButton("Clear Beam")
        self.clear_button.clicked.connect(self.clear_beam)
        hlayout.addWidget(self.clear_button)
        self.layout.addLayout(hlayout)
        self.setLayout(self.layout)
        self.update_displayed_values()
        self.padview.sig_beam_changed.connect(self.update_displayed_values)

    def get_beam(self, none_ok=True):
        beam = self.padview.get_beam()
        if not none_ok and beam is None:
            beam = source.Beam()
        return beam

    def update_displayed_values(self):
        beam = self.get_beam()
        if beam is None:
            self.photon_energy_spinbox.setValueSilently(0)
            self.wavelength_spinbox.setValueSilently(0)
            self.pulse_energy_spinbox.setValueSilently(0)
            self.n_photons_spinbox.setValueSilently(0)
            self.diameter_spinbox.setValueSilently(0)
            self.polarization_spinbox.setValueSilently(0)
            self.polarization_weight_spinbox.setValueSilently(0)
            self.beam_vec_label.setText(f"Undefined")
            self.beam_pol_label.setText(f"Undefined")
            return
        self.photon_energy_spinbox.setValueSilently(beam.photon_energy / eV)
        self.wavelength_spinbox.setValueSilently(beam.wavelength * 1e10)
        self.pulse_energy_spinbox.setValueSilently(beam.pulse_energy * 1e3)
        self.n_photons_spinbox.setValueSilently(beam.n_photons)
        self.diameter_spinbox.setValueSilently(beam.diameter_fwhm * 1e9)
        self.polarization_spinbox.setValueSilently(get_polarization_angle(beam) * 180/np.pi)
        self.polarization_weight_spinbox.setValueSilently(beam.polarization_weight)
        b = beam.beam_vec
        e = beam.e1_vec
        self.beam_vec_label.setText(f"{b[0]:0.4f}, {b[1]:0.4f}, {b[2]:0.4f}")
        self.beam_pol_label.setText(f"{e[0]:0.4f}, {e[1]:0.4f}, {e[2]:0.4f}")

    def update_polarization(self):
        beam = self.get_beam(none_ok=False)
        beam = set_polarization_angle(beam, self.polarization_spinbox.value() * np.pi/180)
        self.padview.update_beam(beam)

    def update_polarization_weight(self):
        beam = self.get_beam(none_ok=False)
        beam.polarization_weight = self.polarization_weight_spinbox.value()
        self.padview.update_beam(beam)

    def update_diameter(self):
        beam = self.get_beam(none_ok=False)
        beam.diameter_fwhm = self.diameter_spinbox.value() / 1e9
        self.padview.update_beam(beam)

    def update_photon_energy(self):
        beam = self.get_beam(none_ok=False)
        beam.photon_energy = self.photon_energy_spinbox.value() * eV
        self.padview.update_beam(beam)

    def update_wavelength(self):
        beam = self.get_beam(none_ok=False)
        beam.wavelength = self.wavelength_spinbox.value() / 1e10
        self.padview.update_beam(beam)

    def update_pulse_energy(self):
        beam = self.get_beam(none_ok=False)
        beam.pulse_energy = self.pulse_energy_spinbox.value() / 1e3
        self.padview.update_beam(beam)

    def update_n_photons(self):
        beam = self.get_beam(none_ok=False)
        beam.pulse_energy = self.n_photons_spinbox.value() * beam.photon_energy
        self.padview.update_beam(beam)

    def clear_beam(self):
        self.padview.set_beam(None)
        self.padview.update_beam()


def get_polarization_angle(beam):
    b = beam.beam_vec
    if b[0] != 0 or b[1] != 0:
        raise ValueError("Your beam is not in the z direction.  We do not presently handle arbitrary beam directions in PADView")
    e1 = beam.e1_vec
    ang = np.arctan2(e1[1], e1[0])
    ang = ((ang * 180 / np.pi) % 90) * np.pi/180
    return ang

def set_polarization_angle(beam, ang):
    ang = ((ang * 180 / np.pi) % 90) * np.pi/180
    beam.polarization_vec = np.array([np.cos(ang), np.sin(ang), 0])
    return beam
