# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

from ....external.pyqt import (
    QApplication,
    QFileDialog,
    QCheckBox,
    QVBoxLayout,
    QDialog,
    QPushButton,
)


class GetMaskFileDialog(QFileDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setFileMode(QFileDialog.ExistingFiles)
        self.combine_checkbox = QCheckBox("Combine Masks")
        self.combine_checkbox.setChecked(False)
        self.overwrite_checkbox = QCheckBox("Overwrite Existing Mask")
        self.overwrite_checkbox.setChecked(True)
        layout = self.layout()
        if layout is None:
            layout = QVBoxLayout()
            self.setLayout(layout)
        layout.addWidget(self.combine_checkbox)
        layout.addWidget(self.overwrite_checkbox)

    @property
    def combine(self):
        return self.combine_checkbox.isChecked()

    @property
    def overwrite(self):
        return self.overwrite_checkbox.isChecked()


# class GetPADGeometryFileDialog(QFileDialog):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         # self.setFileMode(QFileDialog.ExistingFiles)
#         layout = self.layout()
#         if layout is None:
#             layout = QVBoxLayout()
#             self.setLayout(layout)
#         standard_geom_button = QPushButton("Load Standard Detector Geometry")
#         standard_geom_button.clicked.connect(self.)
#         layout.addWidget(self.standard_geom_button)
#
#     def standard_geom
