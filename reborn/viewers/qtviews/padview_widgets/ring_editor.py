# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import sys
import time
import numpy as np
import pyqtgraph as pg
from reborn.external.pyqt import (
    Qt,
    QApplication,
    QWidget,
    QAbstractTableModel,
    pyqtSignal,
    QPen,
    QModelIndex,
    QItemDelegate,
    pyqtSlot,
    QVBoxLayout,
    QTableView,
    QPainter,
    QHeaderView,
    QPersistentModelIndex,
    QComboBox,
    QPushButton,
    QSizePolicy,
    DisplayRole,
    EditRole,
    Horizontal
)
from reborn.gui.misc.pen_editor import PenStylePreview, PenStyleEditor, pen_dialog


debug = 0


def dbgmsg(*args, **kwargs):
    if debug:
        print(*args, **kwargs)


ring_types = ["q=4πsin(θ/2)/λ [A]", "d=2π/q [A]", "r [mm @ 1m]", "θ [degrees]"]


def default_pen():
    return pg.mkPen(color="r", width=3)


class RingTableModel(QAbstractTableModel):
    data_changed = pyqtSignal()

    def __init__(self):
        dbgmsg("RingTableModel.__init__")
        super().__init__()
        self._data = []
        self._headers = ["Type", "Value", "Pen"]
        self._n_columns = 3
        # self.insertRow(0)

    def rowCount(self, parent):
        # dbgmsg("RingTableModel.rowCount")
        return len(self._data)

    def columnCount(self, parent):
        # dbgmsg("RingTableModel.columnCount")
        return self._n_columns

    def data(self, index, role):
        # dbgmsg("RingTableModel.data")
        if role == DisplayRole or role == EditRole:
            return self._data[index.row()][index.column()]
        return None

    def setData(self, index, value, role=EditRole):
        dbgmsg("RingTableModel.setData")
        if role == EditRole:
            column = index.column()
            if column == 0:
                value = ring_types[int(value)]
            if column == 1:
                value = float(value)
            if column == 2:
                assert isinstance(value, QPen)
            if column == 3:
                value = bool(value)
            self._data[index.row()][column] = value
            self.data_changed.emit()
            return True
        return False

    def flags(self, index):
        # dbgmsg("RingTableModel.flags")
        return Qt.ItemFlag.ItemIsEditable | super().flags(index)

    def headerData(self, section, orientation, role):
        # dbgmsg("RingTableModel.headerData")
        if role == DisplayRole and orientation == Horizontal:
            return self._headers[section]

    def row_copy(self, row=None):
        dbgmsg("RingTableModel.row_copy")
        d = [ring_types[0], "3.0", default_pen(), True]
        if row is None:
            row = self.rowCount(self)
        if row == 0:
            return d
        d[0] = self._data[row - 1][0]
        return d

    def insertRow(self, position, rows=1, index=QModelIndex()):
        dbgmsg("RingTableModel.insertRow")
        self.beginInsertRows(QModelIndex(), position, position + rows - 1)
        row_to_copy = self.rowCount(self)
        for i in range(rows):
            row_data = self.row_copy(row_to_copy)
            self._data.insert(position, row_data)
        self.endInsertRows()

    def removeRow(self, position, rows=1, index=QModelIndex()):
        dbgmsg("RingTableModel.removeRow")
        self.beginRemoveRows(QModelIndex(), position, position + rows - 1)
        del self._data[position : position + rows]
        self.endRemoveRows()


class QPenDelegate(QItemDelegate):
    def __init__(self, parent):
        dbgmsg("QPenDelegate.__init__")
        QItemDelegate.__init__(self, parent)

    def paint(self, painter, option, index):
        dbgmsg("QPenDelegate.paint")
        x1, y1, x2, y2 = option.rect.getCoords()
        dy = (y2 - y1) / 2
        y = int(y1 + dy)
        painter.setPen(index.data())
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)
        painter.drawLine(x1 + 2, y, x2 - 2, y)

    def createEditor(self, parent, option, index):
        dbgmsg("QPenDelegate.createEditor")
        editor = QWidget(parent=parent)
        pen = pen_dialog(parent=editor, pen=index.data())
        editor.pen = pen
        self.parent = parent
        return editor

    def setEditorData(self, editor, index):
        dbgmsg("QPenDelegate.setEditorData")
        # value = index.data(EditRole)
        # if isinstance(value, QPen):
        #     editor.dialog.set_pen(value)

    def setModelData(self, editor, model, index):
        dbgmsg("QPenDelegate.setModelData")
        pen = editor.pen
        model.setData(index, pen, EditRole)

    def updateEditorGeometry(self, editor, option, index):
        dbgmsg("QPenDelegate.updateEditorGeometry")
        editor.setGeometry(option.rect)

    def sizeHint(self, option, index):
        dbgmsg("QPenDelegate.sizeHint")
        editor = self.editors.get(QPersistentModelIndex(index))
        if editor:
            return editor.sizeHint()
        return super().sizeHint(option, index)


class RingTypeDelegate(QItemDelegate):
    def __init__(self, parent):
        dbgmsg("RingTypeDelegate.__init__")
        QItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        dbgmsg("RingTypeDelegate.createEditor")
        combo = QComboBox(parent)
        combo.addItems(ring_types)
        combo.currentIndexChanged.connect(self.currentIndexChanged)
        return combo

    def setEditorData(self, editor, index):
        dbgmsg("RingTypeDelegate.setEditorData")
        editor.blockSignals(True)
        editor.setCurrentIndex(0)
        editor.blockSignals(False)

    def setModelData(self, editor, model, index):
        dbgmsg("RingTypeDelegate.setModelData")
        model.setData(index, editor.currentIndex())

    @pyqtSlot()
    def currentIndexChanged(self):
        dbgmsg("RingTypeDelegate.currentIndexChanged")
        self.commitData.emit(self.sender())


class RingEditWidget(QWidget):
    def __init__(self, parent=None, padview=None):
        dbgmsg("RingEditWidget.__init__")
        super().__init__(parent=parent)
        self.padview = padview
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.model = RingTableModel()
        self.model.data_changed.connect(self.update_rings)
        self.table = QTableView()
        self.table.setModel(self.model)
        self.table.setItemDelegateForColumn(0, RingTypeDelegate(self))
        self.table.setItemDelegateForColumn(2, QPenDelegate(self))
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
        self.table.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        self.layout.addWidget(self.table)
        self.setWindowTitle("Ring Editor")
        self.add_button = QPushButton("Add Ring")
        self.add_button.clicked.connect(self.add_row)
        self.layout.addWidget(self.add_button)
        self.remove_button = QPushButton("Remove Ring")
        self.remove_button.clicked.connect(self.remove_selected_rows)
        self.layout.addWidget(self.remove_button)
        self.setGeometry(100, 100, 500, 300)

    def remove_selected_rows(self):
        dbgmsg("RingEditWidget.remove_selected_rows")
        selected_cells = self.table.selectionModel().selectedIndexes()
        unique_rows = set(index.row() for index in selected_cells)
        for row in sorted(unique_rows, reverse=True):
            self.model.removeRow(row)
        self.update_rings()

    def add_row(self):
        dbgmsg("RingEditWidget.add_row")
        row_position = self.model.rowCount(QModelIndex())
        self.model.insertRow(row_position)
        self.update_rings()

    def update_rings(self):
        padview = self.padview
        padview.remove_rings()
        model = self.model
        for i in range(model.rowCount(QModelIndex())):
            t = model.data(index=model.createIndex(i, 0), role=EditRole)
            v = model.data(index=model.createIndex(i, 1), role=EditRole)
            p = model.data(index=model.createIndex(i, 2), role=EditRole)
            pgp = pg.mkPen(color=p.color(), width=p.width())
            if t[0] == "q":
                padview.add_ring(q_mag=float(v) * 1e10, pen=pgp)
            if t[0] == "d":
                padview.add_ring(d_spacing=float(v) * 1e-10, pen=pgp)
            if t[0] == "r":
                padview.add_ring(radius=float(v) / 1000, pen=pgp)
            if t[0] == "θ":
                padview.add_ring(angle=float(v) * np.pi / 180, pen=pgp)


Widget = RingEditWidget

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = RingEditWidget()
    widget.show()
    sys.exit(app.exec_())
