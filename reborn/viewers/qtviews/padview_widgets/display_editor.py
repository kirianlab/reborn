# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

from ....external.pyqt import (
    QFont,
    QWidget,
    QCheckBox,
    QHBoxLayout,
    QVBoxLayout,
    QLabel,
    QPushButton,
)
from ....gui.misc.spinbox import BetterDoubleSpinBox


class Widget(QWidget):
    def __init__(self, *args, padview=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.padview = padview
        levels = self.padview.get_levels()
        self.setWindowTitle("Display Editor")
        self.layout = QVBoxLayout()
        bold = QFont()
        bold.setBold(True)
        # ======  Upper Bound  ================
        label = QLabel("Upper Bound")
        label.setFont(bold)
        self.layout.addWidget(label)
        layout = QHBoxLayout()
        self.upper_fixed_checkbox = QCheckBox()
        self.upper_fixed_checkbox.setText("Fixed Value")
        self.upper_fixed_checkbox.setChecked(False)
        self.upper_fixed_checkbox.toggled.connect(self.upper_fixed_checkbox_action)
        layout.addWidget(self.upper_fixed_checkbox)
        self.upper_fixed_spinbox = BetterDoubleSpinBox()
        self.upper_fixed_spinbox.setValue(levels[1])
        if self.padview._fixed_levels[1] is not None:
            self.upper_fixed_spinbox.setValue(self.padview._fixed_levels[1])
        self.upper_fixed_spinbox.valueChanged.connect(self.upper_fixed_spinbox_action)
        layout.addWidget(self.upper_fixed_spinbox)
        self.layout.addLayout(layout)
        layout = QHBoxLayout()
        self.upper_percentile_checkbox = QCheckBox()
        self.upper_percentile_checkbox.setText("Percentile")
        self.upper_percentile_checkbox.setChecked(False)
        self.upper_percentile_checkbox.toggled.connect(
            self.upper_percentile_checkbox_action
        )
        layout.addWidget(self.upper_percentile_checkbox)
        self.upper_percentile_spinbox = BetterDoubleSpinBox(format="f", decimals=1)
        self.upper_percentile_spinbox.setMaximum(100)
        self.upper_percentile_spinbox.setMinimum(0)
        self.upper_percentile_spinbox.setValue(98)
        self.upper_percentile_spinbox.valueChanged.connect(
            self.upper_percentile_spinbox_action
        )
        if self.padview._percentile_levels[1] is not None:
            self.upper_percentile_spinbox.setValue(self.padview._percentile_levels[1])
        layout.addWidget(self.upper_percentile_spinbox)
        self.layout.addLayout(layout)
        # ======  Lower Bound  ================
        label = QLabel("Lower Bound")
        label.setFont(bold)
        self.layout.addWidget(label)
        layout = QHBoxLayout()
        self.lower_fixed_checkbox = QCheckBox()
        self.lower_fixed_checkbox.setText("Fixed Value")
        self.lower_fixed_checkbox.setChecked(False)
        self.lower_fixed_checkbox.toggled.connect(self.lower_fixed_checkbox_action)
        layout.addWidget(self.lower_fixed_checkbox)
        self.lower_fixed_spinbox = BetterDoubleSpinBox()
        self.lower_fixed_spinbox.setValue(levels[0])
        self.lower_fixed_spinbox.valueChanged.connect(self.lower_fixed_spinbox_action)
        layout.addWidget(self.lower_fixed_spinbox)
        self.layout.addLayout(layout)
        layout = QHBoxLayout()
        self.lower_percentile_checkbox = QCheckBox()
        self.lower_percentile_checkbox.setText("Percentile")
        self.lower_percentile_checkbox.setChecked(False)
        self.lower_percentile_checkbox.toggled.connect(
            self.lower_percentile_checkbox_action
        )
        layout.addWidget(self.lower_percentile_checkbox)
        self.lower_percentile_spinbox = BetterDoubleSpinBox(format="f", decimals=1)
        self.lower_percentile_spinbox.setMaximum(100)
        self.lower_percentile_spinbox.setMinimum(0)
        self.lower_percentile_spinbox.setValue(2)
        self.lower_percentile_spinbox.valueChanged.connect(
            self.lower_percentile_spinbox_action
        )
        layout.addWidget(self.lower_percentile_spinbox)
        self.layout.addLayout(layout)
        # ======  General  ================
        layout = QHBoxLayout()
        label = QLabel("General")
        label.setFont(bold)
        self.layout.addWidget(label)
        self.fix_button = QPushButton("Fix Current Levels")
        self.fix_button.pressed.connect(self.fix_current_levels)
        self.layout.addWidget(self.fix_button)
        self.mirror_checkbox = QCheckBox()
        self.mirror_checkbox.setText("Mirror Levels")
        self.mirror_checkbox.setChecked(False)
        self.mirror_checkbox.toggled.connect(self.mirror_checkbox_action)
        self.layout.addWidget(self.mirror_checkbox)
        self.ignore_masked_checkbox = QCheckBox()
        self.ignore_masked_checkbox.setText("Ignore Masked")
        self.ignore_masked_checkbox.setChecked(True)
        self.ignore_masked_checkbox.toggled.connect(self.ignore_masked_checkbox_action)
        self.layout.addWidget(self.ignore_masked_checkbox)
        self.layout.addLayout(layout)
        self.setLayout(self.layout)
        if padview._fixed_levels[0] is not None:
            self.lower_fixed_spinbox.setValue(padview._fixed_levels[0])
        if padview._fixed_levels[1] is not None:
            self.upper_fixed_spinbox.setValue(padview._fixed_levels[1])
        if padview._percentile_levels[0] is not None:
            self.lower_percentile_spinbox.setValue(padview._percentile_levels[0])
        if padview._percentile_levels[1] is not None:
            self.upper_percentile_spinbox.setValue(padview._percentile_levels[1])

    def fix_current_levels(self):
        # print("fix_current_levels")
        levels = self.padview.get_levels()
        self.lower_fixed_spinbox.setValue(levels[0])
        self.upper_fixed_spinbox.setValue(levels[1])
        self.lower_fixed_checkbox.setChecked(True)
        self.upper_fixed_checkbox.setChecked(True)
        self.mirror_checkbox.setChecked(False)
        self.padview._fixed_levels = self.padview.get_levels()
        self.padview.set_levels()

    def upper_fixed_checkbox_action(self):
        # print("upper_fixed_checkbox_action")
        if self.upper_fixed_checkbox.isChecked():
            self.upper_percentile_checkbox.setChecked(False)
            self.padview._fixed_levels[1] = self.upper_fixed_spinbox.value()
            self.padview._percentile_levels[1] = None
        else:
            self.padview._fixed_levels[1] = None
        self.padview.set_levels()

    def upper_fixed_spinbox_action(self):
        # print("upper_fixed_spinbox_action")
        self.upper_fixed_checkbox_action()

    def upper_percentile_checkbox_action(self):
        # print("upper_percentile_checkbox_action")
        if self.upper_percentile_checkbox.isChecked():
            self.upper_fixed_checkbox.setChecked(False)
            self.padview._fixed_levels[1] = None
            self.padview._percentile_levels[1] = self.upper_percentile_spinbox.value()
        else:
            self.padview._percentile_levels[1] = None
        self.padview.set_levels()

    def upper_percentile_spinbox_action(self):
        # print("upper_percentile_spinbox_action")
        self.upper_percentile_checkbox_action()

    def lower_fixed_checkbox_action(self):
        # print("lower_fixed_checkbox_action")
        if self.lower_fixed_checkbox.isChecked():
            self.lower_percentile_checkbox.setChecked(False)
            self.padview._fixed_levels[0] = self.lower_fixed_spinbox.value()
            self.padview._percentile_levels[0] = None
        else:
            self.padview._fixed_levels[0] = None
        self.padview.set_levels()

    def lower_fixed_spinbox_action(self):
        # print("lower_fixed_spinbox_action")
        self.lower_fixed_checkbox_action()

    def lower_percentile_checkbox_action(self):
        # print("lower_percentile_checkbox_action")
        if self.lower_percentile_checkbox.isChecked():
            self.lower_fixed_checkbox.setChecked(False)
            self.padview._fixed_levels[0] = None
            self.padview._percentile_levels[0] = self.lower_percentile_spinbox.value()
        else:
            self.padview._percentile_levels[0] = None
        self.padview.set_levels()

    def lower_percentile_spinbox_action(self):
        # print("lower_percentile_spinbox_action")
        self.lower_percentile_checkbox_action()

    def mirror_checkbox_action(self):
        # print("mirror_checkbox_action")
        if self.mirror_checkbox.isChecked():
            self.upper_fixed_checkbox.setChecked(False)
            self.upper_percentile_checkbox.setChecked(False)
            self.padview._fixed_levels[1] = None
            self.padview._percentile_levels[1] = None
            self.padview._mirror_levels = True
        else:
            self.padview._mirror_levels = False
        self.padview.set_levels()

    def ignore_masked_checkbox_action(self):
        # print("ignore_masked_checkbox_action")
        if self.ignore_masked_checkbox.isChecked():
            self.padview._levels_ignore_masked = True
        else:
            self.padview._levels_ignore_masked = False
        self.padview.set_levels()
