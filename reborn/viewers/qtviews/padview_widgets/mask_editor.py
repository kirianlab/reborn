# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np

import reborn
from ....external.pyqt import (
    QWidget,
    QPushButton,
    QFont,
    QGridLayout,
    QLabel,
    Qt,
    Key_Space,
    QKeySequence,
    QShortcut,
    QLineEdit,
    QRadioButton,
    AlignCenter,
    QFont,
    QButtonGroup,
    QCheckBox,
    Accepted,
    QInputDialog,
    QComboBox,
    QSpinBox,
    QColorDialog,
    QColor,
    QHBoxLayout,
    QDoubleValidator
)
from ....gui.misc.spinbox import BetterDoubleSpinBox


class Widget(QWidget):
    MAX_HISTORY = 10  # Specific Range for history size

    def __init__(self, padview):
        super().__init__()
        bold = QFont()
        bold.setBold(True)
        self.padview = padview
        self.setWindowTitle("Mask Editor")
        self.layout = QGridLayout()
        row = 0
        row += 1
        label = QLabel("** Type spacebar to do mask action **")
        label.setFont(bold)
        QShortcut(QKeySequence(Key_Space), padview.main_window).activated.connect(
            self.apply_mask
        )

        # Initialize undo/redo history
        self.previous_mask_history = []  # Stack for undo history
        self.redo_mask_history = []      # Stack for redo history

        QShortcut(QKeySequence(Key_Space), self).activated.connect(self.apply_mask)
        self.layout.addWidget(label, row, 1)
        row += 1
        self.clear_button = QPushButton("Clear mask")
        self.clear_button.clicked.connect(padview.clear_masks)
        self.layout.addWidget(self.clear_button, row, 1, 1, 2)
        row += 1
        self.undo_button = QPushButton("Undo")
        self.undo_button.clicked.connect(self.undo)
        self.layout.addWidget(self.undo_button, row, 1, 1, 2)
        row += 1
        self.redo_button = QPushButton("Redo")
        self.redo_button.clicked.connect(self.redo)
        self.layout.addWidget(self.redo_button, row, 1, 1, 2)
        row += 1
        self.visible_button = QPushButton("Toggle mask visible")
        self.visible_button.clicked.connect(padview.toggle_masks)
        self.layout.addWidget(self.visible_button, row, 1, 1, 2)
        row += 1
        self.color_button = QPushButton("Choose mask color")
        self.color_button.clicked.connect(self.choose_mask_color)
        self.layout.addWidget(self.color_button, row, 1, 1, 2)
        row += 1
        self.rroi_button = QPushButton("Add rectangle ROI")
        self.rroi_button.clicked.connect(padview.add_rectangle_roi)
        self.layout.addWidget(self.rroi_button, row, 1, 1, 2)
        row += 1
        self.rroi_button = QPushButton("Add circle ROI")
        self.rroi_button.clicked.connect(padview.add_circle_roi)
        self.layout.addWidget(self.rroi_button, row, 1, 1, 2)
        row += 1
        self.rroi_button = QPushButton("Mask panel edges...")
        self.rroi_button.clicked.connect(self.mask_panel_edges)
        self.layout.addWidget(self.rroi_button, row, 1, 1, 2)
        row += 1
        self.rroi_button = QPushButton("Mask panels by names...")
        self.rroi_button.clicked.connect(self.mask_pads_by_names)
        self.layout.addWidget(self.rroi_button, row, 1, 1, 2)
        row += 1
        label = QLabel("What to do:")
        label.setFont(bold)
        self.layout.addWidget(label, row, 1)
        row += 1
        self.layout.addWidget(QLabel("Mask"), row, 1)
        self.mask_radio = QRadioButton("")
        self.mask_radio.setChecked(True)
        self.layout.addWidget(
            self.mask_radio, row, 2, alignment=AlignCenter
        )
        row += 1
        self.layout.addWidget(QLabel("Unmask"), row, 1)
        self.unmask_radio = QRadioButton("")
        self.layout.addWidget(
            self.unmask_radio, row, 2, alignment=AlignCenter
        )
        row += 1
        self.layout.addWidget(QLabel("Invert"), row, 1)
        self.invert_radio = QRadioButton("")
        self.layout.addWidget(
            self.invert_radio, row, 2, alignment=AlignCenter
        )
        self.what_group = QButtonGroup(self)
        self.what_group.addButton(self.mask_radio)
        self.what_group.addButton(self.unmask_radio)
        self.what_group.addButton(self.invert_radio)
        row += 1
        label = QLabel("Where to do it:")
        label.setFont(bold)
        self.layout.addWidget(label, row, 1)
        row += 1
        self.layout.addWidget(QLabel("Inside selected ROI"), row, 1)
        self.inside_radio = QRadioButton("")
        self.inside_radio.setChecked(True)
        self.layout.addWidget(
            self.inside_radio, row, 2, alignment=AlignCenter
        )
        row += 1
        self.layout.addWidget(QLabel("Outside selected ROI"), row, 1)
        self.outside_radio = QRadioButton("")
        self.layout.addWidget(
            self.outside_radio, row, 2, alignment=AlignCenter
        )
        row += 1
        self.layout.addWidget(QLabel("Everywhere"), row, 1)
        self.everywhere_radio = QRadioButton("")
        self.layout.addWidget(
            self.everywhere_radio, row, 2, alignment=AlignCenter
        )
        self.where_group = QButtonGroup(self)
        self.where_group.addButton(self.inside_radio)
        self.where_group.addButton(self.outside_radio)
        self.where_group.addButton(self.everywhere_radio)
        row += 1
        label = QLabel("Additional filters:")
        label.setFont(bold)
        self.layout.addWidget(label, row, 1)
        row += 1
        h = QHBoxLayout()
        self.value_combo = QComboBox()
        self.value_combo.addItems(["Any value", "Values above", "Values below", "Values equal to"])
        h.addWidget(self.value_combo)
        self.value_line_edit = QLineEdit()
        self.value_line_edit.setText("0")
        self.value_line_edit.setValidator(QDoubleValidator(0.0, 100.0, 2))
        h.addWidget(self.value_line_edit)
        self.layout.addLayout(h, row, 1)
        self.value_checkbox = QCheckBox()
        self.layout.addWidget(
            self.value_checkbox,
            row,
            2,
            alignment=AlignCenter,
        )
        row += 1
        self.layout.addWidget(QLabel("Apply only above upper threshold"), row, 1)
        self.above_upper_checkbox = QCheckBox()
        self.layout.addWidget(
            self.above_upper_checkbox,
            row,
            2,
            alignment=AlignCenter,
        )
        row += 1
        self.layout.addWidget(QLabel("Apply only below upper threshold"), row, 1)
        self.below_upper_checkbox = QCheckBox()
        self.layout.addWidget(
            self.below_upper_checkbox,
            row,
            2,
            alignment=AlignCenter,
        )
        row += 1
        self.layout.addWidget(QLabel("Apply only above lower threshold"), row, 1)
        self.above_lower_checkbox = QCheckBox()
        self.layout.addWidget(
            self.above_lower_checkbox,
            row,
            2,
            alignment=AlignCenter,
        )
        row += 1
        self.layout.addWidget(QLabel("Apply only below lower threshold"), row, 1)
        self.below_lower_checkbox = QCheckBox()
        self.layout.addWidget(
            self.below_lower_checkbox,
            row,
            2,
            alignment=AlignCenter,
        )
        row += 1
        self.layout.addWidget(
            QLabel("Apply only to PAD under mouse cursor"), row, 1
        )
        self.pad_under_mouse_checkbox = QCheckBox()
        self.layout.addWidget(
            self.pad_under_mouse_checkbox,
            row,
            2,
            alignment=AlignCenter,
        )
        row += 1
        self.save_button = QPushButton("Save masks...")
        self.save_button.clicked.connect(padview.save_masks)
        self.layout.addWidget(self.save_button, row, 1, 1, 2)
        row += 1
        self.load_button = QPushButton("Load masks...")
        self.load_button.clicked.connect(padview.load_masks)
        self.layout.addWidget(self.load_button, row, 1, 1, 2)
        self.setLayout(self.layout)

    def undo(self):
        if len(self.previous_mask_history) > 0:
            last_mask = self.previous_mask_history.pop()  # Retrieve the last mask from history

            if len(last_mask) == len(self.padview.get_mask()):  # Ensure mask lengths match
                self.redo_mask_history.append(self.padview.get_mask().copy())  # Save current mask to redo history

                self.padview.set_mask(last_mask)  # Restore the previous mask
                self.padview.update_masks(last_mask)

                print(f"Undo - current mask history length: {len(self.previous_mask_history)}")
            else:
                print("Undo failed: Mask length mismatch")
        else:
            print("No history to undo")

    def redo(self):
        if len(self.redo_mask_history) > 0:
            # Save current mask to undo history before applying redo
            current_mask = self.padview.get_mask().copy()
            self.previous_mask_history.append(current_mask)

            # Limit undo history size
            if len(self.previous_mask_history) > self.MAX_HISTORY:
                self.previous_mask_history.pop(0)

            # Get the most recent mask from the redo stack
            next_mask = self.redo_mask_history.pop()

            if len(next_mask) == len(current_mask):  # Ensure mask lengths match
                self.padview.set_mask(next_mask)
                self.padview.update_masks()
                print(f"Redo - current mask history length: {len(self.redo_mask_history)}")
            else:
                print("Redo failed: Mask length mismatch")
        else:
            print("No history to redo")

    def apply_mask(self):
        # Get or initialize mask
        mask = self.padview.get_mask()
        if mask is None:
            mask = np.ones(self.padview.get_pad_geometry().n_pixels)

        # Append the current mask to undo history
        self.previous_mask_history.append(mask.copy())

        # Limit the undo history to MAX_HISTORY
        if len(self.previous_mask_history) > self.MAX_HISTORY:
            self.previous_mask_history.pop(0)  # Remove the oldest mask

        # Clear redo history as we're applying a new mask
        self.redo_mask_history.clear()
        print("Redo history cleared after new mask applied.")

        # Continue applying the mask
        geom = self.padview.get_pad_geometry()
        data = geom.concat_data(self.padview.get_pad_display_data())
        select = geom.zeros(dtype=int)
        if self.everywhere_radio.isChecked():
            select[:] = 1
        else:
            inds, typ = self.padview._get_hovering_roi_indices()
            if inds is None:
                self.padview.debug("No ROI selected")
                return
            select[inds] = 1
            if self.outside_radio.isChecked():
                select = -(select - 1)
        if self.above_upper_checkbox.isChecked():
            thresh = self.padview.get_levels()[1]
            select *= data > thresh
        if self.below_upper_checkbox.isChecked():
            thresh = self.padview.get_levels()[1]
            select *= data < thresh
        if self.above_lower_checkbox.isChecked():
            thresh = self.padview.get_levels()[0]
            select *= data > thresh
        if self.below_lower_checkbox.isChecked():
            thresh = self.padview.get_levels()[0]
            select *= data < thresh
        if self.value_checkbox.isChecked():
            # New Value Based Filtering
            selected_filter = self.value_combo.currentText()  # Get combo box selection
            filter_value = float(self.value_line_edit.text())  # Get spin box value
            if selected_filter == "Values above":
                select *= data > filter_value
            elif selected_filter == "Values below":
                select *= data < filter_value
            elif selected_filter == "Values equal to":
                select *= data == filter_value

        if self.pad_under_mouse_checkbox.isChecked():
            x, y, pid = self.padview.get_pad_coords_from_mouse_pos()
            pids = []
            for i, p in enumerate(geom):
                d = p.zeros() + i
                pids.append(d)
            pids = geom.concat_data(pids)
            select[pids != pid] = 0
        if self.mask_radio.isChecked():
            mask[select == 1] = 0
        elif self.unmask_radio.isChecked():
            mask[select == 1] = 1
        elif self.invert_radio.isChecked():
            mask[select == 1] = -(mask[select == 1] - 1)
        self.padview.set_mask(mask)
        self.padview.update_masks()

    def choose_mask_color(self):
        color_dialog = QColorDialog(self)
        color = color_dialog.getColor()  # Show the color picker dialog

        if color.isValid():
            self.padview.set_mask_color(color)

    def mask_panel_edges(self, n_pixels=None):
        padview = self.padview
        if n_pixels is None or n_pixels is False:
            text, ok = QInputDialog.getText(
                padview.main_window,
                "Edge mask",
                "Specify number of edge pixels to mask",
                QLineEdit.Normal,
                "1",
            )
            if ok:
                if text == "":
                    return
                n_pixels = int(str(text).strip())
        mask = padview.get_mask(as_list=True)
        for i in range(len(mask)):
            mask[i] *= reborn.detector.edge_mask(mask[i], n_pixels)
        self.padview.set_mask(mask)
        self.padview.update_masks()

    def mask_pads_by_names(self):
        padview = self.padview
        clear_labels = False
        if padview._pad_labels is None:
            padview.show_pad_labels()
            clear_labels = True
        text, ok = QInputDialog.getText(
            padview.main_window,
            "Enter PAD names (comma separated)",
            "PAD names",
            QLineEdit.Normal,
            "",
        )
        if clear_labels:
            padview.hide_pad_labels()
        if ok:
            if text == "":
                return
            names = text.split(",")
            geom = padview.get_pad_geometry()
            mask = padview.get_mask(as_list=True)
            for i in range(len(padview.get_pad_geometry())):
                if geom[i].name in names:
                    mask[i] *= 0
            padview.set_mask(mask)
            padview.update_masks()

            # Modifications of Undo and Redo