# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pyqtgraph.opengl as gl
from reborn.utils import vec_norm
from reborn.external.pyqt import (
    pyqtSignal,
    Qt,
    QWidget,
    QHBoxLayout,
    QVBoxLayout,
    QGridLayout,
    QLabel,
    QCheckBox
)

class PADMeshData(gl.MeshData):
    def __init__(self, vertexes=None, faces=None, edges=None, vertexColors=None, faceColors=None):
        super().__init__(vertexes=vertexes, faces=faces, edges=edges, vertexColors=vertexColors, faceColors=faceColors)
        self.original_colors = faceColors

class QVecMeshItem(gl.GLMeshItem):
    def __init__(self, pad_idx, parentItem=None, **kwds):
        super().__init__(parentItem=None, **kwds)

        self.__pad_idx = pad_idx
        
    def getPADIndex(self):
        return self.__pad_idx

        
class PADMeshItem(gl.GLMeshItem):
    def __init__(self, pad_idx, parentItem=None, **kwds):
        super().__init__(parentItem=None, **kwds)

        self.__pad_idx = pad_idx
        self.alpha = 1.0
        self.__current_highlight = None

    def getPADIndex(self):
        return self.__pad_idx
        
    def showHighlight(self, highlightColor=np.array([0,0,1,1], dtype=float)):
        # check if action necessary
        if self.__current_highlight is not None and \
           np.allclose(self.__current_highlight, highlightColor, rtol=1e-5):
            return

        # get PADMeshData instance
        mesh_data = self.opts['meshdata']
        if not isinstance(mesh_data, PADMeshData):
            return

        # set faces colors to highlight color
        new_colors = np.tile(highlightColor,
                             mesh_data.faceCount())
        new_colors = new_colors.reshape(-1, 4)
        mesh_data.setFaceColors(new_colors)

        # update state
        self.__current_highlight = highlightColor
        self.opts['meshdata'] = mesh_data
        self.meshDataChanged()
        
    def hideHighlight(self):
        # check if action necessary
        if self.__current_highlight is None:
            return

        # get PADMeshData instance
        mesh_data = self.opts['meshdata']
        if not isinstance(mesh_data, PADMeshData):
            return

        # reset colors
        mesh_data.setFaceColors(mesh_data.original_colors)

        # update state
        self.__current_highlight = None
        self.opts['meshdata'] = mesh_data
        self.meshDataChanged()

    
class EwaldGLWidget(gl.GLViewWidget):
    def __init__(self, padview, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.padview = padview
        self.setMouseTracking(True) # enable for pad highlighting on hover
        self.__highlight_on_hover = True
        self.__toggle_q_vecs_on_click = True
        self.__mouse_down_pos = None
        
        data_list = self.padview.get_pad_display_data(as_list=True)
        geom_list = self.padview.get_pad_geometry()
        mask_list = self.padview.get_mask(as_list=True)
        beam = self.padview.get_beam()
        kmag = 2 * np.pi / beam.wavelength
        max_data_val = np.max(data_list)
        
        for pad_idx, (data, geom, mask) in enumerate(zip(data_list, geom_list, mask_list)):
            # generate vertices for detector pixel corners
            cvecs = geom.pixel_corner_position_vecs()
            num_sqs = cvecs.shape[0]
            cvecs /= np.sqrt(np.sum(cvecs ** 2, axis=-1, keepdims=True))
            cvecs = cvecs * 2 * np.pi / beam.wavelength - beam.beam_vec
            cvecs = cvecs.reshape(-1, 3)
            
            # intensity color
            colors = np.ones((num_sqs, 4), dtype=float)
            cdat = data.flatten()/max_data_val
            colors[:, 0] = cdat
            colors[:, 1] = cdat
            colors[:, 2] = cdat
            
            # mask color
            mcolor = np.array(self.padview.get_mask_color()) / 255
            colors[mask.flatten() == 0] = mcolor
        
            # two faces per detector pixel
            colors = np.repeat(colors, 2, axis=0)
            
            # generate drawing indices, i.e. three vertex indices per triangle        
            square = [0, 1, 2, 0, 2, 3]
            cidx = np.arange(num_sqs) * 4
            cidx = np.repeat(cidx, 6)
            cidx = np.tile(square, num_sqs) + cidx
            cidx = cidx.reshape(-1, 3)
            
            # create mesh
            pixels_md = PADMeshData(vertexes=cvecs,
                                    faces=cidx,
                                    faceColors=colors)
            pixels_mesh = PADMeshItem(pad_idx = pad_idx,
                                      meshdata=pixels_md,
                                      smooth=False,
                                      computeNormals=False,
                                      glOptions='opaque')
            pixels_mesh.translate(0, 0, -kmag)
            self.addItem(pixels_mesh)

        # beam mesh
        beam_len = np.sqrt(np.sum(beam.k_in ** 2, axis=-1, keepdims=True)) 
        beam_md = gl.MeshData.cylinder(300, 30,
                                       radius=[1.0e9, 1.0e9],
                                       length=beam_len)

        self.beam_mesh = gl.GLMeshItem(meshdata=beam_md,
                                       color=np.array([0,1,0,1], dtype=float),
                                       smooth=False,
                                       computeNormals=False,
                                       glOptions='opaque')
        self.beam_mesh.translate(0,0,-beam_len)
        self.addItem(self.beam_mesh)

        # initial camera position
        self.setCameraPosition(distance=5 * kmag,
                                         azimuth=0,
                                         elevation=90)

    def getHighlightOnHover(self):
        return self.__highlight_on_hover

    def setHighlightOnHover(self, bool_val):
        self.__highlight_on_hover = bool_val

    def getToggleQVecsOnClick(self):
        return self.__toggle_q_vecs_on_click

    def setToggleQVecsOnClick(self, bool_val):
        self.__toggle_q_vecs_on_click = bool_val

    def toggleQVecForPAD(self, pad_idx):
        for mesh in self.items:
            if isinstance(mesh, QVecMeshItem) and mesh.getPADIndex() == pad_idx:
                self.hideQVecForPAD(pad_idx)
                return

        self.showQVecForPAD(pad_idx)
        
        
    def showQVecForPAD(self, pad_idx):
        # check if action necessary
        for mesh in self.items:
            if isinstance(mesh, QVecMeshItem) and mesh.getPADIndex() == pad_idx:
                return
        
        geom_list = self.padview.get_pad_geometry()
        beam = self.padview.get_beam()

        # determine q vector
        k_out = 2*np.pi/beam.wavelength * vec_norm(geom_list[pad_idx].center_pos_vec())
        q = k_out - beam.k_in
        q_len = np.sqrt(np.sum(q ** 2, axis=-1, keepdims=True)) 

        # generate vector mesh
        q_md = gl.MeshData.cylinder(300, 30,
                                    radius=[2e8, 2e8],
                                    length=q_len)
        
        q_mesh = QVecMeshItem(pad_idx,
                              meshdata=q_md,
                              color=np.array([0,1,1,1], dtype=float),
                              smooth=False,
                              computeNormals=False,
                              glOptions='opaque')

        # determine angle between default mesh axis and q vec
        z = np.array([0.0, 0.0, 1.0])
        angle = np.arccos(np.sum(z*q) / q_len)
        angle = angle * 180 / np.pi

        # transform mesh
        rotate_axis = np.cross(z, q) 
        q_mesh.rotate(angle, rotate_axis[0], rotate_axis[1], rotate_axis[2])

        self.addItem(q_mesh)

    def hideQVecForPAD(self, pad_idx):
        for mesh in self.items:
            if isinstance(mesh, QVecMeshItem) and mesh.getPADIndex() == pad_idx:
                self.removeItem(mesh)

    def mousePressEvent(self, ev):
        super().mousePressEvent(ev)

        self.__mouse_down_pos = ev.pos()
        
    def mouseMoveEvent(self, ev):
        super().mouseMoveEvent(ev)

        # hide all highlights
        for mesh in self.items:
            if not isinstance(mesh, PADMeshItem):
                continue
            mesh.hideHighlight()
        
        # highlight closest pad item
        if self.__highlight_on_hover:
            region = (ev.pos().x()-5, ev.pos().y()-5, 10, 10)
            items = self.itemsAt(region)
            if len(items) and isinstance(items[0], PADMeshItem):
                items[0].showHighlight()
        
        
    def mouseReleaseEvent(self, ev):
        super().mouseReleaseEvent(ev)

        dx = self.__mouse_down_pos.x() - ev.pos().x()
        dy = self.__mouse_down_pos.y() - ev.pos().y()
        is_click = (dx < 2) and (dx > -2) and (dy < 2) and (dy > -2)

        
        if is_click and self.__toggle_q_vecs_on_click:
            region = (ev.pos().x()-5, ev.pos().y()-5, 10, 10)
            items = self.itemsAt(region)
            if len(items) and isinstance(items[0], PADMeshItem):
                self.toggleQVecForPAD(items[0].getPADIndex())

        
        
        


class Plugin:
    widget = None

    def __init__(self, padview):
        self.widget = Widget(padview)
        self.widget.show()


class Widget(QWidget):
    def __init__(self, padview):
        super().__init__()

        self.setWindowTitle("Ewald Sphere Popout")
        self.resize(500,500)
        
        self.ewaldview = EwaldGLWidget(padview)

        opts_layout = QGridLayout()

        l = QLabel("Highlight on Mouse Hover")
        
        w = QCheckBox()
        w.setChecked(self.ewaldview.getHighlightOnHover())
        w.stateChanged.connect(self.handleHighlightOnHoverPress)

        opts_layout.addWidget(l, 0, 0)
        opts_layout.addWidget(w, 0, 1)

        l = QLabel("Show Beam Vector")

        w = QCheckBox()
        w.setChecked(self.ewaldview.beam_mesh.visible())
        w.stateChanged.connect(self.handleShowBeamPress)

        opts_layout.addWidget(l, 1, 0)
        opts_layout.addWidget(w, 1, 1)

        l = QLabel("Toggle Q Vectors on Click")

        w = QCheckBox()
        w.setChecked(self.ewaldview.getToggleQVecsOnClick())
        w.stateChanged.connect(self.handleToggleQVecsOnClickPress)

        opts_layout.addWidget(l, 2, 0)
        opts_layout.addWidget(w, 2, 1)

        
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.ewaldview)
        main_layout.addLayout(opts_layout)
        
        self.setLayout(main_layout)


    def handleHighlightOnHoverPress(self, state):
        self.ewaldview.setHighlightOnHover(state == Qt.Checked)

    def handleShowBeamPress(self, state):
        self.ewaldview.beam_mesh.setVisible(state == Qt.Checked)

    def handleToggleQVecsOnClickPress(self, state):
        self.ewaldview.setToggleQVecsOnClick(state == Qt.Checked)
