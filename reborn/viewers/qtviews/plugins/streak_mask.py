# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

from time import time
import numpy as np
import pyqtgraph as pg
from reborn.analysis.masking import StreakMasker
from reborn.external.pyqt import (
    QWidget,
    QGridLayout,
    QCheckBox,
    QLabel,
    QDoubleSpinBox,
    AlignCenter,
    QSpinBox,
    QPushButton,
)


class Plugin:
    widget = None

    def __init__(self, padview):
        self.widget = Widget(padview)
        self.widget.show()


class Widget(QWidget):
    original_mask = None
    plot = None

    def __init__(self, padview):
        super().__init__()
        self.padview = padview
        self.setWindowTitle("Streak Mask")
        self.padview.sig_dataframe_changed.connect(self.auto_update)
        self.padview.sig_geometry_changed.connect(self.auto_update)
        self.layout = QGridLayout()
        row = 0
        row += 1
        self.layout.addWidget(QLabel("Auto Run"), row, 1)
        self.autorun_button = QCheckBox()
        self.autorun_button.setChecked(False)
        self.layout.addWidget(self.autorun_button, row, 2, alignment=AlignCenter)
        # row += 1
        # self.layout.addWidget(QLabel('Subtract Radial Median'), row, 1)
        # self.subtract_median_button = QCheckBox()
        # self.subtract_median_button.setChecked(False)
        # self.layout.addWidget(self.subtract_median_button, row, 2, alignment=QtCore.Qt.AlignCenter)
        row += 1
        self.layout.addWidget(QLabel("Minimum Q [A]"), row, 1)
        self.minq_spinbox = QDoubleSpinBox()
        self.minq_spinbox.setDecimals(2)
        self.minq_spinbox.setMinimum(0)
        self.minq_spinbox.setValue(0)
        self.layout.addWidget(self.minq_spinbox, row, 2)
        row += 1
        self.layout.addWidget(QLabel("Maximum Q [A]"), row, 1)
        self.maxq_spinbox = QDoubleSpinBox()
        self.maxq_spinbox.setDecimals(2)
        self.maxq_spinbox.setMinimum(0)
        self.maxq_spinbox.setValue(0.3)
        self.layout.addWidget(self.maxq_spinbox, row, 2)
        row += 1
        self.layout.addWidget(QLabel("SNR Threshold"), row, 1)
        self.snr_spinbox = QDoubleSpinBox()
        self.snr_spinbox.setDecimals(2)
        self.snr_spinbox.setMinimum(0)
        self.snr_spinbox.setValue(6)
        self.layout.addWidget(self.snr_spinbox, row, 2)
        row += 1
        self.layout.addWidget(QLabel("Streak Width [deg]"), row, 1)
        self.width_spinbox = QDoubleSpinBox()
        self.width_spinbox.setDecimals(2)
        self.width_spinbox.setMinimum(0)
        self.width_spinbox.setValue(0.25)
        self.layout.addWidget(self.width_spinbox, row, 2)
        row += 1
        self.layout.addWidget(QLabel("Max Streaks"), row, 1)
        self.nstreak_spinbox = QSpinBox()
        self.nstreak_spinbox.setMinimum(1)
        self.nstreak_spinbox.setValue(2)
        self.layout.addWidget(self.nstreak_spinbox, row, 2)
        row += 1
        self.domask_button = QPushButton("Create Mask")
        self.domask_button.clicked.connect(self.create_streak_mask)
        self.layout.addWidget(self.domask_button, row, 1, 1, 2)
        self.setLayout(self.layout)

    def create_streak_mask(self):
        dataframe = self.padview.get_dataframe()
        geom = self.padview.get_pad_geometry()
        beam = self.padview.get_beam()
        data = self.padview.get_pad_display_data()
        if self.original_mask is None:
            m = self.padview.get_mask()
            if m is None:
                m = dataframe.get_mask_flat()
            self.original_mask = m
        t = time()
        kwargs = dict(
            snr=self.snr_spinbox.value(),
            streak_width=self.width_spinbox.value() * np.pi / 180,
            n_q=100,
            q_range=[
                self.minq_spinbox.value() * 1e10,
                self.maxq_spinbox.value() * 1e10,
            ],
            max_streaks=int(self.nstreak_spinbox.value()),
            debug=1,
            beam=beam,
            geom=geom,
        )
        self.padview.tic(f"StreakMasker({kwargs})")
        streak_masker = StreakMasker(**kwargs)
        newmask, extra = streak_masker.get_mask(
            data, self.original_mask.copy(), return_extra=True
        )
        self.padview.toc()
        newmask *= self.original_mask
        self.padview.update_masks(masks=newmask)
        x = extra["angle"] * 180 / np.pi
        y = extra["snr"]
        w = extra["w"]
        if np.sum(w) > 0:
            w = np.concatenate([w] * 2)
            xx = x[w]
            yy = y[w]
        else:
            xx = None
            yy = None
        if self.plot is None:
            self.plot = pg.plot()
        self.plot.plot(x, y, clear=True)
        if xx is not None:
            self.plot.plot(
                xx,
                yy,
                pen=None,
                symbol="o",
                symbolBrush=(255, 0, 0),
                symbolSize=5,
                symbolPen=None,
            )

    def auto_update(self):
        if self.autorun_button.isChecked():
            self.create_streak_mask()
