# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pyqtgraph as pg
import reborn
from reborn.detector import PADGeometryList
from reborn.dataframe import DataFrame
from reborn.viewers.qtviews.padviews import PADView

logger = logging.getLogger(__name__)


concat = reborn.detector.concat_pad_data


class Plugin:
    widget = None

    def __init__(self, padview):
        self.widget = Widget(padview)
        # m = max(self.widget.get_levels())
        # self.widget.set_levels(levels=(-m, m))
        print("showing widget")
        self.widget.show()


class Widget(PADView):
    data_diff = None
    autoscale = True

    def __init__(self, padview):
        self.padview = padview
        geom = padview.get_pad_geometry()
        beam = padview.get_beam()
        data = padview.get_pad_display_data()
        mask = padview.get_mask()
        self.assembler = reborn.detector.PADAssembler(pad_geometry=geom, centered=True)
        data = self.assembler.assemble_data(data)
        mask = self.assembler.assemble_data(mask)
        mask[mask != 1] = 0
        data -= data[::-1, ::-1]
        mask *= mask[::-1, ::-1]
        data *= mask
        geom = PADGeometryList(self.assembler.pad_geometry)
        super().__init__(main=False, pad_geometry=geom, data=data, beam=beam, mask=mask)
        self.set_title("Central symmetry")
        self.padview.sig_geometry_changed.connect(self.on_change)
        self.padview.sig_beam_changed.connect(self.on_change)
        self.padview.sig_dataframe_changed.connect(self.on_change)

    def on_change(self):
        print("on_change")
        padview = self.padview
        geom = padview.get_pad_geometry()
        beam = padview.get_beam()
        data = padview.get_pad_display_data()
        mask = padview.get_mask()
        self.assembler = reborn.detector.PADAssembler(pad_geometry=geom, centered=True)
        data = self.assembler.assemble_data(data)
        mask = self.assembler.assemble_data(mask)
        mask[mask != 1] = 0
        geom = PADGeometryList(self.assembler.pad_geometry)
        # self.update_masks(mask)
        # self.update_pad_geometry(geom)
        data -= data[::-1, ::-1]
        mask *= mask[::-1, ::-1]
        data *= mask
        df = DataFrame(pad_geometry=geom, mask=mask, raw_data=data, beam=beam)
        self.set_pad_geometry(geom)
        self._setup_image_items()
        self.update_display(dataframe=df, beam_changed=True, mask_changed=True, geom_changed=True, data_changed=True)


    # @property
    # def n_pads(self):
    #     return len(self.padview.get_pad_geometry())

    # @property
    # def pad_geometry(self):
    #     return self.padview.get_pad_geometry()

    # def get_pad_display_data(self):
    #     r"""We subtract the Friedel mate from the current display data in padview."""
    #     if self.data_diff is not None:
    #         return self.data_diff
    #     pv = self.padview
    #     df = self.padview.get_dataframe()
    #     return reborn.detector.subtract_pad_friedel_mate(
    #         pv.get_pad_display_data(), df.get_mask_list(), df.get_pad_geometry()
    #     )

    # def setup_pads(self):
    #     # logger.debug(get_caller(), 1)
    #     data = self.get_pad_display_data()
    #     geom = self.padview.get_pad_geometry()
    #     data = geom.split_data(data)
    #     self.images = []
    #     for i in range(0, len(geom)):
    #         d = data[i]
    #         im = pg.ImageItem(d)
    #         self._apply_pad_transform(im, geom[i])
    #         self.images.append(im)
    #         self.viewbox.addItem(im)
    #     self.setup_histogram_tool()
    #     m = max(self.get_levels())
    #     self.set_levels(levels=(-m, m))
    #     self.set_colormap("bipolar2")
    #
    # def setup_histogram_tool(self):
    #     # logger.debug(get_caller(), 1)
    #     self.histogram.setImageItems(self.images)
    #
    # def _apply_pad_transform(self, im, p):
    #     # logger.debug(get_caller(), 2)
    #     f = p.fs_vec.copy()
    #     s = p.ss_vec.copy()
    #     t = p.t_vec.copy() - (f + s) / 2.0
    #     trans = QTransform()
    #     trans.setMatrix(s[0], s[1], s[2], f[0], f[1], f[2], t[0], t[1], t[2])
    #     im.setTransform(trans)
    #
    # def set_colormap(self, preset="flame"):
    #     r"""Change the colormap to one of the presets configured in pyqtgraph.  Right-click on the colorbar to find
    #     out what values are allowed.
    #     """
    #     # logger.debug(get_caller(), 1)
    #     self.histogram.gradient.loadPreset(preset)
    #     self.histogram.setImageItems(self.images)
    #     QApplication.processEvents()
    #
    # def set_levels_by_percentiles(self, percents=(1, 99), colormap=None):
    #     r"""Set upper and lower levels according to percentiles.  This is based on :func:`numpy.percentile`."""
    #     # logger.debug(get_caller(), 1)
    #     d = concat(self.get_pad_display_data())
    #     lower = np.percentile(d, percents[0])
    #     upper = np.percentile(d, percents[1])
    #     self.set_levels(lower, upper, colormap=colormap)
    #
    # def get_levels(self):
    #     r"""Get the minimum and maximum levels of the current image display."""
    #     return self.histogram.item.getLevels()
    #
    # def set_levels(self, levels=None, percentiles=None, colormap=None):
    #     r"""Set the minimum and maximum levels, same as sliding the yellow sliders on the histogram tool."""
    #     # logger.debug(get_caller(), 1)
    #     if colormap is not None:
    #         self.set_colormap(colormap)
    #     if levels is not None:
    #         min_value = levels[0]
    #         max_value = levels[1]
    #     if (min_value is None) or (max_value is None):
    #         self.set_levels(percentiles=percentiles)
    #     else:
    #         self.histogram.item.setLevels(float(min_value), float(max_value))
    #
    # def update_pads(self):
    #     levels = self.get_levels()
    #     processed_data = self.get_pad_display_data()
    #     geom = self.get_pad_geometry()
    #     for i in range(0, len(geom)):
    #         self.images[i].setImage(processed_data[i])
    #     self.set_levels(levels=levels)
    #
    # @pyqtSlot()
    # def update_pad_geometry(self):
    #     self.data_diff = None
    #     levels = self.get_levels()
    #     self.update_pads()
    #     geom = self.padview.get_pad_geometry()
    #     for i in range(0, len(geom)):
    #         self._apply_pad_transform(self.images[i], self.pad_geometry[i])
    #     self.set_levels(
    #         levels=levels
    #     )  # FIXME: Why must this be done?  Very annoying...
