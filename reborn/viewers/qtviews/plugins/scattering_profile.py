# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pyqtgraph as pg
from reborn.analysis import saxs
from reborn.simulate.solutions import water_scattering_factor_squared
from reborn.external.pyqt import (
    QApplication,
    QMainWindow,
    QVBoxLayout,
    QHBoxLayout,
    QCheckBox,
    QPushButton,
    AlignRight,
    QWidget,
    QLabel,
    QLineEdit
)

class Plugin:
    widget = None
    profiler = None
    water_profile = None
    water_plot = None
    first_profile = True

    def __init__(self, padview):
        self.padview = padview
        self.widget = Widget(padview, self)
        self.update_profile()
        self.padview.sig_dataframe_changed.connect(self.update_profile)
        self.padview.sig_beam_changed.connect(self.update_geometry)
        self.padview.sig_geometry_changed.connect(self.update_geometry)

    def get_q_widget(self):
        try:
            min_q = float(self.widget.min_q_input.text()) * 1e10
        except ValueError:
            min_q = None
        try:
            max_q = float(self.widget.max_q_input.text()) * 1e10
        except ValueError:
            max_q = None
        try:
            n_bins = int(self.widget.n_bins_input.text())
        except ValueError:
            n_bins = None
        return n_bins, (min_q, max_q)

    def set_q_widget(self):
        self.widget.min_q_input.setText(f"{self.current_q[0] / 1e10:.2f}")
        self.widget.max_q_input.setText(f"{self.current_q[-1] / 1e10:.2f}")
        self.widget.n_bins_input.setText(f"{self.current_q.size}")

    def update_profile(self):
        self.padview.debug()
        padview = self.padview
        geom = self.padview.get_pad_geometry()
        beam = self.padview.get_beam()
        mask = self.padview.get_mask()
        
        data = geom.concat_data(padview.get_pad_display_data()).astype(float)
        correct_polarization = self.widget.polarization_checkbox.isChecked()
        correct_solid_angles = self.widget.solid_angle_checkbox.isChecked()
        weights = mask.astype(float)
        if correct_polarization or correct_solid_angles:
            weights = weights.copy()
        if correct_polarization:
            pfac = geom.polarization_factors(beam=beam)
            weights *= pfac
        if correct_solid_angles:
            sa = geom.solid_angles()
            weights *= sa

        if self.first_profile:  
            profiler = saxs.RadialProfiler(pad_geometry=geom, beam=beam, mask=mask)
            self.first_profile = False
            self.profiler = profiler
            stats = profiler.quickstats(data, weights)
            self.current_profile = stats["mean"]
            self.current_q = profiler.bin_centers
            self.set_q_widget()
            q_range = (self.current_q.min(), self.current_q.max())
            n_bins = self.current_q.size
        else:
            n_bins, q_range = self.get_q_widget()
            if q_range == (None, None):
                q_range = (self.current_q.min(), self.current_q.max())
            if n_bins is None:
                n_bins = self.current_q.size
            profiler = saxs.RadialProfiler(pad_geometry=geom, beam=beam, mask=mask, n_bins=n_bins, q_range=q_range)
            self.profiler = profiler
            stats = profiler.quickstats(data, weights)
            self.current_profile = stats["mean"]
            self.current_q = profiler.bin_centers

        min_q, max_q = q_range
        if min_q is None:
            min_q = self.current_q.min()
        if max_q is None:
            max_q = self.current_q.max()
        if n_bins is None:
            n_bins = self.current_q.size    

        mask = (self.current_q >= min_q) & (self.current_q <= max_q)
        filtered_q = self.current_q[mask]
        filtered_profile = self.current_profile[mask]

        self.widget.plot_widget.plot(filtered_q / 1e10, filtered_profile, clear=True)

        if self.widget.water_checkbox.isChecked():
            self.plot_water_profile()

        self.set_q_widget()
        QApplication.processEvents()

    def update_geometry(self):
        self.padview.debug()
        self.update_profile()

    def plot_water_profile(self):
        self.padview.debug()
        self.water_profile = water_scattering_factor_squared(self.profiler.bin_centers)
        w = np.where((self.current_q > 1.5e10) * (self.current_q < 2.5e10))
        if len(w[0]) > 0:  # Normalize on water ring peak if possible
            c = np.max(self.current_profile[w]) / np.max(self.water_profile[w])
        else:  # Normalize based on maximum
            c = np.max(self.current_profile) / np.max(self.water_profile)
        self.water_profile *= c
        self.water_plot = self.widget.plot_widget.plot(self.profiler.bin_centers / 1e10, self.water_profile, clear=False)
        print(self.water_plot)
        QApplication.processEvents()

class Widget(QMainWindow):
    def __init__(self, padview, plugin):
        super().__init__()
        self.padview = padview
        self.plugin = plugin
        self.setWindowTitle("Scattering Profile")

        vbox = QVBoxLayout()

        self.plot_widget = pg.PlotWidget()
        vbox.addWidget(self.plot_widget)

        hbox_checks = QHBoxLayout()

        h_polarization = QHBoxLayout()
        self.polarization_checkbox = QCheckBox()
        h_polarization.addWidget(self.polarization_checkbox, alignment=AlignRight)
        h_polarization.addWidget(QLabel("Correct Polarization"))
        hbox_checks.addLayout(h_polarization)

        h_solid_angle = QHBoxLayout()
        self.solid_angle_checkbox = QCheckBox()
        h_solid_angle.addWidget(self.solid_angle_checkbox, alignment=AlignRight)
        h_solid_angle.addWidget(QLabel("Correct Solid Angles"))
        hbox_checks.addLayout(h_solid_angle)

        h_water = QHBoxLayout()
        self.water_checkbox = QCheckBox()
        h_water.addWidget(self.water_checkbox, alignment=AlignRight)
        h_water.addWidget(QLabel("Add Water Profile"))
        hbox_checks.addLayout(h_water)

        b = QPushButton("Update Profile")
        b.clicked.connect(self.plugin.update_profile)
        hbox_checks.addWidget(b)

        vbox.addLayout(hbox_checks)

        hbox_params_xyz = QHBoxLayout()

        # Min Q
        self.min_q_label = QLabel("Min Q [A]")
        self.min_q_input = QLineEdit()
        hbox_params_xyz.addWidget(self.min_q_label)
        hbox_params_xyz.addWidget(self.min_q_input)

        # Max Q
        self.max_q_label = QLabel("Max Q [A]")
        self.max_q_input = QLineEdit()
        hbox_params_xyz.addWidget(self.max_q_label)
        hbox_params_xyz.addWidget(self.max_q_input)

        # N Bins
        self.n_bins_label = QLabel("N Bins")
        self.n_bins_input = QLineEdit()
        hbox_params_xyz.addWidget(self.n_bins_label)
        hbox_params_xyz.addWidget(self.n_bins_input)

        vbox.addLayout(hbox_params_xyz)
        main_widget = QWidget()
        main_widget.setLayout(vbox)
        self.setCentralWidget(main_widget)
