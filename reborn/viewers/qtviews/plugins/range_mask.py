from time import time
from reborn.analysis.masking import snr_mask
from reborn.external.pyqt import (
    QWidget,
    QGridLayout,
    QCheckBox,
    QLabel,
    QSpinBox,
    QDoubleSpinBox,
    QPushButton,
    AlignCenter,
    QLineEdit
)

import sys
import numpy as np
import pandas
from reborn import detector, source
from reborn.detector import PADGeometryList
from reborn.source import Beam
from reborn.dataframe import DataFrame
from reborn.fileio.getters import FrameGetter
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.viewers.qtviews.padviews import PADView
from reborn.const import eV

   
class Plugin:
    widget = None

    def __init__(self, padview):
        self.widget = Widget(padview)
        self.widget.show()



class Widget(QWidget):


    def __init__(self, padview):

        super().__init__()
        self.padview = padview
        padview.sig_dataframe_changed.connect(self.handle_new_image)


        self.setWindowTitle("Range Mask")
        self.layout = QGridLayout()
        row = 0
        row += 1
        self.layout.addWidget(QLabel("Minimum"), row, 1)
        self.minimum_blank = QLineEdit("0")
        self.layout.addWidget(self.minimum_blank, row, 2)

        row += 1
        self.layout.addWidget(QLabel("Maximum"), row, 1)
        self.maximum_blank = QLineEdit("1000000")
        self.layout.addWidget(self.maximum_blank, row, 2)

        row += 1
        self.create_mask_button = QPushButton("Create Mask")
        self.create_mask_button.clicked.connect(self.apply_range_mask)
        self.layout.addWidget(self.create_mask_button, row, 1, 1, 2)
        self.setLayout(self.layout)
        row += 1
        self.clear_mask_button = QPushButton("Clear Mask")
        self.clear_mask_button.clicked.connect(self.clear_masks) 
        self.layout.addWidget(self.clear_mask_button, row, 1, 1, 2)
        self.setLayout(self.layout)
        row += 1

        self.layout.addWidget(QLabel("Update this mask for each new image"), row, 1)
        self.cont_mask_box = QCheckBox()
        self.cont_mask_box.setChecked(False)
        self.layout.addWidget(self.cont_mask_box, row, 2)
        self.setLayout(self.layout)

        # row +=1
        # self.num_masks_blank = QLineEdit()
        # self.layout.addWidget(self.num_masks_blank, row, 1)
        # self.setLayout(self.layout)
        # self.num_masks_btn = QPushButton("Number of Images\nto apply mask to")
        # self.num_masks_btn.clicked.connect(self.multi_mask_handler)
        # self.layout.addWidget(self.num_masks_btn, row, 2)
        # self.setLayout(self.layout)
        # row += 1


    def handle_new_image(self):
        self.clear_masks()
        if self.cont_mask_box.isChecked() == True:
            self.apply_range_mask()

    def clear_masks(self):
        pv = self.padview
        pv.set_mask(None)
        pv.update_masks()

    def apply_range_mask(self):
        pv = self.padview 

        pv.sig_dataframe_changed.connect(self.handle_new_image)
        pv.sig_geometry_changed.connect(self.handle_new_image)
        pv.sig_beam_changed.connect(self.handle_new_image)


        df = pv.get_dataframe()
        geom = df.get_pad_geometry() 
        beam = df.get_beam()
        mask = df.get_mask_flat()
        data = df.get_raw_data_flat()
        qmags = df.get_q_mags_flat()


        # user inputs

        minimum = float(self.minimum_blank.text())
        maximum = float(self.maximum_blank.text())

        num_changed_max = 0
        num_changed_min = 0
        total_num = 0

        new_mask = mask.copy()

        for index, value in enumerate(data):
            print(value)
            total_num += 1
            if value > maximum:
                new_mask[index] = 0
                num_changed_max +=1

            if value < (minimum):
                new_mask[index] = 0
                num_changed_min +=1
<<<<<<< HEAD
 
=======

        data_list = geom.split_data(data)
        mask_list = geom.split_data(new_mask)

        # unnecessary? 
        dataframe_displayed = pv.get_dataframe()
        mask_displayed_2 = dataframe_displayed.get_mask_flat()
       #updated reborn 2:24 
>>>>>>> ab608f74 (Unimportant change. Sturggling with multiple branches/remotes)
        pv.update_masks(new_mask)
       
        print("num_changed_max", num_changed_max)
        print("num_changed_min", num_changed_min)
        print("total_num", total_num)

        print("Done")
        print("Original mask",mask)
        print("New mask",new_mask)

    def multi_mask_handler(self):
        self.orig_num_mask = int(self.num_masks_blank.text())
        self.num_mask = self.orig_num_mask.copy()


