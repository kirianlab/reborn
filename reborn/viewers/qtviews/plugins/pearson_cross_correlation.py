# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

from time import time
import numpy as np
import pyqtgraph as pg
from reborn.external.pyqt import (
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QCheckBox,
    QLabel,
    QDoubleSpinBox,
    AlignCenter,
    QSpinBox,
    QPushButton,
)
from reborn.analysis.beam_center import FriedelPearsonCC
from reborn.external.pyqtgraph import imview

class Plugin:
    widget = None

    def __init__(self, padview):
        self.widget = Widget(padview)
        self.widget.show()


class Widget(QWidget):
    original_mask = None
    plot = None

    def __init__(self, padview, max_shift=50):
        super().__init__()
        self.padview = padview
        pv = padview
        df = pv.get_dataframe()
        self.max_shift = max_shift
        self.correlator = FriedelPearsonCC(df, max_shift=max_shift)
        shifted_geom, x0, y0, c0, extra = self.correlator.get_shifted_geom(dataframe=df)
        if x0 is None:
            x0 = 0
            y0 = 0
        P = extra["pearson_cc"]
        G = extra["gaussian_fit"]
        nx, ny = P.shape
        self.updated_geom = shifted_geom
        self.setWindowTitle("Friedel Pearson Cross Correlator")
        self.padview.sig_dataframe_changed.connect(self.auto_update)
        self.padview.sig_geometry_changed.connect(self.auto_update)
        self.layout = QVBoxLayout()
        row = 0
        row += 1
        pimv = imview(P, fs_lims=(-nx / 2, nx / 2), ss_lims=(-ny / 2, ny / 2))
        pimv.add_line(0, vertical=True, pen=pg.mkPen(color="r", width=3))
        pimv.add_line(0, horizontal=True, pen=pg.mkPen(color="r", width=3))
        self.pxline = pimv.add_line(2 * x0, vertical=True, pen=pg.mkPen(color="g", width=3))
        self.pyline = pimv.add_line(2 * y0, horizontal=True, pen=pg.mkPen(color="g", width=3))
        self.pearson_imview = pimv
        self.layout.addWidget(self.pearson_imview)
        row += 1
        gimv = imview(G, fs_lims=(-nx / 2, nx / 2), ss_lims=(-ny / 2, ny / 2))
        gimv.add_line(0, vertical=True, pen=pg.mkPen(color="r", width=3))
        gimv.add_line(0, horizontal=True, pen=pg.mkPen(color="r", width=3))
        self.gxline = pimv.add_line(2 * x0, vertical=True, pen=pg.mkPen(color="g", width=3))
        self.gyline = pimv.add_line(2 * y0, horizontal=True, pen=pg.mkPen(color="g", width=3))
        self.gaussian_imview = gimv
        self.layout.addWidget(self.gaussian_imview)
        row += 1
        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("Auto Run"))
        self.autorun_button = QCheckBox()
        self.autorun_button.setChecked(False)
        hbox.addWidget(self.autorun_button)
        self.layout.addLayout(hbox)
        self.domask_button = QPushButton("Run Now")
        self.domask_button.clicked.connect(self.run_correlator)
        hbox.addWidget(self.domask_button)
        # row += 1
        self.update_button = QPushButton("Update Geometry")
        self.update_button.clicked.connect(self.update_geometry)
        hbox.addWidget(self.update_button)
        self.setLayout(self.layout)

    def update_geometry(self):
        if self.updated_geom:
            self.padview.update_pad_geometry(self.updated_geom)

    def run_correlator(self):
        df = self.padview.get_dataframe()
        geom, x0, y0, c0, extra = self.correlator.get_shifted_geom(dataframe=df)
        psiz = geom[0].pixel_size()
        print(f"Shift: {x0*psiz}, {y0*psiz}, {c0}")
        self.updated_geom = geom
        self.pearson_imview.setImage(extra["pearson_cc"])
        if geom is None:
            x0 = 0
            y0 = 0
        else:
            self.gaussian_imview.setImage(extra["gaussian_fit"])
        self.pxline.setValue(2*x0)
        self.pyline.setValue(2*y0)
        self.gxline.setValue(2 * x0)
        self.gyline.setValue(2 * y0)

    def auto_update(self):
        if self.autorun_button.isChecked():
            self.run_correlator()

    def update_plan(self):
        df = self.pv.get_dataframe()
        self.correlator = FriedelPearsonCC(df, max_shift=self.max_shift)