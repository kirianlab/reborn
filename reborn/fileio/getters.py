# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.
import time
import inspect
import logging
import threading
from functools import wraps
from abc import ABC, abstractmethod, abstractproperty
import numpy as np
from ..dataframe import DataFrame
from ..source import Beam


logger = logging.getLogger(__name__)
# logger.addHandler(logging.NullHandler())

debug = False

def do_nothing(self, *args, **kwargs):
    return None


def threadlocked(method):
    r""" Simple decorator that ensures that the mutex is locked when calling
    the method.  This ensures that the function cannot be called by another
    thread when one is already executing lines within the method.  This
    wrapper assumes that the class has a threading.Lock attribute named
    _lock."""
    @wraps(method)
    def wrapper(self, *args, **kwargs):
        timeout = kwargs.get("timeout", self._timeout)
        if self._lock.acquire(timeout=timeout):
            return method(self, *args, **kwargs)
        else:
            return do_nothing(self, *args, **kwargs)
    return wrapper


class FrameGetter(ABC):
    r"""
    A |FrameGetter| is meant to serve up |DataFrame| instances.  It exists so that we have a standard interface that
    hides the details of where data come from (disk, shared memory, on-the-fly simulations, etc.).  With such
    standardizations, we can sometimes build software that can work in a way that is agnostic to the data source.
    A good example of this is |PADView|, which allows one to flip through dataframes for any experiment, provided that
    a |FrameGetter| can be provided.

    The |DataFrame| instances contain diffraction raw data, x-ray |Beam| info, |PADGeometry| info, etc.  It is
    assumed that a set of frames can be indexed with integers, starting with zero.

    This |FrameGetter| class is an Abstract Base Class (ABC).  You cannot use it directly.  Instead, you must create
    a subclass.  Here is what a very simple subclass should look like:

    .. code-block:: Python

        class MyFrameGetter(FrameGetter):
            def __init__(self, arguments, **kwargs):
                # Do whatever is needed to set up the data source.
                # Be sure to set the n_frames attribute:
                self.n_frames = something_based_on_arguments
            def get_data(self, frame_number):
                # Do something to fetch data and create a proper DataFrame instance
                return mydataframe

    Minimally, your |FrameGetter| subclass should set the n_frames attribute that specifies how many frames there
    are, and the get_data method should be defined such that it returns a properly constructed |DataFrame| instance.
    The |FrameGetter| base class will then implement other conveniences such as get_next_frame(), get_previous_frame(),
    etc.

    POSTPROCESSING: Sometimes you have a |FrameGetter| subclass that serves up data, but for each frame you need to
    perform a couple of extra manipulations to the |DataFrame| that are not already implemented in your subclass.  For
    example, you need to create a mask for a liquid jet streak in a diffraction pattern.  To do this, you can define
    a list of postprocessing functions as follows:

    .. code-block:: Python

        def myprocessor(self, dataframe):
            # Do something to your dataframe.  The self argument allows access to the internals of the FrameGetter
            return dataframe

        mygetter = MyFrameGetter(param1=something, postprocessors=[myprocessor])
        df = mygetter.get_next_frame()

    In the above, the mygetter instance will perform the usual steps when you call the get_frame method, but the list
    of postprocessor functions will first intercept the |DataFrame| s before they are returned.
    """
    _lock = None  # A recursive mutex lock to avoid internal confusion when multi-threading
    _timeout = 0.010  # How much time (s) to wait before quitting an effort to fetch a frame
    run_id = None  # Identifier for a "run", which can be string defined as the user wishes
    _n_frames = 1  # How many frames are in the framegetter
    _n_frames_original = None
    current_frame = 0  # The most recently delivered frame
    current_random_frame = 0  # Current frame in the randomized list
    current_dataframe = None  # Most recently delivered DataFrame instance
    geom_dict = None
    history_length = 100
    history = np.zeros(history_length, dtype=int)
    history_index = 0
    init_params = None
    skip_empty_frames = True
    _pandas_dataframe = None
    _getter_thread = None
    _cached_data = None
    _cache_forward = False
    _debug = 0
    _args = None
    _kwargs = None
    _sorted_indices = None
    postprocessors = []

    @abstractmethod
    def __init__(self, *args, **kwargs):
        r""" When overriding the __init__ method, you should be sure
        to set the self.n_frames property.  Do not use any positional
        or keyword arguments that cannot be serialized, such as file
        handlers (instead, provide a file path and create the handler
        within the __init__ method).  If you pass in objects that cannot
        be serialized, your subclass will still work, but it will not be
        possible to pass your subclass into any of the parallelized
        processors that utilize FrameGetters"""
        pass

    def __new__(cls, *args, **kwargs):
        self = object.__new__(cls)
        logger.debug("__new__")
        self._lock = threading.RLock()
        self._args = args
        self._kwargs = kwargs
        self.postprocessors = kwargs.get("postprocessors", [])
        self._debug = kwargs.get("debug", False)
        if debug:
            self._debug = True
        return self

    def __copy__(self):
        logger.debug("__copy__")
        self.dbgmsg("__copy__")
        if self.init_params is not None:
            return type(self)(**self.init_params)
        raise ValueError(
            "Cannot copy because init_params is not defined for this FrameGetter subclass."
        )

    def factory(self):
        self.dbgmsg("factory")
        r"""Returns a function that, when called, will replicate this class instance.  Useful if you need to
        replicate a class instance within parallel processes."""
        t = type(self)
        args = self._args
        kwargs = self._kwargs

        def factory():
            return t(*args, **kwargs)

        return factory

    @property
    @threadlocked
    def n_frames(self):
        if self._pandas_dataframe is not None:
            return len(self._pandas_dataframe)
        return self._n_frames

    @n_frames.setter
    @threadlocked
    def n_frames(self, n_frames):
        try:
            n_frames = int(n_frames)
        except OverflowError:  # Could be infinity, which is ok
            pass
        if self._n_frames_original is None:
            self._n_frames_original = n_frames
        self._n_frames = n_frames

    def dbgmsg(self, *args, **kwargs):
        if self._debug:
            print("DEBUG:FrameGetterABC:", *args, **kwargs)

    @abstractmethod
    def get_data(self, frame_number=0) -> DataFrame:
        r"""
        You *must* overrider this when making a subclass.
        You can do anything you want here, but you must return a *valid*
        |DataFrame| instance.  Do not attempt to modify any private
        attributes of the abstract base class.  Any property prefixed with
        an underscore is off-limits.
        """
        pass

    def _get_data_cache_forward(self, frame_number=0):
        r"""
        Experimental.  This will return the requested frame from cache if it is available, and launch a thread to
        fetch the next frame.

        Args:
            frame_number:

        Returns:

        """
        self.dbgmsg("_get_data_cache_forward")
        # Initialize the data to return:
        dat = None
        # If a thread has been launched already, make sure it is done processing:
        if self._getter_thread is not None:
            self.dbgmsg("joining thread")
            self._getter_thread.join()
        # If data has been cached by the thread, check if it is the data we want:
        if self._cached_data is not None:
            if self._cached_data["frame_number"] == frame_number:
                self.dbgmsg("found cached data for frame", frame_number)
                dat = self._cached_data["data"]
        # If there is no cached data (or the wrong data), then we must fetch it now (and wait for it...):
        if dat is None:
            self.dbgmsg("fetching data directly")
            dat = self.get_data(frame_number)
        # Launch a thread to fetch the next frame:
        next_frame = frame_number + 1
        if next_frame < self.n_frames:  # If we expect another frame...
            self.dbgmsg("launching thread for frame", next_frame)
            self._getter_thread = threading.Thread(
                target=self._threaded_get_data, args=(next_frame,)
            )
            self._getter_thread.start()
        else:  # If there are no more frames to fetch...
            self._getter_thread = None
            self._cached_data = None
        return dat

    def _threaded_get_data(self, frame_number=0):
        r"""Gets data, but returns a dictionary that also has frame number."""
        self.dbgmsg("_threaded_get_data")
        self._cached_data = {
            "frame_number": frame_number,
            "data": self.get_data(frame_number=frame_number),
        }
        return None

    @property
    def pandas_dataframe(self):
        if self._pandas_dataframe is None:
            import pandas
            self._pandas_dataframe = pandas.DataFrame(
                {"Frame #": np.arange(self.n_frames)}
            )
        return self._pandas_dataframe

    @pandas_dataframe.setter
    def pandas_dataframe(self, df):
        self._pandas_dataframe = df

    def set_pandas_dataframe(self, df):
        self.dbgmsg("set_pandas_dataframe")
        print(df.head())
        self.pandas_dataframe = df

    @threadlocked
    def sort_by(self, column, ascending=True):
        r""" If you have a pandas dataframe, and you wish to sort by
        a particular column, provide the column name here."""
        self.dbgmsg("sort_by")
        df = self._pandas_dataframe
        indices = df.sort_values(column, ascending=ascending).index
        self.set_sorted_indices(indices)

    @threadlocked
    def set_sorted_indices(self, indices):
        self.dbgmsg("set_sorted_indices")
        self.history = np.zeros(self.history_length, dtype=int)
        self.history_index = 0
        self.current_frame = 0
        self._sorted_indices = np.array(indices)

    @property
    def is_sorted(self):
        return self._sorted_indices is not None

    @threadlocked
    def get_frame(
        self, frame_number=0, raw_frame_number=None, wrap_around=True, log_history=True
    ) -> DataFrame:
        r""" Get the dataframe corresponding to a given index.

        Arguments:
            frame_number (int): The raw frame number of the desired |DataFrame|

        """
        logger.debug("get_frame")
        tic = time.time()
        self.dbgmsg("get_frame:", frame_number)
        self.dbgmsg("\tcurrent_frame =", self.current_frame)
        self.dbgmsg("\tn_frames =", self.n_frames)
        input_frame_number = frame_number
        if wrap_around:
            if frame_number >= self.n_frames or frame_number < 0:
                frame_number = frame_number % self.n_frames
                self.dbgmsg("\twrap_around, frame_number ->", frame_number)
        if self._sorted_indices is not None:
            self.dbgmsg("\tsorted_indices:", self._sorted_indices)
            self.dbgmsg("\t_n_frames_original", self._n_frames_original)
            self.dbgmsg("\thas sorted_indices: requested (sorted) frame_number ->", frame_number)
            fn = frame_number % self.n_frames
            frame_number = self._sorted_indices[fn]
            self.dbgmsg("\thas sorted_indices: actual (raw) frame_number ->", frame_number)
        if raw_frame_number is not None:
            frame_number = raw_frame_number
            self.dbgmsg("raw_frame_number given: frame_number ->", frame_number)
        dat = None
        if frame_number >= self._n_frames:
            raise StopIteration
        # if self.current_frame == frame_number and self.current_dataframe is not None:
        #     dat = self.current_dataframe
        # else:
        self.current_frame = input_frame_number % self.n_frames
        self.dbgmsg("\tcurrent_frame ->", self.current_frame)
        if log_history:
            self._log_history()
        if self._cache_forward:
            dat = self._get_data_cache_forward(frame_number=frame_number)
        else:
            dat = self.get_data(frame_number=frame_number)
        if self.postprocessors:
            for i, processor in enumerate(self.postprocessors):
                if inspect.isfunction(processor):
                    dat = processor(self, dat)
                else:
                    if inspect.isclass(processor):
                        processor = processor(dat)
                        self.postprocessors[i] = processor
                    dat = processor.process(dat)
        self.current_dataframe = dat
        self.dbgmsg(time.time() - tic, "seconds to load frame", frame_number)
        return dat

    @threadlocked
    def _log_history(self):
        r"""Do not override this method."""
        self.dbgmsg("_log_history")
        self.history[self.history_index] = self.current_frame
        self.history_index = int((self.history_index + 1) % self.history_length)

    def get_history_previous(self) -> DataFrame:
        r"""Do not override this method."""
        self.dbgmsg("get_history_previous")
        self.history_index = int((self.history_index - 1) % self.history_length)
        return self.get_frame(self.history[self.history_index], log_history=False)

    def get_history_next(self) -> DataFrame:
        r"""Do not override this method."""
        self.dbgmsg("get_history_next")
        self.history_index = int((self.history_index + 1) % self.history_length)
        return self.get_frame(self.history[self.history_index], log_history=False)

    def get_next_frame(self, skip=1, wrap_around=True) -> DataFrame:
        r"""Do not override this method."""
        self.dbgmsg("get_next_frame")
        df = None
        for _ in range(self.n_frames):
            df = self.get_frame(self.current_frame + skip, wrap_around=wrap_around)
            if (df is None) and self.skip_empty_frames:
                continue
            break
        return df

    def get_previous_frame(self, skip=1, wrap_around=True) -> DataFrame:
        r"""Do not override this method."""
        self.dbgmsg("get_previous_frame")
        df = None
        for _ in range(self.n_frames):
            df = self.get_frame(self.current_frame - skip, wrap_around=wrap_around)
            if (df is None) and self.skip_empty_frames:
                continue
            break
        return df

    def get_random_frame(self) -> DataFrame:
        r"""Do not override this method."""
        self.dbgmsg("get_random_frame")
        df = None
        for _ in range(self.n_frames):
            df = self.get_frame(int(np.floor(np.random.rand(1) * self.n_frames)))
            if (df is None) and self.skip_empty_frames:
                continue
            break
        return df

    def get_first_frame(self) -> DataFrame:
        r"""Get first frame (and skip empty frames if self.skip_empty_frames is True)."""
        self.dbgmsg("get_first_frame")
        df = self.get_frame(frame_number=0)
        if df is None:
            df = self.get_next_frame()
        return df

    def view(self, **kwargs):
        r"""Create a PADView instance and start it with this FrameGetter instance."""
        self.dbgmsg("view")
        from ..viewers.qtviews.padviews import PADView
        pv = PADView(frame_getter=self, **kwargs)
        pv.start()

    def get_padview(self, **kwargs):
        r"""Create a PADView instance with this FrameGetter instance."""
        self.dbgmsg("get_padview")
        from ..viewers.qtviews.padviews import PADView
        return PADView(frame_getter=self, **kwargs)

    def __iter__(self):
        r"""Do not override this method."""
        return _FGIterator(self)


class _FGIterator:
    def __init__(self, fg):
        fg.current_frame = 0
        self.fg = fg

    def __next__(self):
        return self.fg.get_next_frame(wrap_around=False)


class ListFrameGetter(FrameGetter):
    r"""
    Very simple FrameGetter subclass that operates on a list or similar type of iterable object.
    """

    def __init__(self, dataframes):
        super().__init__()
        self.n_frames = len(dataframes)
        self.dataframes = dataframes

    def get_data(self, frame_number=0):
        return self.dataframes[frame_number]


class TestFrameGetter(FrameGetter):

    def __init__(
        self, pad_geometry=None, n_frames=10, experiment_id="__test__", run_id=0
    ):
        r"""
        FrameGetter for testing. Serves unmasked random values.

        Arguments:
            pad_geometry (|PADGeometryList|): Detector geometry for experiment.
            n_frames (int): Number of frames to simulate.
            experiment_id (str): A |FrameGetter| requires experiment id.
            run_id (int): A |FrameGetter| requires a run id.
        """
        super().__init__()
        self.pad_geometry = pad_geometry
        self.n_frames = n_frames
        self.experiment_id = experiment_id
        self.run_id = run_id

    def get_data(self, frame_number=0):
        return DataFrame(
            beam=Beam(),
            raw_data=self.pad_geometry.random(),
            frame_id=frame_number,
            mask=self.pad_geometry.ones(),
        )
