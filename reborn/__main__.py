import re
import os
import glob
import shutil
import logging
import platform
import argparse
import subprocess

logger = logging.getLogger(__name__)

double_line = 75 * "="
single_line = 75 * "-"


def open_docs():
    print("")
    print(double_line)
    import webbrowser
    url = "https://kirianlab.gitlab.io/reborn"
    print("Opening reborn documentation webpage\n"
          "Check your web browser if a window did not pop up.\n"
          "Docs: https://kirianlab.gitlab.io/reborn/\n"
          "Gitlab: https://gitlab.com/kirianlab/reborn")
    print(double_line)
    print("")
    webbrowser.open(url)


def is_meson_available():
    available = False
    version = None
    try:
        print("Checking for the meson build package...", end="")
        result = subprocess.run(['meson', '--version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        version = result.stdout.decode().strip()
        available = True
    except (subprocess.CalledProcessError, FileNotFoundError):
        pass
    if available:
        print(f"found version {version}")
    else:
        print("The meson build package does not appear to be available."
              "Perhaps you can try: 'conda install meson' or equivalent if on Linux."
              "Otherwise, Google search 'how to install meson build system on <insert your OS here>'")


def is_fortran_compiler_available():
    available = False
    compiler = None
    version = None
    compilers = ['gfortran', 'ifort', 'flang']  # Common Fortran compilers
    for compiler in compilers:
        try:
            print(f"Checking for {compiler} compiler...", end="")
            result = subprocess.run([compiler, '--version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
            version = result.stdout.decode().splitlines()[0]  # Get the first line (version information)
            available = True
            print(f"found version {version}")
        except (subprocess.CalledProcessError, FileNotFoundError):
            print(f"not found")
            continue
    if not available:
        print("No Fortran compiler was found."
              "Perhaps you can try: 'apt-get install gfortran' or equivalent if using Linux."
              "Otherwise, Google search: 'how to install fortran compiler on <insert your OS here>'.")
    else:
        print("It appears that you have a fortran compiler."
              "If you are having issues, you might need to look to the numpy.f2py docs.")


def fortran_delete_compiled(prompt=True):
    fortran_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "fortran")
    print(f"Searching for files to delete in {fortran_path}")
    files = glob.glob(os.path.join(fortran_path, '*'))
    files_to_delete = []
    directories_to_delete = []
    for f in files:
        if f.lower().endswith(('.f90', '.py')):
            continue
        if os.path.isdir(f):
            if not f.lower().endswith("__pycache__"):
                directories_to_delete.append(f)
                continue
        else:
            files_to_delete.append(f)
    if len(files_to_delete) == 0 and len(directories_to_delete) == 0:
        print("No files to delete")
        return
    if len(files_to_delete) > 0:
        print("\nFiles to delete:\n---------------")
        for f in files_to_delete:
            print(f)
    if len(directories_to_delete) > 0:
        print("\nDirectories to delete:\n---------------")
        for f in directories_to_delete:
            print(f)
    if prompt:
        proceed = input("\n\nType 'yes' if you wish to delete the above files: ").strip().lower()
    else:
        proceed = "yes"
    if proceed == 'yes':
        if len(files_to_delete) > 0:
            for f in files_to_delete:
                print(f"Deleting file: {f}")
                os.remove(f)
        if len(directories_to_delete) > 0:
            for f in directories_to_delete:
                print(f"Deleting directory: {f}")
                shutil.rmtree(f)
    else:
        print("Aborted.")


def fortran_check_system():
    logger.info("Checking system")
    is_meson_available()
    is_fortran_compiler_available()


def check_opencl():
    try:
        system_platform = platform.system()
        if system_platform == "Windows":
            process = subprocess.run(["clinfo.exe"], capture_output=True, text=True)
        else:
            process = subprocess.run(["clinfo"], capture_output=True, text=True)
        return_code = process.returncode
        if return_code == 0:
            print("Succeeded in running clinfo.")
        in_header = True
        for line in process.stdout.splitlines():
            if in_header:
                if "number of platforms" in line.lower():
                    print(line)
                if len(line.strip()) == 0:
                    in_header = False
            else:
                if line.lower().startswith("  platform name") or line.lower().startswith("  device name") or line.lower().startswith("  device vendor  ") or line.lower().startswith("  device version"):
                    print(line)
        if return_code == 0:
            return process.stdout
        else:
            raise RuntimeError("Failed to run clinfo.")
    except FileNotFoundError:
        raise RuntimeError("clinfo is not installed or not found in the system's PATH."
                           "Install clinfo and try again.")


def print_pyopencl_device_info(device):
    r""" Print some useful information about available devices. """
    d = device
    # Print out some information about the devices
    print("    Name:", d.name)
    print("    Version:", d.opencl_c_version)
    print("    Max. Compute Units:", d.max_compute_units)
    if 'cl_khr_fp64' not in d.extensions.split():
        print("    Double Precision Support: No")
    else:
        print("    Double Precision Support: Yes")
    print("    Local Memory Size:", d.local_mem_size / 1024, "KB")
    print("    Global Memory Size:", d.global_mem_size / (1024 * 1024), "MB")
    print("    Max Alloc Size:", d.max_mem_alloc_size / (1024 * 1024), "MB")
    print("    Max Work-group Total Size:", d.max_work_group_size)
    print("    Cache Size:", d.global_mem_cacheline_size)
    # Find the maximum dimensions of the work-groups
    dim = d.max_work_item_sizes
    print("    Max Work-group Dims:(", dim[0], " ".join(map(str, dim[1:])), ")")


def check_pyopencl(extended=False):
    r"""
    Print out some useful information about platforms and devices that are
    available for running simulations.

    Arguments:
        extended (bool):  Print extended details above the summary.
    """
    print("Checking for a working pyopencl module")
    try:
        print("Attempting to import pyopencl...")
        import pyopencl as cl
    except ImportError:
        print("Failed to import pyopencl.  Install it (e.g. conda install pyopencl)")
        return None
    print(f"Found pyopencl here: {cl.__file__}")
    if extended:
        def print_info(obj, info_cls):
            for info_name in sorted(dir(info_cls)):
                if not info_name.startswith("_") and info_name != "to_string":
                    info = getattr(info_cls, info_name)
                    try:
                        info_value = obj.get_info(info)
                    except:
                        info_value = "<error>"

                    if (info_cls == cl.device_info and info_name == "PARTITION_TYPES_EXT"
                        and isinstance(info_value, list)):
                        print("%s: %s" % (info_name, [
                            cl.device_partition_property_ext.to_string(v,
                                                        "<unknown device partition property %d>")
                            for v in info_value]))
                    else:
                        try:
                            print("%s: %s" % (info_name, info_value))
                        except:
                            print("%s: <error>" % info_name)

        short = False

        for platform in cl.get_platforms():
            print(75 * "=")
            print(platform)
            print(75 * "=")
            if not short:
                print_info(platform, cl.platform_info)

            for device in platform.get_devices():
                if not short:
                    print(75 * "-")
                print(device)
                if not short:
                    print(75 * "-")
                    print_info(device, cl.device_info)
                    ctx = cl.Context([device])
                    for mf in [
                        cl.mem_flags.READ_ONLY,
                        # cl.mem_flags.READ_WRITE,
                        # cl.mem_flags.WRITE_ONLY
                    ]:
                        for itype in [
                            cl.mem_object_type.IMAGE2D,
                            cl.mem_object_type.IMAGE3D
                        ]:
                            try:
                                formats = cl.get_supported_image_formats(ctx, mf, itype)
                            except:
                                formats = "<error>"
                            else:
                                def str_chd_type(chdtype):
                                    result = cl.channel_type.to_string(chdtype,
                                                                       "<unknown channel data type %d>")

                                    result = result.replace("_INT", "")
                                    result = result.replace("UNSIGNED", "U")
                                    result = result.replace("SIGNED", "S")
                                    result = result.replace("NORM", "N")
                                    result = result.replace("FLOAT", "F")
                                    return result

                                formats = ", ".join(
                                    "%s-%s" % (
                                        cl.channel_order.to_string(iform.channel_order,
                                                                   "<unknown channel order 0x%x>"),
                                        str_chd_type(iform.channel_data_type))
                                    for iform in formats)

                            print("%s %s FORMATS: %s\n" % (
                                cl.mem_object_type.to_string(itype),
                                cl.mem_flags.to_string(mf),
                                formats))
                    del ctx

    print(75 * "=")
    print('Summary of platforms and devices:')
    print(75 * "=")
    # Create a list of all the platform IDs
    platforms = cl.get_platforms()
    print("\nNumber of OpenCL platforms:", len(platforms))

    # Investigate each platform
    for p in platforms:
        # Print out some information about the platforms
        print("\n-------------------------\n")
        print("Platform:", p.name)
        print("Vendor:", p.vendor)
        print("Version:", p.version)
        # Discover all devices
        devices = p.get_devices()
        print("Number of devices:", len(devices))
        # Investigate each device
        for d in devices:
            print_pyopencl_device_info(d)

    print("")
    print(75 * "-")
    print('\nEnvironment variables that affect the behavior of ClCore:\n')
    print("PYOPENCL_CTX defines the default device and platform.  Example:")
    print("")
    print("> export PYOPENCL_CTX='0:1'")
    print("")
    print("REBORN_CL_GROUPSIZE sets the default work group size:")
    print("")
    print("> export REBORN_CL_GROUPSIZE=32")
    print("")
    print(75 * "-")


def run_pytest():
    directory = os.path.abspath(os.path.join(__file__, "..", "..", 'test'))
    try:
        # Run pytest in the specified directory and redirect output to stdout
        result = subprocess.run(['pytest'], cwd=directory, check=True)

    except subprocess.CalledProcessError as e:
        # Handle the case where pytest returns a non-zero exit code
        print(f"Pytest failed with error code {e.returncode}")
    except Exception as e:
        # Handle other possible exceptions
        print(f"An error occurred: {e}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Command-line utility for reborn Fortran compilation.")
    parser.add_argument("-y", "--yes", action="store_false", help="Do not ask questions before executing commands (be careful!)")
    parser.add_argument('--docs', action='store_true', help="Open the reborn HTML docs.")
    parser.add_argument("--pytest", action='store_true', help="Run the reborn unit tests with pytest")
    parser.add_argument('--cleanup_fortran', action='store_true', help="Delete all of the compiled objects.")
    parser.add_argument("--recompile_fortran", action='store_true', help="Recompile fortran code.")
    parser.add_argument("--check_fortran", action="store_true", help="Attempt to check your system for compiler and build system.")
    parser.add_argument("--check_opencl", action="store_true", help="Check if pyopencl is installed and working")
    parser.add_argument("--debug", action="store_true", help="Print debugging information to terminal.")
    args = parser.parse_args()
    from reborn.utils import log_to_terminal
    if args.docs:
        open_docs()
        exit()
    if args.pytest:
        run_pytest()
    if args.check_opencl:
        check_opencl()
        check_pyopencl()
    if args.debug:
        log_to_terminal(level=logging.DEBUG)
    else:
        log_to_terminal(level=logging.INFO)
    if args.cleanup_fortran:
        fortran_delete_compiled(prompt=args.yes)
    if args.recompile_fortran:
        fortran_delete_compiled(prompt=args.yes)
        from reborn.fortran import utils_f
        if utils_f is not None:
            print("Successfully recompiled fortran modules")
    if args.check_fortran:
        fortran_check_system()
