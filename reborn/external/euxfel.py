# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

r"""
Utilities for working with EuXFEL data. Uses EuXFEL's extra_data package.
Documentation:

* https://rtd.xfel.eu/docs/data-analysis-user-documentation/en/latest/index.html
* https://extra-data.readthedocs.io/en/latest/index.html

Source Code: https://github.com/European-XFEL/EXtra-data
"""

import numpy as np
import reborn
from reborn import utils
from reborn.source import Beam
try:
    import extra_data
except:
    extra_data = None
from glob import glob

debug = False


def find_proposal(propnum):
    """Find proposal directory given proposal number."""
    return glob(f'/gpfs/exfel/exp/*/*/{propnum:06d}')[0]


def check_extra_data():
    r""" Check if the extra_data package is installed.  """
    if extra_data is None:
        print('You do not have the extra_data python package installed.')
        print('Look here: https://rtd.xfel.eu/docs/data-analysis-user-documentation/en/latest/index.html')
        print('Look here: https://extra-data.readthedocs.io/en/latest/index.html')
        raise ImportError('The extra_data package is required.')


def debug_message(*args, caller=True, **kwargs):
    r""" Standard debug message, which includes the function called. """
    if debug:
        s = ''
        if caller:
            s = utils.get_caller(1)
        print(f'DEBUG:euxfel.{s}:', *args, **kwargs)


def inspect_available_data(experiment_id, run_id, source=None):
    r"""
    Prints out available data sources for a specific proposal & run.
    If given a data source, it will also print out the keys for that data source.

    Args:
        experiment_id (int): Experiment proposal number.
        run_id (int): Experiment run number.
        source (str): data source (example='SPB_XTD9_XGM/XGM/DOOCS').
    """
    debug_message('opening run')
    check_extra_data()
    run = extra_data.open_run(proposal=experiment_id, run=run_id, data='raw')
    debug_message('gathering sources')
    print(f'Data Sources:\n\n{run.all_sources}\n\n')
    if source is not None:
        source_data = run[source]
        print(f'Data source keys:\n\n{source_data.keys()}\n\n')


class EuXFELFrameGetter(reborn.fileio.getters.FrameGetter):
    r"""
    EuXFELFrameGetter to retrieve detector data from EuXFEL endstations in the standard way.

    EuXFEL saves a series of exposures each corresponding to an individual x-ray pulse together,
    indexed by the pulse_train. This framegetter handles that for you so you can iterate directly through
    frames as if they were globally indexed as in (LCLS or SACLA). The trains are cached so the
    data is not reloaded if the next frame is within the same train.

    Args:
        experiment_id (int): Experiment proposal number.
        run_id (int): Experiment run number.
        pad_detectors (str): pad detector data path in H5 (example='SPB_DET_AGIPD1M-1/DET/\*CH0:xtdf',
        default='\*/DET/\*').
        geom (|PADGeometryList|): reborn.detector.PADGeometryList instance with the experiment geometry.
        max_events (int): Maximum number of frames to retrieve.
        beam (|Beam|): reborn.source.Beam instance with the x-ray details for the run.
    """
    current_train_stack = None
    current_train_id = None
    beam = None
    mask = None
    n_trains = None

    def __init__(self, experiment_id, run_id,
                 geom=None, beam=None, mask=None,
                 pad_detectors='*/DET/*',
                 pad_detector_motor='SPB_IRU_AGIPD1M/MOTOR/Z_STEPPER',
                 xray_wavelength_detector='SA1_XTD2_XGM/XGM/DOOCS',
                 max_events=None,
                 data='raw',
                 require_all=False):
        debug_message('Initializing superclass')
        check_extra_data()
        super().__init__()
        self.init_params = {'experiment_id': experiment_id,
                            'run_id': run_id,
                            'pad_detectors': pad_detectors,
                            'geom': geom,
                            'max_events': max_events,
                            'beam': beam}
        self.experiment_id = experiment_id
        self.run_id = run_id
        self.pad_detectors = pad_detectors
        debug_message('setting geometry')
        self.pad_geometry = geom
        self.mask = mask
        # extra data first loads a run
        # in the background this is opening an HDF5 file
        # we can load raw or processed (dark calibrated) data
        debug_message('gathering run data')
        if data not in {'raw', 'proc'}:
            raise NotImplementedError('Only raw and proc data supported')
        run = extra_data.open_run(proposal=self.experiment_id, run=self.run_id, data=data)
        self.train_ids = set(run.train_ids)
        self.n_trains = len(self.train_ids)
        # here we select the type of data we want (pad detector exposures)
        debug_message('finding sources')
        # annoyingly, extra-data will still return all train ids in a run when setting
        #           setting require_all=True
        # it will instead return None for train ids which have missing data
        self.selection = run.select(self.pad_detectors, 'image.data', require_all=require_all)
        # data is saved for each individual panel
        # so there N files for a detector with N pads
        # this finds all the files needed to stitch together a single exposure
        sources = sorted(self.selection.all_sources, key=lambda s: int(s.split('/')[-1].split('CH')[0]))
        # build a mapping from the trains to individual exposures
        debug_message('building frame index')
        t_shots = run[sources[0], 'image.data'].data_counts()
        tids = t_shots.index.values  # all the train ids
        ntids = t_shots.to_numpy()  # number of shots in each train id
        frame_cumsum = ntids.cumsum()
        self.n_frames = frame_cumsum[-1]
        self.frames = np.array([tids, frame_cumsum]).T
        debug_message('enforcing max_events')
        if max_events is not None:
            self.n_frames = min(max_events, self.n_frames)
        # the photon wavelength is easily accessible by opening the raw data
        debug_message('gather photon energy')
        run_raw = extra_data.open_run(proposal=self.experiment_id, run=self.run_id, data='raw')
        self.photon_data = run_raw[xray_wavelength_detector, 'pulseEnergy.wavelengthUsed.value']
        if pad_detector_motor is not None:
            pad_detector_motor_position = run_raw[pad_detector_motor, 'actualPosition.value']
            detector_position = pad_detector_motor_position.as_single_value()  # result is in mm
            vec = np.array([0, 0, 1e-3 * detector_position])  # convert to m (reborn is in SI)
            self.pad_geometry = self.update_detector_distance(vector=vec)
        if self.beam is None:
            for train_id, _ in self.frames:
                try:
                    _, wavelength = self.photon_data.train_from_id(train_id)  # result is in nm
                except Exception as e:
                    print(str(e))
                    continue
                debug_message('Beam not specified, setting from wavelength measurement.')
                self.beam = Beam(wavelength=wavelength * 1e-9)
                break

    def update_detector_distance(self, vector=np.array([0, 0, 1e-3])):
        r"""
        Modify the PADGeometryList.

        Arguments:
            vector (|ndarray|): This is the vector indicating the direction and step size. The stage
                                offset and vector are added and set to the PADGeometry.t_vec
        """
        pads = self.pad_geometry.copy()
        pads.translate(vector)
        debug_message(f'Shifted detector by {vector}')
        return pads

    def _get_train_stack(self, train_id):
        debug_message('looking for stack')
        tid, train_data = self.selection.train_from_id(train_id)
        stack = extra_data.stack_detector_data(train_data, 'image.data')
        return np.double(stack)

    def _find_train_stack_from_frame_number(self, frame_number):
        cond = self.frames[:, 1].astype(int) - frame_number > 0
        tid = self.frames[:, 0][cond][0]
        # if tid not in self.train_ids:
        return tid

    def get_data(self, frame_number=0):
        debug_message()
        # load the data from the HDF5 file
        debug_message('loading train')
        train_id = self._find_train_stack_from_frame_number(frame_number=frame_number)
        # print(train_id)
        # cache current train stack
        if self.current_train_stack is not None:
            if train_id == self.current_train_id:
                stacked = self.current_train_stack
            else:
                try:
                    stacked = self._get_train_stack(train_id)
                except Exception as e:
                    print(str(e))
                    return None
                self.current_train_id = train_id
                self.current_train_stack = stacked
        else:
            try:
                stacked = self._get_train_stack(train_id)
            except Exception as e:
                print(str(e))
                return None
            self.current_train_id = train_id
            self.current_train_stack = stacked
        stacked_pulse = stacked[frame_number]
        debug_message('building dataframe')
        df = reborn.dataframe.DataFrame()
        debug_message('setting calibrated pad detector data')
        df.set_dataset_id(f'run:{self.run_id} (Data)')
        df.set_frame_id(f'run:{self.run_id}:{frame_number}')
        df.set_frame_index(frame_number)
        debug_message('getting detector stage position')
        debug_message('setting PADGeometry')
        df.set_pad_geometry(self.pad_geometry)
        df.set_mask(self.mask)
        df.set_raw_data(stacked_pulse)
        debug_message('retrieving x-ray data')
        try:
            _, wavelength = self.photon_data.train_from_id(train_id)  # result is in nm
        except ValueError:
            return None
        debug_message('setting Beam')
        self.beam = Beam(wavelength=wavelength * 1e-9)
        df.set_beam(self.beam)
        debug_message('returning', df)
        return df
