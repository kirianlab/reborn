# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import os
import logging
import numpy as np
from reborn.detector import PADGeometryList
from reborn.source import Beam
from reborn.target.crystal import CrystalStructure
from reborn.target import crystal, atoms
from scipy import interpolate
from ..utils import vec_mag
try:
    import saxstats.saxstats as saxstats
except ImportError:
    saxstats = None

logger = logging.getLogger(__name__)


def get_crystal_structure(
    pdb_file, cell=100e-9, create_bio_assembly=True, ignore_waters=False
) -> CrystalStructure:
    """
    Retrieve the crystal structure from a given PDB file.

    Args:
        pdb_file (str): The PBD ID. Ex: "1jfp".
        cell (float, optional): Default cell size. Defaults to 100e-9.
        create_bio_assembly (bool, optional): Whether to create a bio assembly.
            Defaults to True.

    Returns:
        object: An instance of CrystalStructure.
    """
    uc = (cell,) * 3 + (np.pi / 2,) * 3
    cryst = crystal.CrystalStructure(
        pdb_file,
        spacegroup="P1",
        unitcell=uc,
        create_bio_assembly=create_bio_assembly,
        ignore_waters=ignore_waters,
    )
    uc = (cryst.molecule.get_size(),) * 3 + (np.pi / 2,) * 3
    cryst = crystal.CrystalStructure(
        pdb_file,
        spacegroup="P1",
        unitcell=uc,
        create_bio_assembly=create_bio_assembly,
        ignore_waters=ignore_waters,
    )
    return cryst


def get_denss_pdb(pdb_file, create_bio_assembly=True, ignore_waters=False):
    """
    This is a hack to add the biomolecular assembly to DENSS PDB format.
    Get the PDB file to be used with DENSS.

    Args:
        pdb_file (str): The PBD ID. Ex: "1jfp".
        create_bio_assembly (bool, optional): Whether to create a bio assembly.
            Defaults to True.
        ignore_waters (bool, optional): Ignore water molecules. Defaults to False.

    Returns:
        DENSS PDB object with bio assembly partners (if requested).
    """
    if saxstats is None:
        raise ImportError("The saxstats module could not be imported."
                          "You need to install the DENSS package."
                          "Look here: https://github.com/tdgrant1/denss")
    cryst = get_crystal_structure(
        pdb_file=pdb_file,
        create_bio_assembly=create_bio_assembly,
        ignore_waters=ignore_waters,
    )
    pdb = saxstats.PDB(pdb_file, ignore_waters=ignore_waters)
    n = int(pdb.natoms)
    s = cryst.molecule.n_atoms / n
    assert s % 1 == 0
    s = int(s)
    if s > 1:
        pdb.natoms = cryst.molecule.n_atoms
        pdb.atomnum = np.concatenate([pdb.atomnum] * s)
        pdb.atomname = np.concatenate([pdb.atomname] * s)
        pdb.atomalt = np.concatenate([pdb.atomalt] * s)
        pdb.resname = np.concatenate([pdb.resname] * s)
        pdb.resnum = np.concatenate([pdb.resnum] * s)
        pdb.chain = np.concatenate([pdb.chain] * s)  # FIXME: Unique names needed here?
        pdb.coords = cryst.molecule.coordinates * 1e10
        pdb.occupancy = np.concatenate([pdb.occupancy] * s)
        pdb.b = np.concatenate([pdb.b] * s)
        pdb.atomtype = np.concatenate([pdb.atomtype] * s)
        pdb.charge = np.concatenate([pdb.charge] * s)
        pdb.nelectrons = np.concatenate([pdb.nelectrons] * s)
        pdb.radius = None  # np.zeros(n*s) #np.concatenate([pdb.radius]*s)
        pdb.vdW = np.concatenate([pdb.vdW] * s)
        pdb.unique_volume = None  # np.zeros(n*s) #np.concatenate([pdb.unique_volume]*s)
        pdb.unique_radius = None  # np.zeros(n*s) #np.concatenate([pdb.unique_radius]*s)
        pdb.numH = np.concatenate([pdb.numH] * s)
    return pdb


def get_scattering_profile(
    pdb_file, create_bio_assembly=True, q_mags=None, ignore_waters=False
):
    """
    Retrieve the scattering profile from a given PDB file.

    Args:
        pdb_file (str): Path to the PDB file.
        create_bio_assembly (bool, optional): Whether to create a bio assembly.
            Defaults to True.
        ignore_waters (bool, optional): Ignore water molecules. Defaults to False.

    Returns:
        tuple: A tuple containing the qbin and I(q) values.
    """
    if saxstats is None:
        raise ImportError("The saxstats module could not be imported."
                          "You need to install the DENSS package."
                          "Look here: https://github.com/tdgrant1/denss")
    pdb = get_denss_pdb(
        pdb_file=pdb_file, create_bio_assembly=create_bio_assembly, ignore_waters=False
    )
    pdb2mrc = saxstats.PDB2MRC(pdb=pdb, run_all_on_init=True)
    if q_mags is not None:  # interpolate Iq to desired q grid
        I_interpolator = interpolate.interp1d(
            pdb2mrc.q_calc * 1e+10,
            pdb2mrc.I_calc,
            kind="cubic",
            fill_value="extrapolate",
        )
        Ic = I_interpolator(q_mags)
    else:
        q_mags = pdb2mrc.q_calc * 1e+10
        Ic = pdb2mrc.I_calc
    return q_mags, Ic


def create_density_map(
    pdb_file,
    solvent_contrast=True,
    create_bio_assembly=True,
    map_resolution=1e-10,
    map_oversampling=3,
    ignore_waters=False,
):
    """
    Create a density map from a given PDB file.

    Args:
        pdb_file (str): The PBD ID. Ex: "1jfp".
        solvent_contrast (bool, optional): If true, adds solvent contrast. Defaults to True.
        create_bio_assembly (bool, optional): Whether to create a bio assembly.
            Defaults to True.
        map_resolution (float, optional): Resolution of the map. Defaults to 0.2e-9.
        map_oversampling (int, optional): The map oversampling factor. Defaults to 2.
        ignore_waters (bool, optional): Ignore water molecules. Defaults to False.

    Returns:
        tuple: A tuple containing the density map [e-], side of the map [m], and extra info.
    """
    if saxstats is None:
        raise ImportError("The saxstats module could not be imported."
                          "You need to install the DENSS package."
                          "Look here: https://github.com/tdgrant1/denss")
    cryst = get_crystal_structure(
        pdb_file=pdb_file,
        create_bio_assembly=create_bio_assembly,
        ignore_waters=ignore_waters,
    )
    side = map_oversampling * cryst.molecule.get_size()
    nsamples = int(np.ceil(side / map_resolution))
    pdb = get_denss_pdb(
        pdb_file, create_bio_assembly=create_bio_assembly, ignore_waters=ignore_waters
    )
    pdb2mrc = saxstats.PDB2MRC(
        pdb=pdb, nsamples=nsamples, side=side * 1e10, run_all_on_init=True
    )
    if solvent_contrast:
        rho = pdb2mrc.rho_insolvent
    else:
        rho = pdb2mrc.rho_invacuo
    side = pdb2mrc.side
    extra_info = {
        "n_bio_assembly_partners": int(cryst.molecule.n_atoms / pdb.natoms),
        "pdb2mrc": pdb2mrc,
    }
    return rho, side * 1e-10, extra_info


def get_pad_pdb_scattering_intensity(pdb_file: str,
    create_bio_assembly: bool=True,
    ignore_waters: bool=False,
    beam: Beam=None,
    pad_geometry: PADGeometryList =None,
    mass_density:float =None,
    sample_thickness:float =None,
    as_list: bool=True):
    r""" 
    Produce biomolecule scattering intensity from a PDB file.
    
    Args:
        pdb_file (str): Path to the PBD file or PDB ID (Ex: "1jfp").
        create_bio_assembly (bool): Whether to create a bio assembly.  Defaults to True.
        ignore_waters (bool, optional): Ignore water molecules. Defaults to False.
        beam (|Beam|): X-ray beam info.
        pad_geometry (|PADGeometryList|): PAD geometry info.
        mass_density (float): Concentration of protein in kg/m^3 (numerically equivalent to mg/ml).
        sample_thickness (float): Thickness of the sample, assumed to be uniform across the x-ray beam.
        as_list (bool): Choose if you want the output to be a list of 2D PAD arrays, or a flat 1D array.  Default is True.

    Returns:

    """
    q_mags = pad_geometry.q_mags(beam=beam)
    _, f2 = get_scattering_profile(pdb_file, create_bio_assembly=create_bio_assembly, q_mags=q_mags, ignore_waters=ignore_waters)
    cryst = get_crystal_structure(pdb_file, cell=100e-9, create_bio_assembly=create_bio_assembly, ignore_waters=ignore_waters)
    molecule_mass = cryst.molecule.get_molecular_weight()
    sample_volume = sample_thickness * np.pi * (beam.diameter_fwhm/2)**2
    n_molecules = sample_volume * mass_density / molecule_mass
    intensity = pad_geometry.f2phot(beam=beam) * f2 * n_molecules
    if as_list:
        intensity = pad_geometry.split_data(intensity)
    return intensity
