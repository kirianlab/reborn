# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

r"""
Utilities for working with LCLS data.  Most of what you need is already in the psana package.  I don't know where the
official psana documentation is but if you work with LCLS data you should at least skim through all of the material in
the `LCLS Data Analysis Confluence pages <https://confluence.slac.stanford.edu/display/PSDM/LCLS+Data+Analysis>`_.
Note that there is documentation on
`LCLS PAD geometry <https://confluence.slac.stanford.edu/display/PSDM/Detector+Geometry>`_.
"""
import os
import re
import numpy as np
from .. import utils, detector, source, fileio, const, dataframe
from . import crystfel, cheetah

try:
    import psana
    from Detector import DetectorTypes
except ImportError:
    psana = None
from ..config import configs

debug = configs["debug"]

# these are the known area (pad) detectors, see:
# https://github.com/lcls-psana/Detector/blob/master/src/DetectorTypes.py
lcls_area_detectors = {
    "Opal1000",
    "Tm6740",
    "pnCCD",
    "Princeton",
    "Fccd",
    "Cspad",
    "Xamps",
    "Cspad2x2",
    "Fexamp",
    "Phasics",
    "Timepix",
    "Opal2000",
    "Opal4000",
    "Opal1600",
    "Opal8000",
    "Fli",
    "Quartz4A150",
    "DualAndor",
    "Andor",
    "OrcaFl40",
    "Epix",
    "Rayonix",
    "EpixSampler",
    "Pimax",
    "Fccd960",
    "Epix10k",
    "Epix10ka",
    "Epix10ka2M",
    "Epix10kaQuad",
    "Epix100a",
    "EpixS",
    "Gotthard",
    "ControlsCamera",
    "Archon",
    "Jungfrau",
    "Zyla",
    "Pixis",
    "Uxi",
    "StreakC7700",
    "Archon",
    "iStar",
    "Alvium",
}
# make case-insensitive
known_lcls_area_detectors = {d.casefold(): d for d in lcls_area_detectors}
# psana typically returns 3d arrays
# we want to follow reborn PADGeometryList conventions, see:
#           pad_to_asic_data_split(data, n, m)
known_detector_splits = {"cspad": (1, 2), "epix100a": (2, 2), "epix10k2m": (2, 2)}
# known reborn PADGeometryLists
known_reborn_geometries = {
    "cspad": detector.cspad_pad_geometry_list(detector_distance=1),
    "pncdd": detector.pnccd_pad_geometry_list(detector_distance=1),
    "epix100a": detector.epix100_pad_geometry_list(detector_distance=1),
    "epix10k2m": detector.epix10k_pad_geometry_list(detector_distance=1),
    "jungfrau": detector.jungfrau4m_pad_geometry_list(detector_distance=1),
    "rayonix": detector.rayonix_mx340_xfel_pad_geometry_list(detector_distance=1),
}
# this is to prevent hard-coding experimental values
# into the LCLSFrameGetter. Could be used as a global
# config, but I would not recommend that.
known_geometry_binnings = {
    "cspad": None,
    "pncdd": None,
    "epix100a": None,
    "epix10k2m": None,
    "jungfrau": None,
    "rayonix": 2,
}


def debug_message(*args, caller=True, **kwargs):
    r"""Standard debug message, which includes the function called."""
    if debug:
        s = ""
        if caller:
            s = utils.get_caller(1)
        print("DEBUG:lcls." + s + ":", *args, **kwargs)


def pad_to_asic_data_split(data, n, m):
    r"""
    Split an array of shape (P, N, M) into a list of n*m*P arrays of shape (N/n, M/m).

    For epix we split into 2x2

    For cspad we split into 1x2 (?)

    Arguments:
        data (np.ndarray): PAD data.
        n (int): Number of ASICS to split "vertically" with vsplit
        m (int): Number of ASICS to split "horizontally" with hsplit

    Returns:
        pads (list): List of separated PADs.
    """
    data = utils.atleast_3d(data)
    pads = []
    for a in data:
        b = np.vsplit(a, n)
        for c in b:
            d = np.hsplit(c, m)
            for e in d:
                pads.append(e)
    return pads


def get_pad_pixel_coordinates(pad_det, run_number, splitter):
    r"""
    This should work for any detector (except Rayonix, not implemented in psana)
    without modification so long as splitter is set up correctly.

    Parameters:
        pad_det (obj): psana detector object
        run_number (int): run number of interest
        splitter (func): function to split the arrays
                         returned by pad_det into
                         individual asics

    Returns:
        x (list)
        y (list)
        z (list)
    """
    s = pad_det.shape()
    if len(s) == 3:
        n_panels, n_ss, n_fs = pad_det.shape()
    elif len(s) == 2:
        n_panels = 1
        n_ss, n_fs = pad_det.shape()
    else:
        raise RuntimeError("Cannot figure out what kind of detector this is.")
    xdc, ydc, zdc = pad_det.coords_xyz(run_number)

    if xdc.size != n_panels * n_ss * n_fs:
        debug_message("Wrong size")
        return None
    if ydc.size != n_panels * n_ss * n_fs:
        debug_message("Wrong size")
        return None
    if zdc.size != n_panels * n_ss * n_fs:
        debug_message("Wrong size")
        return None

    x = splitter(xdc)
    y = splitter(ydc)
    z = splitter(zdc)
    return x, y, z


def get_pad_geometry_from_psana(pad_det, run_number, splitter):
    r"""
    Creates PADGeometryList from psana detector object.

    This should work for any detector without modification
    so long as splitter is set up correctly.

    Parameters:
        pad_det (obj): psana detector object
        run_number (int): run number of interest
        splitter (func): function to split the arrays
                         returned by pad_det into
                         individual asics

    Returns:
        PADGeometryList (list of reborn PADGeometry objects)

    Notes:
        CSPAD: Has rows of pixels that are elongated
               The nominal pixel size is 109.92 x 109.92 microns, but the central two columns (193 and 194)
               have pixels of size 274.80 x 109.92 microns. This is documented here:
               https://confluence.slac.stanford.edu/display/PSDM/Detector+Geometry
               https://confluence.slac.stanford.edu/display/PSDM/CSPAD+Geometry+and+Alignment
    """
    debug_message()
    xx, yy, zz = get_pad_pixel_coordinates(pad_det, run_number, splitter)

    geom = detector.PADGeometryList()
    for x, y, z in zip(xx, yy, zz):
        g = detector.PADGeometry()
        g.t_vec = np.array([x[0, 0], y[0, 0], z[0, 0]]) * 1e-6
        g.ss_vec = (
            np.array([x[2, 1] - x[1, 1], y[2, 1] - y[1, 1], z[2, 1] - z[1, 1]]) * 1e-6
        )
        g.fs_vec = (
            np.array([x[1, 2] - x[1, 1], y[1, 2] - y[1, 1], z[1, 2] - z[1, 1]]) * 1e-6
        )
        g.n_ss, g.n_fs = x.shape
        geom.append(g)
    return geom


class EpicsTranslationStageMotion:
    r"""A class that updates PADGeometry according to stages with positions specified by EPICS PVs."""

    def __init__(self, epics_pv=None, vector=np.array([0, 0, 1e-3])):
        r"""
        Arguments:
            epics_pv ('str'): The EPICS PV string.
            vector (|ndarray|): This is the vector indicating the direction and step size.  The stage position will be
                                multiplied by this vector and added to PADGeometry.t_vec
        """
        self.detector = psana.Detector(epics_pv)
        self.vector = np.array(vector)
        self.epics_pv = epics_pv

    def modify_geometry(self, pad_geometry, event):
        r"""Modify the PADGeometryList.

        Arguments:
            pad_geometry (|PADGeometryList|): PAD geometry.
            event (psana.Event): A psana event from which the stage position derives.
        """
        position = self.detector(event)
        p = pad_geometry.copy()
        p.translate(self.vector * position)
        debug_message(
            "Shifted detector.  %s=%f, vec=" % (self.epics_pv, position),
            self.vector * position,
        )
        return p


class TranslationMotion:
    r"""A class that updates PADGeometry with a shift."""

    def __init__(self, vector=np.array([0, 0, 1e-3])):
        r"""
        Arguments:
            vector (|ndarray|): The translation to apply to the PADGeometry
        """
        self.vector = np.array(vector)

    def modify_geometry(self, pad_geometry, event):
        r"""Modify the PADGeometryList.

        Arguments:
            pad_geometry (|PADGeometryList|): PAD geometry.
            event (psana.Event): A psana event from which the stage position derives.
        """
        p = pad_geometry.copy()
        p.translate(self.vector)
        debug_message("Static translation", self.vector)
        return p


class AreaDetector(object):
    r"""
    Thin wrapper for psana.Detector class. Adds methods to generate list of PADGeometry instances and to split the PAD
    data into a list of 2d arrays.
    """

    splitter = None
    motions = None  # Allow for some translation operations based on epics pvs
    _home_geometry = None  # This is the initial geometry, before translations/rotations
    adu_per_photon = None

    def __init__(
        self,
        pad_id=None,
        geometry=None,
        mask=None,
        data_type="calib",
        motions=None,
        run_number=1,
        adu_per_photon=None,
        **kwargs,
    ):
        r"""
        Instantiate with same arguments you would use to instantiate a psana.Detector instance.
        Usually this means to supply a psana.DataSource instance.

        Arguments:
            pad_id (str): Example: DscCsPad
            geometry (|PADGeometryList|): Geometry, or a path to geometry file.
            mask (|ndarray|): Mask array, or path to mask file
            data_type (str): Default data type ('calib' or 'raw')
            motions (dict): Special dictionaries to describe motorized motions of the detector

        """
        debug_message(f"AreaDetector with kwargs: {kwargs}")
        # check detector id is recognized (regardless of case)
        # if pad_id.casefold() not in known_lcls_area_detectors:
        #     print(f'Given pad_id: {pad_id} is unknown.\n'
        #           f'Please check what you are doing.\n'
        #           f'This return back non-real data.\n')
        # else:
        #     # get pad id as psana recognizes it
        #     pad_id = known_lcls_area_detectors[pad_id.casefold()]
        # this is a function that returns a detector object
        # psana uses this circular naming convention for some reason
        # in our case we want an AreaDetector, see:
        # https://github.com/lcls-psana/Detector/blob/master/src/AreaDetector.py
        self.detector = psana.Detector(pad_id, **kwargs)
        if isinstance(
            self.detector, DetectorTypes.MissingDet
        ):  # psana missing detector class
            raise TypeError(
                f"Detector {pad_id} is missing."
                f"Known psana detectors:\n\n{lcls_area_detectors}\n\n"
            )
        self.detector_type = self.get_detector_type()

        if self.detector_type.casefold() in known_detector_splits:
            split_n, split_m = known_detector_splits[self.detector_type]
            self.splitter = lambda data: pad_to_asic_data_split(data, split_n, split_m)

        self.data_type = data_type
        self.motions = []
        self.setup_motions(motions)

        if isinstance(geometry, str):
            try:  # Check if it is a reborn geometry file
                debug_message("Check for reborn geometry format")
                geometry = detector.load_pad_geometry_list(geometry)
            except:  # Check if it is a crystfel geometry file
                debug_message("Check for CrystFEL geometry format")
                geometry = crystfel.geometry_file_to_pad_geometry_list(geometry)
                if (self.detector_type == "cspad") and geometry.parent_data_shape[
                    0
                ] == 1480:
                    geometry = crystfel.fix_cspad_cheetah_indexing(geometry)
                    print(
                        "Your CrystFEL geometry assumes the psana data has been re-shuffled by Cheetah!!"
                    )
        if geometry is None:
            _detector_type = self.detector_type.casefold()
            # try to use reborn geometry
            if _detector_type in known_reborn_geometries:
                geometry = known_reborn_geometries[_detector_type]
            # check to see if we will bin the geometry
            if _detector_type in known_geometry_binnings:
                _bin_value = known_geometry_binnings[_detector_type]
                if _bin_value is not None:
                    geometry = geometry.binned(2)
            else:  # try to use geometry from psana
                geometry = get_pad_geometry_from_psana(
                    self.detector, run_number, self.splitter
                )
        self._home_geometry = geometry.copy()
        debug_message("Initial (home) geometry", self._home_geometry)
        self.mask = mask
        if isinstance(mask, (str, list)) and isinstance(mask[0], str):
            self.mask = geometry.concat_data(detector.load_pad_masks(mask))
        if self.mask is (None, []):
            self.mask = [p.ones() for p in self._home_geometry]
        self.adu_per_photon = adu_per_photon

    def setup_motions(self, motions):
        debug_message("setup_motions", motions)
        if isinstance(motions, list) or isinstance(motions, tuple):
            debug_message("list type")
            if isinstance(motions[0], float) or isinstance(motions[0], int):
                debug_message("simple vector")
                self.motions.append(TranslationMotion(vector=motions))
                return
            debug_message("list of motions")
            for m in motions:
                self.setup_motions(m)
        if isinstance(motions, str):
            debug_message("str")
            self.motions.append(EpicsTranslationStageMotion(epics_pv=motions))
        if isinstance(motions, dict):
            debug_message("dict")
            self.motions.append(EpicsTranslationStageMotion(**motions))

    def get_detector_type(self):
        """The psana detector fails to provide reliable information on detector type."""
        detector_id = self.detector.source.__str__()
        # print('='*70, '\n', detector_id, '='*70, '\n')
        if re.match(r".*cspad", detector_id, re.IGNORECASE) is not None:
            detector_type = "cspad"
        elif re.match(r".*pnccd.*", detector_id, re.IGNORECASE) is not None:
            detector_type = "pnccd"
        elif re.match(r".*epix10ka2m.*", detector_id, re.IGNORECASE) is not None:
            detector_type = "epix10k2m"
        elif re.match(r".*rayonix.*", detector_id, re.IGNORECASE) is not None:
            detector_type = "rayonix"
        elif re.match(r".*Epix100a.*", detector_id, re.IGNORECASE) is not None:
            detector_type = "epix100a"
        else:
            detector_type = "unknown"
        return detector_type

    def get_pad_geometry(self, event) -> detector.PADGeometryList:
        """See documentation for the function get_pad_geometry()."""
        geometry = self._home_geometry.copy()
        if self.motions:
            for m in self.motions:
                geometry = m.modify_geometry(geometry, event)
        return geometry

    def split_pad(self, data):
        """Split psana data block into a PAD list"""
        if self.splitter is None:
            return self._home_geometry.split_data(data)
        else:
            return self.splitter(data)

    def get_data_split(self, event):
        """
        Just like the calib data but split into a list of panels.
        Just like the raw data but split into a list of panels.
        """
        if self.data_type == "calib":
            data = np.double(self.detector.calib(event))
        elif self.data_type == "raw":
            data = np.double(self.detector.raw(event))
        elif self.data_type == "photons":
            if self.adu_per_photon is None:
                print("adu_per_photon not set\n" "setting it to some garbage number.")
                self.adu_per_photon = 35
            data = np.double(
                self.detector.photons(event, adu_per_photon=self.adu_per_photon)
            )
        else:
            data = None
        if isinstance(data, np.ndarray):
            data = self.split_pad(data)
        else:
            print(
                "Data could not found or was not understood."
                "Setting all pixels to 0, this is not real data."
            )
            data = [g.zeros() for g in self.get_pad_geometry(event)]
        return data

    def get_pixel_coordinates(self, run_number):
        """Get pixel coordinates from psana. Returns None for rayonix."""
        if self.detector_type.casefold() == "rayonix":
            x, y, z = [None, None, None]
        else:
            x, y, z = get_pad_pixel_coordinates(
                self.detector, run_number, self.splitter
            )
        return x, y, z

    def get_mask(self, split=False):
        geom = detector.PADGeometryList(self._home_geometry)
        mask = self.mask
        if isinstance(mask, np.ndarray):
            pass
        elif mask in (None, []):
            mask = geom.ones()
        if split:
            mask = geom.split_data(mask)
        return mask

    @property
    def n_pixels(self):
        return self._home_geometry.n_pixels


def get_event_ids(experiment_id, run_number, cachedir=None, psana_dir=None):
    r"""Get the list of event IDs (3-tuples) and use caching if requested.
    Would be great if we didn't need to do this... maybe there is a better way
    but we don't know where to find it in the psana docs.

    Arguments:
        experiment_id (str): Experiment ID (e.g. cxilu1817)
        run_number (int): Run number
        cachedir (str or None): Where to look for cached data or save data
        psana_dir (str or None): Use this if the XTC data is in a strange
                                 location.  For advanced users...
    """
    data_string = f"exp={experiment_id}:run={run_number}:smd"
    if psana_dir is not None:
        data_string += f"dir={psana_dir}"
    data_source = psana.DataSource(data_string)
    event_ids = None
    if cachedir is not None:
        os.makedirs(cachedir, exist_ok=True)
        cachefile = f"{cachedir}/event_ids_{run_number:04d}.pkl"
        if os.path.exists(cachefile):
            debug_message("Loading cached event Ids:", cachefile)
            event_ids = fileio.misc.load_pickle(cachefile)
    if event_ids is None:
        event_ids = []
        for evt in data_source.events():
            evtId = evt.get(psana.EventId)
            event_ids.append(evtId.time() + (evtId.fiducials(),))
        if cachedir is not None:
            debug_message("Caching event IDs:", cachefile)
            fileio.misc.save_pickle(event_ids, cachefile)
    return event_ids


class LCLSFrameGetter(fileio.getters.FrameGetter):
    mask = None
    event = None
    event_ids = None
    # These codes correspond to the evr system.  They should be modified
    # according to the experiment.
    event_codes = None  # dict(xray_on=40, laser_on=41)

    def __init__(
        self,
        experiment_id,
        run_number,
        pad_detectors,
        max_events=1e6,
        psana_dir=None,
        beam=None,
        idx=True,
        cachedir=".",
        event_ids=None,
        evr_id="evr",
        event_codes=None,
        photon_wavelength_pv=None,
        **kwargs,
    ):
        r"""

        Arguments:
            experiment_id (str): E.g. cxilu5617
            run_number (int): run number
            pad_detectors (): TODO
            max_events (int): Truncate to max number of shots/events
            psana_dir (str): Use this if the XTC data is in a non-standard place
            beam (|Beam|): Beam info
            idx (bool): Set to false if no index files are available (email SLAC to create them)
            cachedir (str): Path to put a cache of event IDs for faster loading.
            event_ids (list or |ndarray|): If you want to pre-select event IDs, provide them here.
            event_codes (dict): Provide event codes to add to the returned |DataFrame| .
                                Example: dict(xray_on=40, laser_on=41)
            evr_id (str): If you know the event reader to search for event codes (e.g. "evr0")
            photon_wavelength_pv (str):
        """
        debug_message("Initializing superclass")
        super().__init__(**kwargs)  # initialize the superclass
        self.run_id = run_number
        pad_detectors = (
            [pad_detectors] if isinstance(pad_detectors, dict) else pad_detectors
        )
        self.evr_id = evr_id
        self.experiment_id = experiment_id
        self.run_number = run_number
        self.data_string = f"exp={experiment_id}:run={run_number}:smd"
        debug_message("datastring", self.data_string)
        if psana_dir is not None:
            self.data_string += f"dir={psana_dir}"
        if event_ids is None:
            event_ids = get_event_ids(
                experiment_id=experiment_id,
                run_number=run_number,
                cachedir=cachedir,
                psana_dir=psana_dir,
            )
        self.event_ids = event_ids
        if len(self.event_ids) > max_events:
            self.event_ids = self.event_ids[:max_events]
        self.n_frames = len(self.event_ids)
        self.has_indexing = idx
        if self.has_indexing:
            self.data_string = self.data_string.replace(":smd", ":idx")
        # TODO: Investigate the possibility of testing for the existence of indexing
        #       so that we are not forced to manually choose.  Presently, a psana
        #       segmentation fault will occur if we try to use indexing when the
        #       indexing data does not exist, which prohibits a try/except pattern...
        #       and the only way to get the index files is to email someone...
        debug_message("datastring", self.data_string)
        self.data_source = psana.DataSource(self.data_string)
        self.run = self.data_source.runs().__next__()
        self.events = self.run.events()
        self.previous_frame = 0
        self.ebeam_detector = psana.Detector("EBeam")
        self.photon_wavelength_pv = photon_wavelength_pv
        if self.photon_wavelength_pv is not None:
            self.photon_wavelength_detector = psana.Detector(self.photon_wavelength_pv)
        detnames = [d for sl in psana.DetNames() for d in sl]
        env = self.data_source.env()
        self.evr_detectors = [
            psana.Detector(d, env) for d in detnames if d.startswith("evr")
        ]
        self.detectors = [
            AreaDetector(**p, run_number=self.run_number, accept_missing=True)
            for p in pad_detectors
        ]
        self.beam = beam

    def get_data(self, frame_number=0):
        debug_message()
        # This is annoying: ideally we would use indexed data for which we can skip to any frame... but
        # sometimes the index file is missing (e.g. due to DAQ crash or data migration).  So we accommodate
        # the 'smd' mode in addition to the 'idx' mode:
        event = None
        ts = self.event_ids[frame_number]
        if self.has_indexing:
            event = self.run.event(psana.EventTime(int((ts[0] << 32) | ts[1]), ts[2]))
        else:
            if frame_number == self.previous_frame + 1:
                event = self.events.__next__()
            else:
                if not frame_number == 0:
                    debug_message(
                        "Skipping frames in the smd mode will be quite slow..."
                    )
                self.data_source = psana.DataSource(self.data_string)
                self.run = self.data_source.runs().__next__()
                self.events = self.run.events()
                for i in range(frame_number + 1):
                    event = self.events.__next__()
        self.previous_frame = frame_number
        self.event = event
        if event is None:
            debug_message("The event is None!")
            return None
        # Fetch all of the event codes
        event_codes = []
        for det in self.evr_detectors:
            c = det.eventCodes(event)
            if c is not None:
                event_codes += c
        events = dict()
        if self.event_codes is not None:
            for k in self.event_codes:
                events[k] = (
                    self.event_codes[k] in event_codes
                )  # E.g. True if key 41 is present
        photon_energy = None
        photon_wavelength = None
        try:
            photon_energy = (
                self.ebeam_detector.get(event).ebeamPhotonEnergy() * const.eV
            )
            if self.beam is None:
                self.beam = source.Beam()
        except AttributeError:
            if self.photon_wavelength_pv is not None:
                photon_wavelength = (
                    self.photon_wavelength_detector(event) * 1e-9
                )  # convert nm to m
                if self.beam is None:
                    self.beam = source.Beam()
            else:
                debug_message(
                    f"Run {self.run_number} frame {frame_number} causes "
                    f"ebeamPhotonEnergy failure. Try using the epics pv."
                )
        geometry = detector.PADGeometryList()
        pad_data = []
        pad_mask = []
        for det in self.detectors:
            g = det.get_pad_geometry(event)
            d = det.get_data_split(event)
            m = det.get_mask(split=True)
            geometry.extend(g)
            pad_data.extend(d)
            pad_mask.extend(m)
        df = dataframe.DataFrame()
        df.set_dataset_id(f"{self.experiment_id}:{self.run_number}")
        df.set_frame_id(ts)
        df.set_frame_index(frame_number)
        if photon_energy is not None:
            beam = self.beam
            beam.photon_energy = photon_energy
            df.set_beam(beam)
        if photon_wavelength is not None:
            beam = self.beam
            beam.photon_wavelength = photon_wavelength
            df.set_beam(beam)
        if geometry:
            df.set_pad_geometry(geometry)
        if pad_data:
            df.set_raw_data(pad_data)
        if pad_mask:
            df.set_mask(pad_mask)
        df.metadata = dict(evr_codes=events)
        debug_message("returning", df)
        return df
