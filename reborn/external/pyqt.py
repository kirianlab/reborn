from pyqtgraph.Qt import QtGui, QtCore, QtWidgets
from pyqtgraph.Qt.QtGui import (
    QKeySequence,
    QTransform,
    QAction,
    QShortcut,
    QPen,
    QPainter,
    QColor,
    QDoubleValidator,
    QFont,
    QPixmap,
    QFontDatabase,
)
from pyqtgraph.Qt.QtCore import (
    QObject,
    Qt,
    QAbstractTableModel,
    QSortFilterProxyModel,
    QModelIndex,
    QRect,
    QPersistentModelIndex,
    QEventLoop,
    QTimer,
    QThread,
    # pyqtSignal,
    # pyqtSlot,
    # QVariant,
    #QFontDatabase,
)
try:
    from pyqtgraph.Qt.QtCore import pyqtSignal, pyqtSlot
except:
    pyqtSignal = QtCore.Signal
    pyqtSlot = QtCore.Slot
# try:
#     from pyqtgraph.Qt.QtCore import QVariant
# except:
#     QVariant = QtCore.QVariant
from pyqtgraph.Qt.QtWidgets import (
    QApplication,
    QMainWindow,
    QWidget,
    QHBoxLayout,
    QVBoxLayout,
    QSplitter,
    QGridLayout,
    QLabel,
    QSpinBox,
    QCheckBox,
    QComboBox,
    QPushButton,
    QDialog,
    QColorDialog,
    QFileDialog,
    QInputDialog,
    QLineEdit,
    QItemDelegate,
    QTableView,
    QHeaderView,
    QSizePolicy,
    QSlider,
    QFrame,
    QDialogButtonBox,
    QDoubleSpinBox,
    QButtonGroup,
    QRadioButton,
    QTextEdit,
    QWidgetAction,
    QStyledItemDelegate,
    QListWidgetItem,
    QStyle,
    QListWidget,
    QMenu,
    #QDesktopWidget,
)
DisplayRole = Qt.ItemDataRole.DisplayRole
EditRole = Qt.ItemDataRole.EditRole
ItemIsEditable = Qt.ItemFlag.ItemIsEditable
AscendingOrder = Qt.SortOrder.AscendingOrder
DescendingOrder = Qt.SortOrder.DescendingOrder
Horizontal = Qt.Orientation.Horizontal
Vertical = Qt.Orientation.Vertical
AlignCenter = Qt.AlignmentFlag.AlignCenter
AlignRight = Qt.AlignmentFlag.AlignRight
Key_Space = Qt.Key.Key_Space
Accepted = QDialog.DialogCode.Accepted
Checked = Qt.CheckState.Checked
Unchecked = Qt.CheckState.Unchecked
ItemIsUserCheckable = Qt.ItemFlag.ItemIsUserCheckable
# Expanding = Qt.QSizePolicy.Policy.Expanding
