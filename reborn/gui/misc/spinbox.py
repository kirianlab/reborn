# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import re
import numpy as np
from ...external.pyqt import QDoubleSpinBox, QDoubleValidator


class BetterDoubleSpinBox(QDoubleSpinBox):
    def __init__(self, *args, format='e', decimals=5, **kwargs):
        self.format = format
        super().__init__() #*args, **kwargs)
        self.lineEdit().setValidator(QDoubleValidator(self))
        self.setDecimals(decimals)
        self.setRange(-np.inf, np.inf)

    def validate(self, text, position):
        return self.lineEdit().validator().validate(text, position)

    def valueFromText(self, text):
        return float(text)

    def textFromValue(self, value):
        return f"{value:#.5g}"
        # text = self.locale().toString(value, self.format, self.decimals())
        # # @COMPATIBILITY: isGroupSeparatorShown is new in Qt 5.3
        # if (not hasattr(self, 'isGroupSeparatorShown') or
        #         not self.isGroupSeparatorShown()):
        #     text = text.replace(self.locale().groupSeparator(), '')
        # text = text.replace("e+", "e")
        # print(text)
        # text = re.sub(r'e(-?)0*(\d+)', r'e\1\2', text)
        # print(text)
        # return text

    def stepBy(self, steps):
        pos = self.lineEdit().cursorPosition()
        text = self.textFromValue(self.value())
        orders = character_orders(text)
        if orders is None:
            return
        index = pos - 1
        index = max(index, 0)
        index = min(index, len(text) - 1)
        order = orders[index]
        step = 10**order
        self.setSingleStep(step)
        super().stepBy(steps)
        self.lineEdit().deselect()
        text = self.textFromValue(self.value())
        orders = character_orders(text)
        index = 0
        for i in range(len(orders)):
            if orders[i] == order:
                index = i
                if text[i] == ".":
                    continue
                break
        self.lineEdit().setCursorPosition(index+1)

    def setValueSilently(self, value):
        self.blockSignals(True)
        self.setValue(value)
        self.blockSignals(False)

def character_orders(text):
    r""" Given a string in scientific notation (e.g. 56.23e-5), return
    a list where each element gives the order-of-magnitude corresponding
    to that character in the string.  For example:

    print(character_orders("4.4966429e2"))

    [ 2.  1.  1.  0. -1. -2. -3. -4. -5. -5. -5.]
    """
    text = text.lower()
    try:
        val = float(text)
        if val == 0:
            order = 0
        else:
            order = int(np.floor(np.log10(np.abs(val))))
    except:
        return None
    nchar = len(text)
    if nchar == 0:
        return None, None, None
    orders = np.zeros([nchar]) + order
    for i in range(nchar):
        if text[i] in "+-":
            orders[i] = order
        if text[i] == "e":
            _, p = text.split("e")
            orders[i:] = order+1
            break
        if text[i] in "0123456789":
            orders[i] = order
            order -= 1
        if text[i] == ".":
            orders[i] = order
    return orders
