# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import sys
import pyqtgraph as pg
from ...external.pyqt import (
    QApplication,
    QMainWindow,
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QPushButton,
    QColorDialog,
    QComboBox,
    QLabel,
    QSlider,
    QFrame,
    QLineEdit,
    QDialog,
    QDialogButtonBox,
    QPainter,
    QPen,
    QColor,
    QDoubleValidator,
    Qt,
    QEventLoop,
    pyqtSignal,
    Horizontal,
)


debug = 1


def dbgmsg(*args, **kwargs):
    if debug:
        print(*args, **kwargs)


class PenStylePreview(QFrame):
    changed = pyqtSignal()

    def __init__(self, parent=None, pen=None, editable=True):
        r"""
        Simple QFrame that displays a QPen instance.

        Arguments:
            parent (QObject): Look at Qt docs for this.
            pen (QPen or None): A QPen instance.
            editable (bool): If True, mouse clicks allow editing.
        """
        dbgmsg("PenStylePreview.__init__")
        super().__init__(parent)
        self.parent = parent
        self.editable = editable
        if pen is None:
            self.pen = QPen(Qt.PenStyle.SolidLine)
            self.pen.setColor(Qt.black)
            self.pen.setWidth(1)
        else:
            self.pen = QPen(pen.style())
            self.pen.setColor(pen.color())
            self.pen.setWidth(pen.width())

    def paintEvent(self, event):
        dbgmsg("PenStylePreview.paintEvent")
        painter = QPainter(self)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)
        painter.setPen(self.pen)
        painter.drawLine(10, self.height() // 2, self.width() - 10, self.height() // 2)

    def update_pen(self, pen):
        dbgmsg("PenStylePreview.update_pen")
        self.pen = QPen(pen)
        self.update()
        self.changed.emit()

    def get_pen(self):
        dbgmsg("PenStylePreview.get_pen")
        return self.pen

    def mousePressEvent(self, event):
        dbgmsg("PenStylePreview.mousePressEvent")
        if self.editable:
            super().mousePressEvent(event)
            self.update_pen(pen_dialog(self.pen))


class PenStyleEditor(QWidget):
    changed = pyqtSignal()

    def __init__(self, parent=None, pen=None):
        r"""
        Simple QWidget for specifying a QPen.

        Arguments:
            parent (): Look up Qt docs... I don't know exactly what this does...
            pen (QPen or None): A QPen instance.
        """
        dbgmsg("PenStyleEditor.__init__")
        super().__init__(parent)
        self.styles = [
            Qt.PenStyle.SolidLine,
            Qt.PenStyle.DashLine,
            Qt.PenStyle.DotLine,
            Qt.PenStyle.DashDotLine,
            Qt.PenStyle.DashDotDotLine,
        ]
        self.style_names = ["Solid", "Dash", "Dot", "Dash Dot", "Dash Dot Dot"]
        if pen is None:
            self.pen = QPen(Qt.PenStyle.SolidLine)
            self.pen.setColor(Qt.red)
            self.pen.setWidth(2)
        else:
            self.pen = QPen(pen.style())
            self.pen.setColor(pen.color())
            self.pen.setWidth(pen.width())
        self._init_ui()
        self.setGeometry(100, 100, 400, 200)
        self.setWindowTitle("Pen Editor")

    def _init_ui(self):
        dbgmsg("PenStyleEditor._init_ui")
        layout = QVBoxLayout()
        # Pen Style Preview
        self.preview_frame = PenStylePreview(self, self.pen)
        self.preview_frame.setFixedHeight(40)  # Set a fixed height for the preview
        self.preview_frame.setFrameShape(QFrame.Shape.Box)
        layout.addWidget(self.preview_frame)
        # Color Selector
        color_layout = QHBoxLayout()
        color_label = QLabel("Color:")
        self.color_button = QPushButton("Select Color")
        self.color_button.clicked.connect(self.set_color)
        color_layout.addWidget(color_label)
        color_layout.addWidget(self.color_button)
        layout.addLayout(color_layout)
        # Line Style Selector
        # TODO: Figure out why line styles don't work with pyqtgraph
        # style_layout = QHBoxLayout()
        # style_label = QLabel("Line Style:")
        # self.style_combobox = QComboBox()
        # self.style_combobox.addItems(self.style_names)
        # self.style_combobox.currentIndexChanged.connect(self.set_style)
        # style_layout.addWidget(style_label)
        # style_layout.addWidget(self.style_combobox)
        # layout.addLayout(style_layout)
        # Line Width Slider and Numeric Input
        width_layout = QHBoxLayout()
        width_label = QLabel("Line Width:")
        self.width_slider = QSlider(Horizontal)
        self.width_slider.setMinimum(1)
        self.width_slider.setMaximum(10)
        self.width_slider.setValue(2)
        self.width_slider.valueChanged.connect(self.set_width)
        # Line Width Numeric Input
        self.width_edit = QLineEdit()
        self.width_edit.setText(str(self.pen.width()))
        self.width_edit.textChanged.connect(self.set_width_from_edit)
        width_layout.addWidget(width_label)
        width_layout.addWidget(self.width_slider)
        width_layout.addWidget(self.width_edit)
        layout.addLayout(width_layout)
        # Finalize
        self.setLayout(layout)
        self.preview_frame.update_pen(self.pen)

    def set_color(self, color=None):
        dbgmsg("PenStyleEditor.set_color")
        if not color:
            color = QColorDialog.getColor(initial=self.pen.color())
        if color.isValid():
            self.pen.setColor(color)
            self.preview_frame.update_pen(self.pen)
            self.changed.emit()

    def set_style(self, style):
        dbgmsg("PenStyleEditor.set_style")
        if isinstance(style, int):
            index = style
        else:
            if style not in self.styles:
                return
            for index in range(len(self.styles)):
                if style == self.styles[index]:
                    break
        self.style_combobox.setCurrentIndex(index)
        self.pen.setStyle(self.styles[index])
        self.preview_frame.update_pen(self.pen)
        self.changed.emit()

    def set_width(self, value):
        dbgmsg("PenStyleEditor.set_width")
        self.pen.setWidth(value)
        self.width_edit.setText(str(value))  # Update the numeric input
        self.preview_frame.update_pen(self.pen)
        self.changed.emit()

    def set_width_from_edit(self):
        dbgmsg("PenStyleEditor.set_width_from_edit")
        width_text = self.width_edit.text()
        if width_text:
            width = int(float(width_text))
            self.width_slider.setValue(width)
            self.set_width(width)
            self.changed.emit()

    def set_pen(self, pen):
        dbgmsg("PenStyleEditor.set_pen")
        self.set_width(pen.width())
        self.set_color(pen.color())
        self.set_style(pen.style())

    def get_pen(self):
        dbgmsg("PenStyleEditor.get_pen")
        return self.pen

    def on_change(self):
        dbgmsg("PenStyleEditor.on_change")
        self.changed.emit()
        self.preview_frame.changed.emit()


class PenStyleDialog(QDialog):
    def __init__(self, parent=None, pen=None):
        r"""
        Simple QDialog for specifying a QPen.  Emits QPen on accept.

        Arguments:
            pen (QPen or None): A QPen instance to start with.
        """
        dbgmsg("PenStyleDialog.__init__")
        super().__init__(parent=parent)
        self.pen = pen
        self.initUI()

    def initUI(self):
        dbgmsg("PenStyleDialog.initUI")
        # Set up layout
        layout = QVBoxLayout()
        pen_editor = PenStyleEditor(pen=self.pen)

        # Add widgets
        ok_button = QPushButton("OK", self)
        ok_button.clicked.connect(
            self.accept
        )  # Close the dialog with accepted status on OK button press

        cancel_button = QPushButton("Cancel", self)
        cancel_button.clicked.connect(
            self.reject
        )  # Close the dialog with rejected status on Cancel button press

        layout.addWidget(pen_editor)
        layout.addWidget(ok_button)
        layout.addWidget(cancel_button)

        self.pen_editor = pen_editor
        self.setLayout(layout)
        self.setWindowTitle("Custom Dialog")

    def on_ok_button_clicked(self):
        dbgmsg("PenStyleDialog.on_ok_button_clicked")
        custom_result = self.pen_editor.get_pen()
        self.accepted.emit(custom_result)  # Emit a signal with the custom result
        self.accept()

    def set_pen(self, pen):
        dbgmsg("PenStyleDialog.set_pen")
        self.pen_editor.set_pen(pen)

    def get_pen(self):
        dbgmsg("PenStyleDialog.get_pen")
        return self.pen_editor.get_pen()


def pen_dialog(parent=None, pen=None):
    r"""
    Opens a simple QDialog to get a QPen.  Returns the QPen.

    Arguments:
        pen (QPen or None): A QPen instance to start with.

    Returns:
        QPen
    """
    dbgmsg("pen_dialog")
    custom_dialog = PenStyleDialog(parent=parent, pen=pen)
    event_loop = QEventLoop()
    custom_dialog.accepted.connect(event_loop.quit)
    custom_dialog.show()
    event_loop.exec()
    return custom_dialog.pen_editor.get_pen()


#
# class MainWindow(QMainWindow):
#     def __init__(self):
#         super().__init__()
#
#         self.initUI()
#
#     def initUI(self):
#         central_widget = QWidget(self)
#         self.setCentralWidget(central_widget)
#         self.pen_preview = PenStylePreview()
#         # Create a button to open the custom dialog
#         open_dialog_button = QPushButton('Edit Pen', central_widget)
#         open_dialog_button.clicked.connect(self.show_custom_dialog)
#
#         layout = QVBoxLayout(central_widget)
#         layout.addWidget(self.pen_preview)
#         layout.addWidget(open_dialog_button)
#
#     def show_custom_dialog(self):
#         result = pen_dialog(pen=None)
#         # custom_dialog = CustomDialog()
#         # custom_dialog.accepted.connect(self.handle_custom_result)
#         # result = custom_dialog.exec_()  # Show the dialog and get the result
#         print(result)
#
#         # if result == QDialog.Accepted:
#         #     print('OK button pressed')
#         # else:
#         #     print('Cancel button pressed')
#
#
# def main():
#     app = QApplication(sys.argv)
#
#     main_window = MainWindow()
#     main_window.show()
#
#     sys.exit(app.exec_())
#
# if __name__ == '__main__':
#     main()
