#!/bin/bash
branch=develop   # Which branch to follow
directory=reborn # Where to put the submodule
# If there is no .gitmodules file:
[ ! -f .gitmodules ] && git submodule add -b $branch https://gitlab.com/kirianlab/reborn.git reborn
# If reborn is not in .gitmodules:
[ "$(grep reborn.git .gitmodules)" == "" ] && git submodule add -b $branch https://gitlab.com/kirianlab/reborn.git directory
# Update the reborn submodule (and all others).  Initialize if not already initialized.
git submodule update --init --recursive --remote