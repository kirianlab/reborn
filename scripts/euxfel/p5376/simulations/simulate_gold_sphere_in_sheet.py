import numpy as np
import matplotlib.pyplot as plt
import reborn
from reborn.simulate import solutions, form_factors
from reborn.analysis.saxs import RadialProfiler
from reborn.viewers.qtviews import PADView
from reborn.const import r_e, eV
detector_distance = 1  # Sample to detector distance
water_thickness = 100e-9  # Assuming a sheet of liquid of this thickness
n_shots = 1  # Number of shots to integrate
pulse_energy = 1e-3  # Pulse energy
photon_energy = 6000 * eV  # Photon energy
beam_diameter = (
    0.3e-6  # X-ray beam diameter (doesn't really matter for solutions scattering)
)
# This is approximately the scattering density of gold:
sphere_radius = 2.5e-9  # Radius of our spherical "protein molecule"
sphere_density = (
    19.3 - 1
) * 1e3  # Density of spherical protein (g/cm^3, convert to SI kg/m^3)
# This is approximately the scattering density of protein:
# sphere_radius = 30e-9  # Radius of our spherical "protein molecule"
# sphere_density = 0.3 * 1e3  # Density of spherical protein (g/cm^3, convert to SI kg/m^3)
geom = reborn.detector.agipd_pad_geometry_list(detector_distance=detector_distance)
beam = reborn.source.Beam(
    diameter_fwhm=beam_diameter, photon_energy=photon_energy, pulse_energy=pulse_energy
)
q = geom.q_vecs(beam=beam)
q_mags = geom.q_mags(beam=beam)
J = beam.photon_number_fluence
P = geom.polarization_factors(beam=beam)
SA = geom.solid_angles()
n_water_molecules = (
    water_thickness
    * np.pi
    * (beam.diameter_fwhm / 2) ** 2
    * solutions.water_number_density()
)
F_water = solutions.water_scattering_factor_squared(q_mags)
F2_water = F_water**2 * n_water_molecules
F_sphere = form_factors.sphere_form_factor(radius=sphere_radius, q_mags=q_mags)
F_sphere *= (
    (sphere_density - 1000) / 1000 * 3.346e29
)  # Protein-water contrast.  Water electron density is 3.35e29.
F2_sphere = np.abs((F_sphere**2))
F2 = F2_water + F2_sphere
I0 = n_shots * r_e**2 * J * P * SA * F2
I = np.random.poisson(I0).astype(np.double)
pv = PADView(data=I, beam=beam, pad_geometry=geom)
pv.start()
profiler = RadialProfiler(
    pad_geometry=geom, beam=beam, n_bins=500, q_range=(0, np.max(q_mags))
)
prof = profiler.get_mean_profile(I0)
w = np.where(prof > 0)
x = profiler.bin_centers[w] * 1e-10
y = prof[w]
plt.figure()
plt.semilogy(x, y)
plt.title("Scattering Profile")
plt.ylabel(r"$I(q)$ [photons/pixel]")
plt.xlabel(r"$q = 4\pi\sin(\theta/2)/\lambda$ [$\AA{}^{-1}$]")
plt.show()
