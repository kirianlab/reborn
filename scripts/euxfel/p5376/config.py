import numpy as np
import reborn
from reborn import utils
from reborn.source import Beam
try:
    import extra_data
except:
    extra_data = None
from reborn.const import eV
from reborn.external.crystfel import geometry_file_to_pad_geometry_list

debug = False


def debug_message(*args, caller=True, **kwargs):
    r""" Standard debug message, which includes the function called. """
    if debug:
        s = ''
        if caller:
            s = utils.get_caller(1)
        print(f'DEBUG:euxfel.{s}:', *args, **kwargs)


class P5376FrameGetter(reborn.fileio.getters.FrameGetter):
    current_image = None
    current_train_stack = None
    current_train_id = None
    beam = None
    mask = None
    n_trains = None

    def __init__(self, experiment_id, run_id,
                 mask=None,
                 max_events=None,
                 data='proc',
                 require_all=False,
                 geom_path=None):
        debug_message('Initializing superclass')
        super().__init__()
        self.experiment_id = experiment_id
        self.run_id = run_id
        if geom_path is None:
            geom_path = "/home/bobca/p005376/usr/Shared/geom/motor_p5376_from_4462.geom"
        self.pad_geometry = geometry_file_to_pad_geometry_list(geom_path)
        self.mask = mask
        # extra data first loads a run
        # in the background this is opening an HDF5 file
        # we can load raw or processed (dark calibrated) data
        debug_message('gathering run data')
        run = extra_data.open_run(proposal=self.experiment_id, run=self.run_id, data=data)
        self.train_ids = set(run.train_ids)
        self.n_trains = len(self.train_ids)
        # here we select the type of data we want (pad detector exposures)
        debug_message('finding sources')
        # annoyingly, extra-data will still return all train ids in a run when setting
        #           setting require_all=True
        # it will instead return None for train ids which have missing data
        self.selection = run.select('*/DET/*', 'image.data', require_all=require_all)
        self.optical_selection = run.select('SPB_EXP_ZYLA/CAM/1:daqOutput', 'data.image.pixels')
        # data is saved for each individual panel
        # so there N files for a detector with N pads
        # this finds all the files needed to stitch together a single exposure
        sources = sorted(self.selection.all_sources, key=lambda s: int(s.split('/')[-1].split('CH')[0]))
        # build a mapping from the trains to individual exposures
        debug_message('building frame index')
        t_shots = run[sources[0], 'image.data'].data_counts()
        tids = t_shots.index.values  # all the train ids
        ntids = t_shots.to_numpy()  # number of shots in each train id
        frame_cumsum = ntids.cumsum()
        self.n_frames = frame_cumsum[-1]
        self.frames = np.array([tids, frame_cumsum]).T
        debug_message('enforcing max_events')
        if max_events is not None:
            self.n_frames = min(max_events, self.n_frames)
        # the photon wavelength is easily accessible by opening the raw data
        debug_message('gather photon energy')
        run_raw = extra_data.open_run(proposal=self.experiment_id, run=self.run_id, data='raw')
        self.photon_data = run_raw['SA1_XTD2_XGM/XGM/DOOCS', 'pulseEnergy.wavelengthUsed.value']
        if self.beam is None:
            for train_id, _ in self.frames:
                try:
                    _, wavelength = self.photon_data.train_from_id(train_id)  # result is in nm
                except Exception as e:
                    print(str(e))
                    continue
                debug_message('Beam not specified, setting from wavelength measurement.')
                self.beam = Beam(wavelength=wavelength * 1e-9)
                break
        pad_det = run['SPB_EXP_ZYLA/CAM/1:daqOutput', 'data.image.pixels'].train_id_coordinates()

    def _get_train_stack(self, train_id):
        debug_message('looking for stack')
        tid, train_data = self.optical_selection.train_from_id(train_id)
        return np.double(train_data['SPB_EXP_ZYLA/CAM/1:daqOutput']['data.image.pixels'])

    def _get_optical_image(self, train_id):
        debug_message('looking for optical image')
        tid, train_data = self.selection.train_from_id(train_id)
        stack = extra_data.stack_detector_data(train_data, 'image.data')
        return np.double(stack)

    def _find_train_stack_from_frame_number(self, frame_number):
        cond = self.frames[:, 1].astype(int) - frame_number > 0
        tid = self.frames[:, 0][cond][0]
        # if tid not in self.train_ids:
        return tid

    def get_data(self, frame_number=0):
        debug_message()
        # load the data from the HDF5 file
        debug_message('loading train')
        train_id = self._find_train_stack_from_frame_number(frame_number=frame_number)
        print(train_id)
        # cache current train stack
        if self.current_train_stack is not None:
            if train_id == self.current_train_id:
                stacked = self.current_train_stack
            else:
                try:
                    stacked = self._get_train_stack(train_id)
                except Exception as e:
                    print(str(e))
                    return None
                self.current_train_id = train_id
                self.current_train_stack = stacked
                self.current_image = self._get_optical_image(train_id)
        else:
            try:
                stacked = self._get_train_stack(train_id)
            except Exception as e:
                print(str(e))
                return None
            self.current_train_id = train_id
            self.current_train_stack = stacked
            self.current_image = self._get_optical_image(train_id)
        stacked_pulse = stacked[frame_number]
        debug_message('building dataframe')
        df = reborn.dataframe.DataFrame()
        debug_message('looking up optical image')
        df.microscope_image = self.current_image
        debug_message('setting calibrated pad detector data')
        df.set_dataset_id(f'run:{self.run_id} (Data)')
        df.set_frame_id(f'run:{self.run_id}:{frame_number}')
        df.set_frame_index(frame_number)
        debug_message('getting detector stage position')
        debug_message('setting PADGeometry')
        df.set_pad_geometry(self.pad_geometry)
        df.set_mask(self.mask)
        df.set_raw_data(stacked_pulse)
        debug_message('retrieving x-ray data')
        try:
            _, wavelength = self.photon_data.train_from_id(train_id)  # result is in nm
        except ValueError:
            return None
        debug_message('setting Beam')
        self.beam = Beam(wavelength=wavelength * 1e-9)
        df.set_beam(self.beam)
        debug_message('returning', df)
        return df

