import argparse
from reborn import detector
from reborn.external import crystfel
parser = argparse.ArgumentParser()
parser.add_argument("--reborn", type=str, required=True, help="Path to reborn geometry file.")
parser.add_argument("--template", type=str, default="", help="Path to crystfel geometry file")
# parser.add_argument("--energy", type=float, default=7, help="Photon energy in keV.")
parser.add_argument("--crystfel", type=str, required=True, help="Path where to save crystfel geometry file converted from reborn format.")
args = parser.parse_args()
geom = detector.load_pad_geometry_list(args.reborn)
if not args.template:
    crystfel.write_geom_file_from_pad_geometry_list(pad_geometry=geom, file_path=args.crystfel)
else:
    crystfel.write_geom_file_from_template(pad_geometry=geom, template_file=args.template, out_file=args.crystfel)
