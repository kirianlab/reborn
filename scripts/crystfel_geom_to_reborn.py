import argparse
from reborn.external import crystfel
from reborn import detector
parser = argparse.ArgumentParser()
parser.add_argument("--reborn", type=str, required=True, help="Path to reborn geometry file (input)")
parser.add_argument("--crystfel", type=str, required=True, help="Path to crystfel geometry file (output)")
args = parser.parse_args()
geom = crystfel.geometry_file_to_pad_geometry_list(args.crystfel)
detector.save_pad_geometry_list(args.reborn, geom)
