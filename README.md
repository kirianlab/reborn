This is a python package for x-ray diffraction data analysis and simulations under the Born approximation.  

The central repository is located here: https://gitlab.com/kirianlab/reborn 

The documentation for reborn is found here: https://kirianlab.gitlab.io/reborn
