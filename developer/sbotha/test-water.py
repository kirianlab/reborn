import numpy as np
from reborn import detector, source
from reborn.simulate import solutions
from reborn.viewers.qtviews import PADView

np.random.seed(0)

detector_distance = 0.1
photon_energy = 6000*1.602e-19
beam_diameter = 5e-6
pulse_energy = 5e-3
jet_diameter = 10e-6
jet_temperature = 300

pads = source.PADGeometryList([PADGeometry(shape=(2162, 2068), pixel_size=75e-6, distance=0.1)])
beam = source.Beam(photon_energy=photon_energy, diameter_fwhm=beam_diameter, pulse_energy=pulse_energy)
intensity = solutions.get_pad_solution_intensity(pad_geometry=pads, beam=beam, thickness=jet_diameter,
                                                 liquid='water', temperature=298, poisson=True)
pv = PADView(pad_geometry=pads, data=intensity, beam=beam)
pv.start()
