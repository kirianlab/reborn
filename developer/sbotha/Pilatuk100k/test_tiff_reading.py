import sys
import numpy as np
import h5py
import hdf5plugin
import pyqtgraph as pg
import fabio
import glob
import os
from reborn import detector
from reborn.fileio.getters import FrameGetter
from reborn.viewers.qtviews import PADView
from reborn.detector import PADGeometry, PADGeometryList
from reborn.dataframe import DataFrame
from reborn.source import Beam
from reborn.const import eV



class MyFrameGetter(FrameGetter):
    def __init__(self, datapath):
        self.filelist = glob.glob(os.path.join(datapath, "*.tif"))
        self.n_frames = len(self.filelist)
        rawdat = np.double(fabio.open(self.filelist[0]).data)
        print(rawdat.shape)
        self.geom = detector.pilatus100k_pad_geometry_list(detector_distance=0.1)
        self.beam = Beam(photon_energy=9500*eV)
    def get_data(self, frame_number=0):
        frame_number = int(frame_number)
        dataframe = DataFrame()
        dataframe.set_pad_geometry(self.geom)
        dataframe.set_beam(self.beam)
        data = np.double(fabio.open(self.filelist[frame_number]).data)
        dataframe.set_raw_data(data)
        return dataframe

if len(sys.argv) >= 2:
    datapath = sys.argv[1]
else:
    datapath = "images/"
mfg = MyFrameGetter(datapath)
frame = mfg.get_frame(0)
pv = PADView(frame_getter=mfg, pad_geometry=frame.get_pad_geometry())
pv.set_levels(levels=(0, 1))
pv.show()
pg.mkQApp().exec()
