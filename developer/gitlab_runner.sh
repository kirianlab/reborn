#!/bin/bash

# You need to install gitlab-runner and docker for this to work.  Avoid gitlab-runner version 11.X.Y since it has
# problems.  Version 13.1.1 works.

# The following might work:
# > curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
# > dpkg -i gitlab-runner_amd64.deb
#
# Gitlab runners are currently running on Rick's laptop and won't launch unless
# the service is running.
# sudo gitlab-runner run

if [[ ! $(basename "$(pwd)") = 'developer' ]]; then
    echo 'This script should run in the developer directory.'
    exit 1
fi

# Note that we use the local docker image when available.  If the local docker image is updated, you need to push it
# to gitlab in order for it to be used by the gitlab servers.  See developer/docker/build_docker.sh .
cd ..

if [[ "$1" == "pytest" ]]; then
	gitlab-runner exec docker --docker-pull-policy="never" pytest
	exit
fi

if [[ "$1" == "doc" ]]; then
  gitlab-runner exec docker --docker-pull-policy="never" docs
  exit 
fi

gitlab-runner exec docker --docker-pull-policy="never" docs
#echo "=================== doc ==============================================="
#gitlab-runner exec docker --docker-pull-policy="never" doc
