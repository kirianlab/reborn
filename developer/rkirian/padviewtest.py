import sys
import time
import logging
import numpy as np
import pandas
from reborn import detector, source, utils
from reborn.detector import PADGeometryList
from reborn.source import Beam
from reborn.dataframe import DataFrame
from reborn.fileio.getters import FrameGetter
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.viewers.qtviews.padviews import PADView
from reborn.const import eV

logger = logging.getLogger(__name__)

# reborn_logger = utils.log_to_terminal(level='debug')
# utils.print_all_loggers_and_handlers(logger_name="reborn")
# logger.debug('test')
# logger.debug('test')
# logger.debug('test')
# logger.debug('test')
# time.sleep(3)


# Simulate the expected water scatter from a sheet.
# ALL UNITS ARE SI IN REBORN
# Use the beam and geometry from the experiment:
detector_distance = 0.1
photon_energy = 9000 * eV
poisson = False  # Include Poisson noise in the result?
jet_diameter = 1000e-9  # Thickness of the sheet
temperature = 300  # Temperature of the water
beam_diameter = 100e-9  # Beam diameter (doesn't really matter)
pulse_energy = 1e-3  # Pulse energy of the x-ray beam
binning = 4  # Bin pixels if you want more speed (but expect more photons per pixel...)
beam = Beam(photon_energy=photon_energy)
beam.photon_energy = photon_energy
beam.pulse_energy = pulse_energy
beam.diameter_fwhm = beam_diameter
# geom = detector.jungfrau4m_pad_geometry_list(detector_distance=detector_distance)
geom = detector.agipd_pad_geometry_list(detector_distance=detector_distance, binning=binning)
# geom = geom.binned(binning)
# geom2 = detector.jungfrau4m_pad_geometry_list(detector_distance=detector_distance*20)
# # geom2.set_average_detector_distance(distance=detector_distance*10, beam=beam)
# geom2 = geom2.binned(binning)
# geom = geom2 + geom
mask = np.ones(geom.n_pixels)
n_frames = 100000


class MyFrameGetter(FrameGetter):
    _debug = 1
    def __init__(self):
        super().__init__()
        self.n_frames = n_frames
        self.intensity = None

    def get_data(self, frame_number=0):
        np.random.seed(frame_number)
        idx = frame_number % 10
        b = beam.copy()
        b.photon_energy = photon_energy * (1 + 0.01 * idx)
        b.pulse_energy = pulse_energy*10
        g = geom.copy()
        # g.translate(np.ones(3) * 1e-3 * idx)
        m = mask.copy()
        m = g.split_data(m)
        m[idx][:] = 0
        d = get_pad_solution_intensity(
            pad_geometry=g,
            beam=b,
            thickness=jet_diameter,
            liquid="water",
            temperature=temperature,
            poisson=True,
        )
        if idx % 3 == 1:
            b = None
        df = DataFrame()
        df.set_pad_geometry(g)
        df.set_beam(b)
        df.set_raw_data(d)
        df.set_mask(m)
        sleep = 1
        time.sleep(sleep)
        return df


fg = MyFrameGetter()
# fg._debug = 1
# fg._cache_forward = True
pv = PADView(frame_getter=fg, debug_level=2)
# pv.add_ring(q_mag=2e10)
# pv.hold_beam = True
# pv.hold_pad_geometry = True
# pv.hold_mask = True
# pv.show_histogram = False
# pv.hold_levels = True
# pv.run_plugin("ewald_popout")
pv.start()
sys.exit()
