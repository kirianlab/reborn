import sys
import time
import numpy as np
import pandas
from reborn import detector, source
from reborn.detector import PADGeometryList
from reborn.source import Beam
from reborn.dataframe import DataFrame
from reborn.fileio.getters import FrameGetter
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.viewers.qtviews.padviews import PADView
from reborn.const import eV

####################################################################
# First, we will create an example FrameGetter subclass that simulates
# water scatter on an Eiger 4M detector.  This FrameGetter subclass
# will later be replaced with a FrameGetter that gets actual data.
# The FrameGetter interface is standard and will not change when we
# switch from simulated data to real data.
####################################################################
# Create a Beam instance to hold information about the x-ray beam
beam0 = Beam(photon_energy=9000*eV)
# Create a PADGeometryList instance to hold the pixel-array detector (PAD) geometry.
# Also, get the standard mask.
geom0, mask0 = detector.eiger4M_pad_geometry_list(detector_distance=0.1, return_mask=True)
# Create a FrameGetter subclass to serve up example water diffraction patterns
class EigerWaterFrameGetter(FrameGetter):
    def __init__(self):
        super().__init__()
        self.n_frames = 100
    def get_data(self, frame_number=0):
        np.random.seed(frame_number)
        geom = geom0.copy()
        beam = beam0.copy()
        scat = get_pad_solution_intensity(
            pad_geometry=geom,
            beam=beam,
            thickness=100e-6,
            poisson=True,
        )
        df = DataFrame(raw_data=scat,
                       pad_geometry=geom,
                       beam=beam,
                       mask=mask0.copy())
        return df

# Create a new instance of the FrameGetter subclass:
fg = EigerWaterFrameGetter()
# Get a dataframe from the framegetter
df = fg.get_frame(frame_number=5)
# Get the geometry corresponding to this dataframe
geom = df.get_pad_geometry()
print("\nGeometry parameters:")
print(geom)
# Get the x-ray beam information
beam = df.get_beam()
print("\nBeam parameters:")
print(beam)
# Get the mask from the dataframe in the form of a flat numpy array
mask = df.get_mask_flat()

print("\nMask shape:", mask.shape)
# Get the raw data from the dataframe in the form of a flat numpy array:
data = df.get_raw_data_flat()
print("\nData shape:", data.shape)
# Get the q magnitudes corresponding to the mask and data arrays
qmags = df.get_q_mags_flat()
print("\nQ mags shape:", qmags.shape)

# Now, the above arrays are "flat" arrays, but we could also convert
# them into lists of 2D arrays like this:
data_list = geom.split_data(data)
mask_list = geom.split_data(mask)
print("Data list:")
print(data_list)
print("Mask list:")
print(mask_list)

# If you want to view the frames in the FrameGetter, you can use PADView
pv = PADView(frame_getter=fg)

# We will start the viewer, but before we do that, let's see how we can
# get the currently displayed data and mask
data_displayed = pv.get_pad_display_data(as_list=False)
mask_displayed = pv.get_mask(as_list=False)
# The mask from PADView should be the same as before.  Let's check:
assert np.all(mask_displayed - mask == 0)

# An alternative way to get the mask from PADView would be to get the
# DataFrame:
dataframe_displayed = pv.get_dataframe()
mask_displayed_2 = dataframe_displayed.get_mask_flat()
assert np.all(mask - mask_displayed_2 == 0)

# Note that the PADView GUI will not start until you tell it to start.
# Once the Qt application starts, the script will be blocked from
# further execution until you close the GUI.
pv.start()

print("Done")
