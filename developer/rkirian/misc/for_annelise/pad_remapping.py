import numpy as np
import pyqtgraph as pg
from reborn import detector, source
from reborn.simulate import solutions
from reborn.viewers.qtviews.padviews import PADView
from reborn.const import eV
photon_energy = 9250*eV
pulse_energy = 1000 * 1e8 * photon_energy
#geom = detector.pilatus100k_pad_geometry_list(detector_distance=0.1)
#geom = detector.eiger4M_pad_geometry_list(detector_distance=0.1)
#geom = detector.rayonix_mx340_xfel_pad_geometry_list(detector_distance=0.1)
#geom = detector.rayonix_mx340_xfel_pad_geometry_list(detector_distance=0.1)
#geom = detector.mpccd_pad_geometry_list(detector_distance=0.1)
#geom = detector.epix100_pad_geometry_list(detector_distance=0.1)
#geom = detector.epix10k_pad_geometry_list(detector_distance=0.1)
geom = detector.jungfrau4m_pad_geometry_list(detector_distance=0.1)
#geom = detector.cspad_2x2_pad_geometry_list(detector_distance=0.1)
#geom = detector.cspad_pad_geometry_list(detector_distance=0.1)
#geom = detector.pnccd_pad_geometry_list(detector_distance=0.1)
#geom = detector.tiled_pad_geometry_list(tiling_shape=(2, 2), pixel_size=100e-6, pad_gap=0, pad_shape=(12, 10), distance=0.1)
beam = source.Beam(photon_energy=photon_energy, pulse_energy=pulse_energy)
data = solutions.get_pad_solution_intensity(pad_geometry=geom, beam=beam, thickness=500e-6, poisson=False)
#data = geom.q_mags(beam=beam)
mask = geom.ones() # geom.beamstop_mask(min_angle=30e-3, beam=beam)
shape = None
assembler = detector.PADAssembler(pad_geometry=geom, centered=True, shape=shape)
mask2d = assembler.assemble_data(mask)
mask2d = np.array(mask2d >= 1, dtype=float)
data2d = assembler.assemble_data(data)
pg.image(data2d * mask2d)
pv1 = PADView(data=data, pad_geometry=geom, beam=beam, mask=mask)
pv1.show_coordinate_axes()
pv1.show_pad_indices()
pv1.show_pad_borders()
pv1.show()
pv2 = PADView(data=data2d, pad_geometry=assembler.pad_geometry, beam=beam, mask=mask2d)
pv2.show_coordinate_axes()
pv2.show_pad_indices()
pv2.show_pad_borders()
pv2.show()
pg.mkQApp().exec()