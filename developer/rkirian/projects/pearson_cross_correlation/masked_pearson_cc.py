import numpy as np
from numpy import conj
from numpy.fft import fft, ifft
import matplotlib.pyplot as plt
import pyqtgraph as pg
n = 100  # Size of the spectra
m = 200  # Number of spectra
# y is the template spectrum, which is a triangle function
y = np.concatenate([np.linspace(0, 1, int(n/2)), np.linspace(1, 0, int(n/2))])
# Pad y with zeros
y = np.concatenate([y, np.zeros(n)])
# This is the mask for the y array.
Y = np.concatenate([np.ones(n), np.zeros(n)])
# The x array will have m spectra of length n, but it is zero-padded to length 2n
x = np.zeros((m, 2*n))
# This is the mask for x.  Same as that for y, but could be different.
X = np.random.choice([0,1], (m, 2*n), p=[0.1, 0.9])*Y
# Generate a bunch of random shifts
d = (np.random.rand(m)*10 - 5).astype(int)
# Shift the spectra in the x array
for i in range(m):
	x[i, :] = np.roll(y, d[i])
# Mask the x array
x *= X
# Now compute the masked Pearson correlations for all the spectra
ftx = fft(x)
fty = fft(y)
ftx2 = fft(x**2)
fty2 = fft(y**2)
ftY = fft(Y)
ftX = fft(X)
xcy = ifft(fft(x)*conj(fft(y)))
xcY = ifft(fft(x)*conj(fft(Y)))
Xcy = ifft(fft(X)*conj(fft(y)))
XcY = ifft(fft(X)*conj(fft(Y)))
x2cY = ifft(fft(x**2)*conj(fft(Y)))
Xcy2 = ifft(fft(X)*conj(fft(y**2)))
A = xcy -  Xcy* xcY / XcY
B = np.sqrt((x2cY-xcY**2/XcY)*(Xcy2-Xcy**2/XcY))
P = A/B
# This will exclude large shifts where the "tails" of the template are
# correlated with the tails of the spectra.  This region is super noisy;
# e.g. when there is just one or two jointly unmasked pixels.
P *= np.roll(Y, -int(n/2))
P = np.real(P)
# The shifts correspond to the indices at which the Pearson correlation
# is maximized.  We need to be mindful of wrap-around.
df = np.argmax(P, axis=1)
df[df > n] = df[df > n] - 2*n
# This is the test: we check that the Pearson correlation
# finds the correct shifts.
assert np.max(np.abs(d-df)) == 0
xcor = x.copy()
for i in range(m):
	xcor[i, :] = np.roll(x[i, :], -df[i])
# pg.image(x)
# pg.image(P)
# pg.image(xcor)
# pg.mkQApp().exec_()
plt.subplot(2, 1, 1)
plt.imshow(x[:, 0:n].T, interpolation="none")
plt.title("Misaligned Spectra with Masked Pixels")
plt.subplot(2, 1, 2)
plt.imshow(xcor[:, 0:n].T, interpolation="none")
plt.title("Corrected Spectra via Masked Pearson CC")
plt.tight_layout()
plt.show()
plt.savefig("pears.pdf")