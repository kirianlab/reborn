import numpy as np
import matplotlib.pyplot as plt
import pyqtgraph as pg
from reborn.misc import correlate
from reborn import detector, source
from reborn.simulate import solutions
from reborn.viewers.qtviews.padviews import PADView
from reborn.const import eV

photon_energy = 9250*eV
pulse_energy = 1000 * 1e6 * photon_energy
geom = detector.PADGeometry(distance=0.01, shape=(101, 101), pixel_size=100e-6)
geom = detector.PADGeometryList(geom)
beam = source.Beam(photon_energy=photon_energy, pulse_energy=pulse_energy)
data = solutions.get_pad_solution_intensity(pad_geometry=geom, beam=beam, thickness=500e-6, poisson=False)
# data = geom.q_mags(beam=beam)
mask = np.ones_like(data) #geom.beamstop_mask(max_angle=100e-3, beam=beam)
# Introduce a geometry error
pixel_size = geom[0].pixel_size()
shift = np.array((0, 0))
geom.translate([shift[0]*pixel_size, shift[1]*pixel_size, 0])
pv = PADView(data=data, pad_geometry=geom, beam=beam, mask=mask)
pv.show_coordinate_axes()
pv.show()
assembler = detector.PADAssembler(pad_geometry=geom, centered=True)
mask2d = assembler.assemble_data(mask)
mask2d = np.array(mask2d >= 1, dtype=float)
data2d = assembler.assemble_data(data)

x = data2d.copy()
y = x.copy()
#y = y[::-1, ::-1]
X = mask2d.copy()
# x *= X
# X = np.ones_like(x)
Y = X.copy()
# y *= Y
#Y = Y[::-1, ::-1]
P = correlate.masked_pearson_cc_2d(x, X, y, Y, max_shift=0.5, min_unmasked=10)
##############################
# Now get the shift
# 1) Use watershed to get the Pearson peak region
# 2) Polynomial fit the watershed region
# 3) Take half the Pearson shift as detector shift
# 4) Update geometry
# 5) Check that the update is correct
from scipy import ndimage
markers = np.zeros_like(P).astype(np.int16)
px, py = np.array(np.unravel_index(P.argmax(), P.shape))
markers[px, py] = 1
res1 = ndimage.watershed_ift((P*255).astype(np.uint8), markers)
##############################
show_plots = True
def pad2(x):
    nv, nw = x.shape
    return np.pad(x, ((0, nv), (0, nw)))
if show_plots:
    mx = np.max(np.abs(np.concatenate([x, y])))
    fig, axs = plt.subplots(2, 4)
    axs[0, 0].imshow(pad2(x), vmin=-mx, vmax=mx, cmap='bwr')
    axs[0, 0].set_title('x (image)')
    axs[0, 1].imshow(pad2(X), vmin=0, vmax=1, cmap='bwr')
    axs[0, 1].set_title('X (mask)')
    axs[1, 0].imshow(pad2(y), vmin=-mx, vmax=mx, cmap='bwr')
    axs[1, 0].set_title('y (image)')
    axs[1, 1].imshow(pad2(Y), vmin=0, vmax=1, cmap='bwr')
    axs[1, 1].set_title('Y (mask)')
    axs[0, 2].imshow(P, cmap='bwr') #, vmin=-1, vmax=1)
    axs[0, 2].set_title('P (correlation)')
    axs[1, 2].imshow(res1, cmap='jet')  # central plot in the image above
    # axs[1, 2].imshow(Pslow, vmin=-1, vmax=1, cmap='bwr')
    # axs[1, 2].set_title('Pslow (correlation)')
    # axs[0, 3].imshow(P - Pslow, vmin=-1, vmax=1, cmap='bwr')
    # axs[0, 3].set_title('P - Pslow')
    plt.show()

pg.image(x)
pg.image(y)
pg.image(X)
pg.image(Y)
pg.image(P)
pg.mkQApp().exec()
