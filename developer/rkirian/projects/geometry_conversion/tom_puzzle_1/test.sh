#!/bin/bash
#conda init
#conda activate reborn
export PYTHONPATH=$(cd ../../../..; pwd)
# Convert a reborn geometry file to a crystfel geometry.  Attempt to make a proper .geom file... might fail...
python ../../../../scripts/reborn_geom_to_crystfel.py --reborn reborn.json --crystfel reborn2crystfel.geom
# Convert a reborn geometry file to a crystfel geometry.  Use an existing crystfel template.  Should work...
python ../../../../scripts/reborn_geom_to_crystfel.py --reborn reborn.json --crystfel reborn2crystfel_template.geom --template crystfel.geom
# For completeness, convert a crystfel geom file to reborn geometry file.
python ../../../../scripts/crystfel_geom_to_reborn.py --crystfel crystfel.geom --reborn crystfel2reborn.json

