import numpy as np
from reborn.simulate import gas

def get_gas_background(
    pad_geometry,
    beam,
    path_length=[0.0, 1.0],
    gas_type="helium",
    temperature=293.15,
    pressure=101325.0,
    n_simulation_steps=20,
    poisson=False,
    verbose=False,
    he_percent=0.8,
    air_percent=0.8,
    water_percent=0.2
):
    air_o2 = 0.21
    air_n2 = 0.79
    # Interpret gas mixture nicknames into partial pressures:
    if gas_type == "air":
        gas_type = ("O2", "N2")
        pressure = pressure * np.array([air_o2, air_n2])
    elif gas_type == "humid_he":
        gas_type = ("H2O", "He")
        pressure = pressure * np.array([water_percent, he_percent])
    elif gas_type == "humid_air":
        gas_type = ("H2O", "O2", "N2")
        pressure = pressure * np.array([water_percent, air_percent*air_o2, air_percent*air_n2])
    elif gas_type == "he_air":
        gas_type = ("He", "O2", "N2")
        pressure = pressure * np.array([he_percent, air_percent*air_o2, air_percent*air_n2])
    return get_gas_background(
    pad_geometry,
    beam,
    path_length=path_length,
    gas_type=gas_type,
    temperature=temperature,
    pressure=pressure,
    n_simulation_steps=n_simulation_steps,
    poisson=poisson,
    verbose=verbose)