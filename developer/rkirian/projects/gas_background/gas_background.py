import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from reborn import detector, source
from reborn.simulate import gas
from reborn.const import eV, mbar, r_e, k

# ============================================================================
# Testing the validity of gas scatter simulations.  To simulate gas, we must
# sample chunks of molecules along the path of the x-ray beam.  Most of the
# scattering comes from gas molecules close to the detector, because pixel
# solid angles scale approximately as the inverse square of distance.  We
# therefore need to sample more finely nearby the detector.
# ============================================================================

# ----------------------------------------------------------------------------
# First check that the calculated scattering actually agrees with our expected
# pencil-and-paper predictions for the simple case of one
# pixel that is so close to the gas that the effective scattering solid angle
# is half of a sphere (i.e. 2 pi).  In principle the total number of scattered
# photons into *all* angles should be
#     total = J N (8 pi r_e^2 / 3)
# where J is the incident number fluence, N is the total number of *electrons*
# in the beam, and the remaining part in parentheses is the Thompson
# scattering cross section.
#
# We only scatter into half a sphere so we divide by the above result by
# two, and moreover we need to multiply by 3/2 because of a subtle
# numerical issue caused by the fact that our polarization factor is only
# sampled in the forward direction, so we need to work out the Thompson
# scattering cross section with the polarization factor set to 1.  The
# change to the integral is as follows:
#     int sin^2(theta) dtheta dphi = 8pi/3  --> int dtheta dphi = 4pi
dist = 0.001
temperature = 300
radius = 1e-6
pressure = mbar
photon_energy = 10000 * eV
n_molecules = pressure * (dist * np.pi * radius**2) / k / temperature
beam = source.Beam(photon_energy=photon_energy, diameter_fwhm=2 * radius)
geom = detector.tiled_pad_geometry_list(
    pad_shape=(1, 1), pixel_size=10, distance=dist, tiling_shape=(1, 1)
)
intensity = gas.get_gas_background(
    pad_geometry=geom,
    beam=beam,
    n_simulation_steps=1000,
    __fixed_f2=1,  # This means we are scattering from individual electrons
    pressure=pressure,
    temperature=temperature,
    path_length=[0, dist],
)
total_calc = np.sum(intensity)
total_exact = beam.photon_number_fluence * n_molecules * 2 * np.pi * r_e**2
error = np.abs(total_calc - total_exact) / total_exact
print("calculated", total_calc)
print("exact", total_exact)
print(f"calculated/exact: {total_calc/total_exact:.2g}")
print("fractional error", error)
assert error < 1e-4

# Next consider a circular pixel of radius R and a path length L.  In order
# to simplify the integral of the intensity, we will take a scattering factor
# that is independent of q, and we will ignore the polarization factor.  The
# total scattering is then
#      I = int J r_e^2 |F|^2 dOmega n pi r^2 dx
# pulling out the constants we need to do the integral
#      int int dOmega dx = 2 pi int_0^L int_0_theta(x) sin(theta) dtheta dx
#                        = 2pi[ L + R - sqrt(L^2 + R^2)]
# which has the following limiting behavior:
#      R/L -> inf  ==>   2pi L
#      L/R -> inf  ==>   2pi R
# The first limit makes sense since huge R means we collect on the half
# sphere for the entire path, so the total collected just depends on how many
# molecules are illuminated, which is proportional to L.  In the second limit
# the L drops out because molecules at large distances contribute negligibly
# with a 1/L^2 dependence.  The remaining linear dependence on R is less
# obvious, but perhaps understood as the result of nearby scatters that
# have no dependence on R (scatter into the half sphere) along with distant
# scatterers that have an R^2 dependence.
dist = 1
detsize = 1
npix = 100
pixsiz = detsize/npix
simsteps = 100
temperature = 300
radius = 1e-6  # Beam radius
pressure = mbar
photon_energy = 10000 * eV
rho = pressure / k / temperature
beam = source.Beam(photon_energy=photon_energy, diameter_fwhm=2 * radius)
geom = detector.tiled_pad_geometry_list(
    pad_shape=(npix, npix), pixel_size=pixsiz, distance=dist, tiling_shape=(1, 1)
)
mask = geom.beamstop_mask(beam=beam, max_radius=detsize/2)
# from reborn.viewers.qtviews.padviews import view_pad_data
# view_pad_data(data=mask, pad_geometry=geom, beam=beam)
intensity = gas.get_gas_background(
    pad_geometry=geom,
    beam=beam,
    n_simulation_steps=simsteps,
    gas_type="he",
    pressure=pressure,
    temperature=temperature,
    path_length=[0, dist],
    __polarization_off=True,
    __fixed_f2=1
)
total_calc = np.sum(intensity*mask)
total_exact = beam.photon_number_fluence * r_e**2 * rho *np.pi* radius**2 * 2 * np.pi * (detsize/2+dist-np.sqrt(dist**2+(detsize/2)**2))
error = np.abs(total_calc - total_exact) / total_exact
print("calculated", total_calc)
print("exact", total_exact)
print(f"calculated/exact: {total_calc/total_exact:.2g}")
print("fractional error", error)
assert error < 1e-3
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
# Now we look at how the total scattering converges in a more realistic
# geometry, as a function of the number of sample points, number of pixels
# in the detector (with fixed total area), path length of the gas, and the
# sampling function.  We choose a relatively small detector at a long distance
# so that the polarization factor is not overly important.
gamma = 10  # Steepness of sampling.  Large gamma => fine sampling near the detector.
            # Set to None for uniform sampling.
photon_energy = 9000*eV
pressure = mbar
dist = 1  # Detector distance
detsize = 0.001  # Detector size
paths = [0.9, 0.99, 0.999, 1]  # Path lengths
Ns = [1, 100]  # Number of pixels
steps = (2 ** np.arange(8)).astype(int)  # Number of steps in the simulations
colors = cm.rainbow(np.linspace(0, 1, len(paths)))
symbols = [".", "+"]
for i, L in enumerate(paths):
    for j, N in enumerate(Ns):
        pixel_size = detsize / N
        pad_shape = np.array([N, N], dtype=int)
        beam = source.Beam(photon_energy=photon_energy)
        geom = detector.tiled_pad_geometry_list(
            pad_shape=pad_shape, pixel_size=pixel_size, distance=dist, tiling_shape=(1, 1)
        )
        totals = np.zeros(len(steps))
        for m, s in enumerate(steps):
            intensity = gas.get_gas_background(
                pad_geometry=geom,
                beam=beam,
                n_simulation_steps=s,
                gas_type="he",
                pressure=pressure,
                path_length=[0, L],
                __gamma=gamma,
            )
            totals[m] = np.sum(intensity)
        plt.plot(steps, totals, symbols[j], color=colors[i], label=f"L={L:1.4f}, N={N}")
plt.title(f"N pixels, fixed total area, distance 1m, gas path [0, L], gamma {gamma}")
plt.xlabel("steps")
plt.ylabel("total intensity")
plt.legend()
plt.show()
# ------------------------------------------------------------------------------