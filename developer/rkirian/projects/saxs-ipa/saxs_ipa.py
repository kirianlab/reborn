import sys
sys.path.append("../../../..")  # Relative path to reborn
import numpy as np
from scipy.linalg import pinv, solve_sylvester
import matplotlib.pyplot as plt
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "Helvetica"
})
plt.ion()


# Suppose we measure a series of solution scattering
# intensities that form a QxT matrix C_{qt} that is
# understood to be the product of two matrices:
#       C_{qt} = sum_k^K X_{qk} Y_{kt}.
# The matrix X_{qk} contains scattering profiles indexed
# by k (K components in total) sampled at scattering
# wave vectors indexed by q.
# The matrix Y_{kt} is the concentration of component
# k as a function of index t (e.g. time).
# We consider an Iterative Projection Algorithm
# that has a projector onto the data constraint C=XY
# along with projectors onto the constraint sets that
# apply to X and Y.  We assume that projectors onto
# X and Y constraints commute and hence can be combined
# into a single operator.
# A projection operator for C=XY
# has been developed by Veit Elser, J Glob Optim 68(2),
# 329–355 (2017).
# Constraints on X_{qk} could include a support on the
# pair distance distribution P(r).  Additional constraints
# can be the explicit knowledge of certain components
# such as water.
# Projection operators on Y_{kt} could be an assumption
# of smoothness on logarithmic timescales (effectively
# a support constraint), or knowledge that certain matrix
# elements are zero (e.g. when buffer alone is injected,
# we know there is no protein).

def qp(x0, y0, x1, y1, c):
    r"""
    Quasiprojection defined in V. Elser, J Glob Optim 68(2), 329–355 (2017).
    See equations 29-36.
    """
    dy = pinv(x1) @ (c - x1 @ y0 )
    dx = (c - x0 @ y1 ) @ pinv(y1)
    errx = np.sum(dy**2) + np.sum((y1-y0)**2)
    erry = np.sum(dx**2) + np.sum((x1-x0)**2)
    if erry > errx:
        return x0 + dx, y1.copy()
    else:
        return x1.copy(), y0 + dy

def tsp(x0, y0, x1, y1):
    r"""
    Tangent-space projection defined in V. Elser, J Glob Optim 68(2), 329–355 (2017).
    See equations 37-41.
    """
    a = x1 @ x1.T
    b = y1.T @ y1
    q = (x0 - x1) @ y1 + x1 @ (y0 - y1)
    f = solve_sylvester(a, b, q)
    x2 = x0 - f @ y1.T
    y2 = y0 - x1.T @ f
    return x2, y2

def proj_xyc(x0, y0, c, n_iter=10):
    r"""
    C = XY matrix-product projection algorithm from V. Elser, J Glob Optim 68(2), 329–355 (2017).
    This is equation 27.
    """
    x1, y1 = qp(x0, y0, x0, y0, c)
    xt = x1
    yt = y1
    for i in range(n_iter):
        x, y = tsp(x0, y0, xt, yt)
        xt, yt = qp(x0, y0, x, y, c)
    return xt, yt

def proj_x(x, state):
    r"""
    Projection operator that acts independently on the X matrix.
    - Hold matrix elements fixed wherever the mask is zero.
    - Enforce positivity.
    """
    w = state.get("wherexmask", None)
    if w is None:
        m = state["xmask"]
        w = np.where(m.flat == 0)
        state["wherexmask"] = w
    v = state["xvals"]
    xm = x.copy()
    xm.flat[w] = v.flat[w]
    xm.flat[w] /= np.sum(xm.flat[w]**2)
    xm[xm < 0] = 0

    # for i in range(xm.shape[1]):
    #     xm[:, i] = xm[:, i]/np.sum(xm[:, i])
    return xm

def proj_y(y, state):
    r"""
    Projection operator that acts independently on the Y matrix.
    - Hold matrix elements fixed wherever the mask is zero.
    - Do not enforce positivity.
    """
    w = state.get("whereymask", None)
    if w is None:
        m = state["ymask"]
        w = np.where(m.flat == 0)
        state["whereymask"] = w
    v = state["yvals"]
    ym = y.copy()
    ym.flat[w] = v.flat[w]
    ym[ym < 0] = 0
    return ym

def proj_d(f, state):
    r"""
    Data projection operator.
    Constraint equation: C = XY.
    Non-convex.
    The input vector f combines the X, Y matrices as follows:
    f = np.hstack([X.flat, Y.flat])
    The vector f is split into X, Y matrices as follows:
    X = f[:r*k].reshape(r, k)
    Y = f[r*k:].reshape(k, r)
    where the matrix shape r, k comes from the state dictionary:
    r = state['r']
    k = state['k']
    The output is a new vector f
    """
    x, y = f2xy(f, state)
    x, y = proj_xyc(x, y, state["c"])
    return xy2f(x, y, state)

def proj_m(f, state):
    r"""
    Projection on to modeled results.
    Combines projection operators that act independently on the X, Y matrices.
    These projections are defined by the functions proj_x and proj_y .
    The input vector f combines the X, Y matrices as follows:
    f = np.hstack([X.flat, Y.flat])
    The vector f is split into X, Y matrices as follows:
    X = f[:r*k].reshape(r, k)
    Y = f[r*k:].reshape(k, r)
    where the matrix shape r, k comes from the state dictionary:
    r = state['r']
    k = state['k']
    The output is a new vector f
    """
    x, y = f2xy(f, state)
    x = proj_x(x, state)
    y = proj_y(y, state)
    return xy2f(x, y, state)

def update_dm(f, state):
    r""" Difference Map update operator.
    f' = (I + β(P_A((1 − γ2 )P_B − γ2 I ) + P_B ((1 − γ1 )P_A − γ1 I ))) f
       = f + β (P_A((1 − γ2 )P_B − γ2 I )f + P_B ( (1 − γ1 ) P_A f − γ1 f ))
    """
    b = state["dm_params"]["beta"]
    g1 = state["dm_params"]["gamma1"]
    g2 = state["dm_params"]["gamma2"]
    pa = lambda f: proj_d(f, state)
    pb = lambda f: proj_m(f, state)
    paf = pa(f)
    pbf = pb(f)
    return f + b*(pa((1-g2)*pbf - g2*f) + pb((1-g1)*paf-g1*f))

def update_raar(f, state):
    r"""
    u_{n+1} = (beta 0.5 (R_S R_M + I ) + (1 − beta)  P_M) u_n
    The relaxed projections are
    R_S = 2 P_S - I
    R_M = 2 P_M - I
    """
    b = state["raar_params"]["beta"]
    ps = lambda f: proj_m(f, state)
    pm = lambda f: proj_d(f, state)
    rs = lambda f: 2*ps(f)-f
    rm = lambda f: 2*pm(f)-f
    return 0.5*b*(rs(rm(f)) + f ) + (1-b)*pm(f)

def update_admm(f, state):
    r""" ADMM update operatator
    x1 = P1 (x2 + x)
    x2' = P2 (x1 − x)
    x' = x + α (x2' − x1)
    """
    alpha = state["admm_params"]["alpha"]
    x2 = state["admm_params"].get("x2", None)
    if x2 is None:
        x2 = np.random.random(f.shape)
    x1 = proj_d(x2 + f, state)
    x2p = proj_m(x1 - f, state)
    fp = f + alpha*(x2p - x1)
    state["admm_params"]["x2"] = x2p
    return f

def update_rrr(f, state):
    r"""
    RRR update operator
    x1 = P1(x)
    x2 = P2(2*x1 − x)
    x' = x + β(x2 − x1)
    """
    beta = state["rrr_params"]["beta"]
    x1 = proj_d(f, state)
    x2 = proj_m(2*x1 - f, state)
    return f + beta*(x2 - x1)

def update_er(f, state):
    r"""
    Error Reduction update
    """
    ps = lambda f: proj_d(f, state)
    pm = lambda f: proj_m(f, state)
    return ps(pm(f))

def projection_tests():
    r"""
    Test the projection operators
    """
    r = 10
    k = 20
    m = 10
    x = np.random.rand(r, k)
    y = np.random.rand(k, m)
    xmask = np.random.rand(r, k) > 0.6
    xvals = x.copy()
    ymask = np.random.rand(r, m) > 0.6
    yvals = y.copy()
    state = default_state()
    state.update(dict(r=r, k=k, m=m, xmask=xmask, ymask=ymask, xvals=xvals, yvals=yvals, c=None))
    x = proj_x(x, state)
    y = proj_y(y, state)
    f = xy2f(x, y, state)
    c = x @ y  # These are the correct x, y, c matrices
    state["c"] = c
    x0 = np.random.rand(r, k)  # Incorrect matrices x0, y0; starting point from which we wish to project
    y0 = np.random.rand(k, m)
    f0 = xy2f(x0, y0, state)
    x00 = x0.copy()  # Keep copies of the initial inputs; at the end, check for unintended in-place array modifications
    y00 = y0.copy()
    f00 = f0.copy()
    x2, y2 = proj_xyc(x0, y0, c)  # Projection from x0, y0 to new point x2, y2 that satisfy c = x2 y2
    # Check that the projection output satisfies the constraint
    assert np.allclose(c, x2 @ y2)
    # Double projection: check that output equals the input (PPx = Px)
    xt2, yt2 = proj_xyc(x2.copy(), y2.copy(), c)
    assert np.allclose(x2, xt2)
    assert np.allclose(y2, yt2)
    # Check that there is not a nearby solution on the constraint set that is better than
    # the output of the projection operator:
    for i in range(1000):
        # x2, y2 is projection onto the set c = xy that minimizes || x2 - x0 ||^2 + || y2 - y0 ||^2
        # if we perturb x2, y2 -> xt3, yt3, the above error should be larger
        # We will slightly perturb x2 and modify y to satisfy the constraint
        xt3 = x2 + (np.random.random(x2.shape)-0.5)*1e-6
        yt3 = pinv(xt3) @ c
        # Check that the error grows upon perturbations
        errt = np.sum((x0 - x2)**2) + np.sum((y0 - y2)**2)
        err3 = np.sum((x0 - xt3)**2) + np.sum((y0 - yt3)**2)
        assert err3 > errt
    # Check that the projection operators obey PPf = Pf
    assert np.allclose(proj_x(proj_x(x0, state), state), proj_x(x0, state))
    assert np.allclose(proj_y(proj_y(y0, state), state), proj_y(y0, state))
    assert np.allclose(proj_d(proj_d(f0, state), state), proj_d(f0, state))
    assert np.allclose(proj_m(proj_m(f0, state), state), proj_m(f0, state))
    assert np.allclose(f0, f00)
    assert np.allclose(x0, x00)
    assert np.allclose(y0, y00)
    # Check that a correct solution stays put when used to initialize ER algorithm
    fm = xy2f(x, y, state)
    for i in range(100):
        fm = update_er(fm, state)
    assert np.allclose(f, fm)
    # Check that DM algortithm is also stable
    for i in range(100):
        fm = update_rrr(fm, state)
    assert np.allclose(f, fm)
    print("All tests passed")

def do_update_display(iteration, n_iterations):
    r""" Condition on when to update plots (very time-consuming; don't do it often). """
    i = iteration
    if i in (0, 1):
        return True
    if i < 100:
        if ((i+1) % 10) == 0:
            return True
    if i < 1000:
        if ((i+1) % 100) == 0:
            return True
    if ((i + 1) % 500) == 0:
        return True
    if (i+1) == n_iterations:
        return True
    return False

class Display:
    r""" Special class for displaying progress of the IPA. """
    def __init__(self, state):
        x = state["x0"]
        y = state["y0"]
        c = x @ y
        xtrue = state["xtrue"]
        ytrue = state["ytrue"]
        ctrue = xtrue @ ytrue
        r = state["r"]
        k = state["k"]
        m = state["m"]
        self.x_errors = state["x_errors"]
        self.y_errors = state["y_errors"]
        self.c_errors = state["c_errors"]
        self.x_derr = state["x_derr"]
        self.y_derr = state["y_derr"]
        self.c_derr = state["c_derr"]

        figure, axis = plt.subplots(3, 4, figsize=(10, 8), dpi=100)
        x_row = 0
        y_row = 1
        c_row = 2
        true_col = 0
        iter_col = 1
        terr_col = 2
        derr_col = 3

        axis[x_row, true_col].imshow(xtrue, cmap='gray', aspect="auto", interpolation="none")
        xmaskrgb = np.zeros((r, k, 4))
        xmaskrgb[:, :, 0] = 1
        xmaskrgb[:, :, 3] = (1-state["xmask"]) * 0.4
        axis[x_row, true_col].imshow(xmaskrgb, aspect="auto", interpolation="none")
        axis[x_row, true_col].set_title(rf"$\mathrm{{\bf X}}_\mathrm{{true}} \; ({r}\times{k})$")
        axis[x_row, true_col].set_xlabel("r")
        axis[x_row, true_col].set_ylabel("k")
        axis[y_row, true_col].imshow(ytrue, cmap='gray', aspect="auto", interpolation="none")
        ymaskrgb = np.zeros((k, m, 4))
        ymaskrgb[:, :, true_col] = 1
        ymaskrgb[:, :, 3] = (1-state["ymask"]) * 0.4
        axis[y_row, true_col].imshow(ymaskrgb, aspect="auto", interpolation="none")
        axis[y_row, true_col].set_title(rf"$\mathrm{{\bf Y}}_\mathrm{{true}} \; ({k}\times{m})$")
        axis[y_row, true_col].set_xlabel("m")
        axis[y_row, true_col].set_ylabel("k")
        axis[c_row, true_col].imshow(ctrue, cmap='gray', aspect="auto", interpolation="none")
        axis[c_row, true_col].set_title(rf"$\mathrm{{\bf C}}_\mathrm{{true}} \; ({r}\times{m})$")
        axis[c_row, true_col].set_xlabel("m")
        axis[c_row, true_col].set_ylabel("r")

        axis_x = axis[x_row, iter_col].imshow(x, cmap='gray', aspect="auto", interpolation="none")
        axis[x_row, iter_col].imshow(xmaskrgb, aspect="auto", interpolation="none")
        axis[x_row, iter_col].set_title(rf"$\mathrm{{\bf X}}_i \; ({r}\times{k})$")
        axis[x_row, iter_col].set_xlabel("k")
        axis[x_row, iter_col].set_ylabel("r")
        axis_y = axis[y_row, iter_col].imshow(y, cmap='gray', aspect="auto", interpolation="none")
        axis[y_row, iter_col].imshow(ymaskrgb, aspect="auto", interpolation="none")
        axis[y_row, iter_col].set_title(rf"$\mathrm{{\bf Y}}_i \; ({k}\times{m})$")
        axis[y_row, iter_col].set_xlabel("m")
        axis[y_row, iter_col].set_ylabel("k")
        axis_c = axis[c_row, iter_col].imshow(c, cmap='gray', aspect="auto", interpolation="none")
        axis[c_row, iter_col].set_title(rf"$\mathrm{{\bf C}}_i \; ({r}\times{m})$")
        axis[c_row, iter_col].set_xlabel("m")
        axis[c_row, iter_col].set_ylabel("r")

        self.x_error_axis = axis[x_row, terr_col]
        x_error_plot, = axis[x_row, terr_col].semilogy(np.ones(2))
        axis[x_row, terr_col].set_title(rf"$\sum |\mathrm{{\bf X}}_\mathrm{{true}} - \mathrm{{\bf X}}|^2/\sum |\mathrm{{\bf X}}_\mathrm{{true}}|^2$")
        axis[x_row, terr_col].set_xlabel("Iteration")
        axis[x_row, terr_col].set_ylabel("Error")
        self.y_error_axis = axis[y_row, terr_col]
        y_error_plot, = axis[y_row, terr_col].semilogy(np.ones(2))
        axis[y_row, terr_col].set_title(rf"$\sum |\mathrm{{\bf Y}}_\mathrm{{true}} - \mathrm{{\bf Y}}|^2/\sum |\mathrm{{\bf Y}}_\mathrm{{true}}|^2$")
        axis[y_row, terr_col].set_xlabel("Iteration")
        axis[y_row, terr_col].set_ylabel("Error")
        self.c_error_axis = axis[c_row, terr_col]
        c_error_plot, = axis[c_row, terr_col].semilogy(np.ones(2))
        axis[c_row, terr_col].set_title(rf"$\sum |\mathrm{{\bf C}}_\mathrm{{true}} - \mathrm{{\bf C}}|^2/\sum |\mathrm{{\bf C}}_\mathrm{{true}}|^2$")
        axis[c_row, terr_col].set_xlabel("Iteration")
        axis[c_row, terr_col].set_ylabel("Error")

        self.x_derr_axis = axis[x_row, derr_col]
        self.x_derr_plot, = axis[x_row, derr_col].semilogy(np.ones(2))
        axis[x_row, derr_col].set_title(
            rf"$\sum |\mathrm{{\bf X}}_{{i}} - \mathrm{{\bf X}}_{{i-1}}|^2/\sum |\mathrm{{\bf X}}_{{i}}|^2$")
        axis[x_row, derr_col].set_xlabel("Iteration")
        axis[x_row, derr_col].set_ylabel("Error")
        self.y_derr_axis = axis[y_row, derr_col]
        self.y_derr_plot, = axis[y_row, derr_col].semilogy(np.ones(2))
        axis[y_row, derr_col].set_title(
            rf"$\sum |\mathrm{{\bf Y}}_{{i}} - \mathrm{{\bf Y}}_{{i-1}}|^2/\sum |\mathrm{{\bf Y}}_{{i}}|^2$")
        axis[y_row, derr_col].set_xlabel("Iteration")
        axis[y_row, derr_col].set_ylabel("Error")
        self.c_derr_axis = axis[c_row, derr_col]
        self.c_derr_plot, = axis[c_row, derr_col].semilogy(np.ones(2))
        axis[c_row, derr_col].set_title(
            rf"$\sum |\mathrm{{\bf C}}_{{i}} - \mathrm{{\bf C}}_{{i-1}}|^2/\sum |\mathrm{{\bf C}}_{{i}}|^2$")
        axis[c_row, derr_col].set_xlabel("Iteration")
        axis[c_row, derr_col].set_ylabel("Error")

        plt.tight_layout()
        plt.show(block=False)
        self.figure = figure
        self.axis = axis
        self.axis_x = axis_x
        self.axis_y = axis_y
        self.axis_c = axis_c
        self.x_error_plot = x_error_plot
        self.y_error_plot = y_error_plot
        self.c_error_plot = c_error_plot
        self.iters = np.arange(state["n_iterations"])
    def update(self, state):
        i = state["i"]
        f = xy2f(state["x"], state["y"], state)
        f = proj_m(f, state)
        x, y = f2xy(f, state)
        c = x @ y
        self.axis_x.set_data(x)
        self.axis_x.autoscale()  # set_clim(PYY.min(), PYY.max())
        self.axis_y.set_data(y)
        self.axis_y.autoscale()
        self.axis_c.set_data(c)
        self.axis_c.autoscale()
        a = self.iters[:i]
        self.x_error_plot.set_data(a, self.x_errors[:i])
        self.y_error_plot.set_data(a, self.y_errors[:i])
        self.c_error_plot.set_data(a, self.c_errors[:i])
        self.x_derr_plot.set_data(a, self.x_derr[:i])
        self.y_derr_plot.set_data(a, self.y_derr[:i])
        self.c_derr_plot.set_data(a, self.c_derr[:i])
        for ax in (self.x_error_axis, self.y_error_axis, self.c_error_axis, self.x_derr_axis, self.y_derr_axis, self.c_derr_axis):
            ax.relim()
            ax.autoscale_view()
        plt.draw()
        plt.pause(1e-6)

def default_state(primary_alg="RRR", secondary_alg="ER"):
    state = dict(dm_params=dict(beta=1, gamma1=-1, gamma2=1),
                 raar_params=dict(beta=1),
                 admm_params=dict(alpha=1),
                 rrr_params=dict(beta=1),
                 primary_alg=primary_alg,
                 secondary_alg=secondary_alg,
                 n_iterations=10000,
                 cycle_period=10,
                 secondary_iterations=5,
                 )
    return state

def xy2f(x, y, state):
    return np.hstack([x.flat, y.flat])

def f2xy(f, state):
    r = state["r"]
    k = state["k"]
    m = state["m"]
    x = f[:r * k].reshape(r, k)
    y = f[r * k:].reshape(k, m)
    return x, y

def run_ipa(state):
    r = state["r"]
    k = state["k"]
    m = state["m"]
    c = state["c"]
    n_iterations = state["n_iterations"]
    # Optional
    silent = state.get("silent", False)
    show_plots = state.get("show_plots", True)
    cp = state.get("cycle_period", 10)
    ns = state.get("secondary_iterations", 5)
    primary_alg = state.get("primary_alg", "RRR")
    secondary_alg = state.get("secondary_alg", "ER")
    xtrue = state.get("xtrue", None)
    ytrue = state.get("ytrue", None)
    ctrue = None
    if xtrue is not None and ytrue is not None:
        ctrue = xtrue @ ytrue
    x0 = state.get("x0", None)
    y0 = state.get("y0", None)
    if x0 is None:
        x0 = np.random.rand(r, k)
    if y0 is None:
        y0 = np.random.rand(k, m)
    f0 = xy2f(x0, y0, state)
    f = f0.copy()
    x_errors = np.zeros(n_iterations)
    y_errors = np.zeros(n_iterations)
    c_errors = np.zeros(n_iterations)
    state["x_errors"] = x_errors
    state["y_errors"] = y_errors
    state["c_errors"] = c_errors
    x_derr = np.zeros(n_iterations)
    y_derr = np.zeros(n_iterations)
    c_derr = np.zeros(n_iterations)
    state["x_derr"] = x_derr
    state["y_derr"] = y_derr
    state["c_derr"] = c_derr
    state["previous_f"] = None
    if show_plots:
        display = Display(state)
    algorithms = [update_er, update_dm, update_raar, update_admm, update_rrr]
    algorithm_names = ["ER", "DM", "RAAR", "ADMM", "RRR"]
    primary_update = [algorithms[i] for i in range(len(algorithms)) if algorithm_names[i] == state["primary_alg"]][0]
    secondary_update = [algorithms[i] for i in range(len(algorithms)) if algorithm_names[i] == state["secondary_alg"]][0]
    for i in range(n_iterations):
        previous_f = f.copy()
        if (i % cp) < (cp - ns):
            alg = primary_alg
            update = primary_update
        else:
            alg = secondary_alg
            update = secondary_update
        f = update(f, state)
        f_error = proj_m(f, state)
        x_error, y_error = f2xy(f_error, state)
        c_error = x_error @ y_error
        x_errors[i] = np.sum((x_error - xtrue) ** 2) / np.sum(xtrue ** 2)
        y_errors[i] = np.sum((y_error - ytrue) ** 2) / np.sum(ytrue ** 2)
        c_errors[i] = np.sum((c_error - ctrue) ** 2) / np.sum(ctrue ** 2)
        xa, ya = f2xy(previous_f, state)
        ca = xa @ ya
        xb, yb = f2xy(f, state)
        cb = xb @ yb
        x_derr[i] = np.sum((xa - xb) ** 2) / np.sum(xb ** 2)
        y_derr[i] = np.sum((ya - yb) ** 2) / np.sum(yb ** 2)
        c_derr[i] = np.sum((ca - cb) ** 2) / np.sum(cb ** 2)
        if not silent:
            print(f"Iteration {i:6d}.  Alg={alg:6s}.  X, Y, C errors: {x_errors[i]:7.2e}, {y_errors[i]:7.2e}, {c_errors[i]:7.2e}")
        if show_plots and do_update_display(i, state["n_iterations"]):
            state["i"] = i
            x, y = f2xy(f, state)
            state["x"] = x
            state["y"] = y
            #state["c"] = x @ y
            # state["x_error"] = x_error
            # state["y_error"] = y_error
            # state["c_error"] = c_error
            # state["x_derr"] = x_derr
            # state["y_derr"] = y_derr
            # state["c_derr"] = c_derr
            display.update(state)
    return state

if __name__ == "__main__":
    # np.random.seed(0)
    projection_tests()  # Check that projection operators are correct
    random_number_test = 0
    simulation_test = 1
    data_test = 0
    if random_number_test:  # Test generic case of random numbers
        r = 100
        k = 4
        m = 20
        fixed_fraction = (r*k + k*m)/(r*m)  # For X, Y projection, hold this fraction of matrix elements fixed.
        start_truth_frac = 0  # Number in range [0, 1].  1: start w solution. 0: start w random nums.
        state = default_state(primary_alg="RRR", secondary_alg="ER")
        state['show_plots'] = True
        state["n_iterations"] = 1000
        state["r"] = r
        state["k"] = k
        state["m"] = m
        state["xmask"] = np.random.rand(r, k) > fixed_fraction
        state["xvals"] = np.random.rand(r, k)
        state["ymask"] = np.random.rand(k, m) > fixed_fraction
        state["yvals"] = np.random.rand(k, m)
        xtrue = proj_x(np.random.rand(r, k), state)
        ytrue = proj_y(np.random.rand(k, m), state)
        state["xtrue"] = xtrue
        state["ytrue"] = ytrue
        # Add noise
        xnoise = 0 # np.random.normal(0, 0.1, xtrue.shape)
        ynoise = 0 # np.random.normal(0, 0.1, ytrue.shape)
        state["c"] = (xtrue + xnoise) @ (ytrue + ynoise)
        b = start_truth_frac
        state["x0"] = xtrue*b + (np.random.rand(r, k) - 0.5)*(1-b)
        state["y0"] = ytrue*b + (np.random.rand(k, m) - 0.5)*(1-b)
        run_ipa(state)
        plt.show(block=True)
        sys.exit()
    if simulation_test:  # Tests simulated saxs patterns
        from reborn import source, detector
        from reborn.simulate import gas, solutions, form_factors
        from reborn.analysis import saxs
        from reborn.viewers import qtviews
        from reborn.const import eV, mbar
        import joblib
        from joblib import Memory
        location = './cachedir'
        memory = Memory(location, verbose=False)
        # memory.clear(warn=False)
        @memory.cache
        def get_data(radii=[5e-9], n_bins=100):
            distance = 0.3
            beam = source.Beam(photon_energy=9000*eV, pulse_energy=1e-3)
            geom = detector.cspad_pad_geometry_list(detector_distance=distance)
            qmags = geom.q_mags(beam)
            f2phot = geom.f2phot(beam)
            profiler = saxs.RadialProfiler(pad_geometry=geom, beam=beam, n_bins=n_bins, q_range=(np.min(qmags), np.max(qmags)))
            q = profiler.q_bin_centers/1e10
            weights = geom.solid_angles()*geom.polarization_factors(beam)
            # Water profile
            water_pattern = solutions.get_pad_solution_intensity(pad_geometry=geom, beam=beam, thickness=500e-6, poisson=False)
            water_pattern = geom.concat_data(water_pattern)
            stats = profiler.quickstats(water_pattern, weights)
            solid_angles = stats["weight_sum"]
            water_sum = stats["sum"]
            water_profile = water_sum/solid_angles
            # Gas profile
            gas_pattern = gas.get_gas_background(pad_geometry=geom, beam=beam, gas_type="he", pressure=1000*mbar, poisson=False)
            stats = profiler.quickstats(gas_pattern, weights)
            gas_sum = stats["sum"]
            gas_profile = gas_sum/solid_angles
            sphere_profiles = []
            for r in radii:
                sphere_pattern = 1e6*(3.346e29*form_factors.sphere_form_factor(r, qmags))**2*f2phot
                stats = profiler.quickstats(sphere_pattern, weights)
                sphere_sum = stats["sum"]
                sphere_profile = sphere_sum/solid_angles
                sphere_profiles.append(sphere_profile)
            components = sphere_profiles + [water_profile, gas_profile, np.ones_like(gas_profile)]
            return dict(components=components, q=q, radii=radii)
        radii = [5e-9]
        n_spheres = len(radii)
        sim = get_data(radii)
        profiles = sim["components"]
        q = sim["q"]
        if 0:
            for p in profiles:
                plt.plot(q, p)
            plt.show(block=True)
        r = len(q)
        k = len(profiles)
        m = 20
        state = default_state(primary_alg="RRR", secondary_alg="ER")
        state['show_plots'] = True
        state["n_iterations"] = 100000
        state["cycle_period"] = 10
        state["secondary_iterations"] = 5
        state["r"] = r
        state["k"] = k
        state["m"] = m
        xtrue = np.vstack(profiles).T.copy()
        # xtrue = np.random.random(xtrue.shape)
        xtrue /= np.max(xtrue)
        state["xtrue"] = xtrue
        state["xvals"] = xtrue
        xmask = np.ones_like(xtrue)
        xmask[:, n_spheres:] = 0
        state["xmask"] = xmask #np.random.choice(np.array([0, 1]), xmask.shape, p=np.array([.5, .5]))
        ytrue = np.random.rand(k, m)
        state["ytrue"] = ytrue
        state["yvals"] = ytrue
        ymask = np.ones((k, m))

        ymask *= 0
        # ymask[0:n_spheres, 0:int(3*m/4)] = 0
        # ymask[0:n_spheres+1, 0:int(m/2)] = 0
        # ymask[0:n_spheres+2, 0] = 0
        # ytrue[ymask == 0] = 0

        state["ymask"] = ymask #np.random.choice(np.array([0, 1]), ymask.shape, p=np.array([.5, .5]))
        state["c"] = xtrue @ ytrue
        b = 0.9
        state["x0"] = xtrue*b + (np.random.rand(r, k) - 0.5)*(1-b)
        state["y0"] = ytrue*b + (np.random.rand(k, m) - 0.5)*(1-b)
        ftrue = xy2f(state["xtrue"], state["ytrue"], state)
        assert np.allclose(ftrue, proj_m(ftrue, state))
        assert np.allclose(ftrue, proj_d(ftrue, state))
        constraint_ratio = (r*m)/(np.sum(xmask)+np.sum(ymask))
        print(f"Constraint ratio = {constraint_ratio}")
        run_ipa(state)
        plt.show(block=True)
        print("Exiting simulation test")
        sys.exit()
    if data_test:  # Tests real data from Adil
        data = np.load("SAXSMain_chunked.npz")
        profiles = sim["components"]
        q = sim["q"]
        if 0:
            for p in profiles:
                plt.plot(q, p)
            plt.show(block=True)
        r = len(q)
        k = len(profiles)
        m = 20
        state = default_state(primary_alg="RRR", secondary_alg="ER")
        state['show_plots'] = True
        state["n_iterations"] = 100000
        state["cycle_period"] = 10
        state["secondary_iterations"] = 5
        state["r"] = r
        state["k"] = k
        state["m"] = m
        xtrue = np.vstack(profiles).T.copy()
        # xtrue = np.random.random(xtrue.shape)
        xtrue /= np.max(xtrue)
        state["xtrue"] = xtrue
        state["xvals"] = xtrue
        xmask = np.ones_like(xtrue)
        xmask[:, n_spheres:] = 0
        state["xmask"] = np.random.choice(np.array([0, 1]), xmask.shape, p=np.array([.5, .5]))
        ytrue = np.random.rand(k, m)
        state["ytrue"] = ytrue
        state["yvals"] = ytrue
        ymask = np.ones((k, m))
        ymask[0:n_spheres, 0:int(3 * m / 4)] = 0
        ymask[0:n_spheres + 1, 0:int(m / 2)] = 0
        ymask[0:n_spheres + 2, 0] = 0
        ytrue[ymask == 0] = 0
        state["ymask"] = np.random.choice(np.array([0, 1]), ymask.shape, p=np.array([.5, .5]))
        state["c"] = xtrue @ ytrue
        b = 0
        state["x0"] = xtrue * b + (np.random.rand(r, k) - 0.5) * (1 - b)
        state["y0"] = ytrue * b + (np.random.rand(k, m) - 0.5) * (1 - b)
        ftrue = xy2f(state["xtrue"], state["ytrue"], state)
        assert np.allclose(ftrue, proj_m(ftrue, state))
        assert np.allclose(ftrue, proj_d(ftrue, state))
        constraint_ratio = (r * m) / (np.sum(xmask) + np.sum(ymask))
        print(f"Constraint ratio = {constraint_ratio}")
        run_ipa(state)
        plt.show(block=True)
        print(xvals.shape)

        # pv1 = qtviews.PADView(data=water, pad_geometry=geom, beam=beam)
        # pv1.show()
        # pv2 = qtviews.PADView(data=gas, pad_geometry=geom, beam=beam)
        # pv2.show()
        # pv3 = qtviews.PADView(data=sphere1, pad_geometry=geom, beam=beam)
        # pv3.start()

