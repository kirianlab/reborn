import sys
import shutil
import numpy as np

np.random.seed(0)
import reborn
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.analysis import runstats, saxs
from reborn.const import eV

geom = reborn.detector.cspad_2x2_pad_geometry_list(detector_distance=0.1, binning=10)
beam = reborn.source.Beam(
    photon_energy=9500 * eV, diameter_fwhm=1e-6, pulse_energy=1e-4
)
mask = geom.edge_mask()
log_file = "logs/padstats"
checkpoint_file = "checkpoints/padstats"
message_prefix = "Test Run -"


def delete_checkpoints():
    shutil.rmtree("logs", ignore_errors=True)
    shutil.rmtree("checkpoints", ignore_errors=True)


class Getter(reborn.fileio.getters.FrameGetter):
    def __init__(self, geom=None, beam=None, mask=None, n_frames=10, **kwargs):
        self.geom = geom
        self.beam = beam
        self.mask = mask
        self.n_frames = n_frames
        self.none_frames = np.zeros(self.n_frames)
        pat = get_pad_solution_intensity(
            pad_geometry=self.geom, thickness=3e-6, beam=self.beam, poisson=False
        )
        self.pat = geom.concat_data(pat)
        self.gain = np.random.normal(loc=20, size=geom.n_pixels, scale=2)
        self.offset = np.random.normal(loc=0, size=geom.n_pixels, scale=2)

    def get_data(self, frame_number=0):
        if self.none_frames[frame_number]:
            return None
        pat = np.random.poisson(self.pat).astype(float) * self.gain
        pat += np.random.normal(loc=self.pat, scale=self.gain / 5)
        df = reborn.dataframe.DataFrame(
            raw_data=pat, mask=self.mask, pad_geometry=self.geom, beam=self.beam
        )
        return df


def test_01():
    # Test single process
    delete_checkpoints()
    framegetter = Getter(geom=geom, mask=mask, beam=beam)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        n_processes=1,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == geom.n_pixels * framegetter.n_frames
    # Stupid test: what happens if you run process_frames again?  It should re-load checkpoints from the
    # previous run.
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == geom.n_pixels * framegetter.n_frames
    # Test reload from checkpoint
    framegetter = Getter(geom=geom, mask=mask, beam=beam)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        n_processes=1,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == geom.n_pixels * framegetter.n_frames
    # Test q histogram conversion
    hist = runstats.PixelHistogram(**stats["histogram_params"])
    hist.histogram = stats["histogram"]
    q_histogram = hist.convert_to_q_histogram(
        pad_geometry=geom, beam=beam, mask=mask, n_q_bins=10, q_range=(0, 3e10)
    )
    assert np.sum(q_histogram) == np.sum(mask) * framegetter.n_frames
    # runstats.view_padstats(stats, histogram=True)


def test_02():
    # Test multiple processes
    delete_checkpoints()
    framegetter = Getter(geom=geom, mask=mask, beam=beam)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        n_processes=2,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == geom.n_pixels * framegetter.n_frames
    # runstats.view_padstats(stats, histogram=True)


def test_03():
    # Test skipping
    delete_checkpoints()
    framegetter = Getter(geom=geom, mask=mask, beam=beam)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        start=1,
        stop=7,
        step=2,
        n_processes=2,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == geom.n_pixels * 3


def test_04():
    # Test fewer frames than processes
    delete_checkpoints()
    framegetter = Getter(geom=geom, mask=mask, beam=beam, n_frames=1)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        n_processes=2,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == geom.n_pixels * framegetter.n_frames
