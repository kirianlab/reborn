import sys
import time
import numpy as np
import pandas
from reborn import detector, source, dataframe
from reborn.fileio.getters import FrameGetter
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.viewers.qtviews.padviews import PADView
from reborn.const import eV
# Simulate the expected water scatter from a sheet.
# ALL UNITS ARE SI IN REBORN
# Use the beam and geometry from the experiment:
detector_distance = 0.1
photon_energy = 7000*eV
poisson = False  # Include Poisson noise in the result?
jet_diameter = 1000e-9  # Thickness of the sheet
temperature = 300  # Temperature of the water
beam_diameter = 100e-9  # Beam diameter (doesn't really matter)
pulse_energy = 1e-3  # Pulse energy of the x-ray beam
binning = 1  # Bin pixels if you want more speed (but expect more photons per pixel...)
beam = source.Beam(photon_energy=photon_energy)
beam.photon_energy = photon_energy
beam.pulse_energy = pulse_energy
beam.diameter_fwhm = beam_diameter
geom = detector.jungfrau4m_pad_geometry_list(detector_distance=detector_distance) #load_pad_geometry_list(geom_file)
geom = geom.binned(binning)
if detector_distance is not None:
    geom.translate(
        [0, 0, detector_distance - geom.average_detector_distance(beam=beam)]
    )
dist = geom.average_detector_distance(beam=beam)
print(f"Average detector distance: {dist*1e3:0.3g} mm")
sheet_scatter = get_pad_solution_intensity(
    pad_geometry=geom,
    beam=beam,
    thickness=jet_diameter,
    liquid="water",
    temperature=temperature,
    poisson=poisson,
)
sheet_scatter = geom.concat_data(sheet_scatter)

n_frames = 100000


class MyFrameGetter(FrameGetter):
    def __init__(self):
        super().__init__()
        self.geom = geom
        self.beam = beam
        self.n_frames = n_frames
        self.pandas_dataframe = pandas.DataFrame({"Frame #": np.arange(self.n_frames)})
        self.df = None
        self.intensity = None

    def get_data(self, frame_number=0):
        np.random.seed(frame_number)
        self.df = None
        if self.df is None:
            # print("Simulating frame")
            tic = time.time()
            self.intensity = sheet_scatter  #get_pad_solution_intensity(
            #     pad_geometry=self.geom, beam=self.beam, thickness=500e-6, poisson=False
            # )
            # print(time.time() - tic, "seconds")
            df = dataframe.DataFrame()
            df.set_beam(self.beam)
            g = self.geom.copy()
            # g.translate([0, 0, 0.001])
            df.set_pad_geometry(g)
            self.df = df
        df = self.df
        intensity = np.random.poisson(np.double(self.intensity) * (10 + 8 * np.random.rand())) + np.random.normal(size=geom.n_pixels)
        df.set_raw_data(
            intensity
        )
        return df
fg = MyFrameGetter()
fg._debug = 1
# fg._cache_forward = True
pv = PADView(frame_getter=fg, debug_level=1)
# pv.hold_beam = True
# pv.hold_pad_geometry = True
# pv.hold_mask = True
# pv.show_histogram = False
# pv.hold_levels = True
pv.start()
sys.exit()
