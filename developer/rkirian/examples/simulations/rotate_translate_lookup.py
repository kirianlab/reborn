import sys
import numpy as np
import matplotlib.pyplot as plt
from reborn.simulate.clcore import ClCore
from reborn.misc.interpolate import trilinear_interpolation
# Suppose we have a density map rho(r) of a molecule.
# The corresponding diffraction amplitudes are A(q) = int rho(r) exp(-i q.r) d3r
#
# Suppose we want to make a 2D diffraction pattern from a superposition of
# many copies of the molecule.  We can do this by first summing the densities
# of all the molecules, taking the FT, and then slicing an Ewald sphere
# through the many-molecule density map.
#
# The above is too slow.  We can speed up calculations by pre-computing amplitudes
# A of a single molecule, and then taking rotated Ewald slices through that map.
# The molecule shifts can be applied by the FT shift theorem.
#
# Suppose that we wish to *actively* rotate the atomic coordinates of the molecule by
# the matrix R, followed by an *active* translation by the vector u.  The atomic
# coordinates become r' = R r + u .  The modified density map becomes
# rho'(r) = rho(R'(r - u)) where R' is the inverse/transpose of R.
# Note how the *passive* operation is applied to the argument of rho!
# The diffraction amplitudes corresponding to the rotated/translated molecule are
#
# A'(q) = int rho'(r) exp(-i q.r) d3r
#       = int rho(R'(r - u)) exp(-i q.r) d3r
#       = int rho(r) exp(-i q . (R r - u)) d3r
#       = exp(-i q.u) int rho(r) exp(-i R'q . r) d3r
#
# From the above, we see that we need to apply the inverse rotation to the q vectors
# when looking up amplitudes, and then we multiply by the phase factor exp(-i q.u).
#
# Now, let's check that we know how to do the above using numpy FFT operators.
#
# It is important to choose some conventions for how we store density maps
# and amplitudes.  Here we choose the real-space density map such that the molecule
# is centered at the "corner" of the map (the coordinate r=(0,0,0) corresponds to
# the first voxel in memory) with negative indices indicating wrap-around.
# With that assumption, the numpy FFT will get us the
# desired diffraction amplitudes, without a giant phase oscillation.  We choose to
# store the diffraction amplitudes such that the "center" of the 3D array corresponds
# to q=(0,0,0), because our interpolation methods make that assumption.

# Real-space grid spacing
d = 1e-10
# Shape of grid, assumed to be a cube.  The code works with even/odd n.
n = 101
# Atomic coordinates (array indices -- the atoms are exactly on grid points)
r = np.array([[0, 0, 0], [4, 0, 0], [5, 6, 2]])
# Atomic scattering densities
f = np.array([1, 3, 5])
# The q vectors that correspond to the density map.  Be careful: we must be
# consistent with the numpy FFT definition!
qs = 2 * np.pi * np.fft.fftshift(np.fft.fftfreq(n, d=d))
qx, qy, qz = np.meshgrid(qs, qs, qs, indexing="ij")
q = np.vstack([qx.ravel(), qy.ravel(), qz.ravel()]).T.copy()
print(q)
# Now create the real-space density map for the molecule
rho = np.zeros((n, n, n))
for i in range(r.shape[0]):
    rho[r[i, 0], r[i, 1], r[i, 2]] = f[i]
# Check that we can make a diffraction amplitude map, do a trilinear interpolation,
# of that map, and then iFFT back to the original real-space density map.
# Make the amplitude lookup table.  Forward numpy FFT.  Note the fftshift!
amps = np.fft.fftshift(np.fft.fftn(rho)).copy()
rho_ft = trilinear_interpolation(amps, q, x_min=[qs[0]] * 3, x_max=[qs[-1]] * 3)
rho_ft = rho_ft.reshape((n, n, n))
rho_ft = np.abs(np.fft.ifftn(rho_ft))
plt.figure()
plt.imshow(np.sum(rho, axis=2))
plt.title("Original density")
plt.xlabel("y")
plt.ylabel("x")
plt.figure()
plt.imshow(np.sum(rho_ft, axis=2))
plt.title("Original density, from FT lookup")
plt.xlabel("y")
plt.ylabel("x")
assert np.max(np.abs(rho_ft - rho)) < 1e-12
# Another real-space density map in which the molecule is directly rotated
# and translated
rho2 = np.zeros((n, n, n))
# First: rotate the density map atoms about z by 90 degrees.
R = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])
r2 = np.dot(r, R.T)
# Check that we are doing an *active* rotation on the coordinates as expected
# assert np.max(np.abs(r2 - np.array([[0, 0, 0], [0, 4, 0], [-6, 5, 2]]))) == 0
# R = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])  # FIXME
# r2 = np.dot(r, R.T)  # FIXME
# Second: translate along y by 10 voxels
U = np.array([0, 10, 0])
r2 += U
# Check that our rotation/translation maintained integer coordinates.  We
# don't want interpolation artifacts in our comparisons.
assert np.max(np.abs(r2 - r2.astype(int))) == 0
r2 = r2.astype(int)
# Make the rotated/translated density map
for i in range(r.shape[0]):
    rho2[r2[i, 0], r2[i, 1], r2[i, 2]] = f[i]
plt.figure()
plt.imshow(np.sum(rho2, axis=2))
plt.title("Rotated/translated density")
plt.xlabel("y")
plt.ylabel("x")
# Now create the rotated/translated real-space map using the original
# amplitude lookup table.  The q vectors are rotated by *transpose* of the
# matrix that actively rotates the position coordinates.
qrot = np.dot(q, R)
rho_ft2 = trilinear_interpolation(
    amps, qrot, x_min=[qs[0]] * 3, x_max=[qs[-1]] * 3
)
# Dot the *active* translation vector with the *original* q vectors!
rho_ft2 *= np.exp(-1j * np.dot(q, U*d))
rho_ft2 = rho_ft2.reshape((n, n, n))
rho_ft2 = np.abs(np.fft.ifftn(rho_ft2))
plt.figure()
plt.imshow(np.sum(rho_ft2, axis=2))
plt.title("Rotated/translated density, from FT lookup")
plt.xlabel("y")
plt.ylabel("x")
assert np.max(np.abs(rho_ft2 - rho2)) < 1e-12
plt.show()
# Note that the above can also be achieved on a GPU using the lookup
# tools.  However, there is something wrong with the code below...
sys.exit()
gpu = ClCore(double_precision=True)
rho_ft3 = gpu.mesh_interpolation(amps, q, N=(n, n, n), q_min=[qs[0]] * 3, q_max=[qs[-1]] * 3, R=R, U=U*d)
rho_ft3 = rho_ft3.reshape((n, n, n))
rho_ft3 = np.abs(np.fft.ifftn(rho_ft3))
plt.figure()
plt.imshow(np.sum(rho_ft3, axis=2))
plt.title("Rotated/translated density, from GPU FT lookup")
plt.xlabel("y")
plt.ylabel("x")
plt.show()
print(np.max(np.abs(rho_ft3 - rho2)))
assert np.max(np.abs(rho_ft3 - rho2)) < 1e-12
plt.show()