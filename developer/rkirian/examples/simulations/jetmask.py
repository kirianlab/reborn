import numpy as np
import reborn
from reborn.analysis.masking import StreakMasker
from reborn.viewers.qtviews import PADView
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.const import eV

geom = reborn.detector.jungfrau4m_pad_geometry_list(detector_distance=0.1)
for g in geom:
    g.do_cache = True
geom.do_cache = True
beam = reborn.source.Beam(
    photon_energy=9500 * eV, diameter_fwhm=1e-6, pulse_energy=1e-3
)
mask = geom.edge_mask()
m2 = np.ones(geom.n_pixels, dtype=int)
m2[0:int(geom.n_pixels/2)] = 0
mask *= m2


class StreakGetter(reborn.fileio.getters.FrameGetter):
    def __init__(self, geom, beam, mask):
        self.geom = geom
        self.beam = beam
        self.mask = mask
        self.pattern = get_pad_solution_intensity(
            pad_geometry=geom, thickness=3e-6, beam=beam, poisson=False
        )
        self.pattern = geom.concat_data(self.pattern)
        self.masker = StreakMasker(
            geom,
            beam,
            n_q=20,
            q_range=(0.3e10, 0.7e10),
            prominence=0.8,
            max_streaks=2,
            streak_width=0.005,
            snr=5,
            debug=1,
        )
        super().__init__()
        self.n_frames = 1e6

    def get_data(self, frame_number=0):
        np.random.seed(frame_number)
        pat = self.pattern.copy()
        for i in range(int(np.random.rand() * 3)):
            angle = (
                (90 - (np.random.rand() - 0.5) * 30) * np.pi / 180
            )  # Angle of the streak
            print("Streak at angle", angle*180/np.pi - 45)
            pixel_vecs = (
                self.geom.s_vecs()
            )  # Unit vectors from origin to detector pixels
            streak_vec = self.beam.e1_vec * np.cos(angle) + self.beam.e2_vec * np.sin(
                angle
            )
            phi = 90 * np.pi / 180 - np.arccos(np.abs(np.dot(pixel_vecs, streak_vec)))
            theta = np.arccos(np.abs(np.dot(pixel_vecs, beam.beam_vec)))
            streak = np.random.rand() * np.random.poisson(
                100 * np.exp(-1000000 * phi**2 - 100 * theta**2)
            )
            streak *= 0.1
            pat += streak
            print("streak signal", np.sum(streak))
        pat = np.random.poisson(pat)
        if np.random.rand() < 0.1:
            pat *= 0
        pat = pat.astype(float)
        smask = self.mask.copy().astype(float)
        smask *= 1 # self.masker.get_mask(pat, self.mask)
        df = reborn.dataframe.DataFrame(
            raw_data=pat, mask=smask * self.mask, pad_geometry=self.geom, beam=self.beam
        )
        return df


getter = StreakGetter(geom, beam, mask)
padview = PADView(getter, debug=1)
padview.set_mask_color([0, 128, 0, 50])
padview.start()
