import numpy as np
import matplotlib.pyplot as plt
from reborn import source, detector
from reborn.simulate.atoms import cromer_mann_scattering_factors
from reborn.simulate.clcore import ClCore
# The diffraction equation is
#
#   I(q) = J_0 r_e^2 P(q) dOmega |int rho(r) exp(- i q.r) dr |^2
#
# First a pair of electrons.  One is at the origin and one at the position (d, 0, 0).  Setting aside polarization and
# solid angle corrections, the diffraction intensity is proportional to
# |sum exp(-i q.r)|^2 = |1 + exp(-i q_x d)|^2 = 2 + 2 cos(q_x d).  First we set up the needed arrays
# with q vectors and atomic coordinates.  Importantly, the q vectors that we choose are not restricted to the Ewald
# sphere [NOTE]; we are assuming the limit of very small scattering angles (or, equivalently, very short wavelength).
# The q vectors therefore lie within a perfect plane, which amounts to taking a central slice [NOTE] through the
d = 1e-10
r = np.array([[0, 0, 0], [d, 0, 0]])
n_electrons = r.shape[0]
nq = 100
qmax = 2*2*np.pi/d  # Should be two peaks from origin
q = np.linspace(-qmax, qmax, nq)  # No Ewald curvature
qy, qx = np.meshgrid(q, q, indexing="ij")
qvecs = np.zeros([nq**2, 3])
qvecs[:, 0] = qx.ravel()
qvecs[:, 1] = qy.ravel()
# Aside:
# Let's be clear that we understand the mapping between the numpy arrays and the matplotlib display.  We modify
# the qx array so that it is obvious which plot axis corresponds to which array index.  It is clear that the first
# element in the array has the indices [0, 0] and appears in the upper left corner, whereas the next element in memory
# is [0, 1] and is located one pixel to the right.
plt.figure()
qxmod = qx.copy()
qxmod[0:nq//10, 0:nq//2] = 0
plt.imshow(qxmod, interpolation='none')
plt.title('$q_x$')
plt.xlabel('Right-most index (short stride)')
plt.ylabel('Left-most index (long stride)')
plt.colorbar()
# Next we do the simulation and plot.
amps = 0
for i in range(n_electrons):
    amps += np.exp(-1j*np.dot(qvecs, r[i]))
pat = np.abs(amps.reshape(nq, nq))**2
plt.figure()
plt.imshow(pat, interpolation='none', extent=[-qmax, qmax, -qmax, qmax])
plt.title('$I(q)$ (CPU)')
plt.xlabel('$q_x$')
plt.ylabel('$q_y$')
plt.grid()
plt.plot(0, 0, '.k')
# We can make these simulations more realistic by adding in some atomic form factors.  With these form factors included
# we have the formula
#
# I(q) = J_0 r_e^2 P(q) dOmega |sum_n int rho_n(r) exp(- i q.(r - r_n)) dr |^2
#      = J_0 r_e^2 P(q) dOmega | f_n(q) exp(i q.r_n) |^2
#
# The example we will use is hydrogen chloride
d = 2.29e-10
r = np.array([[0, 0, 0], [d, 0, 0]])
n_electrons = r.shape[0]
nq = 100
qmax = 2*2*np.pi/d  # Should be two peaks from origin
q = np.linspace(-qmax, qmax, nq)  # No Ewald curvature
qy, qx = np.meshgrid(q, q, indexing="ij")
qvecs = np.zeros([nq**2, 3])
qvecs[:, 0] = qx.ravel()
qvecs[:, 1] = qy.ravel()
qmags = np.sqrt(np.sum(qvecs**2, axis=1))
f = cromer_mann_scattering_factors(qmags, atomic_numbers=[1, 13])
f_he = f[0, :]
f_cl = f[1, :]

# In order to make this simulation more realistic, we can simulate onto an area detector, and with a reasonable
# wavelength.  The effect of "Ewald sphere curvature" will become apparent. First we need the set of scattering
# vectors, which we can get from the reborn tools.  Scattering vectors depend on both geometry and the x-ray source,
# so we will create a beam and a detector geometry:
beam = source.Beam(wavelength=1e-10, polarization_vec=[1, 0, 0], pulse_energy=1e-3, diameter_fwhm=1e-6)
geom = detector.PADGeometry(shape=(100, 100), distance=0.1, pixel_size=200e-6)
q_vecs = geom.q_vecs(beam)


# When a large number of atoms
core = ClCore()
pat = core.phase_factor_qrf(q=qvecs, r=r)
plt.figure()
plt.imshow(np.abs(pat.reshape(nq, nq))**2, interpolation='none', extent=[-qmax, qmax, -qmax, qmax])
plt.title('$I(q)$ (GPU)')
plt.xlabel('$q_x$')
plt.ylabel('$q_y$')
plt.grid()
plt.plot(0, 0, '.k')

#

# Next we want to simulate a detector
pixel_size = 100e-6
p = np.linspace(-nq//2, nq//2)


plt.show()