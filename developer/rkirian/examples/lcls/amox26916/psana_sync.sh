#!/bin/bash
copyfrom=" ../../../../../../reborn/"
copyto="rkirian@psexport.slac.stanford.edu:/reg/d/psdm/cxi/cxily5921/scratch/rkirian/reborn-amox26916"
rsync -arv --exclude doc --exclude tests --exclude '*.so' --exclude '*.mrc' --exclude '.git' --exclude '*.pyc' --exclude '__pycache__' --exclude '.idea' --exclude '*.md5' $copyfrom $copyto