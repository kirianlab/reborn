import sys
import shutil
import numpy as np
import pyqtgraph as pg
import reborn
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.analysis import runstats, saxs
from reborn.const import eV
from reborn.external.pyqtgraph import imview

geom = reborn.detector.cspad_2x2_pad_geometry_list(detector_distance=0.1, binning=4)
beam = reborn.source.Beam(
    photon_energy=9500 * eV, diameter_fwhm=1e-6, pulse_energy=0.05e-4
)
mask = geom.edge_mask()


def delete_checkpoints():
    shutil.rmtree("logs", ignore_errors=True)
    shutil.rmtree("checkpoints", ignore_errors=True)


class Getter(reborn.fileio.getters.FrameGetter):
    def __init__(self, geom=None, beam=None, mask=None, n_frames=1, **kwargs):
        np.random.seed(0)
        self.geom = geom
        self.beam = beam
        self.mask = mask
        self.n_frames = n_frames
        self.none_frames = np.zeros(self.n_frames)
        pat = get_pad_solution_intensity(
            pad_geometry=self.geom, thickness=3e-6, beam=self.beam, poisson=False
        )
        self.pat = geom.concat_data(pat)
        g = kwargs.get("gain", 20)
        self.gain = np.random.normal(loc=g, size=geom.n_pixels, scale=g / 10)
        self.offset = np.random.normal(loc=0, size=geom.n_pixels, scale=g / 10)
        self.enoise = g / 10

    def get_data(self, frame_number=0):
        np.random.seed(frame_number)
        if self.none_frames[frame_number]:
            return None
        pat = (np.random.poisson(self.pat).astype(float) * self.gain) + self.offset
        pat += np.random.normal(loc=self.pat, scale=self.enoise)
        df = reborn.dataframe.DataFrame(
            raw_data=pat, mask=self.mask, pad_geometry=self.geom, beam=self.beam
        )
        return df


n_processes = 8
n_frames = 10000
# delete_checkpoints()
framegetter = Getter(geom=geom, mask=mask, beam=beam, n_frames=n_frames)
histogram_params = dict(bin_min=-40, bin_max=120, n_bins=100, one_photon_peak=20)

config = dict(
    log_file="logs/padstats",
    checkpoint_file="checkpoints/padstats",
    checkpoint_interval=500,
    message_prefix="Test Run -",
    reduce_from_checkpoints=True,
    histogram_params=histogram_params,
)

padstats = runstats.ParallelPADStats(
    framegetter=framegetter, n_processes=n_processes, **config
)
padstats.process_frames()
stats = padstats.to_dict()
# geom = stats["pad_geometry"]
# beam = stats["beam"]
print("view_padstats")
runstats.view_padstats(stats, histogram=True, show_corrected_histogram=True)

# hist = runstats.PixelHistogram(**stats["histogram_params"])
# hist.histogram = stats["histogram"].astype(float)
# qb = hist.convert_to_q_histogram(
#     pad_geometry=geom, n_q_bins=100, q_range=[0, 3e10], beam=beam, normalized=True
# )
# imview(qb, hold=True)
# pg.mkQApp().exec_()

# print("moving on")
#
# from reborn.analysis.saxs import QHistogram
#
# qhist = QHistogram(
#     n_q_bins=100,
#     pad_geometry=geom,
#     beam=beam,
#     bin_min=stats["histogram_params"]["bin_min"],
#     bin_max=stats["histogram_params"]["bin_max"],
#     n_bins=stats["histogram_params"]["n_bins"],
# )
# qhist.process_framegetter(framegetter=framegetter)
# imv = imview(
#     qhist.get_histogram(),
#     hold=True,
#     fs_label="ADU",
#     ss_label="q",
#     ss_lims=(qhist.q_min, qhist.q_max),
#     fs_lims=(stats["histogram_params"]["bin_min"], stats["histogram_params"]["bin_max"]),
# )
#
# print("end")
# sys.exit()
