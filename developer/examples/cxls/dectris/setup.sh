#!bin/bash

# Some notes from Zachary Brown:
#
# Basic python zeromq stream reader:
# > python DEigerStream -i 10.139.1.5 -p 9999 -f junk.log
#
#Just in case it was not clear, I wanted to summarize getting the detector up and running again, and the sequence (and commands) to take an image after the power outage:
#
#Hardware:
#
#    Verify that the chiller is running and set to 22*C
#    Verify that the dry air is flowing (just enough that you can feel it on your lip should be plenty)
#    Verify that the blue LED on the power brick is lit
#    Verify that the detector's POWER switch is depressed
#    Verify that the DCU is powered on and booted, and that the iDRAC LED or display panel do not show any errors
#
# 
#Software:
#
#    Initialize the detector: 
#        curl -X PUT 10.139.1.5/detector/api/1.8.0/command/initialize
#    Enable FileWriter or Stream (or both): 
#        curl -X PUT 10.139.1.5/filewriter/api/1.8.0/config/mode -H "Content-Type:application/json" -d '{"value":"enabled"}'
#    Arm the detector: 
#        curl -X PUT 10.139.1.5/detector/api/1.8.0/command/arm
#    Trigger the detector:  
#         curl -X PUT 10.139.1.5/detector/api/1.8.0/command/trigger
#
#
#Options:
#
#    change count time (eyes open time): 
#        curl -X PUT 10.139.1.5/detector/api/1.8.0/config/count_time -H "Content-Type:application/json" -d '{"value":10}'
#    change framerate: 
#        curl -X PUT 10.139.1.5/detector/api/1.8.0/config/frame_time -H "Content-Type:application/json" -d '{"value":10}'
#    change the images per sequence:  
#         curl -X PUT 10.139.1.5/detector/api/1.8.0/config/nimages -H "Content-Type:application/json" -d '{"value":100}'
#
#
#
##param=$1
##value=$2
#




conda activate reborn
export PYTHONPATH=/home/labuser/Projects/Dectris/reborn



alias initialize="curl -X PUT 10.139.1.5/detector/api/1.8.0/command/initialize"
alias disarm="curl -X PUT 10.139.1.5/detector/api/1.8.0/command/disarm"
alias arm="curl -X PUT 10.139.1.5/detector/api/1.8.0/command/arm"
alias trigger="curl -X PUT 10.139.1.5/detector/api/1.8.0/command/trigger"

enable_monitor () {
curl -X PUT 10.139.1.5/monitor/api/1.8.0/config/mode -H "Content-Type:application/json" -d '{"value":"enabled"}'
}

enable_filewriter () {
curl -X PUT 10.139.1.5/filewriter/api/1.8.0/config/mode -H "Content-Type:application/json" -d '{"value":"enabled"}'
}

enable_stream () {
curl -X PUT 10.139.1.5/stream/api/1.8.0/config/mode -H "Content-Type:application/json" -d '{"value":"enabled"}'
}

# How many images to collect in the series 
nimages () {
curl -X PUT 10.139.1.5/detector/api/1.8.0/config/nimages -H "Content-Type:application/json" -d "{\"value\":${1}}"
}

ntrigger () {
curl -X PUT 10.139.1.5/detector/api/1.8.0/config/ntrigger -H "Content-Type:application/json" -d "{\"value\":${1}}"
}

# Time between readouts or inverse of collection rate
frame_time () {
curl -X PUT 10.139.1.5/detector/api/1.8.0/config/frame_time -H "Content-Type:application/json" -d "{\"value\":${1}}"
}

# Exposure time
count_time () {
curl -X PUT 10.139.1.5/detector/api/1.8.0/config/count_time -H "Content-Type:application/json" -d "{\"value\":${1}}"
}

# Options are:  ????
trigger_mode () {
curl -X PUT 10.139.1.5/detector/api/1.8.0/config/trigger_mode -H "Content-Type:application/json" -d "{\"value\":\"${1}\"}"
}



alias get_count_time="curl -X GET 10.139.1.5/detector/api/1.8.0/config/count_time"
alias get_frame_time="curl -X GET 10.139.1.5/detector/api/1.8.0/config/frame_time"
alias get_nimages="curl -X GET 10.139.1.5/detector/api/1.8.0/config/nimages"

check_settings () {
get_count_time
get_frame_time
get_nimages
}


status () {
curl -X GET 10.139.1.5/detector/api/1.8.0/status/state/configure
}


#alias change_value="curl -X PUT 10.139.1.5/detector/api/1.8.0/config/${param}" -H "Content-Type:application/json" -d '{"value":${value}"}'
#echo $change_value
