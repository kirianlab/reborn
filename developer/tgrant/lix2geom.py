import sys, argparse
import pickle, bz2
#run this script from within software/reborn/developer/tgrant directory
sys.path.append("../..")
import numpy as np
import hdf5plugin
import h5py
import json

import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets

from reborn import utils
from reborn import detector, source, dataframe
from reborn.simulate import solutions
from reborn.viewers.qtviews import PADView
from reborn.fileio.getters import FrameGetter
from reborn.analysis import saxs

"""Lix Paper on detectors:
https://journals.iucr.org/s/issues/2013/02/00/co5025/index.html
where the python software and setup is discussed but is actually for X9 beamline.
A lot of the geometry calculations done here:
https://github.com/NSLS-II-LIX/py4xs/blob/master/py4xs/exp_para.py
"""

def _fit_by_least_squares(radial, vectors, nmin=None,nmax=None):
    # This function fits a set of linearly combined vectors to a radial profile,
    # using a least-squares-based approach. The fit only takes into account the
    # range of radial bins defined by the xmin and xmax arguments.
    if nmin is None:
        nmin = 0
    if nmax is None:
        nmax = len(radial)
    a = np.nan_to_num(np.atleast_2d(vectors).T)
    b = np.nan_to_num(radial)
    a = a[nmin:nmax]
    b = b[nmin:nmax]
    coefficients, _, _, _ = np.linalg.lstsq(a, b, rcond=None)
    # coefficients, _ = optimize.nnls(a, b)
    return coefficients

#adapted from Lin's py4xs/exp_para.py file
def RotationMatrix(axis, angle):
    if axis=='x' or axis=='X':
        rot = np.asarray(
            [[1., 0., 0.],
             [0., np.cos(angle), -np.sin(angle)],
             [0., np.sin(angle),  np.cos(angle)]])
    elif axis=='y' or axis=='Y':
        rot = np.asarray(
            [[ np.cos(angle), 0., np.sin(angle)],
             [0., 1., 0.],
             [-np.sin(angle), 0., np.cos(angle)]])
    elif axis=='z' or axis=='Z':
        rot = np.asarray(
            [[np.cos(angle), -np.sin(angle), 0.],
             [np.sin(angle),  np.cos(angle), 0.],
             [0., 0., 1.]])
    else:
        raise ValueError('unknown axis %s' % axis)
    
    return rot 

def calc_rot_matrix(det_orient, det_tilt, det_phi):
    tm1 = RotationMatrix('z', np.radians(-det_orient))
    tm2 = RotationMatrix('y', np.radians(det_tilt))
    tm3 = RotationMatrix('z', np.radians(det_orient+det_phi))
    rot_matrix = np.dot(np.dot(tm3, tm2), tm1)

    return rot_matrix

def calc_detector_distance(ratioDw, ImageWidth, rot_matrix, pixel_size):
    Dd = ratioDw*ImageWidth*np.dot(np.dot(rot_matrix, np.asarray([0, 0, 1.])), 
                                                      np.asarray([0, 0, 1.])) 
    return Dd * pixel_size

def read_mixed_bytes(data):
    """
    Attempts to read a bytes string containing text and NumPy arrays.

    Args:
      data: The bytes string to be inspected.

    Returns:
      A dictionary containing:
          - 'text': The extracted text data (if any).
          - 'arrays': A list of extracted NumPy arrays (if any).
          - 'errors': A list of encountered errors (if any).
    """
    text = ""
    arrays = []
    errors = []

    # Split the data by null terminators (common for text separation)
    parts = data.split(b'\x00')

    for part in parts:
        try:
            # Attempt decoding as text (adjust encoding as needed)
            if not part:
                continue
            text_candidate = part.decode('utf-8')
            text += text_candidate
        except UnicodeDecodeError:
            # If decoding fails, assume binary data and try NumPy conversion
            try:
                array = np.frombuffer(part, dtype=np.float64)  # Adjust dtype as needed
                arrays.append(array)
            except ValueError:
                errors.append(f"Failed to decode or convert part: {part}")

    return {"text": text, "arrays": arrays, "errors": errors}

#make a reborn geometry from a lix exp.h5 file
def lix_exph5_to_geom_beam(exp_h5_file_name=None):
    exp_h5_file = h5py.File(exp_h5_file_name, 'r')
    tmp = exp_h5_file.attrs['detectors']
    #formatted as string that looks like list of dictionaries. reformat
    #as list of dictionaries using json loads
    detectors = json.loads(tmp)
    h5_attr_saxs_dict = dict(detectors[0])
    h5_attr_waxs_dict = dict(detectors[1])
    h5_qgrid = exp_h5_file.attrs['qgrid']
    saxs_pad_shape = (h5_attr_saxs_dict['ImageWidth'], h5_attr_saxs_dict['ImageHeight'])
    waxs_pad_shape = (h5_attr_waxs_dict['ImageWidth'], h5_attr_waxs_dict['ImageHeight'])
    pixel_size = 0.172e-3  # 172 um
    # saxs_detector_distance = 1.5 # 1m guess
    # waxs_detector_distance = 0.2 #  guess
    saxs_rot_matrix = calc_rot_matrix(
        det_orient = h5_attr_saxs_dict['exp_para']['det_orient'], 
        det_tilt = h5_attr_saxs_dict['exp_para']['det_tilt'], 
        det_phi = h5_attr_saxs_dict['exp_para']['det_phi'])
    waxs_rot_matrix = calc_rot_matrix(
        det_orient = h5_attr_waxs_dict['exp_para']['det_orient'], 
        det_tilt = h5_attr_waxs_dict['exp_para']['det_tilt'], 
        det_phi = h5_attr_waxs_dict['exp_para']['det_phi'])
    saxs_detector_distance = calc_detector_distance(
        ratioDw = h5_attr_saxs_dict['exp_para']['ratioDw'], 
        ImageWidth = h5_attr_saxs_dict['ImageWidth'], 
        rot_matrix = saxs_rot_matrix,
        pixel_size = pixel_size)
    waxs_detector_distance = calc_detector_distance(
        ratioDw = h5_attr_waxs_dict['exp_para']['ratioDw'], 
        ImageWidth = h5_attr_waxs_dict['ImageWidth'], 
        rot_matrix = waxs_rot_matrix,
        pixel_size = pixel_size)
    saxs_beam_center_x = h5_attr_saxs_dict['exp_para']['bm_ctr_x'] #units?
    saxs_beam_center_y = h5_attr_saxs_dict['exp_para']['bm_ctr_y']
    saxs_beam_center = np.array([saxs_beam_center_x, saxs_beam_center_y], dtype=np.double)
    waxs_beam_center_x = h5_attr_waxs_dict['exp_para']['bm_ctr_x'] #units?
    waxs_beam_center_y = h5_attr_waxs_dict['exp_para']['bm_ctr_y']
    waxs_beam_center = np.array([waxs_beam_center_x, waxs_beam_center_y], dtype=np.double)
    geom_saxs = detector.tiled_pad_geometry_list(
        pad_shape=saxs_pad_shape, 
        pixel_size=pixel_size, 
        distance=saxs_detector_distance, 
        tiling_shape=(1,1))
    geom_waxs = detector.tiled_pad_geometry_list(
        pad_shape=waxs_pad_shape, 
        pixel_size=pixel_size, 
        distance=waxs_detector_distance, 
        tiling_shape=(1,1))
    geom_saxs[0].t_vec[0] = -saxs_beam_center[1]*pixel_size
    geom_saxs[0].t_vec[1] = -saxs_beam_center[0]*pixel_size
    geom_waxs[0].t_vec[0] = -waxs_beam_center[1]*pixel_size
    geom_waxs[0].t_vec[1] = -waxs_beam_center[0]*pixel_size
    #merge detectors into one pad list
    geom = geom_waxs + geom_saxs
    geom = detector.PADGeometryList(geom)

    #load mask from lix file
    # mask = None
    # if mask is None:
    #     mask = geom.ones().astype(float)

    # mask_bytes = bz2.decompress(bytes(h5_attr_saxs_dict['exp_para']['mask']))
    # data = read_mixed_bytes(mask_bytes)
    # print(data)
    # h5_saxs_mask_idx = bz2.decompress(bytes(h5_attr_saxs_dict['exp_para']['mask'])))
    # print(h5_saxs_mask_idx)
    # mask[h5_saxs_mask_idx] = 0

    #make a beam
    wavelength = h5_attr_saxs_dict['exp_para']['wavelength']*1e-10
    #add polarization to the beam
    # beam_vec = np.array([])
    # e1_vec = utils.vec_norm(beam_vec)
    beam = source.Beam(wavelength=wavelength)


    return geom, beam, geom_saxs, geom_waxs


#Our FrameGetter for LiX hdf5 files
class LiXFrameGetter(FrameGetter):
    def __init__(self, pad_geometry, beam, data_h5_file_name=None, data_h5_dataset_root=None):
        super().__init__()  # Don't forget to initialize the base FrameGetter class.
        self.pad_geometry = pad_geometry
        self.beam = beam
        # self.n_frames = n_frames  # This is important; you must configure the number of frames.
        self.sa = pad_geometry.solid_angles()
        #metadata about experiment
        # self.load_exp_h5_file(exp_h5_file_name)
        #the actual data
        self.load_data_h5_file(data_h5_file_name, data_h5_dataset_root) #this also sets n_frames

    def load_data_h5_file(self, data_h5_file_name, data_h5_dataset_root):
        self.data_h5_file = h5py.File(data_h5_file_name, 'r')
        self.data_h5_saxs = self.data_h5_file[data_h5_dataset_root+'/primary/data/pil1M_image']
        self.data_h5_waxs = self.data_h5_file[data_h5_dataset_root+'/primary/data/pilW2_image']
        self.n_frames = self.data_h5_saxs.shape[0]

    def get_data(self, frame_number):
        df = dataframe.DataFrame()  # This creates a DataFrame instance, which combines PADGeometry with Beam and data.
        df.set_frame_index(self.current_frame)  # Not mandatory, but good practice.
        df.set_frame_id('Data %s' % frame_number)  # Not mandatory, but good practice.
        df.set_pad_geometry(self.pad_geometry)
        saxs_intensity = self.data_h5_saxs[frame_number][:,::-1].astype(np.double)
        waxs_intensity = self.data_h5_waxs[frame_number][:,:].T.astype(np.double)
        #merge the two detectors
        intensity = self.pad_geometry.concat_data([waxs_intensity, saxs_intensity])
        intensity /= self.sa*1e6
        df.set_raw_data(intensity)
        df.set_beam(self.beam)
        df.validate()  # Not mandatory, but good practice.
        return df

#Our FrameGetter for LiX hdf5 files
class LiXWAXSFrameGetter(FrameGetter):
    def __init__(self, pad_geometry, beam, data_h5_file_name=None, data_h5_dataset_root=None):
        super().__init__()  # Don't forget to initialize the base FrameGetter class.
        self.pad_geometry = pad_geometry
        self.beam = beam
        # self.n_frames = n_frames  # This is important; you must configure the number of frames.
        self.sa = pad_geometry.solid_angles()
        #metadata about experiment
        # self.load_exp_h5_file(exp_h5_file_name)
        #the actual data
        self.load_data_h5_file(data_h5_file_name, data_h5_dataset_root) #this also sets n_frames

    def load_data_h5_file(self, data_h5_file_name, data_h5_dataset_root):
        self.data_h5_file = h5py.File(data_h5_file_name, 'r')
        # self.data_h5_saxs = self.data_h5_file[data_h5_dataset_root+'/primary/data/pil1M_image']
        self.data_h5_waxs = self.data_h5_file[data_h5_dataset_root+'/primary/data/pilW2_image']
        self.n_frames = self.data_h5_waxs.shape[0]

    def get_data(self, frame_number):
        df = dataframe.DataFrame()  # This creates a DataFrame instance, which combines PADGeometry with Beam and data.
        df.set_frame_index(self.current_frame)  # Not mandatory, but good practice.
        df.set_frame_id('Data %s' % frame_number)  # Not mandatory, but good practice.
        df.set_pad_geometry(self.pad_geometry)
        # saxs_intensity = self.data_h5_saxs[frame_number][:,::-1].astype(np.double)
        waxs_intensity = self.data_h5_waxs[frame_number][:,:].T.astype(np.double)
        #merge the two detectors
        intensity = self.pad_geometry.concat_data([waxs_intensity])
        intensity /= self.sa*1e6
        df.set_raw_data(intensity)
        df.set_beam(self.beam)
        df.validate()  # Not mandatory, but good practice.
        return df

    def set_mask(self, mask):
        self._mask = self.pad_geometry.concat_data(mask)

    def get_mask_flat(self, copy=True):
        r"""Get the mask as a contiguous 1D array, with all PADs concatenated."""
        if self._mask is None:
            self.set_mask(np.ones(self.get_raw_data_flat().shape, dtype=int))
        m = self._mask.ravel()
        if copy:
            m = m.copy()
        return m

#Our FrameGetter for LiX hdf5 files
class LiXSAXSFrameGetter(FrameGetter):
    def __init__(self, pad_geometry, beam, data_h5_file_name=None, data_h5_dataset_root=None):
        super().__init__()  # Don't forget to initialize the base FrameGetter class.
        self.pad_geometry = pad_geometry
        self.beam = beam
        # self.n_frames = n_frames  # This is important; you must configure the number of frames.
        self.sa = pad_geometry.solid_angles()
        #metadata about experiment
        # self.load_exp_h5_file(exp_h5_file_name)
        #the actual data
        self.load_data_h5_file(data_h5_file_name, data_h5_dataset_root) #this also sets n_frames

    def load_data_h5_file(self, data_h5_file_name, data_h5_dataset_root):
        self.data_h5_file = h5py.File(data_h5_file_name, 'r')
        self.data_h5_saxs = self.data_h5_file[data_h5_dataset_root+'/primary/data/pil1M_image']
        # self.data_h5_waxs = self.data_h5_file[data_h5_dataset_root+'/primary/data/pilW2_image']
        self.n_frames = self.data_h5_saxs.shape[0]

    def get_data(self, frame_number):
        df = dataframe.DataFrame()  # This creates a DataFrame instance, which combines PADGeometry with Beam and data.
        df.set_frame_index(self.current_frame)  # Not mandatory, but good practice.
        df.set_frame_id('Data %s' % frame_number)  # Not mandatory, but good practice.
        df.set_pad_geometry(self.pad_geometry)
        saxs_intensity = self.data_h5_saxs[frame_number][:,::-1].astype(np.double)
        # waxs_intensity = self.data_h5_waxs[frame_number][:,:].T.astype(np.double)
        #merge the two detectors
        intensity = self.pad_geometry.concat_data([saxs_intensity])
        intensity /= self.sa*1e6
        df.set_raw_data(intensity)
        df.set_beam(self.beam)
        df.validate()  # Not mandatory, but good practice.
        return df

    def set_mask(self, mask):
        self._mask = self.pad_geometry.concat_data(mask)

    def get_mask_flat(self, copy=True):
        r"""Get the mask as a contiguous 1D array, with all PADs concatenated."""
        if self._mask is None:
            self.set_mask(np.ones(self.get_raw_data_flat().shape, dtype=int))
        m = self._mask.ravel()
        if copy:
            m = m.copy()
        return m

def collect_radials(framegetter, qmax=None, nradials=None, frames=None, mask=None):
    """Create a stack of 1D radial profiles from a reborn framegetter."""
    df = framegetter.get_frame(0)
    if mask == None:
        mask = framegetter.get_mask_flat()
    print(len(mask), np.sum(mask))
    geom = df.get_pad_geometry()
    beam = df.get_beam()
    q_mags = df.q_mags
    if qmax is None:
        qmax = np.max(q_mags)
    profiler = saxs.RadialProfiler(mask=mask, beam=beam, pad_geometry=geom, n_bins=args.nq, q_range=np.array([0, qmax]))
    q = profiler.q_bin_centers
    if nradials is None and frames is None:
        frames = range(framegetter.n_frames)
    elif frames is None:
        frames = range(framegetter.n_frames)
    radials = []
    errors = []
    for i in frames:
        print(i)
        df = framegetter.get_frame(i)
        df.set_mask(mask)
        stats = profiler.quickstats(df.get_raw_data_flat(), weights=geom.solid_angles()*mask)
        radial_mean = stats['mean']
        radials.append(radial_mean)
        radial_std = stats['sdev']
        errors.append(radial_std)
    radials = np.array(radials)
    errors = np.array(errors)

    return q, radials, errors


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A tool for using reborn to read LiX beamline SWAXS data.", formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-f", "--file", type=str, help="LiX hdf5 filename (.h5)")
    parser.add_argument("-d", "--dataset", type=str, help="Root of the HDF5 dataset name (typically the specific sample name, e.g.: apo_cyp1mg_1 or buffer9).")
    parser.add_argument("--saxs_mask", default=None,type=str, help="Filename of reborn mask for SAXS detector (required).")
    parser.add_argument("--waxs_mask", default=None,type=str, help="Filename of reborn mask for WAXS detector (required).")
    parser.add_argument("--nq", default=3000, type=int, help="Number of q values in radial profile (optional, default=3.5).")
    parser.add_argument("--qmax", default=None, type=float, help="Maximum q value in inverse angstroms of radial profile (optional, default=detector corner).")
    parser.add_argument("-o", "--output", default=None, help="Output filename prefix")
    parser.set_defaults(plot=True)
    args = parser.parse_args()

    #Example usage: python lix2geom.py -f /Users/tgrant/Projects/SWAXS_Data/Jul2023_LiX/311750b/plate3_H01a.h5 -d apo_cyp1mg_1 --saxs_mask Jul2023_LiX_saxs2.mask --waxs_mask Jul2023_LiX_waxs2.mask --nq 3001 --qmax 3.5

    if args.qmax is not None:
        args.qmax *= 1e10

    geom, beam, geom_saxs, geom_waxs = lix_exph5_to_geom_beam(args.file)
    if args.saxs_mask is not None:
        saxs_mask = detector.load_pad_masks(args.saxs_mask)
    if args.waxs_mask is not None:
        waxs_mask = detector.load_pad_masks(args.waxs_mask)

    my_saxs_framegetter = LiXSAXSFrameGetter(pad_geometry=geom_saxs, beam=beam, 
        data_h5_file_name=args.file, data_h5_dataset_root=args.dataset)
    my_waxs_framegetter = LiXWAXSFrameGetter(pad_geometry=geom_waxs, beam=beam, 
        data_h5_file_name=args.file, data_h5_dataset_root=args.dataset)

    my_saxs_framegetter.set_mask(saxs_mask)
    my_waxs_framegetter.set_mask(waxs_mask)

    nradials = None
    frames = None
    q_saxs, saxs_radials, saxs_errors = collect_radials(my_saxs_framegetter, qmax=args.qmax, nradials=nradials, frames=frames)
    q_waxs, waxs_radials, waxs_errors = collect_radials(my_waxs_framegetter, qmax=args.qmax, nradials=nradials, frames=frames)

    saxs_avg_radial = np.mean(saxs_radials,0)
    waxs_avg_radial = np.mean(waxs_radials,0)

    #scale saxs to waxs in the region of overlap
    saxs_df0 = my_saxs_framegetter.get_frame(0)
    waxs_df0 = my_waxs_framegetter.get_frame(0)
    q_overlap_min = 0.22e10 #waxs_df0.q_mags.min()
    q_overlap_max = 0.26e10 #saxs_df0.q_mags.max()
    q = q_saxs
    idx = np.where((q>=q_overlap_min)&(q<=q_overlap_max))[0]
    sf = _fit_by_least_squares(saxs_avg_radial,[waxs_avg_radial],nmin=idx.min(),nmax=idx.max())

    saxs_qvecs = saxs_df0.q_vecs
    print(saxs_qvecs.shape)

    pw = pg.plot(title="Radials")
    pw.plot(q_saxs,saxs_avg_radial/sf, pen='c')
    pw.plot(q_waxs,waxs_avg_radial, pen='w')

    pv = PADView(frame_getter=my_waxs_framegetter)
    pv.start()




























