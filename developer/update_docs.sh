#!/bin/bash
# Note: when documentation is created and pushed to the master branch of the gitlab repository,
# this script will run automatically and documentation will be made available here:
# https://rkirian.gitlab.io/reborn

# Use the additional arguments to speed thins up:
# nodoctest: Skip doctests (i.e. tests placed directly in the docstrings)
# nohtml: Skip the creation of html pages (this also skips sphinx-gallery examples -- I don't know how to decouple them...)
# nolatex: Skip the creation of latex documents

if [[ ! $(basename "$(pwd)") = 'developer' ]]; then
    echo 'This script should run in the developer directory.'
    exit
fi

reborn=$(cd ../reborn; pwd)
doc=$(cd ../doc; pwd)
export PYTHONPATH="$(cd ..; pwd):$doc/source:$doc:$PYTHONPATH"
#if [[ $(conda env list) =~ " reborn " ]]; then
#conda activate reborn
#echo "reborn conda environment activated"
#else
#echo "reborn conda environment was not found"
#fi
python -c 'from reborn import fortran; print("reborn.fortran compiled successfully.")'
python -c 'from reborn.simulate.clcore import ClCore; core = ClCore(); print("reborn.simulate.clcore.ClCore() instantiated successfully.")'

echo '================= sphinx-apidoc =================='
cd "$doc"
#[ -d "$doc/source/api" ] && rm -r "$doc/source/api"
#mkdir "$doc/source/api"
#[ -d "$doc/source/auto_examples" ] && rm -r "$doc/source/auto_examples"
#mkdir "$doc/source/auto_examples"
echo "running sphinx-apidoc in $(pwd)"
sphinx-apidoc --maxdepth 10 --output-dir "$doc/source/api" "$reborn" \
 "$reborn/math" "$reborn/data" "$reborn/fortran" "$reborn/simulate/atoms.py" "$reborn/simulate/numbacore.py"
# FIXME: How do we properly change the title of the auto-generated API page?  Below we do it brute-force...
tail -n+3 "$doc/source/api/modules.rst" > tmp.rst
echo 'Package API' > "$doc/source/api/modules.rst"
echo '===========' >> "$doc/source/api/modules.rst"
cat tmp.rst >> "$doc/source/api/modules.rst"
rm tmp.rst &> /dev/null
echo "done"
sleep 10

echo '=================== doctest ======================='
if [[ " $@ " =~ " nodoctest " ]]; then
echo "Skipping"
else
make doctest
echo "done"
fi

echo '=================== html ======================='
if [[ " $@ " =~ " nohtml " ]]; then
echo "Skipping"
else
python -c 'from reborn import fortran'
# Note that the latex2html global config file l2hconf.pm on your sytem 
# will affect the resulting html files.  One MacOS I had to set the image
# type to something other than SVG.
make html SPHINXOPTS="-v"
cp -r source/files build/html
echo "done"
fi

echo '================= LaTeX =================='
if [[ " $@ " =~ " nolatex " ]]; then
echo "Skipping"
else
cd "$doc/latex/dipole" || exit
make all >> /dev/null
cp -r "$doc/latex/dipole/dipole_html" "$doc/build/html/_static/dipole_html"
cp -r "$doc/latex/dipole/dipole.pdf" "$doc/build/html/_static/files"
echo ""
echo "done"
fi

echo "Cleaning up"
[[ -f "$doc/source/sg_execution_times.rst" ]] && rm $doc/source/sg_execution_times.rst
echo "done"
