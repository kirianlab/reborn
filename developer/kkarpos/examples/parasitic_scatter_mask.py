r"""
Low q Parasitic Scattering Masking Algorithm
=============================================

Masks the low-q parasitic scattering for isotropic diffraction patterns.

Contributed by Kosta Karpos.

Note that this algorthim assumes that your diffraction data is isotropic.

"""

# %%
# First we import the needed modules and configure the simulation parameters:
import numpy as np
from reborn.source import Beam
from reborn.target import atoms
from reborn.simulate.form_factors import sphere_form_factor
from reborn.detector import mpccd_pad_geometry_list, RadialProfiler, PADGeometry
from reborn.viewers.qtviews import PADView
import scipy.constants as const
import scipy


# %%
# Simulate Some Data
# -------------------------
#
# Before running through the algorithm, we need some data
# to run it on. As a simple test case, we will simulate a water 
# pattern and manually add in some "parasitic scattering" to the low-q. 
# 
# The simulation for the water droplet below is an extension of the
# Water droplet in vacuum example developed by Richard Kirian.
#

np.random.seed(0)  # Make random numbers that are reproducible
eV = const.value('electron volt')
r_e = const.value('classical electron radius')
NA = const.value('Avogadro constant')
h = const.h
c = const.c
water_density = 1000  # SI units, like everything else in reborn!
photon_energy = 2000*eV
detector_distance = .5
pulse_energy = 1e-3
drop_radius = 20e-9
wavelength = h*c/photon_energy



pads = mpccd_pad_geometry_list(detector_distance=detector_distance, binning=10)
beam = Beam(wavelength=wavelength, pulse_energy=pulse_energy)
fluence = beam.photon_number_fluence


ref_idx = atoms.xraylib_refractive_index(compound='H2O', density=1000, beam=beam)

fdens = (1 - ref_idx)*2*np.pi/wavelength**2  # Scattering density
intensities = []  # We loop over all detector panels
for i in range(len(pads)):
    q_mags = pads[i].q_mags(beam=beam)
    solid_angles = pads[i].solid_angles()
    polarization_factors = pads[i].polarization_factors(beam=beam)
    amps = fdens*sphere_form_factor(radius=drop_radius, q_mags=q_mags)
    d = np.abs(amps)**2*solid_angles*polarization_factors*fluence
    d = np.random.poisson(d)  # Add some Poisson noise
    intensities.append(pads[i].reshape(d))


dispim = [np.log10(a + 1) for a in intensities]
dispim = pads.concat_data(dispim)
pv = PADView(data=dispim, pad_geometry=pads)
pv.start()



# %%
# Add in Parasitic Scattering
# -----------------------------
#
# Manually add in parasitic scattering to the low-q region.

# slits = np.zeros

data = dispim.copy()

ps = pads.ones()
q_mags = pads.q_mags(beam=beam)
profiler = RadialProfiler(pad_geometry=pads, beam=beam, n_bins=1000)
bin_edges = profiler.bin_edges
qidx = np.where(q_mags <= bin_edges[80])



ps[qidx[0][10:20]] *= data[qidx[0][10:20]] * 1.2
# ps[qidx[0][10:20]] *= data[qidx[0][10:20]] * 1.2

# ps[qidx[0][200:230]] *= data[qidx[0][200:230]] * 1.2


data *= ps

pv = PADView(data=data, pad_geometry=pads)
pv.start()

# angle = np.arctan(config['kapton_window_diameter'] / (2 * config['rayonix_vacuum_tube_length']))
# rayonix_geom.mask[rayonix_geom.pad_geometry.scattering_angles(beam=xrd.beam) > angle] = 0
# epix_geom.mask[epix_pads.scattering_angles(beam=xrd.beam) < config['epix_plastic_hole_mask_angle']] = 0



# slits = pads.zeros()
# sidx = np.where(pads.scattering_angles(beam=beam) < 0.005)
# slits[sidx] = 1


# # sfft = np.real(scipy.fft.fft(slits))


# pv = PADView(data=slits, pad_geometry=pads)
# pv.start()

