import numpy as np
import pylab as plt
import pandas as pd
import reborn
import os, sys
import time

from scipy.signal import argrelextrema

from simulate import Simulator, shannon_s, n_shots, snr
from reborn.detector import rayonix_mx340_xfel_pad_geometry_list
from reborn.source import Beam
from reborn.const import eV
from reborn.viewers.qtviews import PADView
plt.rcParams['font.family'] = 'Times New Roman'


r'''
    Simulates a jet in vacuum using rhodopsin
'''

# These three are the cis vs trans rhodopsin profiles from Tom
diff_s = "denss_data/cis_v_trans/sim_1-18.3_light_adjusted_0.2-0.0ps_diff.pdb2sas.dat"
s1_s = "denss_data/cis_v_trans/sim_1-18.3_light_0.0ps_adjusted.pdb2sas.dat"
s2_s = "denss_data/cis_v_trans/sim_1-18.3_light_0.2ps_adjusted.pdb2sas.dat"

config = {'detector_distance': 1, # m
            'photon_energy': 9.5e3 * eV, # 8e3 * eV, 
            'sample_delivery_method': "jet", # jet or droplets
            'sample_path_length': 5e-6, #50-6, #300e-6, #5e-6, # m
            'collection_time': 1.5, # data collection time in hours
            'laser_rep_rate': 1000, # laser rep rate in Hz
            'add_gas': True,    # Flip this if you want to add helium into the mix
            'gas_path': [-0.1,0.9],
            'pdb_id':  "1JFP", #"2VEA"
            'concentration': 10, #10, # mg/ml, the sample concentration
            'binned_pixels': 10, # The detector pixel binning, 1 is no binning
            'random_seed': 1,    # The random seed for Poisson noise
            'header_level': 1, # DENSS file sometimes have headers
            'poisson_noise': False,
            'gas_pressure': 1*101325, #101325 = 1atm
            'gas_temperature': 273.15,
            'gas_type': ['he', 'air', 'n2', 'o2', 'humid_he', 'humid_air', 'he_air'],
            'shannon_s': 2,
            'dmax': 100, # Angstroms
            }

pulse_energy = config['photon_energy'] * 1e8
config['n_shots'] = n_shots(config['laser_rep_rate'], 3600 * config['collection_time'])

sample_pl_in_um = config['sample_path_length'] * 1e6
title_sem = f"""Difference Profile,  N Shots Comparison
            Detector Distance = {config['detector_distance']}m, Photon Energy = {config['photon_energy']/(1000 * eV)} keV
            Sample Path Length: {sample_pl_in_um}$\mu$m, Medium: Vacuum, Rep Rate: {config['laser_rep_rate']}Hz
        """

ds = np.loadtxt(diff_s, skiprows=0)
ss1 = np.loadtxt(s1_s, skiprows=1)
ss2 = np.loadtxt(s2_s, skiprows=1)

# Set up the detector and beam
pads = rayonix_mx340_xfel_pad_geometry_list(detector_distance=config['detector_distance'])
pads = pads.binned(config['binned_pixels'])
beam = Beam(pulse_energy=pulse_energy)
config['n_radial_bins'] = int(np.max(pads.q_mags(beam))*1e-10/shannon_s(s=config['shannon_s'], d=config['dmax']))
qrange = np.linspace(0, np.max(pads.q_mags(beam)), config['n_radial_bins']) * 1e-10


gasses = ['He', 'Air', 'N2', 'O2', 'Humid Helium', 'Humid Air', 'Helium + Air']
colors = ['purple', 'blue', 'orange', 'green', 'cyan', 'red', 'black']
fig, ax = plt.subplots(figsize=(8,5))

# for i in range.... loop over the gas type. 
for i, gas in enumerate(config['gas_type']):
    print(gas)

    # Set up the simulator class for the two states
    fg = Simulator(pad_geometry=pads, beam=beam, denss_data=s1_s, pdb=config['pdb_id'],
                        n_radial_bins=config['n_radial_bins'], poisson_noise=config['poisson_noise'],
                        sample_delivery_method=config['sample_delivery_method'],
                        sample_path_length=config['sample_path_length'],
                        random_seed=None, header_level=config['header_level'],
                        gas_background=config['add_gas'], gas_path=config['gas_path'],
                        gas_pressure=config['gas_pressure'], 
                        gas_temperature=config['gas_temperature'],
                        gas_type=gas)

    fg_s2 = Simulator(pad_geometry=pads, beam=beam, denss_data=s2_s, pdb=config['pdb_id'],
                        n_radial_bins=config['n_radial_bins'], poisson_noise=config['poisson_noise'],
                        sample_delivery_method=config['sample_delivery_method'],
                        sample_path_length=config['sample_path_length'],
                        random_seed=None, header_level=config['header_level'],
                        gas_background=config['add_gas'], gas_path=config['gas_path'],
                        gas_pressure=config['gas_pressure'], 
                        gas_temperature=config['gas_temperature'],
                        gas_type=gas)

    # Grab the radial profiles, no weights
    rad_s1 = fg.get_radial(0)
    rad_s2 = fg_s2.get_radial(0)
    rad_s1_w = fg.get_radial(0, weights=fg.f2phot)
    rad_s2_w = fg_s2.get_radial(0, weights=fg_s2.f2phot)

    # Gather the quantities needed for the SNR calculations
    sum_diff = rad_s1['sum'] - rad_s2['sum']    # The differences of the summed profiles
    prof_sum = rad_s1['sum'] + rad_s2['sum']    # The total state 1 + state 2 profile

    mean_diff = rad_s1_w['mean'] - rad_s2_w['mean']

    tdiff = ss1[:,1] - ss2[:,1]
    tqr = ss1[:,0]
    tdiff *= np.max(np.abs(mean_diff)) / np.max(np.abs(tdiff))

    err_diff = {'err': mean_diff / snr(sum_diff, prof_sum, config['n_shots']),
                    'snr': np.abs(snr(sum_diff, prof_sum, config['n_shots'])),
                    'n_shots': config['n_shots']}


    ax.plot(qrange, err_diff['snr'], label=f"{gasses[i]}")



ax.legend(loc='best', fontsize=12)
ax.grid(alpha=0.5)
ax.set_xlabel(r"q = 4 $\pi$ $\sin\theta$ / $\lambda$  $[\AA^{-1}]$", fontsize=12)
ax.set_ylabel(r"SNR = $\dfrac{{\Delta|F(q)|^2}}{{\sqrt{{(I_1+I_2) / M}}}}$", fontsize=12)
# ax.set_title(title_sem)
plt.draw()

# plt.figure(figsize=(8,5))
# lim = 0
# colors = [0.2, 0.5, 0.8] 
# for i, err in enumerate(err_diff[:-1]):
#     fill = (mean_diff + err['err'], mean_diff - err['err'])
#     plt.fill_between(qrange, fill[0], fill[1], 
#                           color='gray', edgecolor='gray', alpha=colors[i],
#                           label=f"Error for {config['collection_time'][i]} hours")
# fill = (mean_diff + err_diff[-1]['err'], mean_diff - err_diff[-1]['err'])
# plt.fill_between(qrange, fill[0], fill[1], 
#                       color='lightsteelblue', edgecolor='lightsteelblue', alpha=0.8,
#                       label=f"Error for {config['collection_time'][-1]} hours")

# plt.plot(qrange, mean_diff, '--', color='black', label='Difference Profile')
# plt.plot(tqr, tdiff, '--', color='red', label='Ground Truth (scaled)')
# plt.xlim(qrange[0], 1.2) #qrange[-1])
# plt.ylim(-np.max(mean_diff)*(1-0.9), np.max(mean_diff)*(1+0.35))
# plt.xlabel(r"q = 4 $\pi$ $\sin\theta$ / $\lambda$  $[\AA^{-1}]$", fontsize=12)
# plt.ylabel(r"Difference Profile, $\Delta I(q)$", fontsize=12)
# plt.legend(loc='best', fontsize=12)
# plt.grid(alpha=0.3)
# # plt.title(title_sem, fontsize=12)
# plt.show()




plt.show()











































