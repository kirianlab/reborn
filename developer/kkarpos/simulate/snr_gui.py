import numpy as np
import matplotlib.pyplot as plt
from simulate import Simulator
from reborn.detector import rayonix_mx340_xfel_pad_geometry_list
from reborn.source import Beam
from reborn.const import eV
from reborn.viewers.qtviews import PADView

import sys
from PyQt5 import QtWidgets
import pyqtgraph as pg
from reborn.viewers.qtviews import PADView


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.setWindowTitle("My Simulator")

        # Create the main layout
        layout = QtWidgets.QVBoxLayout()

        # Create a widget to host our plot
        self.plot_widget = pg.PlotWidget()
        layout.addWidget(self.plot_widget)

        self.detector_view1 = PADView()
        self.detector_view2 = PADView()
        layout.addWidget(self.detector_view1)
        layout.addWidget(self.detector_view2)

        # Create a form layout for your simulation parameters
        form_layout = QtWidgets.QFormLayout()

        # Add fields for your simulation parameters
        self.parameter_edit = QtWidgets.QLineEdit()
        form_layout.addRow("Some Parameter:", self.parameter_edit)

        # When you're done adding fields, add the form_layout to the main layout
        layout.addLayout(form_layout)

        # Apply the layout to the QWidget
        widget = QtWidgets.QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())


































