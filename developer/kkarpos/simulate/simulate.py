import numpy as np
import os
from reborn.dataframe import DataFrame
from reborn.analysis.saxs import RadialProfiler
from reborn.fileio.getters import FrameGetter
from reborn.simulate.gas import get_gas_background
from reborn.target import molecule
from reborn.target import crystal
from scipy import constants
from reborn.external import denss
import logging


def get_snr(sum_difference, sum_prof, m_shots):
    r""" Calculates the signal-to-noise ratio of a difference profile as
            SNR = (I1-I2) / sqrt((I1+I2) / M)

            Args:
                sum_difference (np.ndarray): The calculated difference of the sum radials
                sum_prof (np.ndarray): List containing the sum of the profiles used for the differences
                m_shots (int): Number of x-ray exposure events contributing to the pattern
            Returns:
                (np.ndarray): The signal-to-noise ratio
    """
    return sum_difference / np.sqrt(sum_prof / m_shots)


def shannon_s(s=2, d=100):
    # returns shannon sampling ratio s in units of d
    return 2 * np.pi / (s * d)


def n_shots(rep_rate, time):
    # given a laser rep rate and the desired data collection time, will return the number of shots
    # keep units in mind.
    return rep_rate * time


def get_n_particles(concentration,
                    volume,
                    pdb_id: str,
                    random=True,
                    verbose=True):
    """
    Calculate the number of proteins in the sample.

    Args:
        concentration (float): The protein concentration in kg/m^3 (note that mg/ml = kg/m^3)
        volume (float): The volume of the sample in m^3.
        pdb_id (str): The PDB id of the protein.
        random (bool): Whether to add a random factor to the number of proteins.
        verbose (bool): Whether to print details out to the terminal

    Returns:
        int: The number of proteins in the sample.
    """
    pdb = crystal.CrystalStructure(pdb_id, tempdir="tempdir")
    protein_number_density = concentration / pdb.molecule.get_molecular_weight()
    n_proteins = int(protein_number_density * volume)
    if random:
        n_proteins = int(np.random.normal(loc=n_proteins))
    if verbose:
        print("Number of particles: ", n_proteins)
    return n_proteins


def _fetch_pdb(pdb_file):
    if not os.path.exists(pdb_file):
        pdb_file = crystal.get_pdb_file(
            pdb_file, save_path=".", use_cache=False)
    return pdb_file


def _create_molecule_dict():
    molecule_dict = {
        'N2': molecule.Molecule(coordinates=np.array([[0, 0, 0], [0, 0, 1.07e-10]]), atomic_numbers=[7, 7]),
        'O2': molecule.Molecule(coordinates=np.array([[0, 0, 0], [0, 0, 1.21e-10]]), atomic_numbers=[8, 8]),
        'He': molecule.Molecule(coordinates=np.array([[0, 0, 0]]), atomic_numbers=[2, 2])
    }
    return molecule_dict


def _check_gas_path_length(gas_path_length):
    if isinstance(gas_path_length, (int, float)):
        gas_path_length = [0.0, gas_path_length]
    if len(gas_path_length) != 2:
        raise ValueError('The gas path length must be a list of length 2')


def get_protein_profile(pdb_file, q,
                        ignore_waters=False):
    try:
        pq, intensity = denss.get_scattering_profile(pdb_file=_fetch_pdb(pdb_file),
                                                     create_bio_assembly=True,
                                                     q_mags=q,
                                                     ignore_waters=ignore_waters)
    except Exception as e:
        logging.exception("An error occurred when creating the bio assembly. Proceeding without bio assembly.")
        pq, intensity = denss.get_scattering_profile(pdb_file=_fetch_pdb(pdb_file),
                                                     create_bio_assembly=False,
                                                     q_mags=q,
                                                     ignore_waters=ignore_waters)
    intensity = intensity.astype(float)
    return intensity


class Simulator(FrameGetter):
    def __init__(self, pad_geometry, beam, pdb_id, sample_thickness, n_frames=10, **kwargs):
        self.denss_scattering_profile = None
        self.n_frames = n_frames
        self._set_beam(beam)
        self._set_pads(pad_geometry)
        self._set_constants()
        self._set_mask(kwargs)
        self._set_random_noise(kwargs)
        self._set_sample(sample_thickness, pdb_id, kwargs)
        self._set_gas(kwargs)
        self._set_radial_profiler(kwargs)
        self.molecules = _create_molecule_dict()

    def _set_beam(self, beam):
        self.beam = beam
        self.fluence = self.beam.photon_number_fluence
        self.photon_energy = self.beam.photon_energy
        self.beam_diameter = self.beam.diameter_fwhm

    def _set_pads(self, pads):
        self.pad_geometry = pads
        self.q_vecs = self.pad_geometry.q_vecs(beam=self.beam)
        self.q_mags = self.pad_geometry.q_mags(beam=self.beam)
        self.sa = self.pad_geometry.solid_angles()
        self.pol = self.pad_geometry.polarization_factors(beam=self.beam)
        self.f2phot = self.pad_geometry.f2phot(beam=self.beam)

    def _set_constants(self):
        self.r_e = constants.value('classical electron radius')  # meters
        self.kb = constants.value('Boltzmann constant')  # J  K^-1
        self.na = constants.value('Avogadro constant')  # mole^-1
        self.eV = constants.value('electron volt')  # J

    def _set_mask(self, kwargs):
        self.beamstop_diameter = kwargs.get('beamstop_diameter', 0)
        theta = np.arctan(self.beamstop_diameter / self.pad_geometry[0].t_vec[2])
        self.mask = self.pad_geometry.ones()
        self.mask[self.pad_geometry.scattering_angles(self.beam) < theta] = 0

    def _set_random_noise(self, kwargs):
        self.poisson = kwargs.get('poisson_noise', False)
        random_seed = kwargs.get('random_seed', None)
        if random_seed is not None:
            np.random.seed(random_seed)

    def _set_sample(self, sample_thickness, pdb_id, kwargs):
        self.pdb_save_path = kwargs.get('pdb_save_path', 'tempdir')
        os.makedirs(self.pdb_save_path, exist_ok=True)
        self.pdb_id = pdb_id
        if not os.path.exists(os.path.join(self.pdb_save_path, self.pdb_id)):
            self.pdb_id = crystal.get_pdb_file(pdb_id, save_path=self.pdb_save_path, use_cache=False)
        self.add_water = kwargs.get('add_water', True)
        self.volume = sample_thickness * np.pi * (self.beam_diameter / 2) ** 2
        self.concentration = kwargs.get('concentration', 10)
        self.n_particles = get_n_particles(self.concentration, self.volume, self.pdb_id)
        self._create_bio_assembly = kwargs.get('create_bioassembly', False)
        self.water_temperature = kwargs.get('water_temperature', 293.15)

    def _set_gas(self, kwargs):
        self.gas_type = kwargs.get('gas_type', 'he')
        self.gas_path_length = kwargs.get('gas_path_length', [0.0, 1.0])
        _check_gas_path_length(self.gas_path_length)
        self.gas_pressure = kwargs.get('gas_pressure', 101325.0)
        self.gas_temperature = kwargs.get('gas_temperature', 293.15)
        self.gas_background = None
        self.add_gas_background = kwargs.get('gas_background', False)
        if self.add_gas_background:
            print("Calculating Gas Background...")
            self.gas_background = get_gas_background(pad_geometry=self.pad_geometry,
                                                     beam=self.beam,
                                                     path_length=[self.gas_path_length[0], self.gas_path_length[-1]],
                                                     gas_type=self.gas_type,
                                                     temperature=self.gas_temperature,
                                                     pressure=self.gas_pressure,
                                                     poisson=self.poisson,
                                                     n_simulation_steps=100)

    def _set_radial_profiler(self, kwargs):
        self.n_bins = kwargs.get('n_radial_bins', 1000)
        self.q_mags = self.pad_geometry.q_mags(beam=self.beam)
        self.profiler = RadialProfiler(beam=self.beam, pad_geometry=self.pad_geometry, mask=self.mask,
                                       n_bins=self.n_bins,
                                       q_range=np.array([np.min(self.q_mags), np.max(self.q_mags)]))

    def get_radial(self, frame_number=0, weights=None):
        """
        Get the radial profile of a frame.

        Args:
            frame_number (int): Frame number to be calculated.
            weights (numpy.ndarray): Array of weights for the radial profile.

        Returns:
            RadialProfile: The radial profile of the frame.
        """
        dat = self.get_data(frame_number).get_processed_data_flat()
        if weights is None:
            weights = np.ones(len(self.f2phot))  # TO DO: add to reborn
        return self.profiler.quickstats(dat, weights=weights)

    def get_data(self, frame_number=0):
        """
        Calculates the diffracted intensity of the molecule as
        I(q) = J_0 SA r_e^2 P(q) | sum_n f_n(q) sum_m exp(i q dot r_mn) |^2

        Args:
            frame_number (int): Frame number to be calculated.

        Returns:
            DataFrame: The diffracted intensity of the molecule.
        """

        protein_profile = get_protein_profile(self.pdb_id, self.profiler.q_bin_centers)
        print("I(0) before n particles: ", protein_profile[0])
        protein_profile *= self.n_particles
        print("I(0) after n particles: ", protein_profile[0])
        # exit()
        intensity = np.interp(self.q_mags, self.profiler.bin_centers, protein_profile)
        intensity *= self.f2phot  # converts to photon counts

        if self.add_gas_background:
            intensity += self.gas_background

        if self.poisson:
            intensity = np.random.poisson(intensity).astype(float)

        df = DataFrame(pad_geometry=self.pad_geometry,
                       beam=self.beam,
                       raw_data=intensity)

        df.set_frame_index(self.current_frame)
        df.set_frame_id(f'{self.pdb_id} {frame_number}')
        df.set_pad_geometry(self.pad_geometry)
        df.set_raw_data(intensity)
        df.set_beam(self.beam)
        df.validate()
        return df
