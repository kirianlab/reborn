import os
import sys
from time import time
import numpy as np
from numpy.fft import fftn, ifftn, fftshift, ifftshift
from scipy.spatial.transform import Rotation
import matplotlib.pyplot as plt
import pyqtgraph as pg
import reborn.utils
from reborn import source, detector, dataframe, const
from reborn.misc.interpolate import trilinear_interpolation
from reborn.target import crystal
from reborn.external import denss


config = dict(
    pdb_file = ["2vea", "3zq5"], #["1jfp", "1nl6"], # [state 1, state 2] or [dark state, pumped state] ["2vea", "3zq5"],#
    q_range = [0,5], # 1/A
    sample_dmax = 100, # Angstroms
    detector_distance = 0.112, # meters
    photon_energy = 9.5e3 * const.eV, 
    sample_path_length = 5e-6, # meters
    collection_time = np.array([0.5, 1, 1.5, 2.5]), # Data collection time in hours, only use 4 items in the list otherwise plots look screwy
    laser_rep_rate = 120, # Hz
    add_gas = False,
    gas_path_length = [0, 0.112], # [distance before sample, distance after sample]
    sample_concentration = 10, # mg/ml
    binned_pixels = 5, # the detector pixel binning, 1 is no binning
    random_seed = 1, # the random seed for Poisson noise
    possion_noise = False, # add poisson noise to your profiles
    gas_pressure = 101325, # Pa, 1 atm = 101325 Pa
    gas_temperature = 273.15, # Kelvin
    gas_type = "he", # options are he, air, n2, o2, humid_he, or humid_air. 
    create_bio_assembly_1 = False,
    create_bio_assembly_2 = False,
    normalize_profiles = False
    )

# Fetch files
########################################################################
for i,f in enumerate(config["pdb_file"]):
    if not os.path.exists(config["pdb_file"][i]):
        config["pdb_file"][i] = crystal.get_pdb_file(
            config["pdb_file"][i], save_path="..", use_cache=False
        )
        print("Fetched PDB file:", config["pdb_file"][i])

q_range = config['q_range']
qms = None #np.linspace(q_range[0], q_range[1], 1001) #* 1e-10
print(config['pdb_file'][0])
qbin_dark, I_dark = denss.get_scattering_profile(pdb_file=config['pdb_file'][0],
        create_bio_assembly=config['create_bio_assembly_1'],
        q_mags=qms)

qbin_pump, I_pump = denss.get_scattering_profile(pdb_file=config['pdb_file'][1],
        create_bio_assembly=config['create_bio_assembly_2'],
        q_mags=qms)

if config['normalize_profiles']:
    I_dark /= np.max(I_dark)
    I_pump /= np.max(I_pump)

fig, ax = plt.subplots()
ax.plot(qbin_dark, I_dark, 'r')
ax.plot(qbin_pump, I_pump, 'g')
plt.draw()

fig, ax = plt.subplots()
ax.plot(qbin_pump, I_pump-I_dark)
plt.show()











































