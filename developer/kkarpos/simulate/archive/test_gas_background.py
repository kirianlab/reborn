import numpy as np
import pylab as plt
import pandas as pd
import reborn
import os, sys
import time

from simulate import Simulator
from reborn.detector import PADGeometry
from reborn.source import Beam
from reborn.const import eV
from reborn.viewers.qtviews import PADView
from reborn.simulate.gas import get_gas_background

config = {'detector_distance': 0.2, # m
            'photon_energy': 9.5e3 * eV, # 8e3 * eV, 
            'gas_path': [-0.1,0.2],
            'binned_pixels': 10, # The detector pixel binning, 1 is no binning
            'random_seed': 1,    # The random seed for Poisson noise
            'poisson': False,
            'n_radial_bins': 1000,
            'gas_path_length': [0, 0.05],
            'n_steps': 1
            }

pulse_energy = config['photon_energy'] * 1e8
beam = Beam(pulse_energy=pulse_energy)
pads = PADGeometry(distance=config['detector_distance'], shape=(5, 5), pixel_size=100e-6)

qrange = np.linspace(0, np.max(pads.q_mags(beam)), config['n_radial_bins']) * 1e-10

nsteps = np.arange(1,30)
pl = np.linspace(0.0001,0.15,5)
I0_full = np.zeros((len(pl), len(nsteps)))
for j,p in enumerate(pl):
    I0 = []
    for i in nsteps:
        gas_background = get_gas_background(pad_geometry=pads,
                                             beam=beam,
                                             path_length=[0,p],
                                             gas_type="he",
                                             poisson=config['poisson'],
                                             n_simulation_steps=i)

        q0_idx = np.where(pads.q_mags(beam) == 0)
        I0.append(gas_background[q0_idx][0])
    I0_full[j] = I0

plt.figure(figsize=(10,5))
for i in range(len(I0_full)):
    plt.plot(nsteps, I0_full[i], label=f"{pl[i]:0.5f}")
    plt.scatter(nsteps, I0_full[i])

plt.xlabel("N Simulation Steps")
plt.ylabel("I(0)")
plt.title(f"Testing Gas Background")
plt.grid(alpha=0.5)
plt.legend(ncol=3, title="Path Length [m]", loc='upper right')
plt.show()

# pv = PADView(data=gas_background, pad_geometry=pads, beam=beam)
# pv.start()








