# Import standard libraries
import numpy as np
from scipy import constants, interpolate
from scipy.spatial.transform import Rotation
from time import time
import os
import logging
# Import third-party libraries
import matplotlib.pyplot as plt
import pyqtgraph as pg
import saxstats.saxstats as saxstats  # This is Tom's DENSS package

# Import local/internal libraries
from reborn import source, detector, dataframe, const
from reborn.analysis.saxs import RadialProfiler
from reborn.dataframe import DataFrame
from reborn.external import denss
from reborn.fileio.getters import FrameGetter
from reborn.misc.interpolate import trilinear_interpolation
from reborn.simulate import clcore, gas, solutions
from reborn.simulate.gas import get_gas_background
from reborn.simulate.solutions import water_scattering_factor_squared
from reborn.source import Beam
from reborn.target import atoms, crystal, molecule, placer
import simulate
import scipy.constants


def convert_concentration(concentration_mg_per_ml):
    concentration_kg_per_m3 = concentration_mg_per_ml * 1e6 / 1e3
    return concentration_kg_per_m3


def _fetch_pdb(pdb_file):
    if not os.path.exists(pdb_file):
        pdb_file = crystal.get_pdb_file(
            pdb_file, save_path="..", use_cache=False)
    return pdb_file


def get_n_proteins(concentration: float, volume: float, pdb_id: str, random: bool = True) -> int:
    """
    Calculate the number of proteins in the sample based on the concentration and volume.

    Args:
        concentration (float): The protein concentration in kg/m^3.
        volume (float): The volume of the sample in m^3.
        pdb_id (str): The PDB id of the protein.
        random (bool): Whether to add a random factor to the number of proteins.

    Returns:
        int: The number of proteins in the sample.
    """
    pdb = crystal.CrystalStructure(pdb_id, tempdir="tempdir")
    protein_number_density = concentration / pdb.molecule.get_molecular_weight()
    n_proteins = int(protein_number_density)
    if random:
        n_proteins = np.random.normal(loc=n_proteins, scale=1)
    return int(n_proteins)  # Ensuring we don't end up with a negative count


def get_protein_profile(pdb_file, q,
                        create_bio_assembly=True,
                        ignore_waters=False):
    try:
        pq, Ip = denss.get_scattering_profile(pdb_file=_fetch_pdb(pdb_file),
                                              create_bio_assembly=True,
                                              q_mags=q,
                                              ignore_waters=ignore_waters)
    except Exception as e:
        logging.exception("An error occurred when creating the bio assembly. Proceeding without bio assembly.")
        pq, Ip = denss.get_scattering_profile(pdb_file=_fetch_pdb(pdb_file),
                                              create_bio_assembly=False,
                                              q_mags=q,
                                              ignore_waters=ignore_waters)
    return Ip  # * self.n_particles


class Simulator(FrameGetter):
    """
    Class for simulating molecule diffraction from a PDB file.

    This class is adapted from Roberto Alvarez's initial fluctuation scattering simulations.
    This class inherits from the FrameGetter class.

    Attributes:
    """

    def __init__(self, pad_geometry, beam, pdb_id, **kwargs):
        """
        Initializes the Simulator class with the given parameters.

        Args:
            pad_geometry (PADGeometryList): The detector geometry.
            beam (Beam): The X-ray beam to be used.
            denss_data (str): The path to the denss .dat file.
            pdb (str): PDB id to simulate. Default is None.
            kwargs (dict): Optional keyword arguments. See class attributes for more details.

        Returns:
            None
        """
        super().__init__()

        random_seed = kwargs.get('random_seed', None)
        if random_seed is not None:
            np.random.seed(random_seed)
        self.poisson = kwargs.get('poisson_noise', False)

        # beam init
        self.beam = beam
        self.fluence = self.beam.photon_number_fluence
        self.photon_energy = self.beam.photon_energy

        # constants
        self.r_e = constants.value('classical electron radius')  # meters
        self.kb = constants.value('Boltzmann constant')  # J  K^-1
        self.na = constants.value('Avogadro constant')  # mole^-1
        self.eV = constants.value('electron volt')  # J

        # denss stuff
        self.create_bio_assembly = kwargs.get('create_bio_assembly', True)
        self.ignore_waters = kwargs.get('ignore_waters', True)

        # pad geometry init
        self.pads = pad_geometry
        self.q_vecs = self.pads.q_vecs(beam=self.beam)
        self.q_mags = self.pads.q_mags(beam=self.beam)
        self.sa = self.pads.solid_angles()
        self.pol = self.pads.polarization_factors(beam=beam)
        self.f2phot = self.pads.f2phot(beam=self.beam)

        # add a beamstop mask
        self.mask = self.pads.ones()
        self.beamstop_diameter = kwargs.get('beamstop_diameter', 0)
        self.mask_forward_scatter(beamstop_diameter=self.beamstop_diameter)

        self.pdb_id = pdb_id

        self.sample_path_length = kwargs.get('sample_path_length', 1e-6)
        self.water_temperature = kwargs.get('water_temperature', 293.15)
        self.sample_volume = self.sample_path_length * np.pi * (self.beam.diameter_fwhm / 2) ** 2
        self._add_water = kwargs.get("add_water", True)

        self.gas = kwargs.get('gas_background', False)
        self.sample_concentration = kwargs.get('sample_concentration', 10)  # mg/ml = kg/m^3

        self.n_particles = get_n_proteins(concentration=self.sample_concentration,
                                          volume=self.sample_volume,
                                          pdb_id=self.pdb_id)

        if self.gas:
            self.gas_type = kwargs.get('gas_type', 'he')
            self.gas_path_length = kwargs.get('gas_path_length', [0.0, 1.0])
            self.gas_pressure = kwargs.get('gas_pressure', 101325.0)
            self.gas_temperature = kwargs.get('gas_temperature', 293.15)
            self.gas_background = get_gas_background(pad_geometry=self.pads,
                                                     beam=self.beam,
                                                     path_length=self.gas_path_length,
                                                     gas_type=self.gas_type,
                                                     temperature=self.gas_temperature,
                                                     pressure=self.gas_pressure,
                                                     poisson=self.poisson)

        self.n_bins = kwargs.get('n_q_bins', 1001)
        self.profiler = RadialProfiler(beam=self.beam, pad_geometry=self.pads, mask=self.mask,
                                       n_bins=self.n_bins,
                                       q_range=np.array([np.min(self.q_mags), np.max(self.q_mags)]))

        self.show_raw_profiles = kwargs.get('show_raw_profiles', False)

        self.get_data()

    def mask_forward_scatter(self, beamstop_diameter: float) -> None:
        """
        Mask the forward scattering below a given angle.

        Args:
            beamstop_diameter (float): Diameter of the beamstop in meters.
        """
        theta = np.arctan(beamstop_diameter / self.pads[0].t_vec[2])
        self.mask[self.pads.scattering_angles(self.beam) < theta] = 0

    def get_water_profile(self, q):
        # qw, water = get_water_profile()
        # Iw = np.interp(q, qw, water) * self.sample_volume * water_number_density()
        water = water_scattering_factor_squared(q, self.water_temperature, volume=self.sample_volume)
        return water

    def get_data(self, frame_number=0):
        """
        Calculates the diffracted intensity of the molecule as
        I(q) = J_0 SA r_e^2 P(q) | sum_n f_n(q) sum_m exp(i q dot r_mn) |^2

        Args:
            frame_number (int): Frame number to be calculated.

        Returns:
            DataFrame: The diffracted intensity of the molecule.
        """

        qbin_centers = self.profiler.bin_centers

        intensity = get_protein_profile(pdb_file=self.pdb_id,
                                        q=qbin_centers,
                                        create_bio_assembly=self.create_bio_assembly,
                                        ignore_waters=self.ignore_waters)
        intensity = intensity.astype(float)
        intensity *= float(self.n_particles)

        if self._add_water:
            intensity += self.get_water_profile(q=qbin_centers)

        # Convert to a 2D diffraction pattern
        intensity = np.interp(self.q_mags, qbin_centers, intensity)

        intensity *= self.f2phot

        # if self.poisson:
        print('before', intensity)
        intensity = np.random.poisson(intensity).astype(float)
        print('after', intensity)
        if self.gas:
            intensity += self.gas_background

        self.df = DataFrame(pad_geometry=self.pads,
                            beam=self.beam,
                            raw_data=intensity)
        return self.df

    def get_radial(self, frame_number=0, weights=None):
        """
        Get the radial profile of a frame.

        Args:
            frame_number (int): Frame number to be calculated.
            weights (numpy.ndarray): Array of weights for the radial profile.

        Returns:
            RadialProfile: The radial profile of the frame.
        """
        dat = self.df.get_processed_data_flat()
        return self.profiler.quickstats(dat, weights=weights)
