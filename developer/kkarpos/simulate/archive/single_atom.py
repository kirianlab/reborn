import numpy as np
import pylab as plt
import pandas as pd
import reborn
import os, sys
import time

from simulate import Simulator
from reborn.detector import rayonix_mx340_xfel_pad_geometry_list
from reborn.source import Beam
from reborn.const import eV
from reborn.viewers.qtviews import PADView

from reborn.dataframe import DataFrame
from reborn.detector import RadialProfiler
from reborn.target import atoms

def shannon_s(s=2, d=100):
    # returns shannon sampling ratio s in units of d
    return 2*np.pi / (s*d)

config = {'detector_distance': 1, # m
            'photon_energy': 8e3 * eV, 
            'sample_delivery_method': "jet",
            'jet_thickness': 1e-6, # m
            'droplet_diameter': 40e-6,
            'helium': False,    # Flip this if you want to add helium into the mix
            'binned_pixels': 10, # The detector pixel binning, 1 is no binning
            'random_seed': 0,    # The random seed for Poisson noise
            'poisson_noise': True
            }

pulse_energy = config['photon_energy'] * 1e8

# Set up the detector and beam
pads = rayonix_mx340_xfel_pad_geometry_list(detector_distance=config['detector_distance'])
pads = pads.binned(config['binned_pixels'])
beam = Beam(photon_energy=config['photon_energy']) #pulse_energy=pulse_energy)#, 
config['n_radial_bins'] = 1000 #int(np.max(pads.q_mags(beam))*1e-10/shannon_s(d=1))
q_mags = pads.q_mags(beam=beam)
qrange = np.linspace(0, np.max(q_mags), config['n_radial_bins']) #* 1e-10


def get_atom_profile(atomic_number: int, q_mags, beam, f2phot):

    r"""Calculates scattering intensity for single atom
        
        Arguments:
            atomic_number (int): As is sounds, the number of the desired atom
        Returns:
            Scattering Intensity [photon count]
    """
    # calculate scattering factors for atom
    scatt = atoms.cmann_henke_scattering_factors(q_mags, atomic_number, beam=beam)
    # calculate intensity
    I = np.abs(scatt) ** 2
    return I

I = get_atom_profile(atomic_number=6, q_mags=q_mags, beam=beam, f2phot=pads.f2phot(beam=beam))

profiler = RadialProfiler(beam=beam, pad_geometry=pads, 
                            n_bins=config['n_radial_bins'], q_range=qrange)

rads = profiler.quickstats(I)

fig, ax = plt.subplots()
ax.plot(qrange, rads['mean'])
plt.show()










































