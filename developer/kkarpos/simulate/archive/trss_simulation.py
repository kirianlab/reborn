import numpy as np
import pylab as plt

from developer.kkarpos.simulate.archive.simulate_trss import Simulator
from reborn.detector import rayonix_mx340_xfel_pad_geometry_list
from reborn.source import Beam
from reborn.const import eV
from reborn.viewers.qtviews import PADView

show_det = False  # Shows the 2D detector after the simulation
plot_snr = False  # Plots the SNR
plot_err = True
print_beam_params = True  # Prints the parameters from the reborn beam class to the terminal
side_by_side = True
show_raw_profiles = False


def shannon_s(s=2, d=100):
    # returns shannon sampling ratio s in units of d
    return 2 * np.pi / (s * d)


def n_shots(rep_rate, collection_time):
    # given a laser rep rate and the desired data collection time, will return the number of shots
    return rep_rate * collection_time


s = 5  # shannon sampling ratio
dmax = 100  # 249 # A

config = dict(
    pdb_file=["1jfp", "1nl6"],  # [state 1, state 2] or [dark state, pumped state] ["2vea", "3zq5"], ["1jfp", "1nl6"]#
    sample_dmax=100,  # Angstroms
    detector_distance=1,  # 0.112, # meters
    photon_energy=9.5e3 * eV,
    sample_path_length=5e-6,  # meters
    collection_time=np.array([0.5, 1, 1.5, 2.5]),  # Data collection time in hours (len = 4 required)
    laser_rep_rate=120,  # Hz
    add_gas=False,
    gas_type="he",  # options are he, air, n2, o2, humid_he, or humid_air.
    gas_path_length=[0, 0.112],  # [distance before sample, distance after sample]
    sample_concentration=5,  # mg/ml
    binned_pixels=5,  # the detector pixel binning, 1 is no binning
    random_seed=1,  # the random seed for Poisson noise
    poisson_noise=True,  # add poisson noise to your profiles
    gas_pressure=101325,  # Pa, 1 atm = 101325 Pa
    gas_temperature=273.15,  # Kelvin
    create_bio_assembly=False,  # keep this off for monomers, on for dimers
    ignore_waters=False,
    beamstop_diameter=0.008,  # m
    add_water=False,
)

pulse_energy = config['photon_energy'] * 1e8
config['n_shots'] = n_shots(config['laser_rep_rate'], 3600 * config['collection_time'])

# to do: add beam jittering and a wavelength spectrum
# for the jitter, make a copy of the pad geometry and alter the beam direction for each "shot" (slow)
# --> another way to do this is to apply the jitter straight to the qmags (faster)
# pick a random wavelength and accept/reject based on the prob (randomly sample angles for the incoming beam)


# Set up the detector and beam
pads = rayonix_mx340_xfel_pad_geometry_list(detector_distance=config['detector_distance'])
pads = pads.binned(config['binned_pixels'])
beam = Beam(pulse_energy=pulse_energy)
config['n_q_bins'] = int(np.max(pads.q_mags(beam)) * 1e-10 / shannon_s(s=s, d=dmax))
qrange = np.linspace(0, np.max(pads.q_mags(beam)), config['n_q_bins']) * 1e-10  # inverse A

# Set up the simulator class for the two states
fg = Simulator(pad_geometry=pads, beam=beam, pdb_id=config['pdb_file'][0],
               n_q_bins=config['n_q_bins'], poisson_noise=config['poisson_noise'],
               sample_concentration=config['sample_concentration'],
               sample_path_length=config['sample_path_length'], beamstop_diameter=config['beamstop_diameter'],
               random_seed=None, gas_background=config['add_gas'], gas_path_length=config['gas_path_length'],
               gas_pressure=config['gas_pressure'],
               gas_temperature=config['gas_temperature'],
               gas_type=config['gas_type'], add_water=config['add_water'])

fg_s2 = Simulator(pad_geometry=pads, beam=beam, pdb_id=config['pdb_file'][1],
                  n_q_bins=config['n_q_bins'], poisson_noise=config['poisson_noise'],
                  sample_concentration=config['sample_concentration'],
                  sample_path_length=config['sample_path_length'], beamstop_diameter=config['beamstop_diameter'],
                  random_seed=None, gas_background=config['add_gas'], gas_path_length=config['gas_path_length'],
                  gas_pressure=config['gas_pressure'],
                  gas_temperature=config['gas_temperature'],
                  gas_type=config['gas_type'], add_water=config['add_water'])

if print_beam_params:
    print(f"\n\nBeam Parameters")
    print(f"---------------\n")
    print(f"N Photons Per Pulse: \t {fg.beam.n_photons:0.3e}")
    print(f"Pulse Energy: \t\t {fg.beam.pulse_energy} J")
    print(f"Pulse Fluence: \t\t {fg.beam.energy_fluence:.3e} J/m^2")
    print(f"Wavelength: \t\t {fg.beam.wavelength * 1e9:0.3f} nm")
    print(f"Photon Number Fluence: \t {fg.beam.photon_number_fluence:0.3e} photons/m^2")

# Grab the radial profiles, no weights
rad_s1 = fg.get_radial(0)
rad_s2 = fg_s2.get_radial(0)
rad_s1_w = fg.get_radial(0, weights=fg.f2phot)
rad_s2_w = fg_s2.get_radial(0, weights=fg_s2.f2phot)

# Gather the quantities needed for the SNR calculations
sum_diff = rad_s1['sum'] - rad_s2['sum']  # The differences of the summed profiles
print(rad_s1['sum'])
print(rad_s2['sum'])
if sum_diff[0] < 0:
    sum_diff *= -1
prof_sum = rad_s1['sum'] + rad_s2['sum']  # The total state 1 + state 2 profile

mean_diff = rad_s1_w['mean'] - rad_s2_w['mean']
if mean_diff[0] < 0:
    mean_diff *= -1

print(rad_s1_w['mean'][0], rad_s2_w['mean'][0], mean_diff[0])
# exit()


def snr(difference, data, n_shots):
    r""" Calculates the signal-to-noise ratio of a difference profile as
            SNR = d|F(q)|^2 / sqrt(sum(I1+I2) / N)

            Args:
                difference (list): The calculated difference of the sum radials
                data (list): List containing the sum of the profiles used for the differences
                n_shots (int): Number of x-ray exposure events contributing to the pattern
            Returns:
                (list): The signal-to-noise ratio
    """
    x = difference / np.sqrt(np.abs(data / n_shots))
    return x


err_diff = [{'err': mean_diff / snr(sum_diff, prof_sum, n),
             'snr': snr(sum_diff, prof_sum, n),
             'n_shots': n} for n in config['n_shots']]

# Convert the sample path length to microns
sample_pl_in_um = config['sample_path_length'] * 1e6

if config['add_gas']:
    if config['gas_type'] == 'he':
        medium = "Helium"
    elif config['gas_type'] == 'air':
        medium = "Air"
    elif config['gas_type'] == 'humid_he':
        medium = "Humid Helium"
    elif config['gas_type'] == 'humid_air':
        medium = "Humid Air"
    elif config['gas_type'] == 'he_air':
        medium = "Helium + Air"
    else:
        raise ValueError("Incorrect gas type provided.")
else:
    medium = "Vacuum"
title_sem = f"""Difference Profile,  N Shots Comparison
            Detector Distance = {config['detector_distance']}m, Photon Energy = {config['photon_energy'] / (1000 * eV)} keV
            Sample Path Length: {sample_pl_in_um}$\mu$m, Medium: {medium}, Rep Rate: {config['laser_rep_rate']}Hz
        """

if show_det:
    pv = PADView(frame_getter=fg)
    # pv2 = PADView(frame_getter=fg_s2)
    pv.start()
    # pv2.start()

if plot_snr:
    fig, ax = plt.subplots(figsize=(15, 8))
    for i, err in enumerate(err_diff):
        ax.plot(qrange, err['snr'], label=f"SNR for {config['collection_time'][i]} hours")
    ax.legend(loc='best', fontsize=12)
    ax.grid(alpha=0.5)
    ax.set_xlabel(r"q = 4 $\pi$ $\sin\theta$ / $\lambda$  $[\AA^{-1}]$", fontsize=12)
    ax.set_ylabel(r"SNR = $\dfrac{{\Delta|F(q)|^2}}{{\sqrt{{(I_1+I_2) / M}}}}$", fontsize=12)
    ax.set_title(title_sem)
    plt.draw()

if plot_err:
    plt.figure(figsize=(15, 8))
    lim = 0
    colors = [0.2, 0.5, 0.8]
    for i, err in enumerate(err_diff[:-1]):
        fill = (mean_diff + err['err'], mean_diff - err['err'])
        plt.fill_between(qrange, fill[0], fill[1],
                         color='gray', edgecolor='gray', alpha=colors[i],
                         label=f"Error for {config['collection_time'][i]} hours")
    fill = (mean_diff + err_diff[-1]['err'], mean_diff - err_diff[-1]['err'])
    plt.fill_between(qrange, fill[0], fill[1],
                     color='lightsteelblue', edgecolor='lightsteelblue', alpha=0.8,
                     label=f"Error for {config['collection_time'][i]} hours")

    idx = np.where(~np.isnan(err['err']))
    plt.plot(qrange[idx], mean_diff[idx], '--', color='black', label='Difference Profile')
    # plt.plot(tqr, tdiff, '--', color='red', label='Ground Truth (scaled)')
    plt.xlim(0, qrange[idx][-1])
    plt.ylim(-np.max(mean_diff[idx]) * (1 - 0.9), np.max(mean_diff[idx]) * (1 + 0.35))
    plt.xlabel(r"q = 4 $\pi$ $\sin\theta$ / $\lambda$  $[\AA^{-1}]$", fontsize=12)
    plt.ylabel(r"Difference Profile, $\Delta I(q)$", fontsize=12)
    plt.legend(loc='best', fontsize=12)
    plt.grid(alpha=0.3)
    plt.title(title_sem, fontsize=12)
    plt.draw()

if side_by_side:
    fig, ax = plt.subplots(1, 3, figsize=(12, 4), tight_layout=True)

    ax[0].plot(qrange, rad_s2_w['mean'], 'g')
    ax[0].set_title(f"PDB ID: {config['pdb_file'][1]}", fontsize=12)
    ax[0].set_ylabel("I(q)", fontsize=12)

    ax[1].plot(qrange, rad_s1_w['mean'], color='purple')
    ax[1].set_title(f"PDB ID: {config['pdb_file'][0]}", fontsize=12)
    ax[1].set_xlabel(r"q = 4 $\pi$ $\sin\theta$ / $\lambda$  $[\AA^{-1}]$", fontsize=12)

    ax[2].plot(qrange, mean_diff, 'k')
    ax[2].set_title(f"PDB ID: {config['pdb_file'][1]} - PDB ID: {config['pdb_file'][0]}", fontsize=12)
    ax[2].set_ylabel(r"Difference Profile, $\Delta$I(q)", fontsize=12)

    # for i in [0,1,2]:
    #     ax[i].grid(alpha=0.5)

    # for i in [0,1]:
    #     ax[i].set_ylim(-0.1, 1.75)
    # ax[2].set_ylim()

    plt.draw()

plt.show()
