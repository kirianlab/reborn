"""
This script calculates the difference profile and corresponding errors
between two PDB files. The program uses DENSS to get profiles.
From there, the script calls the simulate.Simulator class to simulate
what the profile would look like with customized detector geometries.

There are multiple user-defined variables that choose what to simulate.
Each variable has a comment that explains what is does.

Eventually, this process will be added as a reborn GUI plugin.

-KK 2024/07/10
"""


import numpy as np
import matplotlib.pyplot as plt
from simulate import Simulator, shannon_s, n_shots, get_snr
from reborn.detector import rayonix_mx340_xfel_pad_geometry_list
from reborn.source import Beam
from reborn.const import eV
from reborn.viewers.qtviews import PADView

show_det = False  # Shows the 2D detector after the simulation, but only for one PDB file
plot_snr = True  # Plots q vs SNR
plot_err = False  # Plots the difference profile, with the corresponding error around the profile
print_beam_params = True  # Prints the parameters from the reborn beam class to the terminal
side_by_side = False  # Plots


config = dict(
    pdb_file=["2vea", "3zq5"],
    # pdb_file=["1nl6", "1jfp"],  # Rhodopsin dark and pumped state
    # ["1jfp", "1nl6"], # [state 1, state 2] or [dark state, pumped state] ["2vea", "3zq5"],#
    sample_dmax=100,  # Angstroms
    detector_distance=1.5,  # meters
    photon_energy=9.3e3 * eV,
    beamstop_diameter=0.01,  # m,
    sample_path_length=5e-6,  # meters
    # Data collection time in minutes, only use 4 items in the list otherwise plots look screwy
    collection_time=np.array([1, 10, 30, 60]),
    timescale='seconds',  # the collection time magnitude
    laser_rep_rate=1000,  # Hz
    add_gas=False,  # Adds a gas background to the profile, according to the path length option below
    gas_path_length=[0, 0.112],  # [distance before sample, distance after sample
    sample_concentration=10,  # mg/ml
    add_water=False,  # add water into the mix, volume is calculated according to the sample path length
    binned_pixels=5,  # the detector pixel binning, 1 is no binning
    random_seed=1,  # the random seed for Poisson noise
    poisson_noise=False,  # add poisson noise to your profiles, useful only displaying 2D patterns
    gas_pressure=101325,  # Pa, 1 atm = 101325 Pa
    gas_temperature=273.15,  # Kelvin
    gas_type="he",  # options are he, air, n2, o2, humid_he, or humid_air.
    shannon_sampling_ratio=3,
    pdb_save_path='tempdir',
)


def map_timescale(timescale:str):
    """ Returns the scale factor to convert a time into seconds """
    if timescale == 'seconds':
        return 1
    elif timescale == 'minutes':
        return 60
    elif timescale == 'hours':
        return 3600
    else:
        raise ValueError('Options are "seconds" or "minutes" or "hours"')


pulse_energy = config['photon_energy'] * 1e8
collection_time = map_timescale(config['timescale']) * config['collection_time']
config['n_shots'] = n_shots(config['laser_rep_rate'], collection_time)

# to do: add beam jittering and a wavelength spectrum
# for the jitter, make a copy of the pad geometry and alter the beam direction for each "shot" (slow)
# --> another way to do this is to apply the jitter straight to the qmags (faster)
# pick a random wavelength and accept/reject based on the prob (randomly sample angles for the incoming beam)


# Set up the detector and beam
pads = rayonix_mx340_xfel_pad_geometry_list(detector_distance=config['detector_distance'])
pads = pads.binned(config['binned_pixels'])
beam = Beam(pulse_energy=pulse_energy)
config['n_radial_bins'] = int(
    np.max(pads.q_mags(beam)) * 1e-10 / shannon_s(s=config['shannon_sampling_ratio'], d=config['sample_dmax']))
qrange = np.linspace(0, np.max(pads.q_mags(beam)), config['n_radial_bins']) * 1e-10

simulator_params = {'pad_geometry': pads,  # required
                    'beam': beam,  # required
                    'sample_thickness': config['sample_path_length'],  # required
                    'poisson_noise': config['poisson_noise'],  # optional
                    'random_seed': config['random_seed'],  # optional
                    'beamstop_diameter': config['beamstop_diameter'],  # optional
                    'concentration': config['sample_concentration'],  # optional
                    'gas_type': config['gas_type'],  # optional
                    'gas_path_length': config['gas_path_length'],  # optional
                    'gas_pressure': config['gas_pressure'],  # optional
                    'gas_temperature': config['gas_temperature'],  # optional
                    'gas_background': config['add_gas'],  # optional
                    'n_radial_bins': config['n_radial_bins'],  # optional
                    'add_water': config['add_water'],
                    'n_frames': config['n_shots'][0]
                    }

# Set up the simulator class for the two states
fg_s1 = Simulator(pdb_id=config['pdb_file'][0], **simulator_params)
fg_s2 = Simulator(pdb_id=config['pdb_file'][1], **simulator_params)

if print_beam_params:
    message = f"""
    \nBeam Parameters
    ---------------
    N Photons Per Pulse: \t {fg_s1.beam.n_photons:0.3e}
    Pulse Energy: \t\t {fg_s1.beam.pulse_energy} J
    Pulse Fluence: \t\t {fg_s1.beam.energy_fluence:.3e} J/m^2
    Wavelength: \t\t {fg_s1.beam.wavelength * 1e9:0.3f} nm
    Photon Number Fluence: \t {fg_s1.beam.photon_number_fluence:0.3e} photons/m^2
    ---------------\n
    """
    print(message)

if show_det:
    pv = PADView(frame_getter=fg_s1)
    # pv2 = PADView(frame_getter=fg_s2)
    pv.start()
    # pv2.start()

# Grab the radial profiles, no weights
rad_s1 = fg_s1.get_radial(0)
rad_s2 = fg_s2.get_radial(0)

# Now calculate the radials with f2phot as the weights
rad_s1_w = fg_s1.get_radial(0, weights=fg_s1.f2phot)
rad_s2_w = fg_s2.get_radial(0, weights=fg_s2.f2phot)

# Gather the quantities needed for the SNR calculations
sum_diff = rad_s1['sum'] - rad_s2['sum']  # The differences of the summed profiles
prof_sum = rad_s1['sum'] + rad_s2['sum']  # The total state 1 + state 2 profile

mean_diff = rad_s1_w['mean'] - rad_s2_w['mean']

err_diff = [{'err': mean_diff / get_snr(sum_diff, prof_sum, n),
             'snr': get_snr(sum_diff, prof_sum, n),
             'n_shots': n} for n in config['n_shots']]

# Convert the sample path length to microns
sample_pl_in_um = config['sample_path_length'] * 1e6
if config['add_gas']:
    if config['gas_type'] == 'he':
        medium = "Helium"
    elif config['gas_type'] == 'air':
        medium = "Air"
    elif config['gas_type'] == 'humid_he':
        medium = "Humid Helium"
    elif config['gas_type'] == 'humid_air':
        medium = "Humid Air"
    elif config['gas_type'] == 'he_air':
        medium = "Helium + Air"
    else:
        raise ValueError("Incorrect gas type provided.")
else:
    medium = "Vacuum"

title_sem = f"""Difference Profile,  N Shots Comparison
            Detector Distance = {config['detector_distance']}m, Photon Energy = {config['photon_energy'] / (1000 * eV)} keV
            Sample Path Length: {sample_pl_in_um}$\mu$m, Medium: {medium}, Rep Rate: {config['laser_rep_rate']}Hz
        """

if plot_snr:
    fig, ax = plt.subplots(figsize=(15, 8))
    for i, err in enumerate(err_diff):
        ax.plot(qrange, err['snr'], label=f"SNR for {config['collection_time'][i]} {config['timescale']}")
    ax.legend(loc='best', fontsize=12)
    ax.grid(alpha=0.5)
    ax.set_xlabel(r"q = 4 $\pi$ $\sin\theta$ / $\lambda$  $[\AA^{-1}]$", fontsize=12)
    ax.set_ylabel(r"SNR = $\dfrac{{(I' - I)\sqrt{M}}}{{\sqrt{{(I_1+I_2)}}}}$", fontsize=12)
    ax.set_title(title_sem)
    plt.draw()

if plot_err:
    plt.figure(figsize=(15, 8))
    lim = 0
    colors = [0.2, 0.5, 0.8]
    for i, err in enumerate(err_diff[:-1]):
        fill = (mean_diff + err['err'], mean_diff - err['err'])
        plt.fill_between(qrange, fill[0], fill[1],
                         color='gray', edgecolor='gray', alpha=colors[i],
                         label=f"Error for {config['collection_time'][i]} {config['timescale']}")
    fill = (mean_diff + err_diff[-1]['err'], mean_diff - err_diff[-1]['err'])
    plt.fill_between(qrange, fill[0], fill[1],
                     color='lightsteelblue', edgecolor='lightsteelblue', alpha=0.8,
                     label=f"Error for {config['collection_time'][-1]} {config['timescale']}")

    plt.plot(qrange, mean_diff, '--', color='black', label='Difference Profile')
    plt.xlim(qrange[0], qrange[-1])
    # plt.ylim(-np.max(mean_diff) * (1 - 0.9), np.max(mean_diff) * (1 + 0.35))
    plt.xlabel(r"q = 4 $\pi$ $\sin\theta$ / $\lambda$  $[\AA^{-1}]$", fontsize=12)
    plt.ylabel(r"Difference Profile, $\Delta I(q)$", fontsize=12)
    plt.legend(loc='best', fontsize=12)
    plt.grid(alpha=0.3)
    plt.title(title_sem, fontsize=12)
    plt.draw()

if side_by_side:
    fig, ax = plt.subplots(1, 3, figsize=(12, 4), tight_layout=True)

    ax[0].plot(qrange, rad_s2_w['mean'], 'g')
    ax[0].set_title(f"PDB: {config['pdb_file'][0]}", fontsize=12)
    ax[0].set_ylabel("I(q)", fontsize=12)

    ax[1].plot(qrange, rad_s1_w['mean'], color='purple')
    ax[1].set_title(f"PDB: {config['pdb_file'][1]}", fontsize=12)
    ax[1].set_xlabel(r"q = 4 $\pi$ $\sin\theta$ / $\lambda$  $[\AA^{-1}]$", fontsize=12)

    ax[2].plot(qrange, mean_diff, 'k')
    ax[2].set_title(f"{config['pdb_file'][0]} - {config['pdb_file'][1]}", fontsize=12)
    ax[2].set_ylabel(r"Difference Profile, $\Delta$I(q)", fontsize=12)

    for i in [0, 1, 2]:
        ax[i].grid(alpha=0.5)

    plt.draw()

plt.show()
