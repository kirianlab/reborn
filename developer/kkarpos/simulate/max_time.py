import numpy as np
import pylab as plt
import pandas as pd
import reborn
import os, sys
import time

from scipy.signal import argrelextrema

from simulate import Simulator, shannon_s, n_shots, snr
from reborn.detector import rayonix_mx340_xfel_pad_geometry_list
from reborn.source import Beam
from reborn.const import eV
from reborn.viewers.qtviews import PADView

r'''
    Simulates a jet in vacuum using rhodopsin
'''

# These three are the cis vs trans rhodopsin profiles from Tom
diff_s = "denss_data/cis_v_trans/sim_1-18.3_light_adjusted_0.2-0.0ps_diff.pdb2sas.dat"
s1_s = "denss_data/cis_v_trans/sim_1-18.3_light_0.0ps_adjusted.pdb2sas.dat"
s2_s = "denss_data/cis_v_trans/sim_1-18.3_light_0.2ps_adjusted.pdb2sas.dat"

config = {'detector_distance': 1, # m
            'photon_energy': 9.5e3 * eV, # 8e3 * eV, 
            'sample_delivery_method': "jet", # jet or droplets
            'sample_path_length': 5e-6, #50-6, #300e-6, #5e-6, # m
            'collection_time': np.linspace(0.1,3,1000), # data collection time in hours
            'laser_rep_rate': 1000, # laser rep rate in Hz
            'add_gas': False,    # Flip this if you want to add helium into the mix
            'gas_path': [-0.1,0.9],
            'pdb_id':  "1JFP", #"2VEA"
            'concentration': 10, #10, # mg/ml, the sample concentration
            'binned_pixels': 10, # The detector pixel binning, 1 is no binning
            'random_seed': 1,    # The random seed for Poisson noise
            'header_level': 1, # DENSS file sometimes have headers
            'poisson_noise': False,
            'gas_pressure': 1*101325, #101325 = 1atm
            'gas_temperature': 273.15,
            'gas_type': 'air', # he, air, n2, o2, humid_he, humid_air, he_air
            'shannon_s': 2,
            'dmax': 100, # Angstroms
            }

pulse_energy = config['photon_energy'] * 1e8
config['n_shots'] = n_shots(config['laser_rep_rate'], 3600 * config['collection_time'])

sample_pl_in_um = config['sample_path_length'] * 1e6
title_sem = f"""Difference Profile,  N Shots Comparison
            Detector Distance = {config['detector_distance']}m, Photon Energy = {config['photon_energy']/(1000 * eV)} keV
            Sample Path Length: {sample_pl_in_um}$\mu$m, Medium: Vacuum, Rep Rate: {config['laser_rep_rate']}Hz
        """

ds = np.loadtxt(diff_s, skiprows=0)
ss1 = np.loadtxt(s1_s, skiprows=1)
ss2 = np.loadtxt(s2_s, skiprows=1)

# Set up the detector and beam
pads = rayonix_mx340_xfel_pad_geometry_list(detector_distance=config['detector_distance'])
pads = pads.binned(config['binned_pixels'])
beam = Beam(pulse_energy=pulse_energy)
config['n_radial_bins'] = int(np.max(pads.q_mags(beam))*1e-10/shannon_s(s=config['shannon_s'], d=config['dmax']))
qrange = np.linspace(0, np.max(pads.q_mags(beam)), config['n_radial_bins']) * 1e-10

# Set up the simulator class for the two states
fg = Simulator(pad_geometry=pads, beam=beam, denss_data=s1_s, pdb=config['pdb_id'],
                    n_radial_bins=config['n_radial_bins'], poisson_noise=config['poisson_noise'],
                    sample_delivery_method=config['sample_delivery_method'],
                    sample_path_length=config['sample_path_length'],
                    random_seed=None, header_level=config['header_level'],
                    gas_background=config['add_gas'], gas_path=config['gas_path'],
                    gas_pressure=config['gas_pressure'], 
                    gas_temperature=config['gas_temperature'],
                    gas_type=config['gas_type'])

fg_s2 = Simulator(pad_geometry=pads, beam=beam, denss_data=s2_s, pdb=config['pdb_id'],
                    n_radial_bins=config['n_radial_bins'], poisson_noise=config['poisson_noise'],
                    sample_delivery_method=config['sample_delivery_method'],
                    sample_path_length=config['sample_path_length'],
                    random_seed=None, header_level=config['header_level'],
                    gas_background=config['add_gas'], gas_path=config['gas_path'],
                    gas_pressure=config['gas_pressure'], 
                    gas_temperature=config['gas_temperature'],
                    gas_type=config['gas_type'])

# Grab the radial profiles, no weights
rad_s1 = fg.get_radial(0)
rad_s2 = fg_s2.get_radial(0)
rad_s1_w = fg.get_radial(0, weights=fg.f2phot)
rad_s2_w = fg_s2.get_radial(0, weights=fg_s2.f2phot)

# Gather the quantities needed for the SNR calculations
sum_diff = rad_s1['sum'] - rad_s2['sum']    # The differences of the summed profiles
prof_sum = rad_s1['sum'] + rad_s2['sum']    # The total state 1 + state 2 profile

mean_diff = rad_s1_w['mean'] - rad_s2_w['mean']

tdiff = ss1[:,1] - ss2[:,1]
tqr = ss1[:,0]
tdiff *= np.max(np.abs(mean_diff)) / np.max(np.abs(tdiff))

err = [{'err': mean_diff / snr(sum_diff, prof_sum, n),
        'snr': np.abs(snr(sum_diff, prof_sum, n))} for n in config['n_shots']]


peak_indices = [argrelextrema(err[i]['snr'], np.greater)[0] for i in range(len(err))]
err_1 = [err[i]['snr'][peak_indices[i][0]] for i in range(len(err))]
err_2 = [err[i]['snr'][peak_indices[i][1]] for i in range(len(err))]
err_3 = [err[i]['snr'][peak_indices[i][2]] for i in range(len(err))]


fig, ax = plt.subplots(figsize=(8,4))
for i, e in enumerate([err_1, err_2, err_3]):
    ax.plot(config['collection_time'], e, label=f"SNR at q={qrange[peak_indices[i][i]]:0.2f} $\AA^{{-1}}$")
ax.set_xlabel('Collection Time [Hours]', fontsize=12)
ax.set_ylabel(r"SNR = $\dfrac{{\Delta|F(q)|^2}}{{\sqrt{{(I_1+I_2) / M}}}}$", fontsize=12)
ax.legend()
ax.grid(alpha=0.5)
plt.show()


















