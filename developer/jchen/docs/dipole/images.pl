# LaTeX2HTML 2019.2 (Released June 5, 2019)
# Associate images original text with physical files.


$key = q/1slash2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$1/2$">|; 

$key = q/1slash{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashScriptR}}}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$1/{\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/ScriptR}}$}}$">|; 

$key = q/1slash{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashScriptR}}}}^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$1/{\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/ScriptR}}$}}^2$">|; 

$key = q/8pislash3;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$8\pi/3$">|; 

$key = q/A;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.77ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img184.svg"
 ALT="$A$">|; 

$key = q/B=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.77ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img183.svg"
 ALT="$B=0$">|; 

$key = q/DeltaOmega;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.77ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\Delta \Omega$">|; 

$key = q/E;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.77ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img245.svg"
 ALT="$E$">|; 

$key = q/F(vec{q});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img125.svg"
 ALT="$F(\vec{q})$">|; 

$key = q/G(R)rightarrowAslashR;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img187.svg"
 ALT="$G(R) \rightarrow A/R$">|; 

$key = q/G(vec{r},vec{r}');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img159.svg"
 ALT="$G(\vec{r}, \vec{r}')$">|; 

$key = q/G(vec{r},vec{r}')=G(R);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img177.svg"
 ALT="$G(\vec{r}, \vec{r}') = G(R)$">|; 

$key = q/I(vec{q});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$I(\vec{q})$">|; 

$key = q/I_0=|hat{vec{E}}_0|^2slash2mu_0c;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$I_0 = \vert\hat{\vec{E}}_0\vert^2/2\mu_0c$">|; 

$key = q/J_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img5.svg"
 ALT="$J_0$">|; 

$key = q/J_0=|vec{E}_0|^2slash2mu_0c;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$J_0 = \vert\vec{E}_0\vert^2/2\mu_0c$">|; 

$key = q/N;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.77ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img140.svg"
 ALT="$N$">|; 

$key = q/Rne0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img179.svg"
 ALT="$R \ne 0$">|; 

$key = q/Rrightarrow0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.77ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img186.svg"
 ALT="$R\rightarrow 0$">|; 

$key = q/V(vec{r});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$V(\vec{r})$">|; 

$key = q/V(vec{r},t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img150.svg"
 ALT="$V(\vec{r}, t)$">|; 

$key = q/alpha;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\alpha$">|; 

$key = q/delta(R)=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\delta(R)=0$">|; 

$key = q/delta(vec{r}-vec{r}');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\delta(\vec{r}-\vec{r}')$">|; 

$key = q/displaystyle(nabla^2+k^2)G(vec{r},vec{r}')=delta(vec{R});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\displaystyle (\nabla^2 + k^2)G(\vec{r}, \vec{r}') = \delta(\vec{R})$">|; 

$key = q/displaystyle(nabla^2+k^2)G(vec{r},vec{r}')=delta(vec{r}-vec{r}');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle (\nabla^2 + k^2)G(\vec{r}, \vec{r}') = \delta(\vec{r}-\vec{r}')$">|; 

$key = q/displaystyle(nabla^2+k^2)V_omega(vec{r})=-rho_omega(vec{r})slashepsilon_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\displaystyle (\nabla^2 + k^2) V_\omega(\vec{r}) = -\rho_\omega (\vec{r})/\epsilon_0$">|; 

$key = q/displaystyle(vecr,t)+vecE_;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle (\vec r, t) + \vec
E_$">|; 

$key = q/displaystyle(vecr,t);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle (\vec r, t) \;.$">|; 

$key = q/displaystyle(vecr,t)=frac{q}{4piepsilon_0}frac{{mbox{resizebox{.09in}{.08in}{incim=1em014em0,clip]{fontsslashBoldR}}}}}}timesvec{E}(vec{r},t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle (\vec r, t) = \frac{q}{4\pi\epsilon_0}
\frac{{\mbox{$\resizebox{....
...graphics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}$}}\times \vec{E}(\vec{r}, t)$">|; 

$key = q/displaystyle(vec{r},t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle (\vec{r},t)$">|; 

$key = q/displaystyle(vec{r},t)=frac{r_e}{r}[hat{vec{r}}times(hat{vec{r}}timesvec{E}_0)]er}'-iomegat+ifrac{omega}{c}(r-hat{vec{r}}cdotvec{r}')right);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.44ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle (\vec{r},t) = \frac{r_e}{r} [\hat{\vec{r}} \times ( \hat{\vec{r}}...
...' - i\omega t + i \frac{\omega}{c}( r - \hat{\vec{r}}\cdot \vec{r}')\right) \;.$">|; 

$key = q/displaystyle(vec{r},t)=frac{r_e}{r}[hat{vec{r}}times(hat{vec{r}}timesvec{E}_0)]exp(ivec{k}_0cdotvec{r}'-iomegat_r);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle (\vec{r},t) = \frac{r_e}{r} [\hat{\vec{r}} \times ( \hat{\vec{r}} \times \vec{E}_0 )] \exp(i\vec{k}_0 \cdot \vec{r}' - i \omega t_r)$">|; 

$key = q/displaystyle(vec{r},t)=r_efrac{e^{ikr}}{r}e^{-iomegat}[hat{vec{r}}times(hat{vec{r}}timesvec{E}_0)]e^{-ivec{q}cdotvec{r}'};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.26ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle (\vec{r},t) = r_e\frac{e^{ikr}}{r} e^{-i\omega t}[\hat{\vec{r}} \times ( \hat{\vec{r}} \times \vec{E}_0 )]e^{-i\vec{q}\cdot\vec{r}'} \;.$">|; 

$key = q/displaystyle(vec{r},t)=r_efrac{e^{ikr}}{r}e^{-iomegat}[hat{vec{r}}times(hat{vec{r}}timesvec{E}_0)]sum_ie^{-ivec{q}cdotvec{r}_i'};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.58ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle (\vec{r},t) = r_e\frac{e^{ikr}}{r} e^{-i\omega t}[\hat{\vec{r}} \times ( \hat{\vec{r}} \times \vec{E}_0 )] \sum_i e^{-i\vec{q}\cdot\vec{r}_i'} \;.$">|; 

$key = q/displaystyle(vec{r},t)=r_efrac{e^{ikr}}{r}e^{-iomegat}[hat{vec{r}}times(hat{vec{}timesvec{E}_0)]intrho(vec{r}')e^{-ivec{q}cdotvec{r}'}d^3r';.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.66ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle (\vec{r},t) = r_e\frac{e^{ikr}}{r} e^{-i\omega t}[\hat{\vec{r}} \...
...}} \times \vec{E}_0 )] \int \rho(\vec{r}') e^{-i\vec{q}\cdot\vec{r}'} d^3r' \;.$">|; 

$key = q/displaystyle;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 0.42ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \;.$">|; 

$key = q/displaystyle=(1-alpha)I_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle = (1-\alpha)I_0$">|; 

$key = q/displaystyle=-frac{omega^2}{omega_0^2-omega^2-igammaomega}frac{r_e}{r}[hat{vec{r}}times(hat{vec{r}}timesvec{E}_0(t_r))];;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.75ex; vertical-align: -2.26ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle = -\frac{\omega^2}{\omega_0^2 - \omega^2 - i \gamma
\omega}\frac{r_e}{r} [\hat{\vec{r}} \times ( \hat{\vec{r}} \times
\vec{E}_0(t_r))] \;$">|; 

$key = q/displaystyle=-frac{partialvec{B}}{partialt};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle = -\frac{\partial\vec{B}}{\partial t}$">|; 

$key = q/displaystyle=-frac{r_e}{r}[hat{vec{r}}times(hat{vec{r}}timesvec{E}_0(t_r))];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle = -\frac{r_e}{r} [\hat{\vec{r}} \times ( \hat{\vec{r}} \times \vec{E}_0(t_r))]$">|; 

$key = q/displaystyle=-mu_0vec{J};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle = -\mu_0 \vec{J} \;.$">|; 

$key = q/displaystyle=-nablaV-frac{partialvec{A}}{partialt};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle = -\nabla V -\frac{\partial \vec{A}}{\partial t} \;.$">|; 

$key = q/displaystyle=-rho(vec{r},t)slashepsilon_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle = -\rho(\vec{r}, t) / \epsilon_0$">|; 

$key = q/displaystyle=-rhoslashepsilon_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle = -\rho / \epsilon_0$">|; 

$key = q/displaystyle=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle = 0$">|; 

$key = q/displaystyle=1+frac{1}{2c}frac{1}{|vec{r}-vec{w}(t')|}frac{d}{dt'}left(vec{r}-vec{w}(t')right)^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.53ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img218.svg"
 ALT="$\displaystyle = 1 + \frac{1}{2c} \frac{1}{ \vert\vec{r} - \vec{w}(t')\vert } \frac{d}{dt'}\left( \vec{r} - \vec{w}(t')\right)^2$">|; 

$key = q/displaystyle=1+frac{1}{2c}frac{1}{|vec{r}-vec{w}(t')|}frac{d}{dt'}left(vec{r}cdotvec{r}+vec{w}(t')cdotvec{w}(t')-2vec{r}cdotvec{w}(t')right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.53ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img219.svg"
 ALT="$\displaystyle = 1 + \frac{1}{2c} \frac{1}{ \vert\vec{r} - \vec{w}(t')\vert } \f...
...cdot \vec{r} + \vec{w}(t')\cdot \vec{w}(t') - 2\vec{r} \cdot \vec{w}(t')\right)$">|; 

$key = q/displaystyle=1+frac{1}{2c}frac{1}{|vec{r}-vec{w}(t')|}left(2vec{w}(t')cdotdot{vec{w}}(t')-2vec{r}cdotdot{vec{w}}(t')right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img220.svg"
 ALT="$\displaystyle = 1 + \frac{1}{2c} \frac{1}{ \vert\vec{r} - \vec{w}(t')\vert } \l...
... 2 \vec{w}(t')\cdot \dot{\vec{w}}(t') - 2\vec{r} \cdot
\dot{\vec{w}}(t')\right)$">|; 

$key = q/displaystyle=1+frac{1}{c}frac{d}{dt'}left(left(vec{r}-vec{w}(t')right)^2right)^{1slash2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.99ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img217.svg"
 ALT="$\displaystyle = 1 + \frac{1}{c} \frac{d}{dt'} \left( \left( \vec{r} - \vec{w}(t')\right)^2 \right)^{1/2}$">|; 

$key = q/displaystyle=1+frac{1}{c}frac{d}{dt'}|vec{r}-vec{w}(t')|;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img216.svg"
 ALT="$\displaystyle = 1 + \frac{1}{c} \frac{d}{dt'} \vert\vec{r} - \vec{w}(t')\vert$">|; 

$key = q/displaystyle=1-alpha(hat{vec{k}}cdothat{vec{E}}_1)^2-(1-alpha)(hat{vec{k}}cdothat{vec{E}}_2)^2;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.99ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle = 1 - \alpha(\hat{\vec{k}} \cdot \hat{\vec{E}}_1 )^2 -
(1-\alpha)(\hat{\vec{k}} \cdot \hat{\vec{E}}_2 )^2 \;.$">|; 

$key = q/displaystyle=1-frac{1}{c}frac{left(vec{r}-vec{w}(t')right)cdotdot{vec{w}}(t')}{|vec{r}-vec{w}(t')|};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img222.svg"
 ALT="$\displaystyle = 1 - \frac{1}{c} \frac{ \left( \vec{r} - \vec{w}(t') \right)\cdot \dot{\vec{w}}(t') }{ \vert\vec{r} - \vec{w}(t')\vert }$">|; 

$key = q/displaystyle=1-frac{1}{c}frac{left(vec{r}cdotdot{vec{w}}(t')-vec{w}(t')cdotdot{vec{w}}(t')right)}{|vec{r}-vec{w}(t')|};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img221.svg"
 ALT="$\displaystyle = 1 - \frac{1}{c} \frac{ \left( \vec{r} \cdot
\dot{\vec{w}}(t') - \vec{w}(t')\cdot \dot{\vec{w}}(t') \right) }{ \vert\vec{r} - \vec{w}(t')\vert }$">|; 

$key = q/displaystyle=2piint_0^inftyrho(r)r^2drfrac{1}{-iqr}int_{iqr}^{-iqr}duexp(u);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.14ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img259.svg"
 ALT="$\displaystyle =2\pi \int_0^\infty \rho(r) r^2 dr \frac{1}{-iqr}\int_{iqr}^{-iqr} du \exp(u)$">|; 

$key = q/displaystyle=2piint_0^inftyrho(r)r^2drfrac{exp(iqr)-exp(-iqr)}{iqr};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.58ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img260.svg"
 ALT="$\displaystyle =2\pi \int_0^\infty \rho(r) r^2 dr \frac{\exp(iqr) - \exp(-iqr)}{iqr}$">|; 

$key = q/displaystyle=2piint_0^inftyrho(r)r^2drint_{-1}^1dcosthetaexp(-iqrcostheta);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.98ex; vertical-align: -2.35ex; " SRC="|."$dir".q|img258.svg"
 ALT="$\displaystyle =2\pi \int_0^\infty \rho(r) r^2 dr \int_{-1}^1 d\cos\theta \exp(-i q r
\cos\theta)$">|; 

$key = q/displaystyle=alphaI_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle =\alpha I_0$">|; 

$key = q/displaystyle=frac{1}{4piepsilon_0}frac{q}{left|{mbox{resizebox{.09in}{.08in}{inc[trim=1em014em0,clip]{fontsslashBoldR}}}}cdotvec{beta}right|};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle = \frac{1}{4\pi\epsilon_0} \frac{q}{\left\vert {\mbox{$\resizebox...
...raphics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}\cdot \vec{\beta} \right\vert}$">|; 

$key = q/displaystyle=frac{1}{4piepsilon_0}intfrac{rho(vec{r}',t_r)}{{mbox{resizebox{.09iludegraphics[trim=1em014em0,clip]{fontsslashScriptR}}}}}d^3r';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.49ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle = \frac{1}{4\pi\epsilon_0}\int \frac{\rho(\vec{r}', t_r)}{{\mbox{...
...09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/ScriptR}}$}}}d^3r'$">|; 

$key = q/displaystyle=frac{1}{c}hat{vec{r}}timesvec{E}_;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle = \frac{1}{c} \hat{\vec{r}} \times \vec{E}_$">|; 

$key = q/displaystyle=frac{1}{mu_0c}E_;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle = \frac{1}{\mu_0c} E_$">|; 

$key = q/displaystyle=frac{delta(x-x')}{|g'(x')|};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img208.svg"
 ALT="$\displaystyle = \frac{\delta(x-x')}{\vert g'(x')\vert}$">|; 

$key = q/displaystyle=frac{mu_0omega^4p^2}{12pic};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle = \frac{\mu_0 \omega^4 p^2}{12\pi c} \; .$">|; 

$key = q/displaystyle=frac{mu_0omega^4p^2}{32pi^2c}|hat{vec{r}}timeshat{vec{p}}|^2dOmega;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle = \frac{\mu_0 \omega^4 p^2}{32\pi^2 c} \vert\hat{\vec{r}}\times\hat{\vec{p}}\vert^2 d\Omega$">|; 

$key = q/displaystyle=frac{mu_0}{4pi}frac{1}{r}hat{vec{r}}times(hat{vec{r}}timesddot{vec{p}}(t_r));MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle = \frac{\mu_0 }{4\pi} \frac{1}{r} \hat{\vec{r}} \times ( \hat{\vec{r}} \times
\ddot{\vec{p}}(t_r))$">|; 

$key = q/displaystyle=frac{mu_0}{4pi}frac{qvec{v}}{left|{mbox{resizebox{.09in}{.08in}{incrim=1em014em0,clip]{fontsslashBoldR}}}}cdotvec{beta}right|};,;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.95ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle = \frac{\mu_0}{4\pi} \frac{q\vec{v}}{\left\vert {\mbox{$\resizebo...
...ics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}\cdot \vec{\beta} \right\vert} \;,$">|; 

$key = q/displaystyle=frac{mu_0}{4pi}intfrac{vec{J}(vec{r}',t_r)}{{mbox{resizebox{.09in}{ludegraphics[trim=1em014em0,clip]{fontsslashScriptR}}}}}d^3r';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.49ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle = \frac{\mu_0}{4\pi}\int \frac{\vec{J}(\vec{r}', t_r)}{{\mbox{$\r...
...09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/ScriptR}}$}}}d^3r'$">|; 

$key = q/displaystyle=frac{q}{4piepsilon_0}frac{1}{|vec{r}-vec{w}(t_r)|}frac{1}{left|1-fr(t_r)right)cdotdot{vec{w}}(t_r)}{|vec{r}-vec{w}(t_r)|}right|};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 7.16ex; vertical-align: -4.02ex; " SRC="|."$dir".q|img228.svg"
 ALT="$\displaystyle = \frac{q}{4\pi\epsilon_0} \frac{ 1 }{\vert\vec{r} - \vec{w}(t_r)...
...ht)\cdot \dot{\vec{w}}(t_r) }{ \vert\vec{r} - \vec{w}(t_r) \vert } \right\vert}$">|; 

$key = q/displaystyle=frac{q}{4piepsilon_0}frac{r}{(vec{r}cdotchat{vec{r}})^3}vec{r}times(chat{vec{r}}timesvec{a});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.92ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle = \frac{q}{4\pi\epsilon_0} \frac{r}{(\vec{r}\cdot c \hat{\vec{r}})^3}
\vec{r} \times (c \hat{\vec{r}} \times \vec{a})$">|; 

$key = q/displaystyle=frac{q}{4piepsilon_0}intfrac{1}{|vec{r}-vec{w}(t')|}frac{delta(t'-t(t')right)cdotdot{vec{w}}(t')}{|vec{r}-vec{w}(t')|}right|}dt';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 7.40ex; vertical-align: -4.02ex; " SRC="|."$dir".q|img227.svg"
 ALT="$\displaystyle = \frac{q}{4\pi\epsilon_0}\int \frac{ 1 }{\vert\vec{r} - \vec{w}(...
...)\cdot \dot{\vec{w}}(t') }{ \vert\vec{r} - \vec{w}(t') \vert } \right\vert} dt'$">|; 

$key = q/displaystyle=g(vec{r});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\displaystyle = g(\vec{r})$">|; 

$key = q/displaystyle=int_0^inftyrho(r)frac{sin(qr)}{qr}4pir^2dr;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.58ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img262.svg"
 ALT="$\displaystyle = \int_0^\infty \rho(r) \frac{\sin(qr)}{qr} 4\pi r^2 dr$">|; 

$key = q/displaystyle=int_0^{2pi}dphiint_0^inftyrho(r)r^2drint_0^pisinthetadthetaexp(-iqrcostheta);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.82ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img257.svg"
 ALT="$\displaystyle = \int_0^{2\pi} d\phi \int_0^\infty \rho(r) r^2 dr \int_0^\pi
\sin\theta d\theta \exp(-i q r \cos\theta)$">|; 

$key = q/displaystyle=int_{-infty}^inftyleft{int_{text{Vol}}frac{rho_omega(vec{r}')}{4pie{r}-vec{r}'|}}{|vec{r}-vec{r}'|}d^3r'right}e^{-iomegat}domega;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.04ex; vertical-align: -2.35ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\displaystyle = \int_{-\infty}^\infty \left\{\int_{\text{Vol}} \frac{\rho_\omeg...
...ec{r}'\vert}}{ \vert\vec{r}-\vec{r}'\vert} d^3 r' \right\}e^{-i\omega t}d\omega$">|; 

$key = q/displaystyle=int_{-infty}^{infty}V_{omega}(vec{r})e^{-iomegat}domega;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.65ex; vertical-align: -2.35ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle = \int_{-\infty}^{\infty} V_{\omega}(\vec{r})e^{- i\omega t} d\omega$">|; 

$key = q/displaystyle=int_{-infty}^{infty}rho_{omega}(vec{r})e^{-iomegat}domega;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.65ex; vertical-align: -2.35ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle = \int_{-\infty}^{\infty} \rho_{\omega}(\vec{r})e^{- i\omega t} d\omega \;.$">|; 

$key = q/displaystyle=int_{text{Vol}}g(vec{r}')delta(vec{r}-vec{r}')d^3r';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\displaystyle = \int_{\text{Vol}} g(\vec{r}') \delta(\vec{r} - \vec{r}') d^3 r'$">|; 

$key = q/displaystyle=int_{text{Vol}}g(vec{r}')mathcal{L}_{vec{r}}G(vec{r},vec{r}')d^3r';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle = \int_{\text{Vol}} g(\vec{r}') \mathcal{L}_{\vec{r}} G(\vec{r}, \vec{r}') d^3 r'$">|; 

$key = q/displaystyle=int_{text{Vol}}left{int_{-infty}^inftyrho_omega(vec{r}')e^{-iomega(garight}frac{1}{4piepsilon_0}frac{1}{|vec{r}-vec{r}'|}d^3r';.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.80ex; vertical-align: -2.35ex; " SRC="|."$dir".q|img194.svg"
 ALT="$\displaystyle =\int_{\text{Vol}} \left\{ \int_{-\infty}^\infty\rho_\omega(\vec{...
...ght\}\frac{1}{4\pi \epsilon_0} \frac{1}{ \vert\vec{r}-\vec{r}'\vert} d^3 r' \;.$">|; 

$key = q/displaystyle=mathcal{L}_{vec{r}}int_{text{Vol}}g(vec{r}')G(vec{r},vec{r}')d^3r';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle = \mathcal{L}_{\vec{r}} \int_{\text{Vol}} g(\vec{r}')G(\vec{r}, \vec{r}')d^3 r'$">|; 

$key = q/displaystyle=mu_0vec{J}+mu_0epsilon_0frac{partialvec{E}}{partialt};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle = \mu_0 \vec{J}+\mu_0 \epsilon_0 \frac{\partial\vec{E}}{\partial t}\;.$">|; 

$key = q/displaystyle=nablatimesvec{A};,;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle = \nabla \times \vec{A}\;,$">|; 

$key = q/displaystyle=qdelta(vec{r}-vec{w}(t));MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img200.svg"
 ALT="$\displaystyle = q \delta(\vec{r} - \vec{w}(t))$">|; 

$key = q/displaystyle=qintdelta(vec{r}-vec{w}(t'))delta(t'-t_r(t,vec{r}'))dt';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.34ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\displaystyle = q \int \delta(\vec{r} - \vec{w}(t')) \delta(t' - t_r(t, \vec{r}')) dt'$">|; 

$key = q/displaystyle=r_e^2frac{1}{r^2}(I_1|hat{vec{r}}timeshat{vec{E}}_1|^2+I_2|hat{vec{r}}timeshat{vec{E}}_2|^2)hat{vec{r}};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle = r_e^2 \frac{1}{r^2} (I_1 \vert \hat{\vec{r}} \times \hat{\vec{E...
...t^2 + I_2 \vert \hat{\vec{r}} \times \hat{\vec{E}}_2 \vert^2) \hat{\vec{r}} \;.$">|; 

$key = q/displaystyle=rhoslashepsilon_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle = \rho/\epsilon_0$">|; 

$key = q/displaystyle=sum_n^Nrho_n^{(0)}(|vec{r}-vec{r}_n|)+delta(vec{r}-vec{r}_n)Deltaf_n(E);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 7.26ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle = \sum_n^N \rho_n^{(0)}(\vert\vec{r}-\vec{r}_n\vert) +
\delta(\vec{r}-\vec{r}_n) \Delta f_n(E) \; .$">|; 

$key = q/displaystyleA=-frac{1}{4pi};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\displaystyle A = -\frac{1}{4\pi} \;.$">|; 

$key = q/displaystyleAint_{text{sphere}}nabla^2frac{1}{R}d^3R+Ak^2int_text{sphere}frac{1}{R}4piR^2dR=1;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.73ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\displaystyle A \int_{\text{sphere}}\nabla^2 \frac{1}{R} d^3R + A k^2 \int_\text{sphere} \frac{1}{R} 4\pi R^2dR = 1 \;.$">|; 

$key = q/displaystyleF(vec{q})=intrho(vec{r}')e^{-ivec{q}cdotvec{r}'}d^3r';.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.34ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\displaystyle F(\vec{q}) = \int \rho(\vec{r}') e^{-i\vec{q}\cdot\vec{r}'} d^3r' \;.$">|; 

$key = q/displaystyleF(vec{q})=sum_nf_n(q)e^{-ivec{q}cdotvec{r}_n};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.52ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle F(\vec{q}) = \sum_n f_n(q)
e^{-i \vec{q}\cdot\vec{r}_n} \; .$">|; 

$key = q/displaystyleG(R)=Afrac{e^{ikR}}{R}+Bfrac{e^{-ikR}}{R};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.26ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img182.svg"
 ALT="$\displaystyle G(R) = A \frac{e^{ikR}}{R} + B \frac{e^{-ikR}}{R} \;.$">|; 

$key = q/displaystyleG(vec{r},vec{r}')=-frac{e^{ik|vec{r}-vec{r}'|}}{4pi|vec{r}-vec{r}'|};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.95ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img191.svg"
 ALT="$\displaystyle G(\vec{r}, \vec{r}') = - \frac{e^{ik\vert\vec{r}-\vec{r}'\vert}}{4\pi
\vert\vec{r}-\vec{r}'\vert} \;.$">|; 

$key = q/displaystyleI(vec{q})=J_0r_e^2DeltaOmegamathcal{P}(vec{q})left|intrho(vec{r})e^{-ivec{q}cdotvec{r}}d^3rright|^2;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\displaystyle I(\vec{q}) = J_0 r_e^2 \Delta \Omega \mathcal{P}(\vec{q}) \left\vert \int
\rho(\vec{r}) e^{-i \vec{q}\cdot\vec{r}} d^3 r \right\vert^2 \;.$">|; 

$key = q/displaystyleI(vec{q})=J_0r_e^2mathcal{P}(vec{q})DeltaOmega;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle I(\vec{q}) = J_0 r_e^2 \mathcal{P}(\vec{q}) \Delta \Omega$">|; 

$key = q/displaystyleI(vec{q})=J_0r_e^2mathcal{P}(vec{q})left|intrho(vec{r}')e^{-ivec{q}cdotvec{r}'}d^3r'right|^2DeltaOmega;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle I(\vec{q}) = J_0 r_e^2 \mathcal{P}(\vec{q})
\left\vert \int \rho(\vec{r}') e^{-i\vec{q}\cdot\vec{r}'} d^3r' \right\vert^2 \Delta \Omega \;.$">|; 

$key = q/displaystyleI(vec{q})=leftlanglevec{S}(vec{r})rightranglecdotdvec{a}=r_e^2J_0|hat|intrho(vec{r}')e^{-ivec{q}cdotvec{r}'}d^3r'right|^2dOmega;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle I(\vec{q}) = \left\langle \vec{S}(\vec{r}) \right\rangle \cdot d\...
...t \int \rho(\vec{r}') e^{-i\vec{q}\cdot\vec{r}'} d^3r'
\right\vert^2 d\Omega\;.$">|; 

$key = q/displaystyleI_0=I_1+I_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle I_0 = I_1 + I_2$">|; 

$key = q/displaystyleI_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle I_1$">|; 

$key = q/displaystyleI_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle I_2$">|; 

$key = q/displaystyleP=frac{8}{3}pir_e^2I_0=sigma_TI_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle P = \frac{8}{3}\pi r_e^2 I_0 = \sigma_T I_0$">|; 

$key = q/displaystyleP=frac{mu_0ddot{p}^2}{6pic};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle P = \frac{\mu_0 \ddot{p}^2}{6\pi c} \; .$">|; 

$key = q/displaystyleP=intfrac{dP}{dOmega}dOmega=frac{mu_0}{16pi^2c}int|hat{vec{r}}timesddot{vec{p}}|^2dOmega;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.36ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle P = \int \frac{dP}{d\Omega} d\Omega = \frac{\mu_0 }{16\pi^2 c} \int \vert\hat{\vec{r}} \times \ddot{\vec{p}}\vert^2 d\Omega \; .$">|; 

$key = q/displaystyleRe{vec{E}}=frac{1}{2}(vec{E}+vec{E}^*);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img237.svg"
 ALT="$\displaystyle \Re\{\vec{E}\} = \frac{1}{2} (\vec{E} + \vec{E}^*)$">|; 

$key = q/displaystyleV(vec{r})=int_{text{Vol}}g(vec{r}')G(vec{r},vec{r}')d^3r';.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\displaystyle V(\vec{r}) = \int_{\text{Vol}} g(\vec{r}')G(\vec{r}, \vec{r}') d^3 r'\;.$">|; 

$key = q/displaystyleV(vec{r},t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle V(\vec{r}, t)$">|; 

$key = q/displaystyleV(vec{r},t)=frac{1}{4piepsilon_0}frac{q}{left|{mbox{resizebox{.09in}[trim=1em014em0,clip]{fontsslashBoldR}}}}cdotvec{beta}right|};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img232.svg"
 ALT="$\displaystyle V(\vec{r}, t) = \frac{1}{4\pi\epsilon_0} \frac{q}{\left\vert {\mb...
...raphics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}\cdot \vec{\beta} \right\vert}$">|; 

$key = q/displaystyleV(vec{r},t)=frac{1}{4piepsilon_0}intfrac{rho(vec{r}',t_r)}{|vec{r}-vec{r}'|}d^3r';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img198.svg"
 ALT="$\displaystyle V(\vec{r}, t) = \frac{1}{4\pi\epsilon_0}\int \frac{\rho(\vec{r}', t_r)}{\vert\vec{r} - \vec{r}'\vert} d^3r'$">|; 

$key = q/displaystyleV(vec{r},t)=frac{q}{4piepsilon_0}iintfrac{delta(vec{r}'-vec{w}(t'))delta(t'-t_r(t,vec{r}'))}{|vec{r}-vec{r}'|}d^3r'dt';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle V(\vec{r}, t) = \frac{q}{4\pi\epsilon_0}\iint \frac{ \delta(\vec{...
...w}(t')) \delta(t' - t_r(t, \vec{r}')) }{\vert\vec{r} - \vec{r}'\vert} d^3r' dt'$">|; 

$key = q/displaystyleV(vec{r},t)=frac{q}{4piepsilon_0}intfrac{delta(t'-t_r(t,vec{w}(t'));)}{|vec{r}-vec{w}(t')|}dt';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img206.svg"
 ALT="$\displaystyle V(\vec{r}, t) = \frac{q}{4\pi\epsilon_0}\int \frac{ \delta(t' - t_r(t, \vec{w}(t') )\;) }{\vert\vec{r} - \vec{w}(t')\vert} dt'$">|; 

$key = q/displaystyleV_omega(vec{r})=int_{text{Vol}}frac{rho_omega(vec{r}')}{4piepsilon_0}frac{e^{ik|vec{r}-vec{r}'|}}{|vec{r}-vec{r}'|}d^3r';.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.95ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img192.svg"
 ALT="$\displaystyle V_\omega(\vec{r}) = \int_{\text{Vol}} \frac{\rho_\omega(\vec{r}')...
... \frac{e^{ik\vert\vec{r}-\vec{r}'\vert}}{ \vert\vec{r}-\vec{r}'\vert} d^3 r'\;.$">|; 

$key = q/displaystyle^2hat{vec{r}}=frac{mu_0}{16pi^2cr^2}|hat{vec{r}}timesddot{vec{p}}|^2hat{vec{r}};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle ^2\hat{\vec{r}}
= \frac{\mu_0}{16 \pi^2 cr^2} \vert\hat{\vec{r}} \times \ddot{\vec{p}}\vert^2\hat{\vec{r}} \;.$">|; 

$key = q/displaystyledP=vec{S}cdotdvec{a}=frac{mu_0}{16pi^2c}|hat{vec{r}}timesddot{vec{p}}|^2dOmega;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle dP = \vec{S}\cdot d\vec{a} = \frac{\mu_0 }{16\pi^2 c} \vert\hat{\vec{r}} \times \ddot{\vec{p}}\vert^2 d\Omega \; .$">|; 

$key = q/displaystyleddot{vec{p}}=-evec{a}=frac{e^2}{m_e}vec{E}_0(t);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.53ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \ddot{\vec{p}} = -e \vec{a} = \frac{e^2}{m_e} \vec{E}_0(t) \;.$">|; 

$key = q/displaystyledelta(g(x));MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img207.svg"
 ALT="$\displaystyle \delta(g(x))$">|; 

$key = q/displaystyledelta(t'-t_r(t,vec{w}(t'));)=frac{delta(t'-t^*)}{left|frac{d}{dt'}left(t-frac{|vec{r}-vec{w}(t')|}{c}right)right)_{t'=t^*}right|};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 7.50ex; vertical-align: -4.12ex; " SRC="|."$dir".q|img214.svg"
 ALT="$\displaystyle \delta(t' - t_r(t, \vec{w}(t') )\;) = \frac{\delta(t' - t^*)}{\le...
...rac{\vert\vec{r} - \vec{w}(t')\vert}{c}\right) \right)_{t' = t^*} \right \vert}$">|; 

$key = q/displaystyleequivfrac{d}{dx}g(x);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img213.svg"
 ALT="$\displaystyle \equiv \frac{d }{dx}g(x) \;.$">|; 

$key = q/displaystylef(q);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img261.svg"
 ALT="$\displaystyle f(q)$">|; 

$key = q/displaystylef(vec{q});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img256.svg"
 ALT="$\displaystyle f(\vec{q})$">|; 

$key = q/displaystylef(vec{q})=intrho(r)exp(-ivec{q}cdotvec{r});d^3r;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.34ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img251.svg"
 ALT="$\displaystyle f(\vec{q}) = \int \rho(r) \exp(-i \vec{q}\cdot\vec{r}) \; d^3r \; .$">|; 

$key = q/displaystylef_n(q)=f_n^{(0)}(q)+Deltaf_n(E)=f_n^{(0)}(q)+f_n'(E)+if_n''(E);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.88ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle f_n(q) = f_n^{(0)}(q) + \Delta f_n(E) = f_n^{(0)}(q) + f_n'(E) + i f_n''(E) \;.$">|; 

$key = q/displaystylef_n^{(0)}(q)=int_{-infty}^inftyrho_n^{(0)}(r)exp(-ivec{q}cdotvec{r});d^3r=int_0^inftyrho_n^{(0)}(r)frac{sin(qr)}{qr}4pir^2dr;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.74ex; vertical-align: -2.35ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle f_n^{(0)}(q) = \int_{-\infty}^\infty \rho_n^{(0)}(r) \exp(-i
\vec...
...}) \; d^3r = \int_0^\infty \rho_n^{(0)}(r)
\frac{\sin(qr)}{qr} 4\pi r^2 dr \; .$">|; 

$key = q/displaystylef_n^{(0)}(vec{q})=int_{-infty}^inftyrho_n(|vec{r}-vec{r}_n|)exp(-ivec{q}cdotvec{r})d^3r=f_n^{(0)}(q)exp(-ivec{q}cdotvec{r}_n);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.65ex; vertical-align: -2.35ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle f_n^{(0)}(\vec{q}) = \int_{-\infty}^\infty \rho_n(\vert\vec{r}-\v...
...-i
\vec{q}\cdot\vec{r}) d^3r = f_n^{(0)}(q)
\exp(-i \vec{q}\cdot\vec{r}_n) \; .$">|; 

$key = q/displaystylefrac{d}{dt'}left(t'-left(t-frac{|vec{r}-vec{w}(t')|}{c}right)right);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img215.svg"
 ALT="$\displaystyle \frac{d}{dt'} \left( t' - \left(t - \frac{\vert\vec{r} - \vec{w}(t')\vert}{c}\right) \right)$">|; 

$key = q/displaystylefrac{partial}{partialR}[RG(R)]=-k^2[RG(R)];MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\displaystyle \frac{\partial}{\partial R}[RG(R)] =- k^2 [R G(R)]$">|; 

$key = q/displaystyleg'(x);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.58ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img212.svg"
 ALT="$\displaystyle g'(x)$">|; 

$key = q/displaystyleg(x');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.58ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img210.svg"
 ALT="$\displaystyle g(x')$">|; 

$key = q/displaystylehat{vec{r}}=hat{vec{k}}=frac{vec{q}-vec{k}_0}{left|vec{q}-vec{k}_0right|};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.53ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \hat{\vec{r}} = \hat{\vec{k}} = \frac{\vec{q} - \vec{k}_0}{\left\vert \vec{q} - \vec{k}_0 \right\vert} \;.$">|; 

$key = q/displaystyleint_{text{sphere}}nabla^2G(R)d^3R+k^2int_text{sphere}G(R)d^3R=1;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.73ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img185.svg"
 ALT="$\displaystyle \int_{\text{sphere}}\nabla^2 G(R) d^3R + k^2 \int_\text{sphere} G(R) d^3R = 1 \;.$">|; 

$key = q/displaystylelanglePrangle;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \langle P \rangle$">|; 

$key = q/displaystyleleft(frac{1}{R}frac{partial}{partialR}R+k^2right)G(R)=delta(R);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle \left(\frac{1}{R}\frac{\partial}{\partial R}R + k^2\right)G(R) = \delta(R) \;.$">|; 

$key = q/displaystyleleftlangledPrightrangle;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \left\langle dP \right\rangle$">|; 

$key = q/displaystyleleftlangledPrightrangle=I_0r_e^2(alpha|hat{vec{r}}timeshat{vec{E}}_1|^2+(1-alpha)|hat{vec{r}}timeshat{vec{E}}_2|^2)dOmega;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle \left\langle dP \right\rangle = I_0 r_e^2 (\alpha \vert \hat{\vec...
...t^2 + (1-\alpha)
\vert \hat{\vec{r}} \times \hat{\vec{E}}_2 \vert^2) d\Omega\;.$">|; 

$key = q/displaystyleleftlangledPrightrangle=frac{r_e^2E_0^2}{2mu_0c}|hat{vec{r}}timeshat_0|^2dOmega=I_0r_e^2|hat{vec{r}}timeshat{vec{E}}_0|^2dOmega;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.63ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \left\langle dP \right\rangle = \frac{r_e^2 E_0^2}{ 2 \mu_0 c} \v...
...d\Omega
= I_0 r_e^2 \vert\hat{\vec{r}}\times\hat{\vec{E}}_0\vert^2 d\Omega \; .$">|; 

$key = q/displaystyleleftlanglevec{S}(vec{r})rightrangle;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \left\langle \vec{S}(\vec{r}) \right\rangle$">|; 

$key = q/displaystyleleftlanglevec{S}(vec{r})rightrangle=frac{1}{mu_0}leftlangleRe{vec{E}rho(vec{r}')e^{-ivec{q}cdotvec{r}'}d^3r'right|^2hat{vec{r}};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \left\langle \vec{S}(\vec{r}) \right\rangle = \frac{1}{\mu_0}\lef...
...\rho(\vec{r}')
e^{-i\vec{q}\cdot\vec{r}'} d^3r' \right\vert^2 \hat{\vec{r}} \;.$">|; 

$key = q/displaystyleleftlanglevec{S}rightrangle=frac{1}{2cmu_0}(left|vec{E}_1right|^2+left|vec{E}_2right|^2)hat{vec{k}};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img250.svg"
 ALT="$\displaystyle \left\langle \vec{S} \right\rangle = \frac{1}{2c\mu_0} ( \left\vert \vec{E}_1 \right\vert^2 + \left\vert \vec{E}_2 \right\vert^2)\hat{\vec{k}}\;.$">|; 

$key = q/displaystyleleftlanglevec{S}rightrangle=frac{1}{2cmu_0}Re{(vec{E}_1+vec{E}_2)times(hat{vec{k}}times(vec{E}_1+vec{E}_2)^*)};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img248.svg"
 ALT="$\displaystyle \left\langle \vec{S} \right\rangle = \frac{1}{2c\mu_0}\Re\{ (\vec{E}_1 + \vec{E}_2)\times(\hat{\vec{k}}\times(\vec{E}_1 + \vec{E}_2)^*)\} \;.$">|; 

$key = q/displaystyleleftlanglevec{S}rightrangle=frac{1}{2cmu_0}Re{vec{E}times(hat{vec{k}}^*)}=frac{1}{2cmu_0}left|Eright|^2hat{vec{k}}=Ihat{vec{k}};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img244.svg"
 ALT="$\displaystyle \left\langle \vec{S} \right\rangle = \frac{1}{2c\mu_0}\Re\{ \vec{...
...\frac{1}{2c\mu_0} \left\vert E \right\vert^2 \hat{\vec{k}} = I\hat{\vec{k}} \;.$">|; 

$key = q/displaystyleleftlanglevec{S}rightrangle=frac{1}{T}int_0^{T}frac{1}{mu_0}Re{vec{E}}timesRe{vec{B}}dt;,;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.88ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img236.svg"
 ALT="$\displaystyle \left\langle \vec{S} \right\rangle = \frac{1}{T}\int_0^{T}\frac{1}{\mu_0} \Re\{ \vec{E} \}\times \Re\{ \vec{B} \} dt \;,$">|; 

$key = q/displaystyleleftlanglevec{S}rightrangle=frac{1}{T}int_0^{T}frac{1}{mu_0}frac{1}{2}(vec{E}+vec{E}^*)timesfrac{1}{2}(vec{B}+vec{B}^*)dt;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.88ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img238.svg"
 ALT="$\displaystyle \left\langle \vec{S} \right\rangle = \frac{1}{T}\int_0^{T} \frac{...
...frac{1}{2} (\vec{E} + \vec{E}^*)\times \frac{1}{2} (\vec{B} + \vec{B}^*) dt \;.$">|; 

$key = q/displaystyleleftlanglevec{S}rightrangle=frac{1}{mu_0}frac{1}{4}(vec{E}timesvec{B+vec{E}^*timesvec{B})=frac{1}{2mu_0}Re{vec{E}timesvec{B}^*};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img242.svg"
 ALT="$\displaystyle \left\langle \vec{S} \right\rangle = \frac{1}{\mu_0} \frac{1}{4} ...
...* + \vec{E}^*\times\vec{B}) = \frac{1}{2\mu_0}\Re\{ \vec{E}\times\vec{B}^*\}\;.$">|; 

$key = q/displaystylem_efrac{d^2vec{x}}{dt^2}+m_egammafrac{dvec{x}}{dt}+m_eomega_0^2vec{x}=evec{E}_0exp(-iomegat);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle m_e \frac{d^2\vec{x}}{dt^2} + m_e \gamma \frac{d\vec{x}}{dt} + m_e\omega_0^2
\vec{x} = e \vec{E}_0 \exp(-i\omega t) \;.$">|; 

$key = q/displaystylemathcal{L}_{vec{r}}G(vec{r},vec{r}')=delta(vec{r}-vec{r}');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.58ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle \mathcal{L}_{\vec{r}} G(\vec{r}, \vec{r}') = \delta(\vec{r}-\vec{r}')$">|; 

$key = q/displaystylemathcal{L}_{vec{r}}V(vec{r});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img163.svg"
 ALT="$\displaystyle \mathcal{L}_{\vec{r}} V(\vec{r})$">|; 

$key = q/displaystylemathcal{L}_{vec{r}}V(vec{r})=g(vec{r});.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \mathcal{L}_{\vec{r}} V(\vec{r}) = g(\vec{r}) \;.$">|; 

$key = q/displaystylemathcal{P}(vec{q});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \mathcal{P}(\vec{q})$">|; 

$key = q/displaystylemathcal{P}(vec{q})=alpha|hat{vec{k}}timeshat{vec{E}}_1|^2+(1-alpha)|hat{vec{k}}timeshat{vec{E}}_2|^2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.99ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle \mathcal{P}(\vec{q}) = \alpha \vert \hat{\vec{k}} \times \hat{\vec{E}}_1 \vert^2 + (1-\alpha) \vert
\hat{\vec{k}} \times \hat{\vec{E}}_2 \vert^2$">|; 

$key = q/displaystylen(vec{r})approx1-frac{1}{2pi}r_erho(vec{r})lambda^2;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle n(\vec{r}) \approx 1 - \frac{1}{2\pi} r_e \rho(\vec{r}) \lambda^2 \;.$">|; 

$key = q/displaystylen=sqrt{1+Nfrac{e^2slashm_eepsilon_0}{omega_0^2-omega^2-igammaomega}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 7.23ex; vertical-align: -2.79ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle n = \sqrt{1+N \frac{e^2/m_e\epsilon_0}{\omega_0^2 - \omega^2 - i
\gamma \omega}}$">|; 

$key = q/displaystylenabla^2V(vec{r},t)-mu_0epsilon_0frac{partial^2V(vec{r},t)}{partialt^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle \nabla^2 V(\vec{r}, t) - \mu_0\epsilon_0 \frac{\partial^2 V(\vec{r}, t)}{\partial t^2}$">|; 

$key = q/displaystylenabla^2V-mu_0epsilon_0frac{partial^2V}{partialt^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \nabla^2 V - \mu_0\epsilon_0 \frac{\partial^2 V}{\partial t^2}$">|; 

$key = q/displaystylenabla^2vec{A}-mu_0epsilon_0frac{partial^2vec{A}}{partialt^2};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \nabla^2 \vec{A} - \mu_0\epsilon_0 \frac{\partial^2 \vec{A}}{\partial t^2}$">|; 

$key = q/displaystylenablacdotvec{A}=-mu_0epsilon_0frac{partialV}{partialt};,;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \nabla \cdot \vec{A} = -\mu_0 \epsilon_0 \frac{\partial V}{\partial t} \;,$">|; 

$key = q/displaystylenablacdotvec{B};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \nabla \cdot \vec{B}$">|; 

$key = q/displaystylenablacdotvec{E};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \nabla \cdot \vec{E}$">|; 

$key = q/displaystylenablatimesvec{B};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \nabla\times\vec{B}$">|; 

$key = q/displaystylenablatimesvec{E};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle  \nabla\times\vec{E}$">|; 

$key = q/displaystylenapprox1-frac{1}{2pi}r_eNlambda^2;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle n \approx 1 - \frac{1}{2\pi} r_e N \lambda^2 \;.$">|; 

$key = q/displaystylenapprox1-frac{Ne^2}{2mepsilon_0omega^2};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.53ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle n \approx 1 - \frac{N e^2}{2 m \epsilon_0 \omega^2} \;.$">|; 

$key = q/displaystyler_e=frac{1}{4piepsilon_0}frac{e^2}{m_ec^2}approx2.8times10^{-15};;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.53ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle r_e = \frac{1}{4\pi \epsilon_0} \frac{e^2}{m_e c^2} \approx 2.8\times10^{-15} \;$">|; 

$key = q/displaystylerho(vec{r});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle \rho(\vec{r})$">|; 

$key = q/displaystylerho(vec{r},t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle \rho(\vec{r}, t)$">|; 

$key = q/displaystylet'-left(t-frac{|vec{r}-vec{w}(t')|}{c}right)=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img224.svg"
 ALT="$\displaystyle t' - \left(t - \frac{\vert\vec{r} - \vec{w}(t')\vert}{c}\right) = 0$">|; 

$key = q/displaystylet_r(t,vec{r}')equivt-|vec{r}-vec{r}'|slashc;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.58ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\displaystyle t_r(t, \vec{r}') \equiv t - \vert\vec{r}-\vec{r}'\vert/c$">|; 

$key = q/displaystylet_r=t-frac{|vec{r}-vec{w}(t_r)|}{c};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img225.svg"
 ALT="$\displaystyle t_r = t - \frac{\vert\vec{r} - \vec{w}(t_r)\vert}{c}$">|; 

$key = q/displaystylet_requivt-{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashScriptR}}}}slashc;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img195.svg"
 ALT="$\displaystyle t_r \equiv t - {\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/ScriptR}}$}}/ c \;.$">|; 

$key = q/displaystyletimesvec{B}_;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.13ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \times\vec{B}_$">|; 

$key = q/displaystylevec{A}(vec{r},t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \vec{A}(\vec{r}, t)$">|; 

$key = q/displaystylevec{A}(vec{r},t)=frac{1}{4piepsilon_0}frac{qvec{v}}{left|{mbox{resizrim=1em014em0,clip]{fontsslashBoldR}}}}cdotvec{beta}right|};.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img233.svg"
 ALT="$\displaystyle \vec{A}(\vec{r}, t) = \frac{1}{4\pi\epsilon_0} \frac{q\vec{v}}{\l...
...ics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}\cdot \vec{\beta} \right\vert} \;.$">|; 

$key = q/displaystylevec{B};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \vec{B}$">|; 

$key = q/displaystylevec{B}_;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.13ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \vec{B}_$">|; 

$key = q/displaystylevec{E}(vecr,t)=vec{E}_;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \vec{E}(\vec r, t) = \vec{E}_$">|; 

$key = q/displaystylevec{E}(vec{r},t)=frac{q}{4piepsilon_0}frac{{mbox{resizebox{.09in}{.0im=1em014em0,clip]{fontsslashBoldR}}}}}}timesvec{E}(vec{r},t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \vec{E}(\vec{r}, t)= \frac{q}{4\pi\epsilon_0} \frac{{\mbox{$\resi...
...graphics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}$}}\times \vec{E}(\vec{r}, t)$">|; 

$key = q/displaystylevec{E};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \vec{E}$">|; 

$key = q/displaystylevec{E}_0(vec{r},t)=vec{E}_0exp(ivec{k}_0cdotvec{r}-iomegat);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle \vec{E}_0(\vec{r},t) = \vec{E}_0 \exp(i\vec{k}_0 \cdot \vec{r} - i \omega t)$">|; 

$key = q/displaystylevec{E}_0=vec{E}_1+e^{iphi}vec{E}_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.57ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \vec{E}_0 = \vec{E}_1 + e^{i\phi}\vec{E}_2$">|; 

$key = q/displaystylevec{E}_1times(hat{vec{k}}timesvec{E}_2^*)=0;.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.99ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img249.svg"
 ALT="$\displaystyle \vec{E}_1 \times(\hat{\vec{k}}\times \vec{E}_2^*) = 0 \;.$">|; 

$key = q/displaystylevec{E}_;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.13ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \vec{E}_$">|; 

$key = q/displaystylevec{P}(t)=Nfrac{e^2slashm_e}{omega_0^2-omega^2-igammaomega}vec{E}_0exp(-iomegat);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.75ex; vertical-align: -2.26ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \vec{P}(t) = N \frac{e^2/m_e}{\omega_0^2 - \omega^2 - i \gamma \omega}\vec{E}_0
\exp(-i\omega t) \;.$">|; 

$key = q/displaystylevec{S}=frac{1}{mu_0}vec{E}_;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \vec{S} = \frac{1}{\mu_0} \vec{E}_$">|; 

$key = q/displaystylevec{a}(t)=-frac{e}{m_e}vec{E}_0(t);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 4.69ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \vec{a}(t) = -\frac{e}{m_e} \vec{E}_0(t) \;.$">|; 

$key = q/displaystylevec{p}(t)=evec{x}(t)=frac{e^2slashm_e}{omega_0^2-omega^2-igammaomega}vec{E}_0exp(-iomegat);.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.75ex; vertical-align: -2.26ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \vec{p}(t) = e \vec{x}(t) = \frac{e^2/m_e}{\omega_0^2 - \omega^2 - i \gamma
\omega}\vec{E}_0 \exp(-i\omega t) \;.$">|; 

$key = q/displaystyle{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{f{r^2+r'^2-2vec{r}cdotvec{r}'}approxr-hat{vec{r}}cdotvec{r}';.;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 3.06ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle {\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14e...
...r^2 + r'^2 -2\vec{r}\cdot
\vec{r}'} \approx r - \hat{\vec{r}}\cdot \vec{r}' \;.$">|; 

$key = q/dot{vec{w}}(t_r)=vec{v};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img229.svg"
 ALT="$\dot{\vec{w}}(t_r) = \vec{v}$">|; 

$key = q/dvec{a}=r^2dOmegahat{vec{r}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img122.svg"
 ALT="$d\vec{a} = r^2 d\Omega \hat{\vec{r}}$">|; 

$key = q/dvec{a}=r^2sinthetadthetadphihat{vec{r}}=r^2dOmegahat{vec{r}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img66.svg"
 ALT="$d\vec{a} = r^2 \sin\theta d\theta d\phi \hat{\vec{r}} = r^2 d\Omega \hat{\vec{r}}$">|; 

$key = q/e;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$e$">|; 

$key = q/e^{-2iomegat};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img241.svg"
 ALT="$e^{-2i\omega t}$">|; 

$key = q/epsilon=epsilon_0(1+chi_e);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\epsilon = \epsilon_0 (1+\chi_e)$">|; 

$key = q/g'(x);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img211.svg"
 ALT="$g'(x)$">|; 

$key = q/hat{vec{E}}_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.74ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\hat{\vec{E}}_0$">|; 

$key = q/hat{vec{E}}_2=hat{vec{k}}_0timeshat{vec{E}}_1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.76ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\hat{\vec{E}}_2 = \hat{\vec{k}}_0 \times \hat{\vec{E}}_1$">|; 

$key = q/k=sqrt{mu_0epsilon_0}omega;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.60ex; vertical-align: -0.88ex; " SRC="|."$dir".q|img172.svg"
 ALT="$k = \sqrt{\mu_0\epsilon_0}\omega$">|; 

$key = q/lambda;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\lambda$">|; 

$key = q/m_e;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img77.svg"
 ALT="$m_e$">|; 

$key = q/mathcal{L}_{vec{r}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\mathcal{L}_{\vec{r}}$">|; 

$key = q/mathcal{P}(vec{q});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\mathcal{P}(\vec{q})$">|; 

$key = q/mathcal{P}=|hat{vec{r}}timeshat{vec{E}}_0|^2=sin^2theta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\mathcal{P}=\vert \hat{\vec{r}}
\times \hat{\vec{E}}_0 \vert^2=\sin^2\theta$">|; 

$key = q/n=sqrt{epsilonslashepsilon_0};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 3.06ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img144.svg"
 ALT="$n =
\sqrt{\epsilon/\epsilon_0}$">|; 

$key = q/nabla^2(1slashR)=-4pidelta(R);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img189.svg"
 ALT="$\nabla^2 (1/R) = -4\pi \delta(R)$">|; 

$key = q/omega;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\omega$">|; 

$key = q/q;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img254.svg"
 ALT="$q$">|; 

$key = q/r;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img255.svg"
 ALT="$r$">|; 

$key = q/r_0omegaslashc=2pir_0slashlambdall1;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$r_0\omega/c = 2\pi r_0/\lambda \ll 1 $">|; 

$key = q/r_e;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img147.svg"
 ALT="$r_e$">|; 

$key = q/r_e=2.818times10^{-15};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.37ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img6.svg"
 ALT="$r_e = 2.818 \times 10^{-15}$">|; 

$key = q/rggr';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.98ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img51.svg"
 ALT="$r \gg r'$">|; 

$key = q/rho(vec{r});MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\rho(\vec{r})$">|; 

$key = q/rho(vec{r},t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\rho(\vec{r}, t)$">|; 

$key = q/rho_n^{(0)}(r);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 3.11ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\rho_n^{(0)}(r)$">|; 

$key = q/sigma_T;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\sigma_T$">|; 

$key = q/t';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.89ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img204.svg"
 ALT="$t'$">|; 

$key = q/t;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img203.svg"
 ALT="$t$">|; 

$key = q/t^*;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img223.svg"
 ALT="$t^*$">|; 

$key = q/t^*=t_r;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.10ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img226.svg"
 ALT="$t^* = t_r$">|; 

$key = q/t_r(t,vec{r}');MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img205.svg"
 ALT="$t_r(t, \vec{r}')$">|; 

$key = q/t_r=t-{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashScriptR}}}}slashc;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$t_r = t -{\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/ScriptR}}$}}/c$">|; 

$key = q/t_rapproxt-rslashc+hat{vec{r}}cdotvecr';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$t_r \approx t - r/c + \hat{\vec{r}} \cdot \vec r'$">|; 

$key = q/vecr;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\vec r$">|; 

$key = q/vec{A};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\vec{A}$">|; 

$key = q/vec{B}(t)=vec{B}_0e^{-iomegat}=frac{1}{c}hat{vec{k}}timesvec{E}_0e^{-iomegat};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 3.21ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img235.svg"
 ALT="$\vec{B}(t)=\vec{B}_0 e^{-i\omega t}=\frac{1}{c} \hat{\vec{k}}\times \vec{E}_0e^{-i\omega t}$">|; 

$key = q/vec{B}=frac{1}{c}hat{vec{k}}timesvec{E};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 3.21ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img243.svg"
 ALT="$\vec{B}=\frac{1}{c}\hat{\vec{k}}\times \vec{E}$">|; 

$key = q/vec{E}(t)=vec{E}_0e^{-iomegat};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.63ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img234.svg"
 ALT="$\vec{E}(t)=\vec{E}_0 e^{-i\omega t}$">|; 

$key = q/vec{E}=vec{E}_1+vec{E}_2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.13ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img246.svg"
 ALT="$\vec{E}=\vec{E}_1 + \vec{E}_2$">|; 

$key = q/vec{E}^*timesvec{B}^*;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.08ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img240.svg"
 ALT="$\vec{E}^*\times\vec{B}^*$">|; 

$key = q/vec{E}_0(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\vec{E}_0(t)$">|; 

$key = q/vec{E}_1cdotvec{E}_2=0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.13ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img247.svg"
 ALT="$\vec{E}_1\cdot\vec{E}_2=0$">|; 

$key = q/vec{E}timesvec{B};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img239.svg"
 ALT="$\vec{E}\times\vec{B}$">|; 

$key = q/vec{P}=epsilon_0chi_evec{E};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\vec{P} = \epsilon_0 \chi_e
\vec{E}$">|; 

$key = q/vec{R};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\vec{R}$">|; 

$key = q/vec{R}=vec{r}-vec{r}';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.08ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img174.svg"
 ALT="$\vec{R} = \vec{r}-\vec{r}'$">|; 

$key = q/vec{a}=ddot{vec{r}}'(t_r);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.59ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\vec{a}=\ddot{\vec{r}}'(t_r)$">|; 

$key = q/vec{beta}=vec{v}slashc;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img230.svg"
 ALT="$\vec{\beta} = \vec{v}/c$">|; 

$key = q/vec{beta}=vec{v}slashc=dot{vec{r}}'(t_r)slashc;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.59ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\vec{\beta} = \vec{v}/c = \dot{\vec{r}}'(t_r)/c$">|; 

$key = q/vec{k};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\vec{k}$">|; 

$key = q/vec{k}=omegaslashc;hat{vec{r}}=2pislashlambdahat{vec{r}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\vec{k} = \omega/c\; \hat{\vec{r}}=2\pi/\lambda \hat{\vec{r}}$">|; 

$key = q/vec{k}_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\vec{k}_0$">|; 

$key = q/vec{p}(t)=qvec{r}'(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\vec{p}(t) = q \vec{r}'(t)$">|; 

$key = q/vec{q};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\vec{q}$">|; 

$key = q/vec{q}=qhat{vec{z}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.28ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img252.svg"
 ALT="$\vec{q} = q \hat{\vec{z}}$">|; 

$key = q/vec{q}=vec{k}-vec{k}_0;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\vec{q}=\vec{k}-\vec{k}_0$">|; 

$key = q/vec{q}cdotvec{r}=qrcostheta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img253.svg"
 ALT="$\vec{q}\cdot\vec{r} = q r \cos\theta$">|; 

$key = q/vec{r}'(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\vec{r}'(t)$">|; 

$key = q/vec{r}';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.89ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\vec{r}'$">|; 

$key = q/vec{r}'=r_0exp(-iomegat)hat{vec{r}}';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\vec{r}'
= r_0 \exp(-i\omega t) \hat{\vec{r}}'$">|; 

$key = q/vec{r};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img197.svg"
 ALT="$\vec{r}$">|; 

$key = q/vec{r}_i';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.46ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\vec{r}_i'$">|; 

$key = q/vec{r}_n;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.57ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\vec{r}_n$">|; 

$key = q/vec{u}=c{mbox{hat{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashBoldR}}}}}}-vec{beta};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.37ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\vec{u} = c {\mbox{$\hat {\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}$}}- \vec{\beta}$">|; 

$key = q/vec{u}approxc{mbox{hat{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashBoldR}}}}}};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.92ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\vec{u} \approx c {\mbox{$\hat {\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}$}}$">|; 

$key = q/vec{w}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\vec{w}(t)$">|; 

$key = q/vec{x}(t);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\vec{x}(t)$">|; 

$key = q/vec{x};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\vec{x}$">|; 

$key = q/vllc;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.53ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img54.svg"
 ALT="$v \ll c$">|; 

$key = q/x';MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.89ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img209.svg"
 ALT="$x'$">|; 

$key = q/{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashBoldR}}}}=vec{r}-vec{r}'(t_r);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="${\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}= \vec{r} - \vec{r}'(t_r)$">|; 

$key = q/{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashBoldR}}}}approxvec{r};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.31ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img52.svg"
 ALT="${\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/BoldR}}$}}\approx \vec{r}$">|; 

$key = q/{mbox{resizebox{.09in}{.08in}{includegraphics[trim=1em014em0,clip]{fontsslashScriptR}}}}=vec{r}-vec{w}(t_r);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img231.svg"
 ALT="${\mbox{$\resizebox{.09in}{.08in}{\includegraphics[trim= 1em 0 14em 0,clip]{fonts/ScriptR}}$}}= \vec{r} - \vec{w}(t_r)$">|; 

$key = q/|hat{vec{r}}timeshat{vec{p}}|^2=sin^2theta;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.69ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\vert\hat{\vec{r}} \times \hat{\vec{p}}\vert^2 = \sin^2\theta$">|; 

$key = q/|vec{k}_0|=|vec{k}|=frac{2pi}{lambda};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.90ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\vert\vec{k}_0\vert =
\vert\vec{k}\vert = \frac{2\pi}{\lambda}$">|; 

1;

