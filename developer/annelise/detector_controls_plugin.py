import numpy as np
import reborn

import subprocess

script_path = '/reborn/developer/annelise/gui_qt_detector_controls.py'


def plugin(self):
    r""" Plugin for Detector Control. """
    self.debug('plugin(detector_controls_plugin)')
    subprocess.run(['python', script_path])
