from time import time
from reborn.external.pyqt import (
    QWidget,
    QGridLayout,
    QCheckBox,
    QLabel,
    QSpinBox,
    QDoubleSpinBox,
    QPushButton,
    AlignCenter
)

import sys
import numpy as np
import pandas
from reborn import detector, source
from reborn.detector import PADGeometryList
from reborn.source import Beam
from reborn.dataframe import DataFrame
from reborn.fileio.getters import FrameGetter
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.viewers.qtviews.padviews import PADView
from reborn.const import eV

   

# class EigerWaterFrameGetter(FrameGetter):
#     def __init__(self):
#         super().__init__()
#         self.n_frames = 100
#     def get_data(self, frame_number=0):
#         np.random.seed(frame_number)
#         geom = geom0.copy()
#         beam = beam0.copy()
#         scat = get_pad_solution_intensity(
#             pad_geometry=geom,
#             beam=beam,
#             thickness=100e-6,
#             poisson=True,
#         )
#         df = DataFrame(raw_data=scat,
#                        pad_geometry=geom,
#                        beam=beam,
#                        mask=mask0.copy())
#         return df


class Plugin:
    widget = None

    def __init__(self, padview):
        self.widget = Widget(padview)
        self.widget.show()


class Widget(QWidget):
    def __init__(self, padview):
        super().__init__()
        self.padview = padview
        self.setWindowTitle("Range Mask")
        self.layout = QGridLayout()
        row = 0
        row += 1
        self.layout.addWidget(QLabel("Minimum"), row, 1)
        self.minimum_spinbox = QDoubleSpinBox()
        self.minimum_spinbox.setMinimum(0)
        self.minimum_spinbox.setValue(0)
        self.layout.addWidget(self.minimum_spinbox, row, 2)
        self.layout.addWidget(QLabel("x10^"), row, 3)
        self.minimum_spinbox_exponent = QSpinBox()
        self.minimum_spinbox_exponent.setValue(1)
        self.layout.addWidget(self.minimum_spinbox_exponent, row, 4)

        row += 1
        self.layout.addWidget(QLabel("Maximum"), row, 1)
        self.maximum_spinbox = QDoubleSpinBox()
        self.maximum_spinbox.setMinimum(0)
        self.maximum_spinbox.setValue(0)
        self.layout.addWidget(self.maximum_spinbox, row, 2)
        self.layout.addWidget(QLabel("x10^"), row, 3)
        self.maximum_spinbox_exponent = QSpinBox()
        self.maximum_spinbox_exponent.setValue(1)
        self.layout.addWidget(self.maximum_spinbox_exponent, row, 4)

        row += 1
        self.create_mask_button = QPushButton("Create Mask")
        self.create_mask_button.clicked.connect(self.apply_range_mask) #FIXME
        self.layout.addWidget(self.create_mask_button, row, 1, 1, 2)
        self.setLayout(self.layout)
        row += 1
        self.save_mask_button = QPushButton("Save Mask")
        self.save_mask_button.clicked.connect(self.apply_range_mask) #FIXME
        self.layout.addWidget(self.save_mask_button, row, 1, 1, 2)
        self.setLayout(self.layout)


    def apply_range_mask(self):
        fg = self.padview #idk if this is a thing but i'm trying to copy the example
        df = self.padview.get_dataframe() # not in ex, but there is a get_frame, ciould easily be the same
        geom = df.get_pad_geometry() 
        beam = df.get_beam()
        mask = df.get_mask_flat()
        data = df.get_raw_data_flat()
        qmags = df.get_q_mags_flat()


        # user inputs
        minimum = self.minimum_spinbox.value()
        minimum_exponent = self.minimum_spinbox_exponent.value()
        maximum = self.maximum_spinbox.value()
        maximum_exponent = self.maximum_spinbox_exponent.value()

        # make save_mask later.... not sure what create_mask does because there already a mask.... maybe it means to copy/paste a new mask
        # create_mask = self.create_mask_button.isChecked()
        # save_mask = self.save_mask_button.isChecked()

        num_changed_max = 0
        num_changed_min = 0
        total_num = 0

        new_mask = mask.copy()

        for index, value in enumerate(qmags):
            print(value)
            total_num += 1
            if value > (maximum*10**maximum_exponent):
                new_mask[index] = 0
                num_changed_max +=1

            if value < (minimum*10**minimum_exponent):
                new_mask[index] = 0
                num_changed_min +=1

        data_list = geom.split_data(data)
        mask_list = geom.split_data(new_mask)

        # pv = PADView(frame_getter = fg)
        pv = fg
        # idk if this is necessary not sure will need future testing
        data_displayed = pv.get_pad_display_data(as_list = False)

        # personally i don't think this is necessary 
        dataframe_displayed = pv.get_dataframe()
        mask_displayed_2 = dataframe_displayed.get_mask_flat()
        print("AAAAAAAAAAAA")
        df.set_mask(new_mask)
        print("BBBBBBBBBBBBBB")
        pv.update_dataframe(df)
        print("CCCCCCCCCCCCCCCC")
        pv.update_display

        # pv.start()

        print("num_changed_max", num_changed_max)
        print("num_changed_min", num_changed_min)
        print("total_num", total_num)
        print("last update: 2:57 (no change)")
        print("Done")
        print(mask)
        print(new_mask)

        #Notes from 7/23 at 2:54
        # It does seem like the mask is being updated (if you use 1*10^10, you get a different mask vs new_mask)
        # So there is a new mask being edited. But I don't know why it's not showing up in padview
        # Nothing changes in padview
        # I haven't checked what happens to the values in mask if you don't create a new mask and just update the old mask\
        # But whether or not anything changes is probably irrelevant
        # Because either way the display doesn't change AT ALL
        # I think I need to play around more with mask_example.py before trying to work on this gui again
        # I need to understand how masks and padview are updated more before working on the range_mask.py again
        # Especially because it takes so long to run on sol
   

