import numpy as np
import h5py
import random

length = 2000 # Dimensions of file = length x length

num_points = 10000 # Number of circle points calculated

min_r = 100 # Inner radius of circle
max_r = 200 # Outer radius of circle


center_x = 500 # Center of circle in x direction
center_y = 500 # Center of circle in y direction

#Make an array of zeros
arr = np.zeros((length, length))
print(arr)

# theta goes from 0 to 2pi and has num_points calculated
theta = np.linspace(0, 2*np.pi, num_points)
print(theta)
specific_random = 0 #Initializing value set to 0

for r in range(min_r,max_r):
    # x and y coordinates from current r plus the center of the circle
    x = r*np.cos(theta) + center_y
    y = r*np.sin(theta) + center_x
    print(x)
    print(y)
    print(theta)
    for i in range(num_points):
        #find_x = int(x[i])
        #find_y = int(y[i])
        find_x = round(x[i])
        find_y = round(y[i])
        difference = max_r-min_r
        
        # Vary the intensity of point based on radius and randomness
        if min_r < r <= min_r + difference/8:
            specific_random = random.randint(0,30)
        if min_r + difference/8 < r <= min_r + difference/4:
            specific_random = random.randint(25,60)
        if min_r + difference/4 < r <= min_r + (3*difference)/8:
            specific_random = random.randint(50,85)
        if min_r + (3*difference)/8 < r <= min_r + difference/2:
            specific_random = random.randint(75,100)
        if min_r + difference/2 < r <= min_r + (5*difference)/8:
            specific_random = random.randint(75,100)
        if min_r + (5*difference)/8 < r <= min_r + (3*difference)/4:
            specific_random = random.randint(50,85)
        if min_r + (3*difference/4) < r <= max_r - difference/8:
            specific_random = random.randint(25,60)
        if max_r - difference/8 < r <= max_r:
            specific_random = random.randint(0,30)
        
        # Add points to array
        arr[find_x,find_y] = specific_random
    
# Add random points with varying intensity to create noise
for i in range(100):
    rand_x = random.randint(0,2000)
    rand_y = random.randint(0,2000)
    specific_random = random.randint(0,100)

    arr[rand_x, rand_y] = specific_random


# make file with final array
with h5py.File('4M_circle.h5', 'w') as hf:
    hf.create_dataset("data", data = arr)


