import numpy as np
import pyqtgraph as pg
# import reborn

import h5py
import cv2
import matplotlib.pyplot as plt
from scipy.ndimage import zoom

from scipy.optimize import curve_fit

#matplotlib.pyplot.use('Agg')

### TODO ###
# turn it into a gui plugin

# Class definitions #


class EstimateCenter():
    r"""Estimate the center off the circle with oversampling and HoughCircles"""
    print('beginning EstimateCenter class')

    def __init__(self, file_name, group_path):

        # Load h5 file
        name = file_name
        self.image_data = self.load_h5_file(name, group_path)
        print("Original shape of data:", self.image_data.shape)

        # Oversample image
        oversample_image_data = self.oversample_image(self.image_data)

        # Find the center of the circle
        self.circle_center_estimate = self.find_circle_center(oversample_image_data)

        print("Estimated center of the circle:", self.circle_center_estimate)


    def load_h5_file(self, name, group_path):
        r"""Load the HF file and extract data"""
        with h5py.File(name, 'r') as file:
            image_data = file[group_path][:]
        print(f"Loaded file with file_name: {name} and group_path: {group_path}")
        return image_data

    def find_circle_center(self, image):
        r"""Function to detect the circle and return its center"""
        # Convert to grayscale if the image is in color (maybe not necessary)
        if len(image.shape) == 3:
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            gray_image = image.astype(np.uint8)
        
        # Apply a Gaussian blur to the image
        blurred_image = cv2.GaussianBlur(gray_image, (9, 9), 0)
        
        # Use the HoughCircles method to detect circles
        circles = cv2.HoughCircles(
            blurred_image,
            cv2.HOUGH_GRADIENT,
            dp=1,
            minDist=1000, # this shouldn't matter because only need one circle
            param1=50,
            param2=50,
            minRadius=0,
            maxRadius=0
        )

        # Check that at least one circle was found and return the first circle found
        if circles is not None:
            print("Circles that were found", circles)
            norm_circles = self.downsample_circles(circles)
            norm_circles = norm_circles[0, :]

            # Return the center of the first detected circle
            for (x, y, r) in norm_circles:
                ## Pixel values are rounded because this is an estimate ##
                x_round = round(x)
                y_round = round(y)
                # print(f"Original x: {x} and Original y: {y}")
                return (x_round, y_round)
                
        else:
            raise ValueError("No circles were found in the image.")

    # Oversampling the image by 2
    def oversample_image(self, image):
        scale_factors = (2,2)
        oversampled_image = zoom(image, scale_factors)
        print("The oversampled shape of image data", oversampled_image.shape)
        return oversampled_image
    
    # Change the coordinates back to original image size (downsample)
    def downsample_circles(self, circles):
        downsampled_circles = np.copy(circles)
        downsampled_circles[0, :, :2]/=np.array((2,2))
        return downsampled_circles

class PerfectCenterPixel(): 
    r"""Find the exact pixel that is the center of the circle by:
            * Finding the radial profile at certain points around the estimated center
            * Finding the Full Width at Half Maximum (FWHM) at each of those points
            * Finding the coordinates with the minimum FWHM which should be the center pixel coordinates"""

    print("beginning PerfectCenterPixel class")
    def __init__(self, image_data, estimate_center):

        # Find radial profile and fwhm for estimated center pixel for reference
        self.current_radial_profile = self.radial_profile(image_data, estimate_center)
        self.current_fwhm = self.find_fwhm(self.current_radial_profile)
        print("reference FWHM for estimated center:", self.current_fwhm)

        # Blank array to store coordinates and FWHM for future comparison
        self.pixel_fwhm_array = []
        
        # Look at points from -2 + center <= center <= 2 + center at every integer in x and y
        values = np.arange(-2, 3, 1)

        for i in values:
            # Edit tuples by changing into a list and adding i to x, then changing back to tuple
            tuple_list = list(estimate_center)
            tuple_list[0] = tuple_list[0] + i # x + i
            testi_center = tuple(tuple_list)
            for j in values:
                # Edit tuples by changing into a list and adding j to y (while x stays constant), then changing back to tuple
                tuple_list = list(testi_center)
                tuple_list[1] = tuple_list[1] + j
                testj_center = tuple(tuple_list)

                # Find radial profile of current coordinates
                self.current_radial_profile = self.radial_profile(image_data, testj_center)
                # print("self.current_radial_profile",self.current_radial_profile)

                # Find fwhm of radial profile from current coordinates
                self.current_fwhm = self.find_fwhm(self.current_radial_profile)
                print(f"and the current_fwhm is {self.current_fwhm} with i: {i} & j: {j}")

                #Store x, y, and FWHM in array for future analysis
                x = testj_center[0]
                y = testj_center[1]
                self.pixel_fwhm_array.append([x,y,self.current_fwhm])

        # Make sure that pixel_fwhm_array is an array that can be iterated through
        self.pixel_fwhm_array = np.array(self.pixel_fwhm_array) 
        print("\nThe final array with x and y coordinates and FWHM is\n", self.pixel_fwhm_array)

        # Find row index of smallest FWHM, then find the corresponding x and y
        minimum_fwhm_index = np.argmin(self.pixel_fwhm_array[:,2]) 
        self.exact_pixel_x = self.pixel_fwhm_array[minimum_fwhm_index, 0]
        self.exact_pixel_y = self.pixel_fwhm_array[minimum_fwhm_index, 1]

            
    # Find the radial profile of the data assuming a specific center
    def radial_profile(self, data, center):
        r"""Find the radial profile of the data assuming a specified center"""
        # Note this is a more computationally expensive way of finding the radial profile, but should be more accurate 

        # Determine radii of all pixels
        y,x = np.indices((data.shape)) 
        r = np.sqrt((x-center[0])**2+(y-center[1])**2)

        # Indices that would sort the r array that has been flattened to 1D in ascending order
        specify_sort = np.argsort(r.flat) # get sorted indices

        # Flatten and sort radii and image data values according to the same parameters from ind
        sorted_r = r.flat[specify_sort] # sorted radii
        sorted_image = data.flat[specify_sort] # image values sorted by radii
        # Change radii to integers (bin size = 1)
        sort_int_r = sorted_r.astype(np.int32) 

        # Find difference between consecutive radii to identify where radii change
        deltar = sort_int_r[1:] - sort_int_r[:-1] 
        # Find indices where radius change to create boundaries for each radial bin
        deltar_location = np.where(deltar)[0] 
        # Number of elements in each bin by taking difference between deltar locations
        num_radii_per_bin = deltar_location[1:] - deltar_location[:-1] # number in radius bin

        # Cumulative sum of sorted image values for each radii bin
        cumsum_sorted_images = np.cumsum(sorted_image, dtype=np.float64) 
        # Total sum of image values in each radius bin (difference of cumulative summ at bin boundaries)
        total_sum_image_per_bin = cumsum_sorted_images[deltar_location[1:]] - cumsum_sorted_images[deltar_location[:-1]] 

        # Average image value at each radial distance from specified center
        radialprofile = total_sum_image_per_bin/num_radii_per_bin 
        print("Found radial profile for", center)
        return radialprofile

    def find_fwhm(self, radial_profile):
        r"""Estimate the FWHM based on points from the radial profile calculations."""
        # Find max value and then find where radial profile coresses half maximum
        max_value = np.max(radial_profile)
        half_max = max_value / 2.0

        # Array of indices where the radial profile is greater than or equal to 0 
        indices_above = np.where(radial_profile >= half_max)[0]
        
        # Error Handling before continuing
        if len(indices_above) == 0:
            raise ValueError("No values found above half maximum")
        
        # Overestimate left and right x values where the radial profile crosses the half maximum
        left_index = indices_above[0] - 1 # one less than the leftmost index from indices above
        right_index = indices_above[-1] + 1 # one more than the rightmost index from indeices above
        
        # Interpolate to find the exact crossing points of the half maximum value on the left and right side
        # (Use the over and underestimate of the index to find the exact half max point)
        left_interpolated = left_index + (half_max - radial_profile[left_index]) / (radial_profile[left_index + 1] - radial_profile[left_index])
        right_interpolated = right_index - (radial_profile[right_index] - half_max) / (radial_profile[right_index] - radial_profile[right_index - 1])
        
        # Calculate FWHM with the difference between right and left x coordinates that mark the point where the radial profile crosses the half maximum point
        fwhm = right_interpolated - left_interpolated
        
        return fwhm


class PerfectCenter(): 
    r"""Uses radial profile and gradient descent to calculate the subpixel center of the circle
            * A gradient descent with a starting point equal to the result of PerfectCenterPixel iterates through gradient
            * The Gradient tests the result of the cost function at some error value above and below the starting point from the gradient
            * The cost function calculates the radial profile for the test center given by the gradient. Then a gaussian curve is fit to the radial profile so that a FWHM can be accurately calculated from the gaussian curve. The FWHM is then returned to the gradient and gradient descent with the intention of minimizing the FWHM

            note:
                Be careful the with changing the error for gradient(), can result in drastically incorrect answers"""
    print("beginning PerfectCenter class")
    def __init__(self, image_data, initial_guess):
        self.image_data = image_data
        learning_rate = 0.01
        num_iterations = 200
        self.optimal_center = self.gradient_descent(initial_guess, learning_rate, num_iterations)
        print("CENTER ACCORDING TO GRADIENT", self.optimal_center)

            
    def cost_function(self, center):
        r"""Find the radial profile of the data with a test center, fit a gaussian curve to the radial profile and calculate the FWHM of the gaussian curve"""
        # Determine radii of all pixels
        data = self.image_data
        y,x = np.indices((data.shape))
        r = np.sqrt((x-center[0])**2+(y-center[1])**2)

        # Indices that would sort the r array that has been flattened to 1D in ascending order
        specify_sort = np.argsort(r.flat) # get sorted indices

        # Flatten and sort radii and image data values according to the same parameters from ind
        sorted_r = r.flat[specify_sort] # sorted radii
        sorted_image = data.flat[specify_sort] # image values sorted by radii
        # Change radii to integers (bin size = 1)
        sort_int_r = sorted_r.astype(np.int32) 

        # Find difference between consecutive radii to identify where radii change
        deltar = sort_int_r[1:] - sort_int_r[:-1] 
        # Find indices where radius change to create boundaries for each radial bin
        deltar_location = np.where(deltar)[0] 
        # Number of elements in each bin by taking difference between deltar locations
        num_radii_per_bin = deltar_location[1:] - deltar_location[:-1] # number in radius bin

        # Cumulative sum of sorted image values for each radii bin
        cumsum_sorted_images = np.cumsum(sorted_image, dtype=np.float64) 
        # Total sum of image values in each radius bin (difference of cumulative summ at bin boundaries)
        total_sum_image_per_bin = cumsum_sorted_images[deltar_location[1:]] - cumsum_sorted_images[deltar_location[:-1]] 

        # Average image value at each radial distance from specified center
        radialprofile = total_sum_image_per_bin/num_radii_per_bin 
        
        # Define gaussian function that the radial profile should be fit to
        def gaussian(x, a, x0, sigma):
            return a * np.exp(-(x - x0)**2 / (2 * sigma**2))
        
        # the Y data is the radial profile, so the X data is the distance from the center which is also represented by the length of the radial profile data
        x_data = np.arange(len(radialprofile))
        
        # Define the max value of the radial profile (or the max Y value)
        init_a = radialprofile.max()
        # Define the X value where the max radial profile occurs
        index_max = np.argmax(radialprofile)
        init_x0 = x_data[index_max]

        # Define an estimate for the FWHM


        half_max = init_a / 2.0
        idx_left = np.abs(radialprofile[:index_max] - half_max).argmin()
        idx_right = np.abs(radialprofile[index_max:] - half_max).argmin() + index_max
        fwhm = x_data[idx_right] - x_data[idx_left]

        init_sigma = fwhm / (2.0 * np.sqrt(2.0 * np.log(2)))
        
        # Find the parameters to best fit the data into a guassian curve with inital parameter guesses from above definitions
        initial_guess = [init_a, init_x0, init_sigma]

        popt, pcov = curve_fit(gaussian, x_data, radialprofile, initial_guess)

        # Optimal parameters
        a_opt, x0_opt, sigma_opt = popt
        
        # Calculate standard deviation/uncertainty
        perr = np.sqrt(np.diag(pcov))
        a_uncertainty, x0_uncertainty, sigma_uncertainty = perr

        # Find the FWHM based on the guassian curve fit parameters
        fwhm = sigma_opt * (2.0 * np.sqrt(2.0 * np.log(2)))
        print(f"at center {center} had fwhm {fwhm}")
        
        # View shape of radial profile and gaussian curve to visually check accuracy
        # x=np.arange(radialprofile.shape[0])
        # plt.plot(x,radialprofile)
        # plt.plot(x,gaussian(x,a_opt,x0_opt,sigma_opt))
    
        # plt.savefig("test.png")

        return fwhm

    # Defines how the possible guesses are defined and iterated through
    def gradient_descent(self, initial_guess, learning_rate, num_iterations):
        params = tuple(initial_guess)
        for i in range(num_iterations):
            grad = self.gradient(params)
            params -= learning_rate * grad
            print(f"Iteration {i+1}: params = {params}, cost = {self.cost_function(params)}")
        return params
    
    def gradient(self, center_guess, error = 0.5): 
        grad = np.zeros_like(center_guess) 
        for i in range(len(center_guess)): 
            params_plus = list(center_guess)
            params_plus[i] += error
            print("params plus",params_plus[i])
            params_minus = list(center_guess)
            params_minus[i] -= error
            print("params minus", params_minus[i])
            grad[i] = (self.cost_function(params_plus) - self.cost_function(params_minus)) / (2 * error)
            print("grad", grad[i])
        return grad


# End of class definitions #

# Function definitions #
# Define functions to run/create instance of class with inputs
def estimate_center(file_name, group_path):
    return EstimateCenter(file_name, group_path)

def perfect_center_pixel(image_data, estimate_center):
    return PerfectCenterPixel(image_data, estimate_center)

def perfect_center(image_data, center_pixel):
    return PerfectCenter(image_data, center_pixel)


#####CURRENT EXECUTION CODE #############
# EVENTUALLY WILL BE RUN WITH PLUGIN/WIDGET, BUT FOR TESTING #

ec = estimate_center('4M_circle.h5', 'data') #USER INPUT VARIABLES 
print("\nEND ESTIMATE: ", ec.circle_center_estimate)
#### FIXME
pcp = perfect_center_pixel(ec.image_data, ec.circle_center_estimate)

pc = perfect_center(ec.image_data, [pcp.exact_pixel_x, pcp.exact_pixel_y])

print("ESTIMATE CENTER:", ec.circle_center_estimate)
print(f"CENTER PIXEL: [{pcp.exact_pixel_x},{pcp.exact_pixel_y}]")
print("EXACT CENTER:", pc.optimal_center)
