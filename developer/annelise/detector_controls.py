#!/dpcxls/sw/miniconda3/bin/python3


import requests
import time
import os

# Configuration
detector_ip = "10.139.1.5"
api_url = f"http://{detector_ip}"
api_version = "1.8.0"
interval = 30  # Interval in seconds for data retrieval
save_directory = "./data/"  # Replace with your desired save directory


##### initialize, disarm, arm, trigger - PUT requests, functions without input or output #####

def initialize():
    try:
        #Sending PUT request to initialize
        response = requests.put(f"{api_url}/detector/api/{api_version}/command/initialize")
        response.raise_for_status()
        print("Initalized successfully.") 
    except requests.exceptions.RequestException as e:
        print(f"Error initializing: {e}")

def disarm():
    try:
        #Sending PUT request to disarm
        response = requests.put(f"{api_url}/detector/api/{api_version}/command/disarm")
        response.raise_for_status()
        print("Disarmed successfully.")
    except requests.exceptions.RequestException as e:
        print(f"Error disarming: {e}")

def arm():
    try:
        #Sending PUT request to arm
        response = requests.put(f"{api_url}/detector/api/{api_version}/command/arm")
        response.raise_for_status()
        print("Armed successfully.")
    except requests.exceptions.RequestException as e:
        print(f"Error arming: {e}")

def trigger():
    try:
        #Sending PUT request to trigger
        response = requests.put(f"{api_url}/detector/api/{api_version}/command/trigger")
        response.raise_for_status()
        print("Triggered successfully.")
    except requests.exceptions.RequestException as e:
        print(f"Error triggering: {e}")
        print("Check that you clicked Arm before Trigger")

##### enable for: monitor, filewriter, stream - PUT requests, functions without input or output #####

def enable_monitor():
    try:
        #Sending PUT request to enable monitor
        ability = {
            "value":"enabled"
        }
        response = requests.put(f"{api_url}/monitor/api/{api_version}/config/mode", json=ability)
        response.raise_for_status()
        print("Monitor enabled successfuly")
    except requests.exceptions.RequestException as e:
        print(f"Error enabling monitor: {e}")

def enable_filewriter():
    try:
        #Sending PUT request to enable filewriter
        ability = {
            "value":"enabled"
        }
        response = requests.put(f"{api_url}/filewriter/api/{api_version}/config/mode", json=ability)
        response.raise_for_status()
        print("Filewriter enabled successfuly")
    except requests.exceptions.RequestException as e:
        print(f"Error enabling filewriter: {e}")

def enable_stream():
    try:
        #Sending PUT request to enable stream
        ability = {
            "value":"enabled"
        }
        response = requests.put(f"{api_url}/stream/api/{api_version}/config/mode", json=ability)
        response.raise_for_status()
        print("Stream enabled successfuly")
    except requests.exceptions.RequestException as e:
        print(f"Error enabling stream: {e}")


##### disable for: monitor, filewriter, stream - PUT requests, functions without input or output #####

def disable_monitor():
    try:
        #Sending PUT request to disable monitor
        ability = {
            "value":"disabled"
        }
        response = requests.put(f"{api_url}/monitor/api/{api_version}/config/mode", json=ability)
        response.raise_for_status()
        print("Monitor disabled successfuly")
    except requests.exceptions.RequestException as e:
        print(f"Error disabling monitor: {e}")

def disable_filewriter():
    try:
        #Sending PUT request to disable filewriter
        ability = {
            "value":"disabled"
        }
        response = requests.put(f"{api_url}/filewriter/api/{api_version}/config/mode", json=ability)
        response.raise_for_status()
        print("Filewriter disabled successfuly")
    except requests.exceptions.RequestException as e:
        print(f"Error disabling filewriter: {e}")

def disable_stream():
    try:
        #Sending PUT request to disable stream
        ability = {
            "value":"disabled"
        }
        response = requests.put(f"{api_url}/stream/api/{api_version}/config/mode", json=ability)
        response.raise_for_status()
        print("Stream disabled successfuly")
    except requests.exceptions.RequestException as e:
        print(f"Error disabling stream: {e}")



##### nimages, ntrigger, frame time, count time - PUT requests, functions require input #####

def nimages(puppy):
    try:
        #Sending PUT request: how many images to collect in series
        set = {
            "value": puppy
        }
        response = requests.put(f"{api_url}/detector/api/{api_version}/config/nimages", json=set)
        response.raise_for_status()
        print(f"nimages now set to {puppy}") 
    except requests.exceptions.RequestException as e:
        print(f"Error running nimages: {e}")

def ntrigger(puppy):
    try:
        #Sending PUT request: (no documentation from original script)
        set = {
            "value": puppy
        }
        response = requests.put(f"{api_url}/detector/api/{api_version}/config/ntrigger", json=set)
        response.raise_for_status()
        print(f"ntrigger now set to {puppy}") 
    except requests.exceptions.RequestException as e:
        print(f"Error running ntrigger: {e}")

def frame_time(puppy):
    try:
        #Sending PUT request: Time between readouts or inverse of collection rate
        set = {
            "value": puppy
        }
        response = requests.put(f"{api_url}/detector/api/{api_version}/config/frame_time", json=set)
        response.raise_for_status()
        print(f"frame_time now set to {puppy}") 
    except requests.exceptions.RequestException as e:
        print(f"Error running frame_time: {e}")

def count_time(puppy):
    try:
        #Sending PUT request: exposure time
        set = {
            "value": puppy
        }
        response = requests.put(f"{api_url}/detector/api/{api_version}/config/count_time", json=set)
        response.raise_for_status()
        print(f"count_time now set to {puppy}")
    except requests.exceptions.RequestException as e:
        print(f"Error running count_time: {e}")

def trigger_mode(puppy):
    try:
        #Sending PUT request: (documentation incomplete from ) ("Options are:  ????")
        set = {
            "value": puppy
        }
        response = requests.put(f"{api_url}/detector/api/{api_version}/config/trigger_mode", json=set)
        response.raise_for_status()
        print(f"trigger_mode now set to {puppy}")
    except requests.exceptions.RequestException as e:
        print(f"Error running trigger_mode: {e}")

##### get for: count time, frame time, nimages, status - GET requests, functions return output #####

def get_count_time():
    try:
        #GET request to find count time 
        response = requests.get(f"{api_url}/detector/api/{api_version}/config/count_time")
        response.raise_for_status()
        output = response.json()
        return output
    except requests.exceptions.RequestException as e:
        print(f"Error returning count time: {e}")

def get_frame_time():
    try:
        #GET request to find frame time
        response = requests.get(f"{api_url}/detector/api/{api_version}/config/frame_time")
        response.raise_for_status()
        output = response.json()
        return output
    except requests.exceptions.RequestException as e:
        print(f"Error returning frame time: {e}")

def get_nimages():
    try:
        #GET request to find num images
        response = requests.get(f"{api_url}/detector/api/{api_version}/config/nimages")
        response.raise_for_status()
        output = response.json()
        return output
    except requests.exceptions.RequestException as e:
        print(f"Error returning number images processed: {e}")

def status():
    try:
        #GET request to find status
        response = requests.get(f"{api_url}/detector/api/{api_version}/status/state/configure")
        response.raise_for_status()
        output = response.json()
        print(f"the result of status was {output}")
        return output
    except requests.exceptions.RequestException as e:
        print(f"Error returning status: {e}")

def check_settings():
    get_count_time()
    get_frame_time()
    get_nimages()

##### Checking functionality #####
def check_initialize():
    try:
        #Sending PUT request to arm to check for error
        response = requests.get(f"{api_url}/detector/api/{api_version}/status/state/configure")
        response.raise_for_status()
        return 1 #initialize done
    except requests.exceptions.RequestException as e:
        return 0 #initialize no done


##### Written by Sabine #####

def clear_data():
    try:
        # Sending a PUT request to clear data
        response = requests.put(f"{api_url}/filewriter/api/{api_version}/command/clear")
        response.raise_for_status()
        print("Data cleared successfully.")
    except requests.exceptions.RequestException as e:
        print(f"Error clearing data: {e}")

def list_h5_files():
    try:
        # Endpoint for listing files
        response = requests.get(f"{api_url}/filewriter/api/{api_version}/files/")
        response.raise_for_status()
        files_dict = response.json()
        #print("Retriving Files: ", files)
        
        if 'value' in files_dict and isinstance(files_dict['value'], list):
            files = files_dict['value']
            return files
        else:
            print("your Dic is weird:", files_dict)
            return[]
    except requests.exceptions.RequestException as e:
        print(f"Error listing files: {e}")
        return None

def download_h5_file(file_name):
    try:
        # Endpoint for downloading a specific file
        response = requests.get(f"{api_url}/data/{file_name}", stream=True)
        response.raise_for_status()
        file_path = os.path.join(save_directory, file_name)
        print("in h5 download")
        with open(file_path, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        print(f"Downloaded {file_name} to {file_path}")
    except requests.exceptions.RequestException as e:
        print(f"Error downloading file {file_name}: {e}")

#The following makes sure that this only get's executed when the script is run directly and not when it is imported as a module. 
if __name__ == "__main__":
    print("Initializing detector:")
    initialize()
    print("Enabling filewriter mode")
    enable_filewriter()
    print("arming detector")
    arm()
    print("triggering detector")
    trigger()


