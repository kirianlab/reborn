import h5py
import numpy as np
import cv2
import matplotlib.pyplot as plt
from scipy.ndimage import zoom

scale_factor_block_size = (2,2)

# Load the H5 file and extract data
def load_h5_file(file_path):
    with h5py.File(file_path, 'r') as file:
        # Assume the image is stored under a dataset named 'data'
        image_data = file['data'][:]
    return image_data

# Function to detect the circle and return its center
def find_circle_center(image):
    # Convert to grayscale if the image is in color (maybe not necessary)
    if len(image.shape) == 3:
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
        print("if gray")

    else:
        gray_image = image.astype(np.uint8)

        print("else gray")
    
    # Apply a Gaussian blur to the image
    blurred_image = cv2.GaussianBlur(gray_image, (9, 9), 0)
    
    # Use the HoughCircles method to detect circles
    circles = cv2.HoughCircles(
        blurred_image,
        #gray_image,
        cv2.HOUGH_GRADIENT,
        dp=1,
        minDist=1000, # this shouldn't matter because only need one circle
        param1=50,
        param2=50,
        minRadius=0,
        maxRadius=0
    )
    print("after circles")


    # Ensure at least one circle was found
    if circles is not None:
        print("circles", circles)
        norm_circles = downsample_circles(circles)
        norm_circles = norm_circles[0, :]
        print("inside if before for")
        for (x, y, r) in norm_circles:
            # Return the center of the first detected circle
            print("in for x,y,r")
            return (x, y)
            
    else:
        raise ValueError("No circles were found in the image.")

def oversample_image(image):
    print("reached oversample image")
    scale_factors = scale_factor_block_size
    oversampled_image = zoom(image, scale_factors)
    print("reached end of oversample image", oversampled_image.shape)
    return oversampled_image


    
def downsample_circles(circles):
    downsampled_circles = np.copy(circles)
    downsampled_circles[0, :, :2]/=np.array(scale_factor_block_size)
    return downsampled_circles


# Load h5 file
file_path = '4M_circle.h5'
image_data = load_h5_file(file_path)
print("orig shape:", image_data.shape)

# Oversample Image
oversample_image_data = oversample_image(image_data)

# Find the center of the circle
print("before circle center")
circle_center = find_circle_center(oversample_image_data)

#circle_center = downsample_image(oversample_circle_center)

print("Center of the circle:", circle_center)


