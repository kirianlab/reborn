import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QGridLayout, QLineEdit, QTextEdit, QMainWindow, QLabel
from PyQt5.QtCore import pyqtSignal, QObject

import time

try:
    from FAKE_detector_controls import *  #for testing code without access to real detector
except:
    from detector_controls import *



# TO DO #
#   make sure no other buttons can be pressed while initializing
#   make text boxes change color and exist with dictionary too
#   what happens if button clicked multiple times?
#   better formatting in the future



## Classes ##

# Makes command terminal info be written into gui and on normal terminal
class CommandTerminalStream(QObject):
    text_written = pyqtSignal(str)

    def __init__(self, original_stdout):
        super().__init__()
        self.original_stdout = original_stdout

    def write(self, text):
        self.text_written.emit(str(text))
        self.original_stdout.write(text)

    def flush(self):
        self.original_stdout.flush()


## Efficient Formatting ##

# Common colors
light_pink = '#ffb6c1'
light_orange = '#eedd82'
light_green = '#90ee90'
light_blue = '#add8e6'
light_purple = '#eee1ff'
lighter_orange = '#f3e7a8'

# Function formats buttons more efficiently
def format_button(button, label, function, color):
    button.setText(label)
    button.clicked.connect(function)
    button.setStyleSheet(f"QPushButton{{border-radius: 10px; min-width: 110px; min-height: 40px; background-color: {color}; border: 2px solid black; font-weight: bold; }}")

# Function formats QLineEdit text boxes more efficiently
def format_txtbx(txtbx, color):
    txtbx.setStyleSheet(f"QLineEdit{{border-radius: 10px; min-width: 110px; min-height: 40px; background-color: {color}; border: 1px solid black; font-weight: bold; }}")

# Function formats QTextEdit text boxes more efficiently 
def format_big_txtbx(txtbx, color):
    txtbx.setStyleSheet(f"QTextEdit{{border-radius: 10px; min-width: 50px; min-height: 100px; background-color: {color}; border: 3px solid black; font-weight: bold; }}")


## Functions for Changes in Formatting ##

# Function resets original function/color settings by iterating through all buttons (and potentially text boxes) in dictionary
def all_norm_buttons_etc():

    for button_name, config in buttons.items():  
        value = specific_buttons[button_name] 
        value.clicked.disconnect()
        format_button(value, config["label"], config["func"], config["color"])
        QApplication.processEvents()

    # Future use for resetting color of text boxes too    
    # for text_name, config in texts.items():
    #     format_txtbx(text, config["color"])
    #     QApplication.processEvents()
    
    
# Function to grey out all buttons (and potentially text boxes), while something is loading
#   Input will call specific error message function to tell user what's loading
def all_grey(error_message):

    for button_name, config in buttons.items():
        value = specific_buttons[button_name]
        value.clicked.disconnect()
        format_button(value, config["label"], error_message, 'grey')
        QApplication.processEvents()

    # Future use for setting text boxes to grey
    # for text_name, config in texts.items():
    #     format_txtbx(text, 'grey')
    #     QApplication.processEvents()




######################################################################################
## Actual button functions ##
# (when functions require processing before use in detector_controls.py)
######################################################################################

# Function that updates terminal text box neatly (keeps cursor at bottom of text box)
def update_txtbx(text_edit, text):
    cursor = text_edit.textCursor()
    cursor.movePosition(cursor.End)
    cursor.insertText(text)
    text_edit.setTextCursor(cursor)
    text_edit.ensureCursorVisible()

# Function for intialize button: Greys out buttons while initializing detector, then returns color/function once complete
def pause_to_initialize():
    all_grey(initialize_not_ready)
    QApplication.processEvents()
    initialize()
    time.sleep(5) 
    all_norm_buttons_etc()
    QApplication.processEvents()

## Submit text input to detector_controls ##

# Function for nimages submit button: Collects user entry from nimages text box and sends to detector if integer
def call_nimages():
    user_input = nimages_txtbx.text()
    if user_input.isdigit():
        nimages(user_input)
    else:
        print("Please enter an integer.")

# Function for frame time submit button: Collects user entry from frame time text box and sends to detector if integer
def call_frame_time():
    user_input = frame_time_txtbx.text()
    if user_input.isdigit():
        frame_time(user_input)
    else:
        print("Please enter an integer.")

# Function for count time submit button: Collects user entry from count time text box and sends to detector if integer
def call_count_time():
    user_input = count_time_txtbx.text()
    if user_input.isdigit():
        count_time(user_input)
    else:
        print("Please enter an integer.")

## Return current value from detector to terminal ##

# Function for get nimages button: prints value detector has nimages stored as
def when_get_nimages():
    print(f"nImages = {get_nimages()}")

# Function for get frame time button: prints value detector has frame time stored as
def when_get_frame_time():
    print(f"frame_time = {get_frame_time()}")

# Function for get count time button: prints value detector has count time stored as
def when_get_count_time():
    print(f"count time = {get_count_time()}")

# Function for get status button: prints detector status (maybe, not working rn)
def when_get_status():
    print(f"status = {status()}")




## Functions Not in Use ##

# User message for waiting for initialize to load
#   currently not used because buttons don't update to new function and revert back
def initialize_not_ready():
    print("Still initializing...please wait")

# Clears button functions
#   should iterate through all buttons and clear the functions assigned to that button.
#   originally used for all_norm_buttons_etc() and all_grey(), but now written into functions
def clear_all_buttons_functions():
    for value in specific_buttons.items(): 
        value.clicked.disconnect()
        QApplication.processEvents()

# Sets ntrigger using function from detector_controls.py and taking user input from text box
#   No longer adding ntrigger to gui
def call_ntrigger():
    user_input = ntrigger_txtbx.text()
    if user_input.isdigit():
        ntrigger(user_input)
    else:
        print("Please enter an integer.")

# Sets trigger_mode using function from detector_controls.py and taking user input from text box
#   No longer adding trigger_mode to gui
def call_triger_mode():
    user_input = trigger_mode_txtbx.text() 
    if user_input.isdigit():
        trigger_mode(user_input)
    else:
        print("Please enter an integer.")







######################################################################################
## Execution of Code ##
if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    # Create gui window
    window = QMainWindow()
    window.setWindowTitle('Detector Controls')
    center = QWidget()
    window.setCentralWidget(center)
    layout = QGridLayout()

    # Dictionary for storing button ids and text ids once they're created
    specific_buttons = {}
    specific_text = {}

    # Dictionary for button names/type/label/function/color/position
    buttons = {
        "initialize_btn":{"type": QPushButton(), "label": "Initialize", "func": pause_to_initialize, "color": light_pink, "row": 0, "col": 0},
        "arm_btn":{"type":QPushButton(), "label": "Arm", "func": arm, "color": light_orange, "row": 0, "col": 1},
        "trigger_btn":{"type":QPushButton(), "label": "Trigger", "func": trigger, "color": light_green, "row":0, "col":2},
        "disarm_btn":{"type":QPushButton(), "label": "Disarm", "func": disarm, "color": light_blue, "row": 0, "col": 3},
        "enable_filewriter_btn":{"type":QPushButton(), "label": "Enable\nfilewriter", "func": enable_filewriter, "color": light_blue, "row": 2, "col": 0},
        "disable_filewriter_btn":{"type":QPushButton(), "label": "Disable\nfilewriter", "func": disable_filewriter, "color": light_pink, "row": 3, "col": 0},
        "enable_stream_btn":{"type":QPushButton(), "label": "Enable stream", "func": enable_stream, "color": light_blue, "row": 2, "col": 1},
        "disable_stream_btn":{"type":QPushButton(), "label": "Disable stream", "func": disable_stream, "color": light_pink, "row": 3, "col": 1},
        "enable_monitor_btn":{"type":QPushButton(), "label": "Enable monitor", "func": enable_monitor, "color": light_blue, "row": 2, "col": 2},
        "disable_monitor_btn":{"type":QPushButton(), "label": "Disable monitor", "func": disable_monitor, "color": light_pink, "row": 3, "col": 2},
        "nimages_submit_btn":{"type":QPushButton(), "label": "nImages\nsubmit", "func": call_nimages, "color": light_green, "row": 6, "col": 0},
        #"ntrigger_submit_btn":{"type":QPushButton(), "label": "nTrigger\nsubmit", "func": call_nimages, "color": light_green},
        "frame_time_submit_btn":{"type":QPushButton(), "label": "Frame time\nsubmit", "func": call_frame_time, "color": light_green, "row": 6, "col": 1},
        "count_time_submit_btn":{"type":QPushButton(), "label": "Count time\nsubmit", "func": call_count_time, "color": light_green, "row": 6, "col": 2},
        #"trigger_mode_submit_btn":{"type":QPushButton(), "label": "Trigger mode\nsubmit", "func": call_triger_mode, "color": light_green},
        "get_nimages_btn":{"type":QPushButton(), "label": "get nImages", "func": when_get_nimages, "color": light_blue, "row": 7, "col": 0},
        "get_frame_time_btn":{"type":QPushButton(), "label": "get Frame Time", "func": when_get_frame_time, "color": light_blue, "row": 7, "col": 1},
        "get_count_time_btn":{"type":QPushButton(), "label": "get Count Time", "func": when_get_count_time, "color": light_blue, "row": 7, "col": 2},
        "get_status_btn":{"type":QPushButton(), "label": "get Status", "func": when_get_status, "color": light_blue, "row": 7, "col": 3},
        "check_settings_btn":{"type":QPushButton(), "label": "Check Settings", "func": check_settings, "color": light_pink, "row": 7, "col": 4}
    }

    # Dictionary for text box names/type/color/position (currently not in use because issues with text box in other sections)
    texts = {
        "nimages_txtbx":{"type": QLineEdit(), "color": lighter_orange, "row": 3, "col": 0},
        #"ntrigger_txtbx":{"type": QLineEdit(), "color": lighter_orange},
        "frame_time_txtbx":{"type": QLineEdit(), "color": lighter_orange, "row": 3, "col": 1},
        "count_time_txtbx":{"type": QLineEdit(), "color": lighter_orange, "row": 3, "col": 2},
        #"trigger_mode_txtbx":{"type": QLineEdit(), "color": lighter_orange}

    }

    # Creates all buttons by looping through buttons dictionary
    for button_name, config in buttons.items():
        name = button_name
        button_name = config["type"]
        format_button(button_name, config["label"], config["func"], config["color"])
        layout.addWidget(button_name, config["row"], config["col"])
        specific_buttons.update({name: button_name})

    # Creates all text boxes by looping through texts dictionary (currently not in use because issues with text box in other sections)
    # for text_name, config in texts.items():
    #     name = text_name
    #     print(name)
    #     text_name = config["type"]
    #     format_txtbx(text_name, config["color"])
    #     layout.addWidget(text_name, config["row"], config["col"])
    #     specific_text.update({name: text_name})

    # Creating and formatting text boxes (should be deleted when texts dictionary works)
    nimages_txtbx = QLineEdit()
    ntrigger_txtbx = QLineEdit() # not necessary
    frame_time_txtbx = QLineEdit()
    count_time_txtbx = QLineEdit()
    trigger_mode_txtbx = QLineEdit()  # not necessary 
    format_txtbx(nimages_txtbx, lighter_orange)
    format_txtbx(ntrigger_txtbx, lighter_orange) # not necessary
    format_txtbx(frame_time_txtbx, lighter_orange)
    format_txtbx(count_time_txtbx, lighter_orange)
    format_txtbx(trigger_mode_txtbx, lighter_orange) # not necessary

    # Create and format QTextEdit() for "terminal"
    log_output = QTextEdit()
    format_big_txtbx(log_output, light_purple) 
    log_output.setReadOnly(True)

    # Changing system output to run CommandTerminalStream and update_txtbx so terminal info will displa on gui
    original_stdout = sys.stdout
    emitting_stream = CommandTerminalStream(original_stdout)
    emitting_stream.text_written.connect(lambda text: update_txtbx(log_output, text))
    sys.stdout = emitting_stream
    
    # Adding widgets that weren't added by buttons dictionary loop (eventually should only have log_output here)
    layout.addWidget(nimages_txtbx, 5, 0)
    layout.addWidget(frame_time_txtbx, 5, 1)
    layout.addWidget(count_time_txtbx, 5, 2)
    layout.addWidget(log_output, 20, 0, 1, layout.columnCount())

    # For spacing between buttons #
    label1 = QLabel()
    layout.addWidget(label1, 1, 0)
    label4 = QLabel()
    layout.addWidget(label4, 4, 0)

    # Set the layout for the window
    center.setLayout(layout)

    window.show()
    sys.exit(app.exec_())

