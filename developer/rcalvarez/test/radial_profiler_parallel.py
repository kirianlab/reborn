import reborn
from reborn.simulate.examples import WaterFrameGetter
from reborn.analysis.saxs import ParallelRadialProfiler

n_q_bins = 1000
detector_distance = 0.123
xray_photon_energy = 9 * reborn.const.eV * 1e3

start = 0
stop = 501
parallel = True
n_processes = 10

beam = reborn.source.Beam(photon_energy=xray_photon_energy)
pads = reborn.detector.jungfrau4m_pad_geometry_list(detector_distance=detector_distance)
detector_geometry = pads.binned(binning=4)

framegetter = WaterFrameGetter(pad_geometry=detector_geometry, beam=beam)


profiler = ParallelRadialProfiler(framegetter=framegetter, n_q_bins=n_q_bins,
                                  pad_geometry=detector_geometry, beam=beam, mask=detector_geometry.ones(),
                                  start=start, stop=stop, parallel=True, n_processes=n_processes)

profiler.process_frames()
