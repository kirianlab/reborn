# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import matplotlib.pyplot as plt
from reborn.analysis import fluctuations as fxs

n = 100
x = np.zeros((n - 10, n))
xm = np.ones((n - 10, n))
x0 = int(n / 2 - n / 10)
xf = int(n / 2 + n / 10)
x[x0:xf, x0:xf] = 1


def test_correlate():
    y = fxs.correlate(x)
    assert np.max(y) == 2 * n / 10


def test_data_correlation():
    z = fxs.data_correlation(0, x, xm)
    assert np.max(z) == 0.16


if __name__ == '__main__':
    plt.figure()
    plt.title('data pre-subtraction')
    plt.imshow(x)

    xn = fxs.subtract_masked_data_mean(x, xm)
    y = fxs.correlate(xn)

    xp = np.sum(xn, axis=0)
    xpf = np.fft.fft(xp)
    xpac = np.fft.ifft(xpf * np.conj(xpf))

    plt.figure()
    plt.title('data projection')
    plt.plot(xp, '.')

    plt.figure()
    plt.title('data with mean subtraction')
    plt.imshow(xn)

    plt.figure()
    plt.title('data projection auto-correlation')
    plt.plot(np.real(xpac), '.')

    plt.figure()
    plt.title('correlate')
    plt.imshow(np.real(y))

    plt.show()
