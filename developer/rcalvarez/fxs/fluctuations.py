# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import reborn.dataframe
from .parallel import ParallelAnalyzer

try:
    from joblib import delayed
    from joblib import Parallel
except ImportError:
    Parallel = None
    delayed = None


def correlate(s1, s2=None, cached=False):
    r"""
    Computes correlation function.
    If two signals are provided computes cross correlation.
    If one signal is provided computes auto correlation.
    If cached, assumes Fourier transforms are already computed.

    Computed via Fourier transforms:
        cf = iFT(FT(s1) FT(s2)*)

    Arguments:
        s1 (|ndarray|): signal 1
        s2 (|ndarray|): signal 2
        cached (bool): provide ffts instead of computing
    Returns:
        correlation (|ndarray|): correlation of s1 and s2
    """
    if not cached:
        s1 = np.fft.fft(s1, axis=1)
        if s2 is not None:
            s2 = np.fft.fft(s2, axis=1)
    if s2 is None:
        s2 = s1.copy()
    cor_fft = s1 * s2.conj()
    correlation = np.fft.ifft(cor_fft, axis=1)
    return np.real(correlation)


def subtract_masked_data_mean(data, mask):
    r"""
    Subtract average q for each q ring in data, ignores masked pixels.
    This normalizes and centers the data around 0.

    Arguments:
        data (|ndarray|): data
        mask (|ndarray|): mask (i.e. data to ignore)
    Returns:
        data (|ndarray|): data - <data>_q
    """
    data[mask == 0] = 0
    d_sum = np.sum(data, axis=1)
    count = np.sum(mask, axis=1)
    d_avg = np.zeros_like(d_sum, dtype=float)
    np.divide(d_sum, count, out=d_avg, where=count != 0)
    d = (data.T - d_avg).T
    d[mask == 0] = 0  # re-zero masked pixels
    return d


def data_correlation(n, data, mask, cached=False):
    r"""
    Computes cross correlation of data with data shifted by n.

    Note: For n = 0 this returns the auto correlation of the data.

    Arguments:
        n (int): number of q rings to shift
        data (|ndarray|): data
        mask (|ndarray|): mask (i.e. data to ignore)
        cached (bool): provide ffts instead of computing
    Returns:
        ccf (|ndarray|): cross correlation of data
    """
    data[mask == 0] = 0
    d_roll = None
    m_roll = None
    if not cached:
        data = subtract_masked_data_mean(data=data, mask=mask)
    if n > 0:
        d_roll = np.roll(data, shift=n, axis=0)
        m_roll = np.roll(mask, shift=n, axis=0)
    d_cf = correlate(s1=data, s2=d_roll, cached=cached)
    m_cf = correlate(s1=mask, s2=m_roll, cached=cached)
    zs = np.zeros_like(data, dtype=float)
    return np.divide(d_cf, m_cf, out=zs, where=m_cf != 0)


def compute_data_correlations(data, mask):
    r"""
    Computes cross correlation of data with data shifted by n.

    Note: For n = 0 this returns the auto correlation of the data.

    Arguments:
        data (|ndarray|): data
        mask (|ndarray|): mask (i.e. data to ignore)
    Returns:
        correlations (dict): correlations of data
    """
    data[mask == 0] = 0
    n_q = data.shape[0]
    data = subtract_masked_data_mean(data=data, mask=mask)
    d = np.fft.fft(data, axis=1)
    m = np.fft.fft(mask, axis=1)
    return [data_correlation(n=n, data=d, mask=m, cached=True) for n in range(n_q)]


class FXSError(Exception):
    def __int__(self, message):
        super.__init__(message)


class ParallelFXS(ParallelAnalyzer):
    experiment_id = None
    n_frames = None
    polar_assembler = None
    run_id = None
    run_sum_correlations = None

    def __init__(
        self,
        framegetter: reborn.fileio.getters.FrameGetter = None,
        polar_assembler: reborn.detector.PolarPADAssembler = None,
        **kwargs,
    ):
        r"""
        Class to compute angular cross correlations.

        Arguments:
            framegetter (|FrameGetter|): The FrameGetter that serves the data for analysis.
            polar_assembler (|PolarPADAssembler|): Assembler to bin data onto a polar grid.
            **kwargs: Any key-word arguments you would like to pass to the base class.
                      See: ..:py:class::`~reborn.analysis.parallel.ParallelAnalyzer`
        """
        super().__init__(framegetter=framegetter, **kwargs)
        self.experiment_id = self.framegetter.experiment_id
        self.run_id = self.framegetter.run_id
        self.polar_assembler = polar_assembler
        if self.polar_assembler is None:
            self.logger.warning(
                "polar_assembler is None, incoming data will be assumed to be polar-binned"
                "and analysis shall proceed."
            )
        else:
            self.logger.info(f"  q range: {self.polar_assembler.q_range}")
            self.logger.info(f"   q bins: {self.polar_assembler.n_q_bins}")
            self.logger.info(f"phi range: {self.polar_assembler.phi_range}")
            self.logger.info(f" phi bins: {self.polar_assembler.n_phi_bins}")
        self.kwargs["polar_assembler"] = polar_assembler
        self.n_frames = 0

    def __str__(self):
        out = f"     Experiment ID: {self.experiment_id}\n"
        out += f"           Run ID: {self.run_id}\n"
        out += f"Patterns Averaged: {self.n_processed}\n"
        return out

    def add_frame(self, dat):
        if dat.validate():
            self.logger.info("Dataframe is valid, adding to analysis ...")
            frame_data = dat.get_raw_data_list()
            frame_mask = dat.get_mask_list()
            if self.polar_assembler is None:
                n_q, n_p = frame_data[0].shape
                frame_polar_data = dat.get_raw_data_flat().reshape(n_q, n_p)
                frame_polar_mask = dat.get_mask_flat().reshape(n_q, n_p)
            else:
                frame_polar_data, frame_polar_mask = self.polar_assembler.get_mean(
                    data=frame_data, mask=frame_mask
                )
            frame_correlations = compute_data_correlations(
                data=frame_polar_data, mask=frame_polar_mask
            )
            if self.run_sum_correlations is None:
                self.run_sum_correlations = frame_correlations
            else:
                for i, fc in enumerate(frame_correlations):
                    self.run_sum_correlations[i] += fc
            self.n_frames += 1
            self.logger.info(f"Patterns Averaged: {self.n_processed}\n")
        else:
            self.logger.warning("Dataframe not valid, skipping ...")

    def to_dict(self):
        fxs_dict = dict(
            experiment_id=self.experiment_id,
            run_id=self.run_id,
            polar_assembler=self.polar_assembler,
            n_frames=self.n_frames,
            run_sum_correlations=self.run_sum_correlations,
        )
        return fxs_dict

    def from_dict(self, fxs_dict):
        self.experiment_id = fxs_dict["experiment_id"]
        self.run_id = fxs_dict["run_id"]
        self.polar_assembler = fxs_dict["polar_assembler"]
        self.n_frames = fxs_dict["n_frames"]
        self.run_sum_correlations = fxs_dict[f"run_sum_correlations"]

    def concatenate(self, fxs_dict):
        self.logger.warning("Merging")
        self.n_frames += fxs_dict["n_frames"]
        if self.run_sum_correlations is None:
            self.run_sum_correlations = fxs_dict["run_sum_correlations"]
        else:
            for i, c in enumerate(fxs_dict["run_sum_correlations"]):
                self.run_sum_correlations[i] += c


FXS = ParallelFXS  # backwards compatibility
