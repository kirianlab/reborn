import h5py
import os

from cxidb import CXIDBWriter, CXIDBFrameGetter
from reborn.dataframe import DataFrame
from reborn.fileio.getters import FrameGetter
from reborn.source import Beam
from reborn.detector import PADGeometry, PADGeometryList


class TestFrameGetter(FrameGetter):

    def __init__(self):
        self.n_frames = 10
        self.beam = Beam()
        g1 = PADGeometry(distance=0.5,
                         pixel_size=75e-6,
                         shape=(128, 128))
        self.geom = PADGeometryList(g1)
        self.experiment_id = "testing"

    def get_data(self, frame_number: int = 0) -> DataFrame:
        return DataFrame(pad_geometry=self.geom,
                         beam=self.beam,
                         raw_data=self.geom.ones() + frame_number ** 2)


def test_CXIDBWriter():
    fg = TestFrameGetter()
    writer = CXIDBWriter(filename="./testing.cxi",
                         framegetter=fg,
                         chunk_size=1024,
                         check_geometry=False)
    writer.write_frames()
    assert os.path.isfile("./testing.cxi")
    with h5py.File("./testing.cxi", "r") as h5file:
        assert "cxi_version" in h5file.keys()
        assert "entry_1" in h5file.keys()
        assert "data" in h5file["entry_1"].keys()


def test_CXIDBFrameGetter():
    cxidb_fg = CXIDBFrameGetter(filename="./testing.cxi")
    assert cxidb_fg.n_frames == 10
    df = cxidb_fg.get_first_frame()
    assert df.get_raw_data_flat().sum() == 128 ** 2
    assert isinstance(df.get_beam(), Beam)
    assert isinstance(df.get_pad_geometry(), PADGeometryList)

