import argparse
import dask.array as da
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import coo_matrix

import os
import sys

reborn_path = os.path.abspath(
    os.path.join("/home/bobca/p005376/usr/Shared/rcalvarez/reborn/")
)
sys.path.insert(0, reborn_path)

import reborn
from reborn.const import eV
from reborn.external.crystfel import geometry_file_to_pad_geometry_list
from reborn.viewers.mplviews import view_pad_data

from dask import compute
from dask.distributed import Client, progress
from dask_jobqueue import SLURMCluster
from extra_data import open_run
from extra_geom import AGIPD_1MGeometry


def get_run_data(run_num):
    run = open_run(proposal=5376, run=run_num, data="proc")
    detector_data = [run.get_dask_array(f'SPB_DET_AGIPD1M-1/DET/{m}CH0:xtdf', 'image.data') for m in range(1, 16)]
    data = da.stack(detector_data)
    return data.transpose(1, 0, 2, 3).rechunk((100, 15, 512, 128))  # 790 MB (64 bit)


def compute_run_statistics(run_data, rejection_frames=None):
    if rejection_frames is not None:
        run_data = run_data[rejection_frames]
    n, x, s, b, s2 = compute(run_data.min(axis=0),
                             run_data.max(axis=0),
                             run_data.sum(axis=0),
                             run_data.sum(axis=3).sum(axis=2).sum(axis=1),
                             (run_data ** 2).sum(axis=0))
    return {"sum": s,
            "min": n,
            "max": x,
            "sum2": s2,
            "brightness": b}


def compute_radial_operator(n_bins, bin_fudge=1.001, q_mags, weights):
    bins = np.linspace(0, q_mags.max() * bin_fudge, n_bins + 1)
    qbin_i = np.digitize(q_mags, bins=bins) - 1
    return coo_matrix((weights, (qbin_i, np.arange(15 * 512 * 128))), shape=(n_bins, 15 * 512 * 128))


def compute_radial_stack(radial_operator, data):
    return da.dot(radial_operator.tocsr(), data.T).compute()


parser = argparse.ArgumentParser()
parser.add_argument("--bins", type=int, default=1024, help="number of radial bins")
parser.add_argument("--run", type=int, default=1, help="run number")
args = parser.parse_args()

print(f"PAD statics and radial stack ({args.bins} bins) will be computed for run {args.run}.")
print(f"The frame brightnesses, log10(run-sum), and radial stack will be plotted.")
print(f"The results will be saved to ./{args.run}.npz")

detector_distance = 1
photon_energy = 6 * 1000 * eV
beam_diameter = 300e-9
mask = np.load("/home/bobca/p005376/usr/Shared/geom/weird_panel_mask.npy")[1:].ravel()
geompath = "/home/bobca/p005376/usr/Shared/geom/motor_p5376_from_4462.geom"
pad = geometry_file_to_pad_geometry_list(geompath)
g = extra_geom.AGIPD_1MGeometry.from_crystfel_geom(geompath)
pad.translate([0, 0, detector_distance])
beam = reborn.source.Beam(photon_energy=photon_energy, diameter_fwhm=beam_diameter)
q_mags = pad.q_mags(beam=beam).reshape((16, 512, 128))[1:].ravel()
sa = pad.solid_angles().reshape((16, 512, 128))[1:].ravel()

cluster = SLURMCluster(
    queue='upex',
    local_directory='/scratch',
    processes=16, cores=16, memory='512GB',
)
cluster.scale(128)
client = Client(cluster)
print("Created dask client:", client)

print(f"Collecting data from run: {args.run}")
data = get_run_data(run_num=args.run)
print(f"Computing data statics")
stats = run_statistics(run_data=data * mask)
print(f"Computing radial stack")
ro = compute_radial_operator(n_bins=args.bin, q_mags=q_mags, weights=sa * mask)
radials = compute_radial_stack(radial_operator=ro, data=data)

client.close()
cluster.close()

plt.figure()
plt.title("Frame Brightness")
plt.xlabel("Frame")
plt.ylabel("Intensity")
plt.plot(stats["brightness"], ".k")
plt.show()

g.plot_data_fast(np.log10(np.append(np.zeros((512, 128)), stats["sum"]).reshape((16, 512, 128))))

plt.figure()
plt.title(f"Run {args.run} Radial Stack")
plt.xlabel("$q = 4 \pi \sin(\theta) / \lambda [\AA^{-1}]$")
plt.ylabel("Frame")
plt.xticks()
plt.yticks()
plt.imshow(radials)
plt.show()

np.savez(f"./{args.run}", radials=radials, **stats)
