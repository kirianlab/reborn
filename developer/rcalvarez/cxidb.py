import h5py
import numpy as np
import os

from reborn.dataframe import DataFrame
from reborn.detector import PADGeometry, PADGeometryList
from reborn.fileio.getters import FrameGetter
from reborn.source import Beam


class CXIDBWriter:

    _file = None
    _getter = None

    def __init__(self,
                 filename: str,
                 framegetter: FrameGetter,
                 chunk_size: int = 1024,
                 check_geometry: bool = False):
        r"""
        Class to write CXIDB files from a FrameGetter.

        Args:
            filename (str): File where data will be written.
            framegetter (|FrameGetter): FrameGetter that serves data.
            chunk_size (int, optional): How many frames to save per file (default = 1024).
            check_geometry (bool, optional): Check if geometry changes between frames (default = False).
        """
        self._file = filename
        self._getter = framegetter
        self._chunk_size = chunk_size
        self._check_geometry = check_geometry
        self.n_frames = self._getter.n_frames
        try:
            expid = self._getter.experiment_id
        except AttributeError:
            expid = None
        try:
            runid = self._getter.run_id
        except AttributeError:
            runid = None
        dataframe = self._getter.get_first_frame()
        beam = dataframe.get_beam()
        geom = dataframe.get_pad_geometry()
        data = dataframe.get_raw_data_flat()
        with h5py.File(name=self._file, mode="w") as hf:
            hf.create_dataset(name="cxi_version", data=160)
            hf.create_dataset(name="entry_1/experimental_identifier",
                              data=f"{expid}:{runid}")
            hf.create_dataset(name="entry_1/source_1/energy",
                              data=beam.photon_energy)
            hf.create_dataset(name="entry_1/source_1/pulse_energy",
                              data=beam.pulse_energy)
            hf.create_dataset(name="entry_1/source_1/pulse_width",
                              data=beam.diameter_fwhm)
            hf.create_dataset(name="/entry_1/instrument_1/detector_1/distance",
                              data=geom.average_detector_distance(beam=beam))
            hf.create_dataset(name="/entry_1/instrument_1/detector_1/basis_vectors",
                              data=np.array([(g.t_vec, g.fs_vec, g.ss_vec) for g in geom]))
            hf.create_dataset(name="/entry_1/instrument_1/detector_1/t_vec",
                              data=np.array([g.t_vec for g in geom]))
            hf.create_dataset(name="/entry_1/instrument_1/detector_1/fs_vec",
                              data=np.array([g.fs_vec for g in geom]))
            hf.create_dataset(name="/entry_1/instrument_1/detector_1/ss_vec",
                              data=np.array([g.ss_vec for g in geom]))
            hf.create_dataset(name="/entry_1/instrument_1/detector_1/n_fs",
                              data=np.array([g.n_fs for g in geom]))
            hf.create_dataset(name="/entry_1/instrument_1/detector_1/n_ss",
                              data=np.array([g.n_ss for g in geom]))
        os.makedirs(name=f"{self._file}_chunks", exist_ok=True)
        frame_nums = list(range(self.n_frames))
        self.indexer = {f"{self._file}_chunks/{self._file}_chunk_{i:06d}": frame_nums[f:f + self._chunk_size]
                        for i, f in enumerate(range(0, self.n_frames, self._chunk_size))}
        for k in self.indexer.keys():
            with h5py.File(name=k, mode="w") as hf:
                hf.create_dataset(name="cxi_version", data=160)
        self.n_pixels = geom.n_pixels
        self.layout = h5py.VirtualLayout(shape=(self.n_frames, self.n_pixels),
                                         dtype=data.dtype)

    def write_frames(self):
        for k, frames in self.indexer.items():
            with h5py.File(name=k, mode="a") as hf:
                dset = hf.create_dataset(name="/entry_1/instrument_1/detector_1/data",
                                         shape=(len(frames), self.n_pixels))
                for f in frames:
                    dataframe = self._getter.get_frame(frame_number=f)
                    dset[f] = dataframe.get_raw_data_flat()
        with h5py.File(name=self._file, mode="a") as hf:
            for k, frames in self.indexer.items():
                with h5py.File(name=k, mode="a") as h5:
                    self.layout[frames] = h5py.VirtualSource(h5["/entry_1/instrument_1/detector_1/data"])
            hf.create_virtual_dataset(name="/entry_1/data",
                                      layout=self.layout,
                                      fillvalue=0)


class CXIDBFrameGetter(FrameGetter):

    _file = None

    def __init__(self, filename: str):
        self._file = h5py.File(name=filename, mode="r")
        self.beam = Beam(photon_energy=self._file["entry_1/source_1/energy"][()],
                         pulse_energy=self._file["entry_1/source_1/pulse_energy"][()],
                         diameter_fwhm=self._file["entry_1/source_1/pulse_width"][()])
        distance = self._file["entry_1/instrument_1/detector_1/distance"][()]
        basis_t = self._file["/entry_1/instrument_1/detector_1/t_vec"][:]
        basis_f = self._file["/entry_1/instrument_1/detector_1/fs_vec"][:]
        basis_s = self._file["/entry_1/instrument_1/detector_1/ss_vec"][:]
        n_fs = self._file["/entry_1/instrument_1/detector_1/n_fs"][:]
        n_ss = self._file["/entry_1/instrument_1/detector_1/n_ss"][:]
        self.geom = PADGeometryList()
        for t, f, s, nf, ns in zip(basis_t, basis_f, basis_s, n_fs, n_ss):
            pad = PADGeometry(distance=distance)
            pad.n_fs = nf
            pad.n_ss = ns
            pad.t_vec = t
            pad.fs_vec = f
            pad.ss_vec = s
            self.geom.append(pad)
        self.n_frames = self._file["/entry_1/data"].shape[0]

    def get_data(self, frame_number: int) -> DataFrame:
        df = DataFrame(beam=self.beam,
                       raw_data=self._file["entry_1/data"][frame_number],
                       pad_geometry=self.geom)
        return df
