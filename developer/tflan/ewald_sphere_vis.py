import numpy as np
import matplotlib.pyplot as plt
from reborn.detector import jungfrau4m_pad_geometry_list
from reborn.viewers.qtviews.padviews import PADView
from reborn.source import Beam
from reborn.utils import vec_norm
from reborn.const import keV
# This instance of the "Beam" class holds the incoming wavevector.  It also
# holds other parameters like polarization and pulse energy.
beam = Beam(photon_energy=5*keV, beam_vec=[0, 0, 1])
# This function makes a PADGeometryList class instance corresponding to a
# detector called the "Jungfrau 4M" which we use at LCLS often.
# The PAD ("pixel array detector") geometry specification is described in
# the docs here:
# https://kirianlab.gitlab.io/reborn/geometry.html
# Like most detectors made for XFELs, the Jungrau 4M comprises several
# PADs with separate electronics for fast data collection.
geom = jungfrau4m_pad_geometry_list()
# We need to set the average distance from the "interaction point" (where
# the proteins are hit by the focused x-rays beam) to the nominal "detector
# plane"
geom.set_average_detector_distance(beam=beam, distance=0.1)
# (Note that the units are SI throughout reborn, for consistency.)
# The "q" vectors are the outgoing wavevectors minus the incoming wavevector.
# There is just a single incoming wavevector, but each pixel has its own
# outgoing wavevector.  This can be done easily in reborn like this:
q = geom.q_vecs(beam)
# However, for our task we will need the q vectors for each of the four
# corners of each pixel, whereas the above produces q vectors that
# correspond to the centers of the pixels.
# Let's break down the process of making the q vectors.  Here is the
# incoming wavevector:
k0 = beam.k_in
print(k0)
# The outgoing wavevectors require that we compute unit vectors pointing
# to each pixel, and then we multipy those unit vectors by 4 pi / lambda.
# We can get the pixel positions from the PADGeometryList class:
pvecs = geom.position_vecs()


###############################################################################
# # # Let's have a look at the raw pixel coordinates.  I will display only
# # # every 100th pixel since matplotlib sucks with large data (that's partly
# # # why we use pyqtgraph in the reborn GUIs).
# # fig = plt.figure()
# # ax = fig.add_subplot(projection='3d')
# # ax.scatter(pvecs[::100, 0], pvecs[::100, 1], pvecs[::100, 2], marker='.', s=0.5)
# # # Include one point (orange "x") for the origin:
# # ax.scatter(0, 0, 0, marker='x')
# # ax.set_aspect('equal')
# # plt.show()
# # # We can also display the diffraction as a projection onto a plane using
# # # the PADView GUI.  We don't have a diffraction pattern to look at, so
# # # let's just display some random numbers:
# # pv = PADView(data=np.random.rand(geom.n_pixels), pad_geometry=geom, beam=beam)
# # pv.start()
# # # Notice the "hole" in the middle of the detector - the XFEL beam would
# # # burn a hole into the detector if there wasn't one there already...
# # # Now let's display the pixel coordinates in "q space" instead of
# # # "laboratory space".  First we need the outgoing wavevectors:
# # k = 2*np.pi/beam.wavelength * vec_norm(pvecs)
# # # then we make the q vectors simply by taking a difference:
# # q = k - k0
# # # Have a look:
# # fig = plt.figure()
# # ax = fig.add_subplot(projection='3d')
# # ax.scatter(q[::100, 0], q[::100, 1], q[::100, 2], marker='.', s=1)
# # # Include one point (orange "x") for the origin:
# # ax.scatter(0, 0, 0, marker='x')
# # ax.set_aspect('equal')
# # plt.show()
###############################################################################



# As you can see, all the detector pixels in laboratory space map to
# q vectors that lie on the surface of the "Ewald sphere".  The spherical
# geometry is due to the vector difference q = k - k0 making a sphere when
# you vary the direction of k while holding its length fixed.  The lengths
# of k and k0 are fixed because the scattering is elastic, and hence there
# is no wavelength/energy shift in the scattered wave.
# Note that the origin (orange x) in laboratory space is not in the plane
# of any PADs.  That makes sense, since we would not put the PAD at the
# x-ray focus.  But in q space, the origin is *always* on the surface of the
# Ewald sphere.  The origin corresponds to "forward scattering" where k = k0,
# i.e q=0.

# With the basics of the mapping between PAD pixels and the Ewald sphere
# understood, now comes the complicated part.  We want to render each
# detector pixel as a square in 3D space.  We want the color of the square
# to correspond to the diffraction intensity, and we want the corners
# of each square to lie in the appropriate positions on the Ewald sphere.
# I believe that in gl we must split each square into two triangles. I
# added a method to get the four corners of the pixels as follows:
cvecs = geom.pixel_corner_position_vecs()
# Convert to q vectors
cvecs /= np.sqrt(np.sum(cvecs**2, axis=-1, keepdims=True))
cvecs = cvecs*4*np.pi/beam.wavelength - k0


###############################################################################
# # # Let's have a look.  We'll plot the results by using different markers
# # # for each corner.  We'll show only every 100th pixel.  Zooming in should
# # # reveal squares with different colored corners for each displayed pixel.
# # fig = plt.figure()
# # ax = fig.add_subplot(projection='3d')
# # markers = ["d", "o", "x", "."]
# # for i in range(4):
# #     ax.scatter(cvecs[::100, i, 0], cvecs[::100, i, 1], cvecs[::100, i, 2], marker=markers[i], s=1)
# # ax.set_aspect('equal')
# # plt.show()
###############################################################################

# Now we have an array of q vectors corresponding to the corners of the
# pixels.  The array has shape (N, 4, 3), where there are N total pixels
# in the detector.  To split the detector squares into two triangles, we
# can get the triangles vertices as follows: The first triangles are:
# v1 = cvecs[:, 0, :], v2 = cvecs[:, 1, :] and v3 = cvecs[:, 2, :]
# where the arrayse have shape (N, 3).  The other triangle vertices are:
# v1 = cvecs[:, 1, :], v2 = cvecs[:, 2, :] and v3 = cvecs[:, 3, :].




import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
import pyqtgraph.opengl as gl

from pyqtgraph.opengl import MeshData



app = pg.mkQApp()
w = gl.GLViewWidget()
w.show()
w.setWindowTitle('pyqtgraph example')
w.setCameraPosition(distance=15*10**9, azimuth=-90)

sphere_md = gl.MeshData.sphere(rows=10, cols=20)



#print(q.shape)
#print(q[1:10])

#print(pvecs[1:10])
print(cvecs[0:2])
print(cvecs.shape)

print('*******')
#print(q[1:3])

#q = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]], dtype=float)


######## point based #######
# sp2 = gl.GLScatterPlotItem(pos=q[::100],
#                            size=0.05,
#                            color=(0,1,0,1),
#                            pxMode = False,
#                            glOptions='opaque')
# w.addItem(sp2)

#sphere = gl.GLMeshItem(meshdata=sphere_md, smooth=True, color=(1, 0, 0, 0.2), shader='balloon', glOptions='opaque')
#sphere.translate(x[2], 0, 0)
#w.addItem(sphere)


###### square based #########

#cvecs = cvecs[::100]

num_sqs = cvecs.shape[0]
square = [0,1,2, 0,2,3]

cvecs = cvecs.reshape(-1,3)

print(cvecs[0:8])
print(cvecs.shape)
print(num_sqs)

colors = np.ones((num_sqs, 4), dtype=float)
colors[:,1] = np.random.rand(num_sqs)


print(colors[0:5])
print( np.repeat(colors[0:5],2, axis=0))
colors = np.repeat(colors,2, axis=0)


cidx = np.arange(num_sqs) * 4
cidx = np.repeat(cidx, 6)
cidx = np.tile(square, num_sqs) + cidx

print(cidx.shape)
cidx = cidx.reshape(-1, 3) 

print(cidx[0:8])

pixels_md = gl.MeshData(vertexes = cvecs,
                        faces = cidx,
                        faceColors = colors)
pixels_mesh = gl.GLMeshItem(meshdata=pixels_md,
                            smooth=False,
                            computeNormals=False,
                            #color=(1, 0, 0, 1),
                            glOptions='opaque')
w.addItem(pixels_mesh)






## create three grids, add each to the view
xgrid = gl.GLGridItem()
ygrid = gl.GLGridItem()
zgrid = gl.GLGridItem()
#w.addItem(xgrid)
#w.addItem(ygrid)
w.addItem(zgrid)

## rotate x and y grids to face the correct direction
xgrid.rotate(90, 0, 1, 0)
ygrid.rotate(90, 1, 0, 0)

## scale each grid differently
xgrid.scale(2, 2, 1)
ygrid.scale(2, 2, 1)
zgrid.scale(2, 2, 1)



if __name__ == '__main__':
    pg.exec()



