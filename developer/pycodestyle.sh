#!/bin/bash
# Checks for compliance with PEP8, excluding some exceptions.
if [ "$1" = "" ]; then
  find ../reborn -name '*.py' -exec bash pycodestyle.sh \{} \;
else
  # sed --in-place 's/[[:space:]]\+$//' "$1"
  black "$1"
  pycodestyle --first --show-source "$1"  # --max-line-length 120
  pylint --disable too-few-public-methods \
         --disable too-many-instance-attributes \
         --disable too-many-arguments \
         --disable invalid-name \
			--disable too-many-public-methods \
			--disable too-many-lines \
			--disable too-many-return-statements \
			--disable too-many-locals \
			--max-line-length=120 \
			--extension-pkg-whitelist=numpy,reborn.fortran \
			$1
fi
