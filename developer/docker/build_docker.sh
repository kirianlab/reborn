#!/bin/bash

# Note: this will build the docker image locally.
# A lot of space might be taken on your HD if you do many builds.
# You can clear out space via the command
# > docker system prune -a
#
# In order to make the docker image available for the Gitlab runner,
# you need to "push" it, which can be done by adding the word push
# upon execution of this script.  I.e., do the following
# > bash build_docker.sh push

if [[ ! $(basename "$(pwd)") = 'docker' ]]; then
    echo 'This script should run in the developer/docker directory.'
    exit 1
fi

version=24.04

# This builds the Docker image:
docker build -t registry.gitlab.com/kirianlab/reborn/ubuntu-$version:thinkpad .

# This makes the Docker image available to gitlab runners:
if [[ "$1" == "push" ]]; then
    docker login registry.gitlab.com
    docker push registry.gitlab.com/kirianlab/reborn/ubuntu-$version:thinkpad
fi

if [[ "$1" == "run" ]]; then
  docker run -it registry.gitlab.com/kirianlab/reborn/ubuntu-$version:thinkpad /bin/bash
fi
