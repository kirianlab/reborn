# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import shutil
import numpy as np
from reborn import detector
from reborn import source
from reborn import dataframe
from reborn import temp_dir
from reborn.fileio.getters import ListFrameGetter
import reborn
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.analysis import runstats, saxs
from reborn.const import eV

np.random.seed(0)


def test_padstats():
    geom = detector.cspad_2x2_pad_geometry_list()
    geom = geom.binned(10)
    beam = source.Beam(wavelength=1e-10)
    dataframes = []
    for i in range(3):
        dat = geom.zeros() + i
        df = dataframe.DataFrame(raw_data=dat, pad_geometry=geom, beam=beam)
        dataframes.append(df)
    fg = ListFrameGetter(dataframes)
    histparams = dict(bin_min=0, bin_max=10, n_bins=11, n_pixels=geom.n_pixels)
    pad_stats = runstats.ParallelPADStats(
        framegetter=fg, histogram_params=histparams, n_processes=1
    )
    pad_stats.process_frames()
    stats = pad_stats.to_dict()
    assert isinstance(stats, dict)
    assert stats["sum"].flat[0] == 3
    filepath = temp_dir + "/stats.npz"
    runstats.save_padstats(stats, filepath)
    stats2 = runstats.load_padstats(filepath)
    assert isinstance(stats, dict)
    assert stats["sum"].flat[0] == stats2["sum"].flat[0]


geom = reborn.detector.cspad_2x2_pad_geometry_list(detector_distance=0.1, binning=10)
beam = reborn.source.Beam(
    photon_energy=9500 * eV, diameter_fwhm=1e-6, pulse_energy=1e-4
)
mask = geom.edge_mask()
log_file = "logs/padstats"
checkpoint_file = "checkpoints/padstats"
message_prefix = "Test Run -"


def delete_checkpoints():
    shutil.rmtree("logs", ignore_errors=True)
    shutil.rmtree("checkpoints", ignore_errors=True)


class Getter(reborn.fileio.getters.FrameGetter):
    def __init__(self, geom=None, beam=None, mask=None, n_frames=10, **kwargs):
        self.geom = geom
        self.beam = beam
        self.mask = mask
        self.n_frames = n_frames
        self.none_frames = np.zeros(self.n_frames)
        pat = get_pad_solution_intensity(
            pad_geometry=self.geom, thickness=3e-6, beam=self.beam, poisson=False
        )
        self.pat = geom.concat_data(pat)
        self.gain = np.random.normal(loc=20, size=geom.n_pixels, scale=2)
        self.offset = np.random.normal(loc=0, size=geom.n_pixels, scale=2)

    def get_data(self, frame_number=0):
        if self.none_frames[frame_number]:
            return None
        pat = np.random.poisson(self.pat).astype(float) * self.gain
        pat += np.random.normal(loc=self.pat, scale=self.gain / 5)
        df = reborn.dataframe.DataFrame(
            raw_data=pat, mask=self.mask, pad_geometry=self.geom, beam=self.beam
        )
        return df


def test_01():
    # Test single process
    delete_checkpoints()
    framegetter = Getter(geom=geom, mask=mask, beam=beam)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        n_processes=1,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    print(stats["histogram"].shape)
    assert np.sum(stats["histogram"]) == np.sum(mask) * framegetter.n_frames
    # Stupid test: what happens if you run process_frames again?  It should re-load checkpoints from the
    # previous run.
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == np.sum(mask) * framegetter.n_frames
    # Test reload from checkpoint
    framegetter = Getter(geom=geom, mask=mask, beam=beam)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        n_processes=1,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == np.sum(mask) * framegetter.n_frames
    # Test q histogram conversion
    hist = runstats.PixelHistogram(**stats["histogram_params"])
    hist.histogram = stats["histogram"]
    q_histogram = hist.convert_to_q_histogram(
        pad_geometry=geom, beam=beam, mask=mask, n_q_bins=10, q_range=(0, 3e10)
    )
    assert np.sum(q_histogram) == np.sum(mask) * framegetter.n_frames
    # runstats.view_padstats(stats, histogram=True)


def test_02():
    # Test multiple processes
    delete_checkpoints()
    framegetter = Getter(geom=geom, mask=mask, beam=beam)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        n_processes=2,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == np.sum(mask) * framegetter.n_frames
    # runstats.view_padstats(stats, histogram=True)


def test_03():
    # Test skipping
    delete_checkpoints()
    framegetter = Getter(geom=geom, mask=mask, beam=beam)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        start=1,
        stop=7,
        step=2,
        n_processes=2,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == np.sum(mask) * 3


def test_04():
    # Test fewer frames than processes
    delete_checkpoints()
    framegetter = Getter(geom=geom, mask=mask, beam=beam, n_frames=1)
    padstats = runstats.ParallelPADStats(
        framegetter=framegetter,
        n_processes=2,
        log_file=log_file,
        checkpoint_file=checkpoint_file,
        checkpoint_interval=5,
        message_prefix=message_prefix,
        histogram_params=dict(bin_min=0, bin_max=10, n_bins=10),
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    assert np.sum(stats["histogram"]) == np.sum(mask) * framegetter.n_frames
