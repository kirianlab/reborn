# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from reborn import detector
from reborn import source
from reborn.analysis import saxs
from reborn.simulate import gas
from reborn.const import eV

np.random.seed(0)


def test_radial_profiler_00():
    """ Before moving on to RadialProfiler tests, first confirm
    that PADGeometry beam centering is correct. """
    n = 3
    geom = detector.PADGeometry(pixel_size=100e-6, distance=1, shape=(n, n))
    beam = source.Beam(wavelength=1.0e-10)
    q_mags = geom.q_mags(beam=beam)
    q_mags = geom.reshape(q_mags)
    assert q_mags[1, 1] == 0
    assert q_mags[0, 1] == q_mags[2, 1]


def test_radial_profiler_01():
    """ Make a simple 3x3 detector.  Make sure that integrations are as expected
    for a totally flat diffraction pattern, with the inclusion of masks.  """
    geom = detector.PADGeometry(shape=(3, 3), distance=1.0, pixel_size=100e-6)
    beam = source.Beam(wavelength=1.0e-10)
    # q_mags = geom.q_mags(beam=beam)
    # print(q_mags)
    # q_mags = [8885765.80967349 6283185.28361764 8885765.80967349 6283185.28361764 0.
    #           6283185.28361764 8885765.80967349 6283185.28361764 8885765.80967349]
    dat = np.ones([3, 3])
    rad = saxs.RadialProfiler(pad_geometry=geom, beam=beam, mask=None, n_bins=3, q_range=[0, 9283185])
    # print(rad.bin_edges)
    # rad.bin_edges = [-2320796.25  2320796.25  6962388.75 11603981.25]
    profile = rad.get_sum_profile(dat)
    assert profile[0] == 1
    assert profile[1] == 4
    assert profile[2] == 4
    profile = rad.get_mean_profile(dat)
    assert profile[0] == 1
    assert profile[1] == 1
    assert profile[2] == 1
    mask = np.ones([3, 3])
    mask[0, 0] = 0
    profile = rad.get_sum_profile(dat, mask=mask)  # Sums over
    assert profile[0] == 1
    assert profile[1] == 4
    assert profile[2] == 3
    mask = np.ones([3, 3])
    mask[0, 0] = 0
    rad.set_mask(mask)
    profile = rad.get_sum_profile(dat)
    assert profile[0] == 1
    assert profile[1] == 4
    assert profile[2] == 3
    mask = np.ones([3, 3])
    mask[0, 0] = 0
    rad.set_mask(mask)
    profile = rad.get_mean_profile(dat)
    assert profile[0] == 1
    assert profile[1] == 1
    assert profile[2] == 1
    stats = rad.quickstats(dat)
    profile = stats["mean"]
    assert profile[0] == 1
    assert profile[1] == 1
    assert profile[2] == 1


def test_radial_profiler_02():
    """ Make a simple 3x3 detector.  Make sure that integrations are as expected
    for a non-uniform diffraction pattern, with the inclusion of masks.  """
    geom = detector.PADGeometry(shape=(3, 3), distance=1.0, pixel_size=100e-6)
    beam = source.Beam(wavelength=1.0e-10)
    # q_mags = geom.q_mags(beam=beam)
    # print(q_mags)
    # q_mags = [8885765.80967349 6283185.28361764 8885765.80967349 6283185.28361764 0.
    #           6283185.28361764 8885765.80967349 6283185.28361764 8885765.80967349]
    dat = np.ones([3, 3])
    dat[0, 0] = 9
    dat[0, 1] = 5
    # The data looks like:
    # 9  5  1
    # 1  1  1
    # 1  1  1
    rad = saxs.RadialProfiler(pad_geometry=geom, beam=beam, mask=None, n_bins=3, q_range=[0, 9283185])
    # print(rad.bin_edges)
    # rad.bin_edges = [-2320796.25  2320796.25  6962388.75 11603981.25]
    profile = rad.get_sum_profile(dat)
    assert profile[0] == 1
    assert profile[1] == 8
    assert profile[2] == 12
    profile = rad.get_mean_profile(dat)
    assert profile[0] == 1
    assert profile[1] == 2
    assert profile[2] == 3
    mask = np.ones([3, 3])
    mask[0, 0] = 0
    mask[0, 1] = 0
    profile = rad.get_sum_profile(dat, mask=mask)  # Sums over
    assert profile[0] == 1
    assert profile[1] == 3
    assert profile[2] == 3
    rad.set_mask(mask)
    profile = rad.get_sum_profile(dat)
    assert profile[0] == 1
    assert profile[1] == 3
    assert profile[2] == 3
    # rad.set_mask(mask)
    profile = rad.get_mean_profile(dat)
    assert profile[0] == 1
    assert profile[1] == 1
    assert profile[2] == 1
    stats = rad.quickstats(dat)
    profile = stats["mean"]
    assert profile[0] == 1
    assert profile[1] == 1
    assert profile[2] == 1
    # Uniform weights should have no effect on previous results
    weights = np.ones((3, 3))
    stats = rad.quickstats(dat, weights=weights*mask)
    profile = stats["mean"]
    assert profile[0] == 1
    assert profile[1] == 1
    assert profile[2] == 1
    # Reduce the solid angle of the second ring by 0.5.  We do
    # not reduce the intensities.  Hence, the weighted-average
    # profile should be 2-fold higher in the second bin.
    weights = np.ones((3, 3))
    weights[0, 1] = 0.5
    weights[1, 0] = 0.5
    weights[1, 2] = 0.5
    weights[2, 1] = 0.5
    stats = rad.quickstats(dat, weights=weights*mask)
    profile = stats["mean"]
    assert profile[0] == 1
    assert profile[1] == 2
    assert profile[2] == 1


def test_radial_profiler_04():
    geom = []
    pad = detector.PADGeometry()
    pad.t_vec = [0, 0.01, 0.5]
    pad.fs_vec = [100e-6, 0, 0]
    pad.ss_vec = [0, 100e-6, 0]
    pad.n_fs = 100
    pad.n_ss = 150
    geom.append(pad)
    pad = detector.PADGeometry()
    pad.t_vec = [0, -0.01, 0.5]
    pad.fs_vec = [100e-6, 0, 0]
    pad.ss_vec = [0, 100e-6, 0]
    pad.n_fs = 100
    pad.n_ss = 150
    geom.append(pad)
    geom = detector.PADGeometryList(geom)
    beam = source.Beam(wavelength=1.5e-10)
    rad = saxs.RadialProfiler(pad_geometry=geom, beam=beam, mask=None, n_bins=100, q_range=None)
    data = np.linspace(0, 1, geom.n_pixels)
    prof = rad.get_mean_profile(data)
    assert np.max(prof) <= 1
    assert np.min(prof) >= 0


def test_radial_profiler_05():
    geom = detector.cspad_2x2_pad_geometry_list()
    beam = source.Beam(wavelength=1.0e-10)
    rad = saxs.RadialProfiler(beam=beam, pad_geometry=geom, mask=None, n_bins=100)
    data = geom.random()
    prof1 = rad.get_mean_profile(data)
    prof2 = rad.get_profile_statistic(data, statistic=np.mean)
    prof3 = rad.get_profile_statistic(data=data, statistic=[np.mean])
    assert np.max(np.abs(prof1 - prof2)) < 1e-10
    assert np.max(np.abs(prof3 - prof2)) < 1e-10


def test_radial_profiler_06():
    geom = detector.cspad_2x2_pad_geometry_list(binning=4, detector_distance=0.1)
    beam = source.Beam(photon_energy=9000*eV, pulse_energy=1e-3)
    q_mags = geom.q_mags(beam=beam)
    q_range = [0, np.max(q_mags)]
    n_bins = 100
    pressure = 101325.0
    data = gas.get_gas_background(pad_geometry=geom, beam=beam, gas_type=("He", "H2O"), pressure=pressure*np.array([1, 0]), path_length=[0, 1e-6])
    sap = geom.solid_angles()*geom.polarization_factors(beam=beam)
    data *= sap
    mask = geom.ones()
    mask[0:100] = 0
    weights = mask*sap
    profiler = saxs.RadialProfiler(beam=beam, pad_geometry=geom, mask=weights, q_range=q_range, n_bins=n_bins)
    stats = profiler.quickstats(data=data/sap, weights=weights)
    data_profile = stats["mean"]
    profile_q_mags = profiler.bin_centers
    expected_profile = gas.isotropic_gas_intensity_profile(q_mags=profile_q_mags, molecule="He", beam=beam)
    w = np.where(data_profile > 0)
    a = data_profile[w] / np.mean(data_profile[w])
    b = expected_profile[w] / np.mean(expected_profile[w])
    # print("a", a)
    # print("b", b)
    # import matplotlib.pyplot as plt
    # plt.plot(profile_q_mags[w], a, label="a")
    # plt.plot(profile_q_mags[w], b, label="b")
    # plt.legend()
    # plt.show()
    assert np.max(np.abs((a-b)/b)) < 1e-3
