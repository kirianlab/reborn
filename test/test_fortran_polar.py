# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from reborn.misc import polar


def test_polar_stats():
    print()
    nq = 3
    nphi = 4
    data = np.arange(nq * nphi, dtype=np.float64).reshape(nq, nphi)
    data[0, 1]
    weights = np.ones_like(data)
    weights[0, 1] = 0
    q = np.arange(nq, dtype=np.float64) + 1
    p = np.arange(nphi, dtype=np.float64) + 1
    q, p = np.meshgrid(q, p, indexing="ij")
    q = q.ravel()
    p = p.ravel()
    stats = polar.stats(
        data,
        q,
        p,
        weights=weights,
        n_q_bins=nq,
        q_min=1,
        q_max=nq,
        n_p_bins=nphi,
        p_min=1,
        p_max=nphi,
        sum_=None,
        sum2=None,
        w_sum=None,
    )
    assert stats["mean"][0, 0] == 0
    assert stats["mean"][0, 1] == 0
    assert stats["mean"][0, 2] == 2
