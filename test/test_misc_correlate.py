import numpy as np
import matplotlib.pyplot as plt
from reborn.misc import correlate
# from reborn.experimental import pearson


def test_misc():
    a = np.zeros((5, 10))
    a[2, 4] = 1
    a[3, 6] = 1
    slc = correlate._compact_slice(a)
    assert slc[0].start == 2
    assert slc[0].stop == 4
    assert slc[1].start == 4
    assert slc[1].stop == 7
    b = a[slc]
    assert b[0, 0] == 1
    assert b[-1, -1] == 1
    assert np.sum(b) == 2


def shift_array(x, shift):
    nv, nw = x.shape
    x = np.pad(x, ((0, np.abs(shift[0])), (0, np.abs(shift[1]))))
    x = np.roll(x, shift, (0, 1))
    return x[0:nv, 0:nw]


def pad2(x):
    nv, nw = x.shape
    return np.pad(x, ((0, nv), (0, nw)))


def check_errors(shape, shift, maskfrac, show_plots, print_stuff):
    nv, nw = shape
    v = np.linspace(-nv / 2, nv / 2, nv)
    w = np.linspace(-nw / 2, nw / 2, nw)
    vv, ww = np.meshgrid(v, w, indexing="ij")
    rr = np.sqrt(vv ** 2 + ww ** 2)
    x = rr.copy()
    X = rr <= min(nv, nw) / 3
    if maskfrac > 0:
        X *= np.random.choice([0, 1], shape, p=[maskfrac, 1 - maskfrac])
    x *= X
    y = shift_array(x, shift)
    Y = shift_array(X, shift)
    P = correlate.masked_pearson_cc_2d(x, X, y, Y, max_shift=5, min_unmasked=9)
    Pslow = correlate._masked_pearson_cc_slow(x, X, y, Y, max_shift=5, min_unmasked=9)
    assert np.allclose(P, Pslow)
    plan = correlate.plan_masked_pearson_cc_2d(X, Y, max_shift=5, min_unmasked=9)
    Pfast = correlate.masked_pearson_cc_2d(x, X, y, Y, plan=plan)
    assert np.allclose(P, Pfast)
    pearson_error = np.max(np.abs(P - Pslow))
    dx, dy, _, _ = correlate.pearson_friedel_shift(P)
    shift_recovered = np.array((dx, dy))
    shift_error = np.max(np.abs(shift - shift_recovered))
    if print_stuff:
        print("Shape =", shape)
        print("Shift input =", shift)
        print("Shift recovered =", shift_recovered)
        print("Max pearson error =", pearson_error)
        print("Max shift error =", shift_error)
    if show_plots:
        mx = np.max(np.abs(np.concatenate([x, y])))
        fig, axs = plt.subplots(2, 4)
        axs[0, 0].imshow(pad2(x), vmin=-mx, vmax=mx, cmap='bwr')
        axs[0, 0].set_title('x (image)')
        axs[0, 1].imshow(pad2(X), vmin=0, vmax=1, cmap='bwr')
        axs[0, 1].set_title('X (mask)')
        axs[1, 0].imshow(pad2(y), vmin=-mx, vmax=mx, cmap='bwr')
        axs[1, 0].set_title('y (image)')
        axs[1, 1].imshow(pad2(Y), vmin=0, vmax=1, cmap='bwr')
        axs[1, 1].set_title('Y (mask)')
        axs[0, 2].imshow(P, vmin=-1, vmax=1, cmap='bwr')
        axs[0, 2].set_title('P (correlation)')
        axs[1, 2].imshow(Pslow, vmin=-1, vmax=1, cmap='bwr')
        axs[1, 2].set_title('Pslow (correlation)')
        axs[0, 3].imshow(P - Pslow, vmin=-1, vmax=1, cmap='bwr')
        axs[0, 3].set_title('P - Pslow')
        plt.show()
        mx = np.max(np.abs(np.concatenate([x, y])))
        fig, axs = plt.subplots(2, 4)
        axs[0, 0].imshow(pad2(x), vmin=-mx, vmax=mx, cmap='bwr')
        axs[0, 0].set_title('x (image)')
        axs[0, 1].imshow(pad2(X), vmin=0, vmax=1, cmap='bwr')
        axs[0, 1].set_title('X (mask)')
        axs[1, 0].imshow(pad2(y), vmin=-mx, vmax=mx, cmap='bwr')
        axs[1, 0].set_title('y (image)')
        axs[1, 1].imshow(pad2(Y), vmin=0, vmax=1, cmap='bwr')
        axs[1, 1].set_title('Y (mask)')
        axs[0, 2].imshow(P, vmin=-1, vmax=1, cmap='bwr')
        axs[0, 2].set_title('P (correlation)')
        axs[1, 2].imshow(Pfast, vmin=-1, vmax=1, cmap='bwr')
        axs[1, 2].set_title('Pfast (correlation)')
        axs[0, 3].imshow(P - Pslow, vmin=-1, vmax=1, cmap='bwr')
        axs[0, 3].set_title('P - Pslow')
        plt.show()


def test_pearson_cc_2d():
    r""" Check that the FFT masked Pearson CC gives the same result as does
    a slower, direct computation. """
    maskfrac = 0
    show_plots = False
    print_stuff = False
    shape = (20, 23)
    shift = np.array((1, 0))
    check_errors(shape, shift, maskfrac, show_plots, print_stuff)
    shape = (20, 23)
    shift = np.array((0, 0))
    check_errors(shape, shift, maskfrac, show_plots, print_stuff)
    shape = (20, 23)
    shift = np.array((0, 1))
    check_errors(shape, shift, maskfrac, show_plots, print_stuff)
    shape = (20, 23)
    shift = np.array((1, 1))
    check_errors(shape, shift, maskfrac, show_plots, print_stuff)
    shape = (20, 20)
    shift = np.array((1, 1))
    check_errors(shape, shift, maskfrac, show_plots, print_stuff)
    shape = (23, 23)
    shift = np.array((1, 1))
    check_errors(shape, shift, maskfrac, show_plots, print_stuff)


if __name__ == "__main__":
    test_pearson_cc_2d()