# This file is part of reborn <https://kirianlab.gitlab.io/reborn/>.
#
# reborn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reborn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with reborn.  If not, see <https://www.gnu.org/licenses/>.

import os
from reborn import detector
from reborn.data import cspad_crystfel_geom_file, cspad_geom_file
from reborn.external import crystfel


def test_geom_file_loading():
    geom = crystfel.geometry_file_to_pad_geometry_list(cspad_crystfel_geom_file)
    assert isinstance(geom, list)
    assert isinstance(geom, detector.PADGeometryList)
    assert len(geom) == 64


def test_stream_file_frame_getter():
    streamfile = crystfel.example_stream_file_path
    fg = crystfel.StreamfileFrameGetter(stream_file=streamfile)


def test_file_writing_roundtrips():
    # Check that we can load a crystfel file, convert to PADGeometryList, save as
    # reborn format, load the reborn format, confirm that the PADGeometryLists are the same
    geom = crystfel.geometry_file_to_pad_geometry_list(cspad_crystfel_geom_file)
    filename = "test.json"
    detector.save_pad_geometry_list(geom_list=geom, file_name=filename)
    # crystfel.write_geom_file_from_pad_geometry_list(file_path=filename, pad_geometry=geom)
    geom2 = detector.load_pad_geometry_list(filename)
    os.remove(filename)
    assert geom == geom2
    geom = detector.load_pad_geometry_list(cspad_geom_file)
    filename = "test.geom"
    crystfel.write_geom_file_from_pad_geometry_list(file_path=filename, pad_geometry=geom)
    geom2 = crystfel.geometry_file_to_pad_geometry_list(filename)
    os.remove(filename)
    assert geom == geom2
